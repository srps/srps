/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "prngs_plugin.h"
#include "prngs_dsfmt.h"

using SRPS::Implementation::PRNGFactory;

PRNGFactory::PRNGFactory(QObject *parent) :
  QObject(parent),
  SRPS::PRNGFactory()
{
}

PRNGFactory::~PRNGFactory()
{
}

// PRNGFactory
QStringList PRNGFactory::prngFactoryKeys() const
{
  return QStringList(SRPS::Implementation::dSFMT::StaticClassName);
}

SRPS::PRNG *
    PRNGFactory::prngFactoryCreate (const QString & prng,
                                                  const QVariantHash &/*parameters*/)
{
  if( prng == SRPS::Implementation::dSFMT::StaticClassName )
    return new SRPS::Implementation::dSFMT;
  return 0;
}

Q_EXPORT_PLUGIN2(SRPSPRNGs,SRPS::Implementation::PRNGFactory)
