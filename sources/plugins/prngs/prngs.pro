include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}PRNGs
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
!exists( dSFMT-src-2.1/dSFMT.cpp ) {
    message( transforming dSFMT.c )
    unix:{
                message( "cp dSFMT-src-2.1/dSFMT.c dSFMT-src-2.1/dSFMT.cpp" )
                system( cp dSFMT-src-2.1/dSFMT.c dSFMT-src-2.1/dSFMT.cpp )
        }
        win32:{
                message( "copy dSFMT-src-2.1\dSFMT.c dSFMT-src-2.1\dSFMT.cpp" )
                system( copy dSFMT-src-2.1\dSFMT.c dSFMT-src-2.1\dSFMT.cpp )
        }
}
SOURCES += \
    dSFMT-src-2.1/dSFMT.cpp \
    prngs_plugin.cpp \
    prngs_dsfmt.cpp
HEADERS += \
    prngs_plugin.h \
    prngs_dsfmt.h

QMAKE_CXXFLAGS += -DDSFMT_MEXP=19937

contains(CONFIG,SIMD){

  unix {
    message("Configuring to use sse2, if test not pass disable it.")
    QMAKE_CXXFLAGS += -msse2 \
        -fno-strict-aliasing \
        -DHAVE_SSE2=1 -O3
  }
  win32 {
    message("Win32 sse2 no enabled")
    QMAKE_CXXFLAGS += -DHAVE_SSE2=0
  }
}else {
    message("For sse2 use: qmake CONFIG+=SIMD")
}

RESOURCES += \
    resources.qrc
