/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef PRNGS_PRNG_DSFMT_H
#define PRNGS_PRNG_DSFMT_H

#include <SRPS/PRNG>

namespace SRPS {
namespace Implementation {

class dSFMT : public SRPS::PRNG
{
  SRPS_DISABLE_COPY(dSFMT)

    class Private;
    Private * p;

public:

    dSFMT();
    virtual ~dSFMT();

    static const QString StaticClassName;

    QString componentClassName() const;

    virtual double nextOO();
    virtual double nextOC();
    virtual double nextCO();
    virtual double nextCC();

    QByteArray seed() const;
    bool setSeed( const QByteArray & s );
    bool setStream( quint32 stream );
};

} // namespace Implementation
} // namespace SRPS

#endif // PRNGS_PRNG_DSFMT_H
