/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "prngs_dsfmt.h"
#include "dSFMT-src-2.1/dSFMT.h"

namespace SRPS {
namespace Implementation {

class dSFMT::Private
{
  public:

    Private():engine( new dsfmt_t ){}

    ~Private(){ delete engine; }

    dsfmt_t * engine;
};

const QString dSFMT::StaticClassName = QLatin1String("srps.prng.dSFMT");

dSFMT::dSFMT() :
    SRPS::PRNG(),
    p( new Private )
{
  setStream(qrand());
}

dSFMT::~dSFMT()
{
    delete p;
}

QString dSFMT::componentClassName() const
{
    return StaticClassName;
}

double dSFMT::nextOO()
{
  return dsfmt_genrand_open_open( p->engine );
}

double dSFMT::nextOC()
{
  return dsfmt_genrand_open_close( p->engine );
}

double dSFMT::nextCO()
{
  return dsfmt_genrand_close_open( p->engine );
}

double dSFMT::nextCC()
{
  return dsfmt_genrand_close_open( p->engine );
}

QByteArray dSFMT::seed() const
{
    QByteArray internal;
    QDataStream stream( &internal, QIODevice::WriteOnly );
    stream << StaticClassName;
    stream << p->engine->idx;
    stream << DSFMT_N + 1;
    for( int i = 0; i < DSFMT_N + 1; ++i )
        stream << p->engine->status[i].u32[0] << p->engine->status[i].u32[1];
    return internal;
}

bool dSFMT::setSeed( const QByteArray & s )
{
    QDataStream stream( s );
    QString name;
    stream >> name;
    if ( name != StaticClassName )
        return false;

    dsfmt_t * restoredInternal = new dsfmt_t;
    stream >> restoredInternal->idx;

    int size;
    stream >> size;
    if ( size < DSFMT_N + 1 )
        return false;

    for( int i = 0; i < DSFMT_N + 1; ++i )
        stream >> restoredInternal->status[i].u32[0] >> restoredInternal->status[i].u32[1];

    delete p->engine;
    p->engine = restoredInternal;

    return true;
}

bool dSFMT::setStream( quint32 stream )
{
    dsfmt_init_gen_rand(p->engine,stream);
    return true;
}

} // namespace Implementation
} // namespace SRPS
