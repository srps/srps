/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "simulators_secuential.h"
#include "simulators_simulatorstatuswidget.h"

#include <SRPS/AccumulatorFactory>
#include <SRPS/Accumulator>
#include <SRPS/Space>
#include <SRPS/Model>
#include <SRPS/ModelFactory>
#include <SRPS/PRNG>
#include <SRPS/PRNGFactory>
#include <SRPS/DPRNG>
#include <SRPS/DPRNGFactory>
#include <SRPS/Curve>
#include <SRPS/CurveFactory>

#include <QtCore/QHash>
#include <QtCore/QVariantHash>
#include <QtCore/QTextStream>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>

#include <QtCore/QVector>

#include "Timer.h"

namespace SRPS {
namespace Implementation {

  SecuentialSimulator::SecuentialSimulator( ):
    SimulatorBase()
{
}

SecuentialSimulator::~SecuentialSimulator()
{
}

const QString SecuentialSimulator::ClassName = QLatin1String("srps.simulator.secuential");

// Component
QString SecuentialSimulator::componentClassName() const
{
    return ClassName;
}

QWidget * SecuentialSimulator::componentStatusWidget() const
{
 QWidget * w = new SimulatorStatusWidget(const_cast<SecuentialSimulator*>(this),0);

 return w;
}

void SecuentialSimulator::run()
{
  Timer general_setup;

  general_setup.start();
  src_cmids = space->cellMIDs(Space::CreationOrder);
  src_smids = space->speciesMIDs(Space::CreationOrder);
  src_imids = space->individualMIDs(Space::CreationOrder);
  src_cells = space->constCells(Space::CreationOrder);
  src_species = space->constSpecies(Space::CreationOrder);
  src_individuals = space->constIndividuals(Space::CreationOrder);
  general_setup.stop();
  appendDetail(trUtf8("Copiado de referencias e identificadores: %1 ms").arg(general_setup.getElapsedTimeInMilliSec()));
  appendDetail("");

  int top_count = jobs.count() + 1;
  emit topOperationChanged(trUtf8("Progreso total"));
  emit topOperationRangeChanged(0,top_count);
  emit topOperationValueChanged(0);

  while(!jobs.isEmpty() && !isCanceled())
  {
    Job job =jobs.takeFirst();
    current_model = job.modelConfiguration().component;

    if ( job.accumulateOnly() )
    {
      appendDetail(trUtf8("Trabajo de acumulación encontrado:"));
      appendDetail("");

      AccumulatorJobs ajobs = createAccumulatorJobs(job.accumulatorConfigurations());

      emit subOperationChanged(trUtf8("Acumulación"));
      emit subOperationRangeChanged(0,ajobs.count()+1);
      emit subOperationValueChanged(0);

      for( int i = 0; i < ajobs.count() && !isCanceled(); ++i )
      {
        AccumulatorJob * aj = ajobs.at(i);

        accumulateAccumulatorJob(space,aj);
        finalizeAccumulatorJob(aj);

        appendDetail(trUtf8("Acumulador: %1").arg(aj->accumulator->componentClassName()));
        appendDetail(trUtf8("\tConfiguracion: %1 ms").arg(aj->setup_time));
        appendDetail(trUtf8("\tAcumulación: %1 ms").arg(aj->accumulate_time));
        appendDetail(trUtf8("\tFinalización: %1 ms").arg(aj->finalize_time));

        privateResults.append(aj->result);

        delete aj->accumulator;

        emit subOperationValueChanged(i+1);
      }

      qDeleteAll(ajobs);

      if ( isCanceled( ))
      {
        continue;
      }

      appendDetail("--------------------");
      appendDetail("");
    }
    else // Model Job
    {
      appendDetail(trUtf8("Trabajo de modelación encontrado: %1").arg(job.modelConfiguration().component));
      appendDetail("");
      appendDetail(trUtf8("PRNG: %1").arg(job.prngConfiguration().component));
      appendDetail(trUtf8("DPRNG: %1").arg(job.dprngConfiguration().component));
      appendDetail(trUtf8("Iteraciones: %1").arg(job.iterations()));
      appendDetail("");
      appendDetail(trUtf8("Modelo:"));

      emit subOperationChanged(trUtf8("Modelación"));
      emit subOperationRangeChanged(0,job.iterations()+1);
      emit subOperationValueChanged(0);

      ModelJob result;

      Timer timer;
      timer.start();
      double total_model = 0;

      for( int i = 0; i < job.iterations() && !isCanceled(); ++i )
      {
        ModelJob * mj = createModelJob(job);
        doModelJob(mj);
        accumulateModelJob(&result,mj);
        delete mj;
        emit subOperationValueChanged(i+1);
      }
      if( isCanceled() )
      {
        qDeleteAll(result.accumulatorJobs);
        continue;
      }
      timer.stop();
      total_model = timer.getElapsedTimeInMilliSec();

      emit subOperationChanged(trUtf8("Finalización"));
      finalizeAccumulatorJobs(result.accumulatorJobs);
      emit subOperationValueChanged(job.iterations()+1);

      result.setup_time /= job.iterations();
      result.model_time /= job.iterations();

      appendDetail(trUtf8("\tConfiguracion promedio: %1 ms").arg(result.setup_time));
      appendDetail(trUtf8("\tModelación promedio: %1 ms").arg(result.model_time));
      appendDetail("");

      foreach( AccumulatorJob * aj, result.accumulatorJobs )
      {
        appendDetail(trUtf8("Acumulador: %1").arg(aj->accumulator->componentClassName()));
        appendDetail(trUtf8("\tConfiguracion: %1 ms").arg(aj->setup_time));
        appendDetail(trUtf8("\tAcumulación: %1 ms").arg(aj->accumulate_time));
        appendDetail(trUtf8("\tFinalización: %1 ms").arg(aj->finalize_time));
        appendDetail("");
        privateResults.append(aj->result);
        delete aj->accumulator;
        delete aj;
      }
      appendDetail(trUtf8("Total modelación: %1 ms").arg(total_model));
      appendDetail("--------------------");
      appendDetail("");
    }

    if (!isCanceled())
      emit topOperationValueChanged(++top_count);
  }
  if ( isCanceled() )
  {
    appendDetail(trUtf8("Cancelado"));
    qDeleteAll(privateResults);
    privateResults.clear();
    publicResults.clear();
  }
  else // all good
  {
    appendDetail(trUtf8("Combinando resultados"));
    mergeResults();
    emit topOperationValueChanged(++top_count);
  }
}

} // namespace Implementation
} // namespace SRPS

