include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}Simulators
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += \
    simulators_plugin.cpp \
    simulators_secuential.cpp \
    ../../extra/timer/src/Timer.cpp \
    simulators_simulatorstatuswidget.cpp \
    simulators_simulatorbase.cpp \
    simulators_concurrent.cpp
HEADERS += \
    simulators_plugin.h \
    simulators_secuential.h \
    simulators_simulatorstatuswidget.h \
    simulators_simulatorbase.h \
    simulators_concurrent.h

INCLUDEPATH += \
../../extra/timer/src/

FORMS += \
    simulators_simulatorstatuswidget.ui

RESOURCES += \
    resources.qrc
