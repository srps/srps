/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SIMULATORS_SIMULATORBASE_H
#define SIMULATORS_SIMULATORBASE_H

#include <SRPS/Space>
#include <SRPS/Simulator>
#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtCore/QTextStream>


namespace SRPS {
namespace Implementation {

class SimulatorBase : public QThread, public SRPS::Simulator
{
  Q_OBJECT
  Q_INTERFACES(SRPS::Simulator)

public:

  struct AccumulatorJob // acumulador phase
  {
    AccumulatorJob()
    {
      interations = 0;
      accumulator = 0;
      result = 0;
      setup_time = 0;
      accumulate_time = 0;
      finalize_time = 0;
      curveFactory = 0;
      sb = 0;
    }
    int interations; // counted
    SRPS::CurveFactory * curveFactory;
    SRPS::Accumulator * accumulator;
    SRPS::Curve * result;
    // means
    double setup_time;
    double accumulate_time;
    double finalize_time;

    SimulatorBase * sb;
  };

  typedef QList<AccumulatorJob*> AccumulatorJobs;

  struct ModelJob // general model job, contains all necesary for modeling
                  // including accumulator jobs
  {
    ModelJob()
    {
      valid = false;
      accumulatorFactory = 0;
      modelFactory = 0;
      prngFactory = 0;
      dprngFactory = 0;
      curveFactory = 0;

      src_space = 0;
      space = 0;

      setup_time = 0;
      model_time = 0;
      sb = 0;
    }

    bool valid ;
    SRPS::AccumulatorFactory * accumulatorFactory;
    SRPS::CurveFactory * curveFactory;
    SRPS::ModelFactory * modelFactory;
    SRPS::PRNGFactory * prngFactory;
    SRPS::DPRNGFactory * dprngFactory;

    Simulator::Job simulator_job;

    AccumulatorJobs accumulatorJobs; // empty until first accumulate

    // SRC Space
    const SRPS::Space * src_space;

    SRPS::Space * space;
    // means
    double setup_time;
    double model_time;

    SimulatorBase * sb;
  };

protected:

  SRPS::AccumulatorFactory * accumulatorFactory;
  SRPS::ModelFactory * modelFactory;
  SRPS::PRNGFactory * prngFactory;
  SRPS::DPRNGFactory * dprngFactory;
  SRPS::CurveFactory * curveFactory;

  bool canceled;

  const SRPS::Space * space; // base space
  Space::MIDList src_cmids;
  Space::MIDList src_smids;
  Space::MIDList src_imids;
  Space::ConstCellList src_cells;
  Space::ConstSpeciesList src_species;
  Space::ConstIndividualList src_individuals;

  Simulator::JobList jobs;
  QString current_model;

  mutable QList<SRPS::Curve *> privateResults; // delete and clear
  mutable QList<SRPS::Curve *> publicResults; // clear when requested

  QString detailtText;
  QTextStream detailtStream;

  mutable QMutex mutex;

public:

  explicit SimulatorBase(QObject *parent = 0);
  virtual ~SimulatorBase();

  void waitSimulator();

  void cancel();

  bool isCanceled() const;

  bool hasFinished() const;

  void setFactory( SRPS::CurveFactory * );
  void setFactory( SRPS::AccumulatorFactory * );
  void setFactory( SRPS::ModelFactory * );
  void setFactory( SRPS::PRNGFactory * );
  void setFactory( SRPS::DPRNGFactory * );

  void setSourceSpace( const SRPS::Space * );

  QList<SRPS::Curve*> getResults() const;
  void startJobs( const QList<Simulator::Job> & jobs ); // copy jobs and start thread

protected:

  void mergeResults();

  void appendDetail( const QString & d );

signals:

  void topOperationChanged( const QString & tO ) const;
  void subOperationChanged( const QString & sO ) const;

  void topOperationRangeChanged( int min, int max ) const;
  void subOperationRangeChanged( int min, int max ) const;

  void topOperationValueChanged( int value ) const;
  void subOperationValueChanged( int value ) const;

  void detailTextChanged( const QString t ) const;

public:

  //utiliy function for simulation

  AccumulatorJobs createAccumulatorJobs( const ComponentConfigurationList & ccs );

  static AccumulatorJobs accumulateAccumulatorJobs( const SRPS::Space * space, const AccumulatorJobs  & ajs );
  static AccumulatorJob * accumulateAccumulatorJob( const SRPS::Space * space, AccumulatorJob * aj );

  static AccumulatorJobs finalizeAccumulatorJobs( const AccumulatorJobs  & ajs );
  static AccumulatorJob * finalizeAccumulatorJob( AccumulatorJob * aj );

  ModelJob * createModelJob( const  Simulator::Job  & job );

  static AccumulatorJobs accumulatorJobsFor( const ModelJob * job );
  static ModelJob * doModelJob( const ModelJob * );
  static void accumulateModelJob( ModelJob * merged, const ModelJob *  model );

};

} // namespace Implementation
} // namespace SRPS

#endif // SIMULATORS_SIMULATORBASE_H
