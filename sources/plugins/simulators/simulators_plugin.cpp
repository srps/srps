/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "simulators_plugin.h"
#include "simulators_secuential.h"
#include "simulators_concurrent.h"

#include <QtGui/QTextEdit>

using SRPS::Implementation::SimulatorFactory;

SimulatorFactory::SimulatorFactory(QObject *parent) :
  QObject(parent),
  SRPS::SimulatorFactory()
{
}

SimulatorFactory::~SimulatorFactory()
{
}

// SimulatorFactory
QStringList SimulatorFactory::simulatorFactoryKeys() const
{
  return QStringList(SRPS::Implementation::SecuentialSimulator::ClassName)
      << SRPS::Implementation::ConcurrentSimulator::ClassName;
}

SRPS::Simulator * SimulatorFactory::simulatorFactoryCreate (const QString &simulator,
                                                const QVariantHash &/*parameters*/)
{
  if ( SRPS::Implementation::SecuentialSimulator::ClassName == simulator )
    return new SRPS::Implementation::SecuentialSimulator;
  if ( SRPS::Implementation::ConcurrentSimulator::ClassName == simulator )
    return new SRPS::Implementation::ConcurrentSimulator;
  return 0;
}

//QWidget * SimulatorFactory::componentFactoryWidgetFor(const QString &key) const
//{
//  if ( SRPS::Implementation::SecuentialSimulator::ClassName == key )
//  {
//    QTextEdit * t = new QTextEdit;
//    t->setReadOnly(true);
//    t->setText(trUtf8("Simulador secuencial, en un solo hilo de ejecución separado"));
//    return t;
//  }
//  if ( SRPS::Implementation::ConcurrentSimulator::ClassName == key )
//  {
//    QTextEdit * t = new QTextEdit;
//    t->setReadOnly(true);
//    t->setText(trUtf8("Simulador concurrente, utiliza el API QtConcurrent"));
//    return t;
//  }
//  return 0;
//}

Q_EXPORT_PLUGIN2(SRPSSimulators,SRPS::Implementation::SimulatorFactory)
