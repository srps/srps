/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "simulators_concurrent.h"
#include "simulators_simulatorstatuswidget.h"

#include <SRPS/AccumulatorFactory>
#include <SRPS/Accumulator>
#include <SRPS/Space>
#include <SRPS/Model>
#include <SRPS/ModelFactory>
#include <SRPS/PRNG>
#include <SRPS/PRNGFactory>
#include <SRPS/DPRNG>
#include <SRPS/DPRNGFactory>
#include <SRPS/Curve>
#include <SRPS/CurveFactory>

#include <QtCore/QHash>
#include <QtCore/QVariantHash>
#include <QtCore/QTextStream>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>

#include <QtCore/QVector>
#include <QtCore/QTimer>

#include <QtConcurrentMap>
#include <QtConcurrentRun>

namespace SRPS {
namespace Implementation {

  ConcurrentSimulator::ConcurrentSimulator( ):
    SimulatorBase()
{
}

ConcurrentSimulator::~ConcurrentSimulator()
{
}

const QString ConcurrentSimulator::ClassName = QLatin1String("srps.simulator.concurrent");

// Component
QString ConcurrentSimulator::componentClassName() const
{
    return ClassName;
}

QWidget * ConcurrentSimulator::componentStatusWidget() const
{
 QWidget * w = new SimulatorStatusWidget(const_cast<ConcurrentSimulator*>(this),0);

 return w;
}

struct AccumulatorAccumulateFunctor
{
  const SRPS::Space * space;
  AccumulatorAccumulateFunctor(const SRPS::Space * s):space(s){}

  void operator()( SimulatorBase::AccumulatorJob * job )
  {
    SimulatorBase::accumulateAccumulatorJob(space,job);
  }
};

void ConcurrentSimulator::cancel()
{
  QMutexLocker l(&mutex);
  canceled = true;
  appendDetail(tr("Cancelando..."));
  modelDoJobWatcher.cancel();
}

void ConcurrentSimulator::run()
{
  Timer general_setup;
  modeling_result = 0;

  general_setup.start();
  src_cmids = space->cellMIDs(Space::CreationOrder);
  src_smids = space->speciesMIDs(Space::CreationOrder);
  src_imids = space->individualMIDs(Space::CreationOrder);
  src_cells = space->constCells(Space::CreationOrder);
  src_species = space->constSpecies(Space::CreationOrder);
  src_individuals = space->constIndividuals(Space::CreationOrder);
  general_setup.stop();
  appendDetail(trUtf8("Copiado de referencias e identificadores: %1 ms").arg(general_setup.getElapsedTimeInMilliSec()));
  appendDetail("");

  // connections
  connect(&accumulateJobWatcher,SIGNAL(progressRangeChanged(int,int)),
          this,SIGNAL(subOperationRangeChanged(int,int)));
  connect(&accumulateJobWatcher,SIGNAL(progressValueChanged(int)),
          this,SIGNAL(subOperationValueChanged(int)));


  connect(&modelDoJobWatcher,SIGNAL(progressRangeChanged(int,int)),
          this,SIGNAL(subOperationRangeChanged(int,int)));
  connect(&modelDoJobWatcher,SIGNAL(progressValueChanged(int)),
          this,SIGNAL(subOperationValueChanged(int)));

  QTimer::singleShot(0,this,SLOT(processNextJob()));

  // start evenlopp
  exec();

  if ( isCanceled() )
  {
    appendDetail(trUtf8("Cancelado"));
    qDeleteAll(privateResults);
    privateResults.clear();
    publicResults.clear();
    emit subOperationChanged(tr("Cancelado"));
    emit subOperationRangeChanged(0,1);
    emit subOperationValueChanged(1);
  }
  else // all good
  {
    appendDetail(trUtf8("Combinando resultados"));
    mergeResults();
    emit topOperationValueChanged(++thread_top_count);
  }
}

void ConcurrentSimulator::processNextJob()
{
  int top_count = jobs.count() + 1;
  emit topOperationChanged(trUtf8("Progreso total"));
  emit topOperationRangeChanged(0,top_count);
  emit topOperationValueChanged(0);

  if ( jobs.isEmpty() || isCanceled() )
  {
    quit();
    return;
  }

  Simulator::Job job =jobs.takeFirst();
  current_model = job.modelConfiguration().component;
  thread_current_job = job;

  if ( job.accumulateOnly() )
  {
    processAccumulativeJob();
  }
  else // Model Job
  {
    processModelJob();
  }
}

void ConcurrentSimulator::endCurrentJob()
{
    if (!isCanceled())
  {
      emit topOperationValueChanged(++thread_top_count);
      QTimer::singleShot(1,this,SLOT(processNextJob()));
    }
    else
      quit();
}

void ConcurrentSimulator::processAccumulativeJob()
{
  appendDetail(trUtf8("Trabajo de acumulación encontrado:"));
  appendDetail("");

  accumulateJobs = createAccumulatorJobs(thread_current_job.accumulatorConfigurations());

  emit subOperationChanged(trUtf8("Acumulación"));
  emit subOperationRangeChanged(0,accumulateJobs.count()+1);
  emit subOperationValueChanged(0);

  connect(&accumulateJobWatcher,SIGNAL(finished()),
          this,SLOT(processAccumulativeJob_stage1()));

  accumulateJobWatcher.setFuture(
      QtConcurrent::map(accumulateJobs,AccumulatorAccumulateFunctor(space))
      );
}

void ConcurrentSimulator::processAccumulativeJob_stage1()
{
  disconnect(&accumulateJobWatcher,SIGNAL(finished()),
          this,SLOT(processAccumulativeJob_stage1()));

  connect(&accumulateJobWatcher,SIGNAL(finished()),
          this,SLOT(processAccumulativeJob_stage2()));

  accumulateJobWatcher.setFuture(
      QtConcurrent::map(accumulateJobs,SimulatorBase::finalizeAccumulatorJob)
      );
}

void ConcurrentSimulator::processAccumulativeJob_stage2()
{
  disconnect(&accumulateJobWatcher,SIGNAL(finished()),
          this,SLOT(processAccumulativeJob_stage2()));

  for( int i = 0; i < accumulateJobs.count() && !isCanceled(); ++i )
  {
    AccumulatorJob * aj = accumulateJobs.at(i);

    appendDetail(trUtf8("Acumulador: %1").arg(aj->accumulator->componentClassName()));
    appendDetail(trUtf8("\tConfiguracion: %1 ms").arg(aj->setup_time));
    appendDetail(trUtf8("\tAcumulación: %1 ms").arg(aj->accumulate_time));
    appendDetail(trUtf8("\tFinalización: %1 ms").arg(aj->finalize_time));

    privateResults.append(aj->result);

    delete aj->accumulator;

//        emit subOperationValueChanged(i+1);
  }

  qDeleteAll(accumulateJobs);
  accumulateJobs.clear();

  appendDetail("--------------------");
  appendDetail("");

  QTimer::singleShot(1,this,SLOT(endCurrentJob()));
}

void ConcurrentSimulator::processModelJob()
{
  appendDetail(trUtf8("Trabajo de modelación encontrado: %1").arg(thread_current_job.modelConfiguration().component));
  appendDetail("");
  appendDetail(trUtf8("PRNG: %1").arg(thread_current_job.prngConfiguration().component));
  appendDetail(trUtf8("DPRNG: %1").arg(thread_current_job.dprngConfiguration().component));
  appendDetail(trUtf8("Iteraciones: %1").arg(thread_current_job.iterations()));
  appendDetail("");
  appendDetail(trUtf8("Modelo:"));

  emit subOperationChanged(trUtf8("Modelación"));
  emit subOperationRangeChanged(0,thread_current_job.iterations()+1);
  emit subOperationValueChanged(0);

  model_timer.start();
  m_iters.clear();
  for( int i = 0; i <thread_current_job.iterations(); ++i )
  {
    BaseData d;
    d.job = thread_current_job;
    d.cs = this;
    m_iters << d;
  }

  connect(&modelDoJobWatcher,SIGNAL(finished()),
          this,SLOT(processModelJob_stage1()));

  modelDoJobWatcher.setFuture(
      QtConcurrent::mappedReduced(m_iters,mapper,reduccer)
      );
}

void ConcurrentSimulator::processModelJob_stage1()
{
  model_timer.stop();
  m_iters.clear();

  disconnect(&modelDoJobWatcher,SIGNAL(finished()),
          this,SLOT(processModelJob_stage1()));

  modelDoJobWatcher.waitForFinished();
  if( isCanceled() )
  {
    if ( modeling_result )
    {
      qDeleteAll(modeling_result->accumulatorJobs);
      delete modeling_result;
    }
    quit();
  }
  else
  {
    connect(&modelFinalizeWatcher,SIGNAL(finished()),
            this,SLOT(processModelJob_stage2()));

    emit subOperationChanged(trUtf8("Finalización"));

    modeling_result->setup_time /= thread_current_job.iterations();
    modeling_result->model_time /= thread_current_job.iterations();

    appendDetail(trUtf8("\tConfiguracion promedio: %1 ms").arg(modeling_result->setup_time));
    appendDetail(trUtf8("\tModelación promedio: %1 ms").arg(modeling_result->model_time));
    appendDetail("");

    modelFinalizeWatcher.setFuture(QtConcurrent::run(finalizeAccumulatorJobs,modeling_result->accumulatorJobs));
  }
}

void ConcurrentSimulator::processModelJob_stage2()
{
  disconnect(&modelFinalizeWatcher,SIGNAL(finished()),
          this,SLOT(processModelJob_stage2()));
  modelFinalizeWatcher.waitForFinished();

  foreach( AccumulatorJob * aj, modeling_result->accumulatorJobs )
  {
    appendDetail(trUtf8("Acumulador: %1").arg(aj->accumulator->componentClassName()));
    appendDetail(trUtf8("\tConfiguracion: %1 ms").arg(aj->setup_time));
    appendDetail(trUtf8("\tAcumulación: %1 ms").arg(aj->accumulate_time));
    appendDetail(trUtf8("\tFinalización: %1 ms").arg(aj->finalize_time));
    appendDetail("");
    privateResults.append(aj->result);
    delete aj->accumulator;
    delete aj;
  }

  delete modeling_result;
  modeling_result = 0;

  appendDetail(trUtf8("Total modelación: %1 ms").arg(model_timer.getElapsedTimeInMilliSec()));
  appendDetail("--------------------");
  appendDetail("");

  QTimer::singleShot(1,this,SLOT(endCurrentJob()));
}

} // namespace Implementation
} // namespace SRPS

