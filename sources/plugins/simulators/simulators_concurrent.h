/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SIMULATORS_SIMULATORCONCURRENT_H
#define SRPS_SIMULATORS_SIMULATORCONCURRENT_H

#include "simulators_simulatorbase.h"

#include <QtCore/QFuture>
#include <QtCore/QFutureWatcher>

#include "Timer.h"

namespace SRPS {
namespace Implementation {

class ConcurrentSimulator : public SimulatorBase
{
  Q_OBJECT

public: // helper things

  struct BaseData
  {
    ConcurrentSimulator * cs;
    Simulator::Job job;
  };

  struct MappedData
  {
    ConcurrentSimulator * cs;
    SimulatorBase::ModelJob * modelJob;
    Simulator::Job job;
  };

  struct ResultData
  {
    ConcurrentSimulator * cs;
  };

  static MappedData mapper( const BaseData & data )
  {
    MappedData r;
    r.modelJob = data.cs->createModelJob(data.job);
    r.cs = data.cs;
    r.job = data.job;
    data.cs->doModelJob(r.modelJob);
    return r;
  }

  static void reduccer( ResultData & r, const MappedData & i )
  {
    if ( !r.cs )
    {
      r.cs =i.cs;
      r.cs->modeling_result =r.cs->createModelJob(i.job);
    }

    SimulatorBase::accumulateModelJob(r.cs->modeling_result,i.modelJob);
    delete i.modelJob;
  }

private:

  int thread_top_count;
  Simulator::Job thread_current_job;

  QFutureWatcher<void> accumulateJobWatcher;
  AccumulatorJobs accumulateJobs;

  QFutureWatcher<ResultData> modelDoJobWatcher;
  QFutureWatcher<void> modelFinalizeWatcher;
  Timer model_timer;
  QList<BaseData> m_iters;
  ModelJob * modeling_result;

public:

    ConcurrentSimulator();
    virtual ~ConcurrentSimulator();

    static const QString ClassName;

    // Component

    QString componentClassName() const;
    QWidget * componentStatusWidget() const;

    void cancel();

protected:

    void run();

protected slots:

    void processNextJob();
    void endCurrentJob();

    void processAccumulativeJob();
    void processAccumulativeJob_stage1();
    void processAccumulativeJob_stage2();

    void processModelJob();
    void processModelJob_stage1();
    void processModelJob_stage2();

};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_SIMULATORS_SIMULATOR_H
