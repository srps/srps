/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "simulators_simulatorbase.h"

#include <SRPS/AccumulatorFactory>
#include <SRPS/Accumulator>
#include <SRPS/Space>
#include <SRPS/Model>
#include <SRPS/ModelFactory>
#include <SRPS/PRNG>
#include <SRPS/PRNGFactory>
#include <SRPS/DPRNG>
#include <SRPS/DPRNGFactory>
#include <SRPS/Curve>
#include <SRPS/CurveFactory>

#include <QtCore/QMutexLocker>
#include <QtCore/QDebug>

#include "Timer.h"

namespace SRPS {
namespace Implementation {

// SimulatorBase

SimulatorBase::SimulatorBase(QObject *parent) :
    QThread(parent)
{
  accumulatorFactory = 0;
  modelFactory = 0;
  prngFactory = 0;
  dprngFactory = 0;
  curveFactory = 0;

  canceled = false;

  space = 0;

  detailtStream.setString(&detailtText);
}

SimulatorBase::~SimulatorBase()
{
  qDeleteAll(privateResults);
  privateResults.clear();
}

void SimulatorBase::waitSimulator()
{
  wait();
}

bool SimulatorBase::isCanceled() const
{
  QMutexLocker l(&mutex);
  return canceled;
}

void SimulatorBase::cancel()
{
  QMutexLocker l(&mutex);
  canceled = true;
  appendDetail(tr("Cancelando..."));
}

bool SimulatorBase::hasFinished() const
{
  return !isRunning();
}

void SimulatorBase::setFactory( SRPS::CurveFactory * f )
{
  curveFactory = f;
}

void SimulatorBase::setFactory( SRPS::AccumulatorFactory * f )
{
  accumulatorFactory = f;
}

void SimulatorBase::setFactory( SRPS::ModelFactory * f )
{
  modelFactory = f;
}

void SimulatorBase::setFactory( SRPS::PRNGFactory * f )
{
  prngFactory = f;
}

void SimulatorBase::setFactory( SRPS::DPRNGFactory * f )
{
  dprngFactory = f;
}

void SimulatorBase::setSourceSpace( const SRPS::Space * s )
{
  space = s;
}

QList<SRPS::Curve*> SimulatorBase::getResults() const
{
  QList<SRPS::Curve*> copy = publicResults;
  publicResults.clear();
  return copy;
}

void SimulatorBase::startJobs( const QList<Simulator::Job> & jobs )
{
  if ( jobs.isEmpty() )
  {
    registerLastOperationResult(-1,tr("Empty Jobs"));
    return;
  }

  if ( !space )
  {
    registerLastOperationResult(-1,tr("No Space"));
    return;
  }

  if ( !curveFactory )
  {
    registerLastOperationResult(-1,tr("No Curve Factory"));
    return;
  }

  if ( !accumulatorFactory )
  {
    registerLastOperationResult(-1,tr("No Accumulator Factory"));
    return;
  }

  if ( !modelFactory )
  {
    registerLastOperationResult(-1,tr("No Model Factory"));
    return;
  }

  if ( !prngFactory)
  {
    registerLastOperationResult(-1,tr("No PRNG Factory"));
    return;
  }

  if ( !dprngFactory)
  {
    registerLastOperationResult(-1,tr("No DPRNG Factory"));
    return;
  }

  detailtText.clear();

  this->jobs = jobs;
  start();
}

void SimulatorBase::appendDetail( const QString & d )
{
  detailtStream << d << endl;
  emit detailTextChanged(detailtText);
}

void SimulatorBase::mergeResults()
{
  SRPS::Curve * merged = curveFactory->curveFactoryCreate(QString());
  if (!merged)
  {
    appendDetail(tr("No se puede combinar los resultados."));
    publicResults = privateResults;
    privateResults.clear();
  }
  else
  {
    Timer timer;
    timer.start();
    merged->clear();

    merged->componentSetProperty(componentClassName(),detailtText);

    QList<Curve::MemoryIdentifier> tmids;

    foreach( SRPS::Curve * r, privateResults )
    {
      if (!r) continue;

      QList<Curve::MemoryIdentifier> s_tmids = r->tupleMIDs(Curve::CreationOrder);
      int diff = tmids.count() - s_tmids.count();
      if( diff < 0 )
        while( diff++ )
          tmids << merged->createTupleMID();

      Curve::ConstVariableList svars = r->constVariables(Curve::CreationOrder);
      while( svars.count( ))
      {
        Curve::ConstVariable s_var = svars.takeFirst();
        bool valid = s_var.isValid();
        Curve::Variable var = merged->createVariable();
        valid = var.isValid();
        valid = s_var.isValid();

        foreach( QString n, s_var.propertyNames() )
          var.setProperty(n,s_var.property(n));

        for( int t = 0; t < qMin(tmids.count(),s_tmids.count()); ++t )
        {
          merged->tupleSetValue(tmids.at(t),
                                var.mid(),
                                r->tupleValue(s_tmids.at(t),s_var.mid())
                                );
        }
      }
    }
    qDeleteAll(privateResults);
    privateResults.clear();
    publicResults.append(merged);
    timer.stop();

    appendDetail(tr("Combinacion de resultados en: %1 ms").arg(timer.getElapsedTimeInMilliSec()));
  }

}


// modeling helpers

SimulatorBase::AccumulatorJobs
    SimulatorBase::createAccumulatorJobs( const SimulatorBase::ComponentConfigurationList & ccs )
{
  AccumulatorJobs ajobs;

  foreach( ComponentConfiguration cc, ccs )
  {
    AccumulatorJob * ajob = new AccumulatorJob;

    ajob->sb = this;
    ajob->curveFactory = curveFactory;
    ajob->accumulator = accumulatorFactory->accumulatorFactoryCreate(cc.component,
                                                                    cc.configuration);
    if( !ajob->accumulator )
      appendDetail(tr("No se puede crear el accumulador: %1").arg(cc.component));
    else
      ajobs.append(ajob);
  }

  return ajobs;
}

SimulatorBase::AccumulatorJobs SimulatorBase::accumulateAccumulatorJobs( const SRPS::Space * space,
                                              const AccumulatorJobs  & ajs )
{
  if (!space) return ajs;

  foreach( AccumulatorJob * ajob, ajs )
  {
    accumulateAccumulatorJob(space,ajob);
  }

  return ajs;
}

SimulatorBase::AccumulatorJob *
    SimulatorBase::accumulateAccumulatorJob( const SRPS::Space * space, SimulatorBase::AccumulatorJob * ajob )
{
  if ( !ajob ) return ajob;

  if(ajob->accumulator)
  {
    Timer timer;
    timer.start();
    if ( ajob->accumulator->needSpace() )
      ajob->accumulator->setSpace(space);
    if ( ajob->accumulator->needCellMIDs() )
      ajob->accumulator->setCellMIDs(space->cellMIDs(Space::CreationOrder));
    if (ajob->accumulator->needSpeciesMIDs() )
      ajob->accumulator->setSpeciesMIDs(space->speciesMIDs(Space::CreationOrder));
    if (ajob->accumulator->needIndividualMIDs() )
      ajob->accumulator->setIndividualsMIDs(space->individualMIDs(Space::CreationOrder));
    if( ajob->accumulator->needCells() )
      ajob->accumulator->setCells(space->constCells(Space::CreationOrder));
    if (ajob->accumulator->needSpecies() )
      ajob->accumulator->setSpeciess(space->constSpecies(Space::CreationOrder));
    if( ajob->accumulator->needIndividuals() )
      ajob->accumulator->setIndividuals(space->constIndividuals(Space::CreationOrder));
    ajob->accumulator->setup();
    timer.stop();
    ajob->setup_time += timer.getElapsedTimeInMilliSec();

    timer.start();
    ajob->accumulator->accumulate();
    timer.stop();
    ajob->accumulate_time += timer.getElapsedTimeInMilliSec();
    ++(ajob->interations);
  }

  return ajob;
}

SimulatorBase::AccumulatorJobs SimulatorBase::finalizeAccumulatorJobs( const SimulatorBase::AccumulatorJobs  & ajs )
{
  foreach( AccumulatorJob * ajob, ajs )
  {
    finalizeAccumulatorJob(ajob);
  }
  return ajs;
}

SimulatorBase::AccumulatorJob * SimulatorBase::finalizeAccumulatorJob( SimulatorBase::AccumulatorJob  *aj )
 {
   AccumulatorJob * r = aj;
   if(r->accumulator)
   {
     r->result = r->curveFactory->curveFactoryCreate(QString());
     if (!r->result)
       delete r->accumulator;
     Timer timer;
     timer.start();
     r->accumulator->finalize(r->result);
     timer.stop();
     r->finalize_time += timer.getElapsedTimeInMilliSec();

     r->setup_time /= r->interations;
     r->accumulate_time /= r->accumulate_time;

     if ( r->result )
       foreach( Curve::Variable var, r->result->variables() )
       {
         var.setProperty("accumulator",r->accumulator->componentClassName());
         var.setProperty("model",r->sb->current_model);
       }
   }
   return r;
 }

SimulatorBase::ModelJob * SimulatorBase::createModelJob( const  Simulator::Job  & job )
{
  ModelJob * mj = new ModelJob;

  mj->accumulatorFactory = accumulatorFactory;
  mj->modelFactory = modelFactory;
  mj->prngFactory = prngFactory;
  mj->dprngFactory = dprngFactory;
  mj->curveFactory = curveFactory;

  mj->src_space = space;
  mj->space = 0;

  mj->setup_time = 0;
  mj->model_time = 0;

  mj->simulator_job = job;

  mj->sb = this;

  return mj;
}

SimulatorBase::ModelJob * SimulatorBase::doModelJob( const SimulatorBase::ModelJob * mj )
{
  SimulatorBase::ModelJob * rmj = const_cast<SimulatorBase::ModelJob*>(mj);

  if ( !rmj->src_space ) return rmj;

  rmj->space = rmj->src_space->cloneUndistributed(); // deleted by accumulator

  if ( !rmj->space ) return rmj;

  // configure

  SRPS::Model * model = 0;
  SRPS::PRNG * prng1 = 0;
  SRPS::PRNG * prng2 = 0;
  SRPS::DPRNG * dprng = 0;

  model = rmj->modelFactory->modelFactoryCreate(rmj->simulator_job.modelConfiguration().component,
                                               rmj->simulator_job.modelConfiguration().configuration);

  if ( !model ) return rmj;

  // setup model
  Timer timer;
  timer.start();

  if( model->needSourceSpace() )
    model->setSourceSpace(rmj->src_space);
  if( model->needSourceCellMIDs() )
    model->setSourceCellMIDs(rmj->sb->src_cmids);
  if( model->needSourceSpeciesMIDs() )
    model->setSourceSpeciesMIDs(rmj->sb->src_smids);
  if( model->needSourceIndividualMIDs() )
    model->setSourceIndividualsMIDs(rmj->sb->src_imids);
  if( model->needSourceCells() )
    model->setSourceCells(rmj->sb->src_cells);
  if( model->needSourceSpecies() )
    model->setSourceSpeciess(rmj->sb->src_species);
  if( model->needSourceIndividuals() )
    model->setSourceIndividuals(rmj->sb->src_individuals);

  if( model->needTargetSpace() )
    model->setTargetSpace(rmj->space);
  if( model->needTargetCellMIDs() )
    model->setTargetCellMIDs(rmj->space->cellMIDs());
  if( model->needTargetSpeciesMIDs() )
    model->setTargetSpeciesMIDs(rmj->space->speciesMIDs());
  if( model->needTargetIndividualMIDs() )
    model->setTargetIndividualsMIDs(rmj->space->individualMIDs());
  if( model->needTargetCells() )
    model->setTargetCells(rmj->space->cells());
  if( model->needTargetSpecies() )
    model->setTargetSpeciess(rmj->space->species());
  if( model->needTargetIndividuals() )
    model->setTargetIndividuals(rmj->space->individuals());

  if (model->needPRNG())
  {
    prng1 = rmj->prngFactory->prngFactoryCreate(rmj->simulator_job.prngConfiguration().component,
                                               rmj->simulator_job.prngConfiguration().configuration);
    if(!prng1)
    {
      delete model;
      delete rmj->space;
      rmj->valid = false;
      return rmj;
    }
    else
      model->setPRNG(prng1);
  }

  if (model->needDPRNG())
  {
    prng2 = rmj->prngFactory->prngFactoryCreate(rmj->simulator_job.prngConfiguration().component,
                                               rmj->simulator_job.prngConfiguration().configuration);
    if(!prng2)
    {
      delete prng1;
      delete model;
      delete rmj->space;
      rmj->valid = false;
      return rmj;
    }
    else
    {
      dprng = rmj->dprngFactory->dprngFactoryCreate(rmj->simulator_job.dprngConfiguration().component,
                                                 rmj->simulator_job.dprngConfiguration().configuration);
      if ( !dprng )
      {
        delete prng1;
        delete prng2;
        delete model;
        delete rmj->space;
        rmj->valid = false;
        return rmj;
      }
      else
      {
        dprng->setPRNG(prng2);
        model->setDPRNG(dprng);
      }
    }
  }

  model->setup();
  timer.stop();
  rmj->setup_time = timer.getElapsedTimeInMilliSec();

  timer.start();
  while( !model->finished() )
    model->model();
  timer.stop();
  rmj->model_time = timer.getElapsedTimeInMilliSec();

  rmj->valid = true;

  delete model;
  delete prng1;
  delete prng2;
  delete dprng;

  return rmj;
}

SimulatorBase::AccumulatorJobs SimulatorBase::accumulatorJobsFor( const SimulatorBase::ModelJob * job )
{
  AccumulatorJobs ajobs;

  foreach( ComponentConfiguration cc, job->simulator_job.accumulatorConfigurations() )
  {
    AccumulatorJob * ajob = new AccumulatorJob;
    ajob->sb = job->sb;

    ajob->curveFactory = job->curveFactory;

    ajob->accumulator = job->accumulatorFactory->accumulatorFactoryCreate(cc.component,
                                                                    cc.configuration);
    if( ajob->accumulator )
      ajobs.append(ajob);
  }

  return ajobs;
}

void SimulatorBase::accumulateModelJob( SimulatorBase::ModelJob * merged, const SimulatorBase::ModelJob * model )
{
  if ( !model->valid ) return;

//  if ( !merged )
//    merged = new ModelJob;

  if ( merged->accumulatorJobs.isEmpty() ) // setup merged
  {
    merged->accumulatorJobs = accumulatorJobsFor(model);
  }

  merged->accumulatorJobs = accumulateAccumulatorJobs(model->space,merged->accumulatorJobs);
  delete model->space;

  merged->setup_time +=model->setup_time;
  merged->model_time += model->model_time;
}


} // namespace Implementation
} // namespace SRPS
