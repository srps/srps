/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "simulators_simulatorstatuswidget.h"
#include "ui_simulators_simulatorstatuswidget.h"

namespace SRPS {
namespace Implementation {

SimulatorStatusWidget::SimulatorStatusWidget(SimulatorBase * base, QWidget *parent) :
    QWidget(parent),
    s(base),
    ui(new Ui::SimulatorStatusWidget)
{
    ui->setupUi(this);

    ui->generalLabel->hide();
    ui->generalOperationProgess->hide();

    if( s )
    {

      connect(s,SIGNAL(destroyed(QObject*)),this,SLOT(deleted()));

      connect(s,SIGNAL(detailTextChanged(QString)),
              ui->detailText,SLOT(setText(QString))/*,Qt::QueuedConnection*/);

      connect(s,SIGNAL(topOperationChanged(QString)),
              ui->generalLabel,SLOT(setText(QString))/*,Qt::QueuedConnection*/);

      connect(s,SIGNAL(subOperationChanged(QString)),
              ui->subLabel,SLOT(setText(QString))/*,Qt::QueuedConnection*/);

      connect(s,SIGNAL(topOperationRangeChanged(int,int)),
              ui->generalOperationProgess,SLOT(setRange(int,int))/*,Qt::QueuedConnection*/);

      connect(s,SIGNAL(subOperationRangeChanged(int,int)),
              ui->subOperationProgress,SLOT(setRange(int,int))/*,Qt::QueuedConnection*/);

      connect(s,SIGNAL(topOperationValueChanged(int)),
              ui->generalOperationProgess,SLOT(setValue(int))/*,Qt::DirectConnection*/);

      connect(s,SIGNAL(subOperationValueChanged(int)),
              ui->subOperationProgress,SLOT(setValue(int))/*,Qt::QueuedConnection*/);
    }
}

void SimulatorStatusWidget::deleted()
{
  s = 0;
}

SimulatorStatusWidget::~SimulatorStatusWidget()
{
    delete ui;
}

void SimulatorStatusWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS
