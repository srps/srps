/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curvevisualizers_visualizer.h"
#include "ui_curvevisualizers_visualizer.h"

#include <SRPS/QCurve>
#include <qwt_plot_curve.h>
#include <QtCore/QPointer>
#include <QtCore/QMutableHashIterator>
#include <QtGui/QCheckBox>
#include <QtGui/QColorDialog>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>

#include <QtGui/QMessageBox>

#include <QtGui/QFileDialog>


Q_DECLARE_METATYPE(QwtPlotCurve*);

namespace SRPS {
namespace Implementation {

class VariableData : public QwtData
{
  QPointer<SRPS::QCurve> curve;
  Curve::MemoryIdentifier vmid;
  Curve::MIDList tuples;

public:

  VariableData( SRPS::QCurve * _curve,
                Curve::MemoryIdentifier _vmid,
                Curve::MIDList _tuples ) :
      curve(_curve),vmid(_vmid),tuples(_tuples)
  {
  }

  QwtData * copy () const
  {
    return new VariableData(curve,vmid,tuples);
  }

  size_t size () const
  {
    if ( curve ) return tuples.count();

    return 0;
  }

  double x (size_t i) const
  {
    return double(int(i))/double(tuples.count()-1);
  }

  double y (size_t i) const
  {
    if ( !curve ) return 0;

    return curve->tupleValue(tuples.at(int(i)),vmid,0).toDouble();
  }
};

//////////////////////////////

class PropertyData : public QwtData
{
  QPointer<SRPS::QCurve> curve;
  QString property;
  int count;

public:

  PropertyData( SRPS::QCurve * _curve,
                QString _property,
                int _size ) :
      curve(_curve),property(_property),count(_size)
  {
  }

  QwtData * copy () const
  {
    return new PropertyData(curve,property,count);
  }

  size_t size () const
  {
    return count;
  }

  double x (size_t i) const
  {
    return int(i)+1;
  }

  double y (size_t /*i*/) const
  {
    if ( !curve ) return 0;

    return curve->componentProperty(property,0).toDouble();
  }
};

//////////////////////////////

class CurveVisualizer::Private
{
  public:
  QPointer<SRPS::QCurve> curve;
  Curve::MIDList tuples;

  QHash<Curve::MemoryIdentifier,QwtPlotCurve*> variableCurveTable;
  QList<Curve::MemoryIdentifier> variableMIDs;

  QHash<QString,QwtPlotCurve*> propertyCurveTable;
  QList<QString> properyNames;
};

CurveVisualizer::CurveVisualizer( QWidget * parent )
  : QMainWindow(parent),
    SRPS::CurveVisualizer(),
    p( new Private ),
    ui( new Ui::CurveVisualizer )
{
  ui->setupUi(this);
  ui->qwtPlot->setCanvasBackground( QColor(Qt::white) );

  connect(ui->variableTable,SIGNAL(cellDoubleClicked(int,int)),
          this,SLOT(change_variable_curve_color(int, int)));

  delete ui->propertyTab;

  ui->label_4->hide();
  ui->label_5->hide();
  ui->xRangeMaxSpin->hide();
  ui->xRangeMinSpin->hide();
  ui->yRangeMaxSpin->hide();
  ui->yRangeMinSpin->hide();

  ui->plotNameEdit->setText("");
  ui->xAxisNameEdit->setText(trUtf8("Dominio"));
  ui->yAxisNameEdit->setText(trUtf8("Riqueza"));
}

CurveVisualizer::~CurveVisualizer()
{
  delete ui;
  delete p;
}

const QString CurveVisualizer::StaticClassName = QLatin1String("srps.curve.visualizer");

QString CurveVisualizer::componentClassName() const
{
  return StaticClassName;
}

const QObject * CurveVisualizer::componentAsConstQObject() const
{
  return this;
}

QObject * CurveVisualizer::componentAsQObject()
{
  return this;
}

const SRPS::QCurve* CurveVisualizer::visualizedCurve() const
{
  return p->curve;
}

void CurveVisualizer::curveVisualize( const SRPS::QCurve * curve )
{
  if( p->curve )
  {
    clearVisualizer();

    disconnect(p->curve.data(),SIGNAL(destroyed()),
            this,SLOT(clearVisualizer()));

    disconnect(p->curve.data(),SIGNAL(tupleValueUpdated(SRPS::Curve::ConstTuple,SRPS::Curve::ConstVariable,QVariant)),
            ui->qwtPlot,SLOT(replot()));
//    disconnect(p->curve.data(),SIGNAL(curvePropertyUpdated(QString,QVariant)),
//               this,SLOT(curvePropertyUpdated(QString,QVariant)));

    disconnect(p->curve.data(),SIGNAL(tupleCreated(SRPS::Curve::ConstTuple)),
            this,SLOT(tupleCountChanged()));
    disconnect(p->curve.data(),SIGNAL(tupleDeleted(SRPS::Curve::ConstTuple)),
            this,SLOT(tupleCountChanged()));

    disconnect(p->curve.data(),SIGNAL(variableCreated(SRPS::Curve::ConstVariable)),
               this,SLOT(variableCreated(SRPS::Curve::ConstVariable)));
    disconnect(p->curve.data(),SIGNAL(variableDeleted(SRPS::Curve::ConstVariable)),
            this,SLOT(variableDeleted(SRPS::Curve::ConstVariable)));
    disconnect(p->curve.data(),SIGNAL(variablePropertyUpdated(SRPS::Curve::ConstVariable,QString,QVariant)),
            this,SLOT(variablePropertyUpdated(SRPS::Curve::ConstVariable,QString,QVariant)));
  }

  p->curve = const_cast<SRPS::QCurve*>(curve);

  if ( p->curve )
  {
    connect(p->curve.data(),SIGNAL(destroyed()),
            this,SLOT(clearVisualizer()));

    connect(p->curve.data(),SIGNAL(tupleValueUpdated(SRPS::Curve::ConstTuple,SRPS::Curve::ConstVariable,QVariant)),
            ui->qwtPlot,SLOT(replot()));
//    connect(p->curve.data(),SIGNAL(curvePropertyUpdated(QString,QVariant)),
//               this,SLOT(curvePropertyUpdated(QString,QVariant)));

    connect(p->curve.data(),SIGNAL(tupleCreated(SRPS::Curve::ConstTuple)),
            this,SLOT(tupleCountChanged()));
    connect(p->curve.data(),SIGNAL(tupleDeleted(SRPS::Curve::ConstTuple)),
            this,SLOT(tupleCountChanged()));

    connect(p->curve.data(),SIGNAL(variableCreated(SRPS::Curve::ConstVariable)),
            this,SLOT(variableCreated(SRPS::Curve::ConstVariable)));
    connect(p->curve.data(),SIGNAL(variableDeleted(SRPS::Curve::ConstVariable)),
            this,SLOT(variableDeleted(SRPS::Curve::ConstVariable)));
    connect(p->curve.data(),SIGNAL(variablePropertyUpdated(SRPS::Curve::ConstVariable,QString,QVariant)),
            this,SLOT(variablePropertyUpdated(SRPS::Curve::ConstVariable,QString,QVariant)));

    buildPlot();
  }
}

void CurveVisualizer::createRow(QTableWidget* /*table*/)
{
//  int r = ui->curveTableWidget->rowCount();

//  QTableWidgetItem * key = new QTableWidgetItem;
//  key->setCheckState(Qt::);
}

void CurveVisualizer::clearVisualizer()
{
  // remove variable plot curves
  {
    QMutableHashIterator<Curve::MemoryIdentifier,QwtPlotCurve*>
        iterator(p->variableCurveTable);
    while( iterator.hasNext() )
    {
      iterator.next();
      delete iterator.value();
      iterator.remove();
    }
  }
  // remove variable table items
  {
    while( ui->variableTable->rowCount() )
      ui->variableTable->removeRow(0);
  }
  // remove property plot curves
  {
    QMutableHashIterator<QString,QwtPlotCurve*>
        iterator(p->propertyCurveTable);
    while( iterator.hasNext() )
    {
      iterator.next();
      delete iterator.value();
      iterator.remove();
    }
  }
  // remove property table items
  {
    while( ui->propertyTable->rowCount() )
      ui->propertyTable->removeRow(0);
  }

  ui->qwtPlot->replot();
}

void CurveVisualizer::buildPlot()
{
  if ( !p->curve ) return;

  p->tuples = p->curve->tupleMIDs(Curve::CreationOrder);

//  foreach( QString property, p->curve->componentPropertyNames() )
//    createPropertyCurve(property);

  foreach( Curve::ConstVariable variable, p->curve->variables(Curve::CreationOrder) )
    variableCreated(variable);


  ui->qwtPlot->setAxisScale(QwtPlot::xBottom,0,1);

  ui->qwtPlot->replot();
}

void CurveVisualizer::tupleCountChanged() // for variables, update x axis
{
  p->tuples.clear();

  if ( p->curve ) p->tuples = p->curve->tupleMIDs(Curve::CreationOrder);

  foreach( Curve::MemoryIdentifier vmid, p->variableCurveTable.keys() )
    p->variableCurveTable.value(vmid)->setData(VariableData(p->curve,vmid,p->tuples));

  foreach( QString pname, p->propertyCurveTable.keys() )
    p->propertyCurveTable.value(pname)->setData(PropertyData(p->curve,pname,p->tuples.count()));

  ui->xRangeMaxSpin->setValue(p->tuples.count());
  ui->xRangeMinSpin->setValue(1);

  ui->qwtPlot->replot();
}

// remove, create or replot
void CurveVisualizer::curvePropertyUpdated( const QString & name,
                                            const QVariant & value )
{
  bool del = !value.isValid();

  int index = p->properyNames.indexOf(name);

  if ( !del && index != -1 ) ui->qwtPlot->replot(); // update

  if ( del && index != -1 ) deletePropertyCurve(name);

  if ( !del && index == -1 ) createPropertyCurve(name);
}

void CurveVisualizer::createPropertyCurve( const QString & name )
{
  QwtPlotCurve * curve = new QwtPlotCurve(name);
  p->propertyCurveTable.insert(name,curve);
  curve->setData(PropertyData(p->curve,name,p->tuples.count()));
  curve->attach(ui->qwtPlot);
  p->properyNames.insert(0,name);

  ui->propertyTable->insertRow(0);

  QCheckBox * check = new QCheckBox;
  check->setChecked(true);
  check->setProperty("srps.property.name",name);
  ui->propertyTable->setCellWidget(0,0,check);

  ui->qwtPlot->replot();
}

void CurveVisualizer::deletePropertyCurve( const QString & name )
{
  ui->propertyTable->removeRow(p->properyNames.indexOf(name));
  delete p->propertyCurveTable.value(name,0);
  p->properyNames.removeAll(name);

  ui->qwtPlot->replot();
}

void CurveVisualizer::variableCreated( const Curve::ConstVariable & variable )
{
  Curve::MemoryIdentifier vmid = variable.mid();
  QString name;
  if ( p->curve )
    name = p->curve->variableProperty(vmid,QString("srps.name"),"").toString().trimmed();
  if ( name.isEmpty() )
    name = tr("Unnamed variable %1").arg(vmid);

  QwtPlotCurve * curve = new QwtPlotCurve;
  curve->setTitle(name);
  p->variableCurveTable.insert(vmid,curve);
  curve->setData(VariableData(p->curve,vmid,p->tuples));
  curve->attach(ui->qwtPlot);
  p->variableMIDs.append(vmid);

  ui->variableTable->insertRow(0);

  // toggle view
  QCheckBox * check = new QCheckBox;
  check->setChecked(true);
  check->setText(name);
  check->setProperty("_srps_index_",vmid);
  ui->variableTable->setCellWidget(0,0,check);
  connect(check,SIGNAL(toggled(bool)),
          this,SLOT(toggle_variable_curve_view(bool)));

  // change color
  QTableWidgetItem * colorItem = new QTableWidgetItem;
  colorItem->setFlags(Qt::ItemIsEnabled);
  colorItem->setBackgroundColor(QColor(Qt::black));
  ui->variableTable->setItem(0,1,colorItem);

  // change style
  QComboBox * combo = new QComboBox;
  combo->addItems(
      QStringList() << tr("Lines") << tr("Sticks") << tr("Steps") << tr("Dots")
      );
  combo->setCurrentIndex(0);
  combo->setProperty("_srps_index_",vmid);
  ui->variableTable->setCellWidget(0,2,combo);
  connect(combo,SIGNAL(activated(QString)),
          this,SLOT(change_variable_curve_style(QString)));

  // change width
  QDoubleSpinBox * spin = new QDoubleSpinBox;
  spin->setValue(1);
  spin->setRange(0,10);
  spin->setProperty("_srps_index_",vmid);
  ui->variableTable->setCellWidget(0,3,spin);
  connect(spin,SIGNAL(valueChanged(double)),
          this,SLOT(change_variable_curve_width(double)));

  ui->qwtPlot->replot();
}

void CurveVisualizer::variableDeleted( const Curve::ConstVariable & variable )
{
  Curve::MemoryIdentifier vmid = variable.mid();
  delete p->variableCurveTable.value(vmid,0);
  ui->variableTable->removeRow(p->variableMIDs.indexOf(vmid));
  p->variableMIDs.removeAll(vmid);

  ui->qwtPlot->replot();
}

void CurveVisualizer::variablePropertyUpdated(
    const Curve::ConstVariable & variable,
    const QString & name,
    const QVariant & value )
{
  Curve::MemoryIdentifier vmid = variable.mid();
  int index = p->variableMIDs.indexOf(vmid);
//  bool del = !value.isValid();

  QwtPlotCurve * curve = p->variableCurveTable.value(vmid);

  if ( !curve ) return;

  if ( name == "srps.name" )
  {
    QString val = value.toString().trimmed();
    if ( val.isEmpty() )
      val = tr("Unnamed variable %1").arg(vmid);

    curve->setTitle(val);
    if ( index != -1 )
    {
      QCheckBox * check = qobject_cast<QCheckBox*>(ui->variableTable->cellWidget(index,0));
      if ( check )
        check->setText(val);
    }

  }

  ui->qwtPlot->replot();
}

void CurveVisualizer::toggle_variable_curve_view(bool t)
{
  QCheckBox * check = qobject_cast<QCheckBox*>(sender());
  if ( !check ) return;

  Curve::MemoryIdentifier vmid = check->property("_srps_index_").value<Curve::MemoryIdentifier>();

  QwtPlotCurve * curve = p->variableCurveTable.value(vmid);
  if ( !curve ) return;

  if ( t )
    curve->attach(ui->qwtPlot);
  else
    curve->detach();

  ui->qwtPlot->replot();
}

void CurveVisualizer::change_variable_curve_color(int r, int c)
{
  if ( c != 1 ) return;

  QTableWidgetItem * item = ui->variableTable->item(r,c);
  if ( !item ) return;

  QColor color = item->backgroundColor();
  color = QColorDialog::getColor(color,this);
  if ( !color.isValid() ) return;

  Curve::MemoryIdentifier vmid =
      ui->variableTable->cellWidget(r,0)->property("_srps_index_").value<Curve::MemoryIdentifier>();

  QwtPlotCurve * curve = p->variableCurveTable.value(vmid,0);
  if ( !curve ) return;

  QPen pen = curve->pen();
  pen.setColor(color);
  curve->setPen(pen);
  item->setBackgroundColor(color);

  ui->qwtPlot->replot();
}

void CurveVisualizer::change_variable_curve_style(QString t)
{
  QComboBox * combo = qobject_cast<QComboBox*>(sender());
  if ( !combo ) return;

  Curve::MemoryIdentifier vmid = combo->property("_srps_index_").value<Curve::MemoryIdentifier>();

  QwtPlotCurve * curve = p->variableCurveTable.value(vmid);
  if ( !curve ) return;

  if ( t == tr("Lines") )
    curve->setStyle(QwtPlotCurve::Lines);
  else if ( t == tr("Sticks") )
    curve->setStyle(QwtPlotCurve::Sticks);
  else if ( t == tr("Steps") )
    curve->setStyle(QwtPlotCurve::Steps);
  else if ( t == tr("Dots") )
    curve->setStyle(QwtPlotCurve::Dots);

  ui->qwtPlot->replot();
}

void CurveVisualizer::change_variable_curve_width(double i)
{
  QDoubleSpinBox * spin = qobject_cast<QDoubleSpinBox*>(sender());
  if ( !spin ) return;

  Curve::MemoryIdentifier vmid = spin->property("_srps_index_").value<Curve::MemoryIdentifier>();

  QwtPlotCurve * curve = p->variableCurveTable.value(vmid);
  if ( !curve ) return;

  QPen pen = curve->pen();
  pen.setWidthF(i);
  curve->setPen(pen);

  ui->qwtPlot->replot();
}

void SRPS::Implementation::CurveVisualizer::on_yRangeMinSpin_valueChanged(int v)
{
  ui->yRangeMaxSpin->setMinimum(v);
  ui->qwtPlot->setAxisScale(QwtPlot::yLeft,v,ui->yRangeMaxSpin->value());
  ui->qwtPlot->replot();
}

void SRPS::Implementation::CurveVisualizer::on_yRangeMaxSpin_valueChanged(int v)
{
  ui->yRangeMinSpin->setMaximum(v);
  ui->qwtPlot->setAxisScale(QwtPlot::yLeft,ui->yRangeMinSpin->value(),v);
  ui->qwtPlot->replot();
}

void SRPS::Implementation::CurveVisualizer::on_xRangeMinSpin_valueChanged(int v)
{
  ui->xRangeMaxSpin->setMinimum(v);
  ui->qwtPlot->setAxisScale(QwtPlot::xBottom,v,ui->xRangeMaxSpin->value());
  ui->qwtPlot->replot();
}

void SRPS::Implementation::CurveVisualizer::on_xRangeMaxSpin_valueChanged(int v)
{
  ui->xRangeMinSpin->setMaximum(v);
  ui->qwtPlot->setAxisScale(QwtPlot::xBottom,ui->xRangeMinSpin->value(),v);
  ui->qwtPlot->replot();
}

void SRPS::Implementation::CurveVisualizer::on_plotNameEdit_textChanged(QString t)
{
    ui->qwtPlot->setTitle(t);
}

void SRPS::Implementation::CurveVisualizer::on_xAxisNameEdit_textChanged(QString t)
{
  ui->qwtPlot->setAxisTitle(QwtPlot::xBottom,t);
}

void SRPS::Implementation::CurveVisualizer::on_yAxisNameEdit_textChanged(QString t)
{
  ui->qwtPlot->setAxisTitle(QwtPlot::yLeft,t);
}

} // namespace Implementation
} // namespace SRPS

void SRPS::Implementation::CurveVisualizer::on_actionSave_triggered()
{
  QString f = QFileDialog::getSaveFileName(this,tr("Guardar"),
                                           "",
                                           "Imagen PNG (*.png)");

  if (f.isEmpty()) return;

  QPixmap pixmap(640, 480);
  pixmap.fill(Qt::white); // Qt::transparent ?

  QwtPlotPrintFilter filter;
  int options = QwtPlotPrintFilter::PrintAll;
  options &= ~QwtPlotPrintFilter::PrintBackground;
  options |= QwtPlotPrintFilter::PrintFrameWithScales;
  filter.setOptions(options);

  ui->qwtPlot->print(pixmap, filter);

  if ( pixmap.save(f,"PNG") )
    QMessageBox::information(this,trUtf8("Guardar"),trUtf8("Gráfica guarda en: %1").arg(f));

}
