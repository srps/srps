include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}CurveVisualizers
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += curvevisualizers_plugin.cpp \
    curvevisualizers_visualizer.cpp
HEADERS += curvevisualizers_plugin.h \
    curvevisualizers_visualizer.h
FORMS += curvevisualizers_visualizer.ui

INCLUDEPATH *= ../../../sources/extra/qwt/src

LIBS *= -L../../../

unix:LIBS += -lqwt

win32 {
  CONFIG(debug,debug|release){
    LIBS    *= -lqwtd5
  }else {
    LIBS    *= -lqwt5
  }
}

RESOURCES += \
    resources.qrc
