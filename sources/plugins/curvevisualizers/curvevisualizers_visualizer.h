/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_IMPLEMENTATION_CURVEVISUALIZER_H
#define SRPS_IMPLEMENTATION_CURVEVISUALIZER_H

#include <SRPS/QCurve>
#include <SRPS/CurveVisualizer>

#include <QtGui/QMainWindow>

class QTableWidget;

namespace SRPS {

class QCurve;

namespace Implementation {

namespace Ui {
  class CurveVisualizer;
}

class CurveVisualizer : public QMainWindow, public SRPS::CurveVisualizer
{
  Q_OBJECT
  Q_DISABLE_COPY(CurveVisualizer)
      Q_INTERFACES(SRPS::CurveVisualizer)
  private:
    class Private;
    Private * p;
    Ui::CurveVisualizer * ui;

  public:
    CurveVisualizer( QWidget * parent = 0 );
    virtual ~CurveVisualizer();

    static const QString StaticClassName;

    QString componentClassName() const;

    const QObject * componentAsConstQObject() const;
    QObject * componentAsQObject();

    const SRPS::QCurve* visualizedCurve() const;

    void curveVisualize( const SRPS::QCurve * curve );

private:

    void createRow(QTableWidget *);

  private slots:

    void on_actionSave_triggered();
    void on_yAxisNameEdit_textChanged(QString );
    void on_xAxisNameEdit_textChanged(QString );
    void on_plotNameEdit_textChanged(QString );
    void clearVisualizer();
    void buildPlot();

    void tupleCountChanged();

    void curvePropertyUpdated( const QString & name, const QVariant & value );
    void createPropertyCurve( const QString & name );
    void deletePropertyCurve( const QString & name );

    void variableCreated( const SRPS::Curve::ConstVariable & variable );
    void variableDeleted( const SRPS::Curve::ConstVariable & variable );
    void variablePropertyUpdated( const SRPS::Curve::ConstVariable & variable,
                                  const QString & name,
                                  const QVariant & value );

    void toggle_variable_curve_view(bool);
    void change_variable_curve_color(int r, int c);
    void change_variable_curve_style(QString t);
    void change_variable_curve_width(double i);

private slots:
    void on_yRangeMinSpin_valueChanged(int );
    void on_yRangeMaxSpin_valueChanged(int );

    void on_xRangeMinSpin_valueChanged(int );
    void on_xRangeMaxSpin_valueChanged(int );
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_IMPLEMENTATION_IMPLEMENTATION_CURVEVISUALIZER_H
