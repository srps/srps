#include "spaces_space.h"

#include <QtCore/QVariantHash>
#include <QtCore/QSet>

namespace SRPS {
namespace Implementation {

// utility template functions :D

template <class T>
QSet<T> toSet( const QList<T> & list )
{
  QSet<T> result;

  typename QList<T>::const_iterator iterator = list.constBegin();
  typename QList<T>::const_iterator end      = list.constEnd();
  for( ; iterator != end ; ++iterator )
    result.insert(*iterator);

  return result;
}

template <class T>
QList<T> toList( const QSet<T> & list )
{
  QList<T> result;

  typename QSet<T>::const_iterator iterator = list.constBegin();
  typename QSet<T>::const_iterator end      = list.constEnd();
  for( ; iterator != end ; ++iterator )
    result.append(*iterator);

  return result;
}

template <class T, class V>
QSet<T> toSet( const QHash<T,V> & hash )
{
  QSet<T> result;

  typename QHash<T,V>::const_iterator iterator = hash.constBegin();
  typename QHash<T,V>::const_iterator end      = hash.constEnd();
  for( ; iterator != end ; ++iterator )
    result.insert(iterator.key());

  return result;
}

template <class T, class V>
QList<T> toList( const QHash<T,V> & hash )
{
  QList<T> result;

  typename QHash<T,V>::const_iterator iterator = hash.constBegin();
  typename QHash<T,V>::const_iterator end      = hash.constEnd();
  for( ; iterator != end ; ++iterator )
    result.append(iterator.key());

  return result;
}

// base data class, manage properties

class PropertyData
{
private:

  QVariantHash properties;

public:

  bool hasProperty( const QString & name ) const
  {
    return properties.contains(name);
  }

  int propertyCount() const
  {
    return properties.count();
  }

  QStringList propertyNames() const
  {
    return properties.keys();
  }

  QVariant property( const QString & name, const QVariant & defaultValue ) const
  {
    return properties.value(name,defaultValue);
  }

  int setProperty( const QString & name, const QVariant & value )
  {
    if ( !properties.contains(name) )
      return -1;

    if ( !value.isValid() )
      properties.remove(name);
    else
      properties.insert(name,value);

    return 0;
  }

};

// individual data class

class IndividualData : public PropertyData
{
private:

  Space::MemoryIdentifier species;
  Space::MemoryIdentifier cell;

  bool inCell;

public:

  IndividualData( const Space::MemoryIdentifier & smid ):
      PropertyData(),
      species(smid),
      cell(),
      properties(),
      inCell(false)
  {}

  Space::MemoryIdentifier speciesMID() const {
    return species;
  }

  Space::MemoryIdentifier cellMID() const
  {
    return cell;
  }

  bool isDistributed() const
  {
    return inCell;
  }

  void setDistributed( const Space::MemoryIdentifier & cmid )
  {
    inCell = true;
    cell = cmid;
  }

  void setUndistributed()
  {
    inCell = false;
    cell = Space::MemoryIdentifier(0);
  }

};

// species data class

class SpeciesData : public PropertyData
{
private:

  QSet<Space::MemoryIdentifier> individuals;

  QSet<Space::MemoryIdentifier> distributedIndividuals;
  QSet<Space::MemoryIdentifier> undistributedIndividuals;

  QSet<Space::MemoryIdentifier> distributedCells;
  QSet<Space::MemoryIdentifier> undistributedCells;

public:

  SpeciesData():
      PropertyData()
  {}

  bool isEmpty()
  {
    return individuals.isEmpty();
  }

  int individualCount() const
  {
    return individuals.count();
  }

  Space::MIDList individualMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(individuals);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  void removeIndividual( const Space::MemoryIdentifier & imid )
  {
    individuals.remove(imid);
    distributedIndividuals.remove(imid);
    undistributedIndividuals.remove(imid);
    Q_ASSERT( individuals.count() == distributedIndividuals.count() + undistributedIndividuals.count() );
  }

  void clearIndividuals()
  {
    individuals.clear();
    distributedIndividuals.clear();
    undistributedIndividuals.clear();
  }

  void undistributeIndividualMIDs()
  {
    undistributedIndividuals.unite(distributedIndividuals);
    distributedIndividuals.clear();
    undistributedCells.unite(distributedCells);
    distributedCells.clear();
    Q_ASSERT( individuals.count() == distributedIndividuals.count() + undistributedIndividuals.count() );
  }

  void distributeIndividual( const Space::MemoryIdentifier & imid )
  {
    undistributedIndividuals.remove(imid);
    distributedIndividuals.insert(imid);
    individuals.insert(imid);
    Q_ASSERT( individuals.count() == distributedIndividuals.count() + undistributedIndividuals.count() );
  }

  void undistributeIndividual( const Space::MemoryIdentifier & imid )
  {
    undistributedIndividuals.insert(imid);
    distributedIndividuals.remove(imid);
    individuals.insert(imid);
    Q_ASSERT( individuals.count() == distributedIndividuals.count() + undistributedIndividuals.count() );
  }

  int undistributedIndividualCount() const
  {
    return undistributedIndividuals.count();
  }

  Space::MIDList undistributedIndividualMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(undistributedIndividuals);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int distributedIndividualCount() const
  {
    return distributedIndividuals.count();
  }

  Space::MIDList distributedIndividualMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(distributedIndividuals);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int distributedCellCount() const
  {
    return distributedCells.count();
  }

  Space::MIDList distributedCellMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(distributedCells);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  void distributeCell( const Space::MemoryIdentifier & cmid )
  {
    distributedCells.insert(cmid);
    undistributedCells.remove(cmid);
  }

  int undistributedCellCount() const
  {
    return undistributedCells.count();
  }

  Space::MIDList undistributedCellMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(undistributedCells);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  void undistributeCell( const Space::MemoryIdentifier & cmid )
  {
    distributedCells.remove(cmid);
    undistributedCells.insert(cmid);
  }

  void clearCells()
  {
    distributedCells.clear();
    undistributedCells.clear();
  }

};

// cell data class

class CellData : public PropertyData
{
private:

  QHash<Space::MemoryIdentifier,Space::MIDList> table;
  Space::MIDList species;
  Space::MIDList individuals;

public:

  CellData():
      PropertyData()
  {}

  bool isEmpty()
  {
    return individuals.isEmpty();
  }

  int individualCount() const
  {
    return individuals.count();
  }

  Space::MIDList individuals( const Space::SortOrder & so ) const
  {
    Space::MIDList result;

    if ( so == Space::InverseCreationOrder )
      qCopyBackward(individuals.begin(),individuals.end(),result.end());
    else
      result = individuals;

    return result;
  }

  int individualCount( const Space::MemoryIdentifier & smid ) const
  {
    if ( !species.contains(smid) ) return 0;

    return table[smid].count();
  }

  Space::MIDList individuals( const Space::MemoryIdentifier & smid,
                              const Space::SortOrder & so ) const
  {
    if ( !species.contains(smid) ) return Space::MIDList();

    Space::MIDList result;

    if ( so == Space::InverseCreationOrder )
      qCopyBackward(table[smid].begin(),table[smid].end(),result.end());
    else
      result = table.value(smid);

    return result;
  }

  int speciesCount() const
  {
    return species.count();
  }

  Space::MIDList species( const Space::SortOrder & so ) const
  {
    Space::MIDList result;

    if ( so == Space::InverseCreationOrder )
      qCopyBackward(species.begin(),species.end(),result.end());
    else
      result = species;

    return result;
  }

  void insertIndividual( const Space::MemoryIdentifier & smid,
                         const Space::MemoryIdentifier & imid )
  {
    if ( !table.contains(smid) )
      species.append( smid );
    individuals.append(imid);
    table[smid].append(imid);
    Q_ASSERT( species.count() == table.count() );
  }

  void removeIndividual( const Space::MemoryIdentifier & smid,
                         const Space::MemoryIdentifier & imid )
  {
    if ( !table.contains(smid) ) return;

    table[smid].removeAll(imid);
    individuals.removeAll(imid);
    if ( !table[smid].count() )
    {
      table.remove(smid);
      species.removeAll(smid);
    }
    Q_ASSERT( species.count() == table.count() );
  }

  int removeIndividuals()
  {
    int count = individuals.count();

    table.clear();
    species.clear();
    individuals.clear();

    return count;
  }

  int removeAllSpecies()
  {
    int count = species.count();

    table.clear();
    species.clear();
    individuals.clear();

    return count;
  }

  bool removeSpecies( const Space::MemoryIdentifier & smid )
  {
    if ( !table.contains(smid) ) return false;

    species.remove(smid);
    QSet<Space::MemoryIdentifier> removed_individual_set = toSet(table.take(smid));
    Space::MIDList copy = individuals;
    individuals = Space::MIDList();

    // filter
    Space::MIDList::const_iterator iterator = copy.constBegin();
    Space::MIDList::const_iterator end      = copy.constEnd();
    for( ; iterator != end ; ++iterator )
      if( !removed_individual_set.contains(*iterator) )
        individuals.append( *iterator );

    return true;
  }
};

// containers, keep tracks of data status

class IndividualContainer
{
private:

  QHash<Space::MemoryIdentifier,IndividualData> individuals;
  QSet<Space::MemoryIdentifier> distributed;
  QSet<Space::MemoryIdentifier> undistributed;

public:

  bool valid( const Space::MemoryIdentifier & imid )
  {
    return individuals.contains(imid);
  }

  // warning, use valid first
  IndividualData & operator[]( const Space::MemoryIdentifier & imid )
  {
    return individuals[imid];
  }

  int count() const
  {
    return individuals.count();
  }

  Space::MIDList MIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(individuals);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int distributedCount() const
  {
    return distributed.count();
  }

  Space::MIDList distributedMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(distributed);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int undistributedCount() const
  {
    return undistributed.count();
  }

  Space::MIDList undistributedMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(distributed);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  void create( const Space::MemoryIdentifier & imid,
                         const Space::MemoryIdentifier & smid )
  {
    IndividualData id(smid);
    individuals.insert(imid,id);
    undistributed.insert(imid);
    Q_ASSERT( individuals.count() == distributed.count() + undistributed.count() );
  }

  void deleteAll()
  {
    individuals.clear();
    distributed.clear();
    undistributed.clear();
  }

  void deleteOne( const Space::MemoryIdentifier & imid )
  {
    individuals.remove(imid);
    distributed.remove(imid);
    undistributed.remove(imid);
    Q_ASSERT( individuals.count() == distributed.count() + undistributed.count() );
  }

  void deleteSpecies( const Space::MemoryIdentifier & smid )
  {
    QMutableHashIterator<Space::MemoryIdentifier,IndividualData> iterator(individuals);
    while( iterator.hasNext() )
    {
      iterator.next();
      if ( iterator.value().speciesMID() == smid )
      {
        distributed.remove(iterator.key());
        undistributed.remove(iterator.key());
        iterator.remove();
      }
    }
    Q_ASSERT( individuals.count() == distributed.count() + undistributed.count() );
  }

  void distribute( const Space::MemoryIdentifier & imid )
  {
    undistributed.remove(imid);
    distributed.insert(imid);
    Q_ASSERT( individuals.count() == distributed.count() + undistributed.count() );
  }

  void undistribute( const Space::MemoryIdentifier & imid )
  {
    distributed.remove(imid);
    undistributed.insert(imid);
    Q_ASSERT( individuals.count() == distributed.count() + undistributed.count() );
  }

  void undistributeAll()
  {
    undistributed.unite(distributed);
    distributed.clear();

    QHash<Space::MemoryIdentifier,IndividualData>::iterator iterator = individuals.begin();
    QHash<Space::MemoryIdentifier,IndividualData>::iterator end      = individuals.end();
    for( ; iterator != end; ++iterator )
      iterator->setUndistributed();

    Q_ASSERT( individuals.count() == distributed.count() + undistributed.count() );
  }
};

class SpeciesContainer
{
private:

  QHash<Space::MemoryIdentifier,SpeciesData> species;
  QSet<Space::MemoryIdentifier> distributed;
  QSet<Space::MemoryIdentifier> undistributed;

public:

  bool valid( const Space::MemoryIdentifier & smid ) const
  {
    return species.contains(smid);
  }

  SpeciesData & operator[](const Space::MemoryIdentifier & smid)
  {
    return species[smid];
  }

  int count() const
  {
    return species.count();
  }

  Space::MIDList MIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(species);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int distributedCount() const
  {
    return distributed.count();
  }

  Space::MIDList distributedMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(distributed);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int undistributedCount() const
  {
    return undistributed.count();
  }

  Space::MIDList undistributedMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(undistributed);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  void create( const Space::MemoryIdentifier & smid )
  {
    SpeciesData sd;
    species.insert(smid,sd);
    undistributed.insert(smid);
    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }

  void deleteOne( const Space::MemoryIdentifier & smid )
  {
    species.remove(smid);
    distributed.remove(smid);
    undistributed.remove(smid);
    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }

  void deleteAll()
  {
    species.clear();
    distributedSpecies.clear();
    undistributedSpecies.clear();
  }

  void distribute( const Space::MemoryIdentifier & smid )
  {
    distributed.insert(smid);
    undistributed.remove(smid);
    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }

  void undistribute( const Space::MemoryIdentifier & smid )
  {
    undistributed.insert(smid);
    distributed.remove(smid);
    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }

  void undistributeAll()
  {
    undistributed.unite(distributed);
    distributed.clear();

    QHash<Space::MemoryIdentifier,SpeciesData>::iterator iterator = species.begin();
    QHash<Space::MemoryIdentifier,SpeciesData>::iterator end      = species.end();
    for( ; iterator != end; ++iterator )
      iterator->setUndistributed();

    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }
};

class CellContainer
{
private:

  QHash<Space::MemoryIdentifier,CellData> cells;
  QSet<Space::MemoryIdentifier> distributed;
  QSet<Space::MemoryIdentifier> undistributed;

public:

  bool valid( const Space::MemoryIdentifier & smid ) const
  {
    return cells.contains(smid);
  }

  CellData & operator[](const Space::MemoryIdentifier & cmid)
  {
    return cells[cmid];
  }

  int count() const
  {
    return cells.count();
  }

  Space::MIDList MIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(cells);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int distributedCount() const
  {
    return distributed.count();
  }

  Space::MIDList distributedMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(distributed);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  int undistributedCount() const
  {
    return undistributed.count();
  }

  Space::MIDList undistributedMIDs( const Space::SortOrder & so ) const
  {
    Space::MIDList result = toList(undistributed);

    if ( so == Space::CreationOrder )
      qSort(result);
    else if ( so == Space::InverseCreationOrder )
      qSort(result.begin(),result.end(),qGreater<Space::MemoryIdentifier>());

    return result;
  }

  void create( const Space::MemoryIdentifier & cmid )
  {
    CellData cd;
    cells.insert(cmid,cd);
    undistributed.insert(cmid);
    Q_ASSERT( cells.count() == undistributed.count() + distributed.count() );
  }

  void deleteOne( const Space::MemoryIdentifier & cmid )
  {
    cells.remove(cmid);
    distributed.remove(cmid);
    undistributed.remove(cmid);
    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }

  void deleteAll()
  {
    cells.clear();
    distributed.clear();
    undistributed.clear();
  }

  void distribute( const Space::MemoryIdentifier & cmid )
  {
    distributed.insert(cmid);
    undistributed.remove(cmid);
    Q_ASSERT( cells.count() == undistributed.count() + distributed.count() );
  }

  void undistribute( const Space::MemoryIdentifier & cmid )
  {
    undistributed.insert(cmid);
    distributed.remove(cmid);
    Q_ASSERT( cells.count() == undistributed.count() + distributed.count() );
  }

  void undistributeAll()
  {
    undistributed.unite(distributedSpecies);
    distributedSpecies.clear();
    Q_ASSERT( species.count() == undistributed.count() + distributed.count() );
  }
};

class Implementation::Space::Private
{
  public:
    Private():counter(0){}

    Space::MemoryIdentifier counter;

    PropertyData properties;

    IndividualContainer individuals;

    SpeciesContainer species;

    CellContainer cells;
};

Space::Space( const QVariantHash & /*parameters*/ )
    : SRPS::Space(),
    p ( new Private )
{
}

Space::~Space()
{
    delete p;
}

const QString Space::StaticClassName = QLatin1String("srps.space");

QString Space::componentClassName() const
{
    return StaticClassName;
}

bool Space::componentHasProperty( const QString & name ) const
{
    return p->properties.contains(name);
}

int Space::componentPropertyCount() const
{
    return p->properties.count();
}

QStringList Space::componentPropertyNames() const
{
    return p->properties.keys();
}

QVariant Space::componentProperty(const QString & name, const QVariant & defaultResult) const
{
    return p->properties.value(name,defaultResult);
}

// SRPS::PropertySetter
int Space::componentSetProperty(const QString & name, const QVariant & value )
{
    return p->properties.setProperty(name,value);
}

// SRPS::Space
SRPS::Space * Space::clone() const
{
    Implementation::Space * clone = new Implementation::Space;

    clone->p->counter = p->counter;

    clone->p->properties = p->properties;

    clone->p->individuals = p->individuals;

    clone->p->species = p->species;

    clone->p->cells = p->cells;

    return clone;
}

SRPS::Space * Space::cloneUndistributed() const
{
  Implementation::Space * cloned = new Implementation::Space;

  clone->p->counter = p->counter;

  clone->p->properties = p->properties;

  clone->p->individuals = p->individuals;

  clone->p->species = p->species;

  clone->p->cells = p->cells;

  cloned->undistribute();

  return cloned;
}

bool Space::undistribute()
{
  // Cells
  p->cells.undistributeAll();

  // Individuals
  p->individuals.undistributeAll();

  // Species
  p->species.undistributeAll();

  return true;
}

// Invidual MID Interface

bool Space::individualIsValid( const Space::MemoryIdentifier & imid ) const
{
  return p->individuals.valid(imid);
}

bool Space::individualIsDistributed(const Space::MemoryIdentifier & imid ) const
{
  if ( ! individualIsValid(imid) ) return false;

  return p->individuals[imid].isDistributed();
}

Space::MemoryIdentifier Space::individualSpeciesMID(
    const Space::MemoryIdentifier & imid ) const
{
  if ( !individualIsValid(imid ) ) return MemoryIdentifier();

  return p->individuals[imid].speciesMID();
}

Space::MemoryIdentifier Space::individualCellMID(
    const Space::MemoryIdentifier & imid ) const
{
  if ( !individualIsValid(imid ) ) return MemoryIdentifier();

  return p->individuals[imid].cellMID();
}

int Space::individualCount() const
{
  return p->individuals.count();
}

Space::MIDList Space::individualMIDs( const Space::SortOrder & so ) const
{
  p->individuals.MIDs(so);
}

int Space::distributedIndividualCount() const
{
  return p->individuals.distributedCount();
}

Space::MIDList Space::distributedIndividualMIDs( const Space::SortOrder & so ) const
{
  return p->individuals.distributedMIDs(so);
}

int Space::undistributedIndividualCount() const
{
  return p->individuals.undistributedCount();
}

Space::MIDList Space::undistributedIndividualMIDs( const Space::SortOrder & so ) const
{
  return p->individuals.undistributedMIDs(so);
}

bool Space::individualHasProperty( const Space::MemoryIdentifier & mid,
                                   const QString & name ) const
{
  if ( !individualIsValid(imid) ) return false;

  return p->individuals[imid].property(name);
}

int Space::individualPropertyCount( const Space::MemoryIdentifier & mid) const
{
  if ( !individualIsValid(imid) ) return 0;

  return p->individuals[imid].propertyCount();
}

QStringList Space::individualPropertyNames( const Space::MemoryIdentifier & mid ) const
{
  if ( !individualIsValid(imid) ) return QStringList();

  return p->individuals[imid].propertyNames();
}

QVariant Space::individualProperty( const Space::MemoryIdentifier & mid,
                                    const QString & name,
                                    const QVariant & defaultValue ) const
{
  if ( !individualIsValid(imid) ) return defaultValue;

  return p->individuals[imid].property(name,defaultValue);
}

bool Space::individualSetProperty( const Space::MemoryIdentifier & mid,
                                   const QString & name,
                                   const QVariant & value )
{
  if ( !individualIsValid(imid) ) return 0;

  return p->individuals[imid].setProperty(name,value);
}

Space::MemoryIdentifier Space::createIndividualMID( const Space::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return MemoryIdentifier(0);

  MemoryIdentifier imid = ++(p->counter);
  p->individuals.createIndividual(imid,smid);
  p->species[smid].undistributeIndividual(imid);

  return imid;
}

bool Space::deleteIndividual( const Space::MemoryIdentifier & imid )
{
  if ( !individualIsValid(imid) ) return false;

  p->individuals.deleteIndividual(imid);

  return true;
}

int Space::deleteAllIndividuals()
{
  int count = p->individuals.count();

  // update individuals
  p->individuals.clear();
  p->distributedIndividuals.clear();
  p->undistributedIndividuals.clear();

  // update species
  p->emptySpecies = toSet(p->species);
  p->nonEmptySpecies.clear();
  p->undistributedSpecies = p->emptySpecies;
  p->distributedSpecies.clear();
  {
    SpeciesTable::iterator iterator = p->species.begin();
    SpeciesTable::iterator end      = p->species.end();
    for( ; iterator != end; ++iterator )
    {
      iterator->individuals.clear();
      iterator->distributedIndividuals.clear();
      iterator->undistributedIndividuals.clear();
      iterator->distributedCells.clear();
      iterator->undistributedCells.clear();
    }
  }

  // update cells
  p->emptyCells = toSet(p->cells);
  p->nonEmptyCells.clear();
  {
    CellTable::iterator iterator = p->cells.begin();
    CellTable::iterator end      = p->cells.end();
    for( ; iterator != end; ++iterator )
    {
      iterator->individuals.clear();
      iterator->table.clear();
    }
  }

  return count;
}

// Species MID Interface

bool Space::speciesIsValid( const Space::MemoryIdentifier & smid ) const
{
  return p->species.contains(smid);
}

bool Space::speciesIsDistributed(const Space::MemoryIdentifier & smid ) const
{
  if ( !p->species.contains(smid) ) return false;

  return p->distributedSpecies.contains(smid);
}

bool Space::speciesIsUndistributed(const Space::MemoryIdentifier & smid) const
{
  if ( !p->species.contains(smid) ) return false;

  return p->undistributedSpecies.contains(smid);
}

int Space::speciesDistributedCellCount( const Space::MemoryIdentifier & smid ) const
{
  if ( !p->species.contains(smid) ) return 0;

  return p->species[smid].distributedCells.count();
}

Space::MIDList Space::speciesDistributedCellMIDs( const Space::MemoryIdentifier & smid,
                                                  const SortOrder & so ) const
{
  if ( !p->species.contains(smid) ) return MIDList();

  MIDList result = toList(p->species[smid].distributedCells);

  if ( so == CreationOrder )
    qStableSort(result);
  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::speciesUndistributedCellCount( const MemoryIdentifier & smid ) const
{
  if ( !p->species.contains(smid) ) return 0;

  return p->species[smid].undistributedCells.count();
}

Space::MIDList Space::speciesUndistributedCellMIDs( const MemoryIdentifier & smid,
                                                    const SortOrder & so ) const
{
  if ( !p->species.contains(smid) ) return MIDList();

  MIDList result = toList(p->species[smid].undistributedCells);

  if ( so == CreationOrder )
    qStableSort(result);
  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::speciesCount() const
{
  return p->species.count();
}

Space::MIDList Space::speciesMIDs( const Space::SortOrder & so ) const
{
  MIDList result = toList(p->species);

  if ( so == CreationOrder )
    qStableSort(result);

  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::distributedSpeciesCount() const
{
  return p->distributedSpecies.count();
}

Space::MIDList Space::distributedSpeciesMIDs( const Space::SortOrder & so ) const
{
  MIDList result = toList(p->distributedSpecies);

  if ( so == CreationOrder )
    qStableSort(result);
  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::undistributedSpeciesCount() const
{
  return p->undistributedSpecies.count();
}

Space::MIDList Space::undistributedSpeciesMIDs( const Space::SortOrder & so ) const
{
  MIDList result = toList(p->undistributedSpecies);

  if ( so == CreationOrder )
    qStableSort(result);
  else if( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::speciesIndividualCount(const Space::MemoryIdentifier & smid) const
{
  if ( !speciesIsValid(smid) ) return 0;

  return p->species[smid].individuals.count();
}

Space::MIDList Space::speciesIndividualMIDs( const Space::MemoryIdentifier & smid,
                                             const SortOrder & so ) const
{
  if ( !speciesIsValid(smid) ) return MIDList();

  MIDList result = toList(p->species[smid].individuals);

  if ( so == CreationOrder )
    qStableSort(result);
  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::speciesDistributedIndividualCount( const Space::MemoryIdentifier & smid ) const
{
  if ( !speciesIsValid(smid) ) return 0;

  return p->species[smid].distributedIndividuals.count();
}

Space::MIDList Space::speciesDistributedIndividualMIDs(
    const Space::MemoryIdentifier & smid,
    const Space::SortOrder & so ) const
{
  if ( !speciesIsValid(smid) ) return MIDList();

  MIDList result = toList(p->species[smid].distributedIndividuals);

  if ( so == CreationOrder )
    qStableSort(result);
  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::speciesUndistributedIndividualCount( const Space::MemoryIdentifier & smid ) const
{
  if ( !speciesIsValid(smid) ) return 0;

  return p->species[smid].undistributedIndividuals.count();
}

Space::MIDList Space::speciesUndistributedIndividualMIDs(
    const Space::MemoryIdentifier & smid,
    const Space::SortOrder & so ) const
{
  if ( !speciesIsValid(smid) ) return MIDList();

  MIDList result = toList(p->species[smid].undistributedIndividuals);

  if ( so == CreationOrder )
    qStableSort(result);
  else if ( so == InverseCreationOrder )
    qStableSort(result.begin(),result.end(),qGreater<MemoryIdentifier>());

  return result;
}

int Space::emptySpeciesCount() const
{
  return p->species.emptyCount();
}

Space::MIDList Space::emptySpeciesMIDs( const Space::SortOrder & so ) const
{
  return p->species.emptyMIDs(so);
}

int Space::nonEmptySpeciesCount() const
{
  return p->species.nonEmptyCount();
}

Space::MIDList Space::nonEmptySpeciesMIDs( const Space::SortOrder & so ) const
{
  return p->species.nonEmptyMIDs(so);
}

int Space::speciesUndistribute( const Space::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return 0;

  int count = p->individuals.undistributeSpecies(smid);

  p->species.undistribute(smid);
  p->cells.undistributeSpecies(smid);

  return count;
}

bool Space::speciesHasProperty( const Space::MemoryIdentifier & smid,
                                   const QString & name ) const
{
  if ( !speciesIsValid(smid) ) return false;

  return p->species[smid].hasProperty(name);
}

int Space::speciesPropertyCount( const Space::MemoryIdentifier & smid) const
{
  if ( !speciesIsValid(smid) ) return 0;

  return p->species[smid].propertyCount();
}

QStringList Space::speciesPropertyNames( const Space::MemoryIdentifier & smid ) const
{
  if ( !speciesIsValid(smid) ) return QStringList();

  return p->species[smid].propertyNames();
}

QVariant Space::speciesProperty( const Space::MemoryIdentifier & smid,
                                    const QString & name,
                                    const QVariant & defaultValue ) const
{
  if ( !speciesIsValid(smid) ) return defaultValue;

  return p->species[smid].propert(name,defaultValue);
}

bool Space::speciesSetProperty( const Space::MemoryIdentifier & mid,
                                   const QString & name,
                                   const QVariant & value )
{
  if ( !speciesIsValid(smid) ) return false;

  return p->species[smid].setProperty(name,value);
}

Space::MemoryIdentifier Space::createSpeciesMID()
{
  MemoryIdentifier smid = ++(p->counter);

  p->species.create(smid);
  p->species[smid].setCells(p->cells.MIDs());

  return smid;
}

bool Space::deleteSpecies( const Space::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  p->individuals.deleteSpecies(smid);
  p->species.deleteOne(smid);
  p->cells.undistributeSpecies(smid);

  return true;
}

int Space::deleteAllSpecies()
{
  int count = p->species.count();

  p->individuals.deleteAll();
  p->species.deleteAll();
  p->cells.undistributeAll();

  return count;
}

// Cell MID Interface

bool Space::cellIsValid( const Space::MemoryIdentifier & cmid ) const
{
  return p->cells.isValid(cmid);
}

int Space::cellCount() const
{
  return p->cells.count();
}

Space::MIDList Space::cellMIDs( const Space::SortOrder & so ) const
{
  return p->cells.MIDs(so);
}

int Space::emptyCellCount() const
{
  return p->cells.emptyCount();
}

Space::MIDList Space::emptyCellMIDs( const Space::SortOrder & so ) const
{
  return p->cells.emptyMIDs(so);
}

int Space::nonEmptyCellCount() const
{
  return p->species.emptyCount();
}

Space::MIDList Space::nonEmptyCellMIDs( const Space::SortOrder & so ) const
{
  return p->species.emptyMIDs(so);
}

bool Space::cellHasProperty( const Space::MemoryIdentifier & mid,
                                   const QString & name ) const
{
  if ( !cellIsValid(cmid) ) return false;

  return p->cells[cmid].hasProperty(name);
}

int Space::cellPropertyCount( const Space::MemoryIdentifier & mid) const
{
  if ( !cellIsValid(cmid) ) return 0;

  return p->cells[cmid].propertyCount();
}

QStringList Space::cellPropertyNames( const Space::MemoryIdentifier & mid ) const
{
  if ( !cellIsValid(cmid) ) return QStringList();

  return p->cells[cmid].propertyNames();
}

QVariant Space::cellProperty( const Space::MemoryIdentifier & mid,
                                    const QString & name,
                                    const QVariant & defaultValue ) const
{
  if ( !cellIsValid(cmid) ) return defaultValue;

  return p->cells[cmid].property(name,defaultValue);
}

bool Space::cellSetProperty( const Space::MemoryIdentifier & mid,
                             const QString & name,
                             const QVariant & value )
{
  if ( !cellIsValid(cmid) ) return false;

  return p->cells[cmid].setProperty(name,value);
}

Space::MemoryIdentifier Space::createCellMID()
{
  MemoryIdentifier cmid = ++(p->counter);

  p->cells.create(cmid);
  p->species.createCell(cmid);

  return cmid;
}

bool Space::deleteCell(const Space::MemoryIdentifier & mid)
{
  if ( !cellIsValid(mid) ) return false;

  p->cells.deleteOne(mid);
  p->species.deleteCell(mid);
  p->individuals.deleteCell(mid);
  return true;
}

int Space::deleteAllCells()
{
  int count = p->cells.count();

  p->cells.deleteAll();
  p->species.undistributeAll();
  p->individuals.undistributeAll();

  return count;
}

int Space::cellIndividualCount( const Space::MemoryIdentifier & mid ) const
{
  if ( !cellIsValid(cmid) ) return 0;

  return p->cells[cmid].individualCount();
}

Space::MIDList Space::cellIndividualMIDs( const Space::MemoryIdentifier & cmid,
                                          const Space::SortOrder & so ) const
{
  if ( !cellIsValid(cmid) ) return MIDList();

  return p->cells[cmid].individualMIDs(so);
}

int Space::cellSpeciesCount( const Space::MemoryIdentifier & cmid ) const
{
  if ( !cellIsValid(cmid) ) return;

  return p->cells[cmid].speciesCount();
}

Space::MIDList Space::cellSpeciesMIDs( const Space::MemoryIdentifier & cmid,
                                       const Space::SortOrder & so ) const
{
  if ( !cellIsValid(cmid) ) return MIDList();

  return p->cells[cmid].speciesMIDs(so);
}

bool Space::cellHasSpecies( const Space::MemoryIdentifier & cmid,
                            const Space::MemoryIdentifier & smid ) const
{
  if ( !cellIsValid(cmid) ) return false;

  if ( !speciesIsValid(smid) ) return false;

  return p->cells[cmid].hasSpecies(smid);
}

int Space::cellSpeciesIndividualCount( const Space::MemoryIdentifier & cmid,
                                       const Space::MemoryIdentifier & smid ) const
{
  if ( !cellIsValid(cmid) ) return MIDList();

  if ( !speciesIsValid(smid) ) return MIDList();

  return p->cells[cmid].individualCount(smid);
}

Space::MIDList Space::cellSpeciesIndividualMIDs(
    const Space::MemoryIdentifier & cmid,
    const Space::MemoryIdentifier & smid,
    const Space::SortOrder & so ) const
{
  if ( !cellIsValid(cmid) ) return MIDList();

  if ( !speciesIsValid(smid) ) return MIDList();

  return p->cells[cmid].individualMIDs(smid,so);
}

int Space::cellRemoveSpecies(const Space::MemoryIdentifier & cmid,
                             const Space::MemoryIdentifier & smid )
{
  if ( !cellIsValid(cmid) ) return 0;

  if ( !speciesIsValid(smid) ) return 0;

  p->individuals.undistributeCellSpecies(cmid,smid);
  p->species[smid].undistributeCell(cmid);
  return p->cells[cmid].removeSpecies(smid);
}

int Space::cellRemoveAllSpecies(const Space::MemoryIdentifier & cmid )
{
  if ( !cellIsValid(cmid) ) return 0;

  p->individuals.undistributeCell(cmid);
  p->species.undistributeCell(cmid);
  return p->cells[cmid].removeAllSpecies();
}

} // namespace Implementation
} // namespace SRPS
