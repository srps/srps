include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}Spaces
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += \
    spaces_plugin.cpp \
    spaces_space_packed.cpp
HEADERS += \
    spaces_plugin.h \
    spaces_space_packed.h

RESOURCES += \
    resources.qrc
