/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spaces_plugin.h"
#include "spaces_space_packed.h"

namespace SRPS{
namespace Implementation {

SpaceFactory::SpaceFactory(QObject *parent) :
  QObject(parent),
  SRPS::SpaceFactory()
{
}

SpaceFactory::~SpaceFactory()
{
}

// SpaceFactory
QStringList SpaceFactory::spaceFactoryKeys() const
{
  return QStringList(SpacePacked::StaticClassName);
}

SRPS::Space * SpaceFactory::spaceFactoryCreate (const QString &space,
                                                const QVariantHash &/*parameters*/)
{
  if( space == SpacePacked::StaticClassName )
      return new SpacePacked;
  return 0;
}

};
};

Q_EXPORT_PLUGIN2(SRPSSpaces,SRPS::Implementation::SpaceFactory)
