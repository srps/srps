/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SPACES_SPACEPACKED_H
#define SPACES_SPACEPACKED_H

#include <SRPS/Space>
#include  <QtConcurrentMap>

namespace SRPS {
namespace Implementation {

class SpacePacked : public SRPS::Space
{
    class Private;
    Private * p;

    // Transform MemoryIdentifier to Reference
    template <typename R,typename S>
    class Transformer
    {
    public:
      typedef R (SpacePacked::*ConverterFunction)(const S &) const;

      inline Transformer(const SpacePacked *_obj,ConverterFunction _converter) :
          obj(_obj),
          converter(_converter){}

      R operator()( const S & mi ) const
      {
        return (obj->*converter)(mi);
      }

    private:
      const SpacePacked *obj;
      ConverterFunction converter;
    };

    // qLess equivalent in References
    template <typename R>
    class RLess
    {
    public:
      bool operator()( const R & r1, const R & r2 ) const
      {
        return r1.mid() > r2.mid();
      }
    };

    // qGreater equivalent in References
    template <typename R>
    class RGreater
    {
    public:
      bool operator()( const R & r1, const R & r2 ) const
      {
        return r1.mid() < r2.mid();
      }
    };

    // qLess equivalent in MemoryIdentifier
    template <typename R>
    class MILess
    {
    public:
      bool operator()( const R & r1, const R & r2 ) const
      {
        return r1 > r2;
      }
    };

    // qGreater equivalent in MemoryIdentifier
    template <typename R>
    class MIGreater
    {
    public:
      bool operator()( const R & r1, const R & r2 ) const
      {
        return r1 < r2;
      }
    };


    // Takes Hash MemoryIdentifier and sorted them using insertion sort
    template <typename K, typename V, typename Comparator>
    inline QList<K> sortedList( const QHash<K,V> & hash,
                                const Comparator & comparator ) const
    {
      QList<K> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        typename QList<K>::iterator ri = result.begin();
        typename QList<K>::iterator re = result.end();
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, iterator.key() ) ) break;
        }
        result.insert(ri,iterator.key());
      }

      return result;
    }

    // Takes Set MemoryIdentifier and sorted them using insertion sort
    template <typename K, typename Comparator>
    inline QList<K> sortedList( const QSet<K> & set,
                                const Comparator & comparator ) const
    {
      QList<K> result;

      typename QSet<K>::const_iterator iterator = set.constBegin();
      typename QSet<K>::const_iterator end      = set.constEnd();
      for( ; iterator != end; ++iterator )
      {
        typename QList<K>::iterator ri = result.begin();
        typename QList<K>::iterator re = result.end();
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, *iterator ) ) break;
        }
        result.insert(ri,*iterator);
      }

      return result;
    }

    // Takes List MemoryIdentifier and sorted them using insertion sort
    template <typename K, typename Comparator>
    inline QList<K> sortedList( const QList<K> & set,
                                const Comparator & comparator ) const
    {
      QList<K> result;

      typename QList<K>::const_iterator iterator = set.constBegin();
      typename QList<K>::const_iterator end      = set.constEnd();
      for( ; iterator != end; ++iterator )
      {
        typename QList<K>::iterator ri = result.begin();
        typename QList<K>::iterator re = result.end();
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, *iterator ) ) break;
        }
        result.insert(ri,*iterator);
      }

      return result;
    }

    // Takes Hash MemoryIdentifier transform them and sorted them using insertion sort
    template <typename T, typename K, typename V, typename Comparator>
    inline QList<T> sortedTransformedList( const QHash<K,V> & hash,
                                          const Comparator & comparator,
                                          const Transformer<T,K> & transformer ) const
    {
      QList<T> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        typename QList<T>::iterator ri = result.begin();
        typename QList<T>::iterator re = result.end();
        T transmorfed = transformer(iterator.key());
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, transmorfed ) ) break;
        }
        result.insert(ri,transmorfed);
      }

      return result;
    }

    // Takes Set MemoryIdentifier transform them and sorted them using insertion sort
    template <typename T, typename K, typename Comparator>
    inline QList<T> sortedTransformedList( const QSet<K> & hash,
                                          const Comparator & comparator,
                                          const Transformer<T,K> & transformer ) const
    {
      QList<T> result;

      typename QSet<K>::const_iterator iterator = hash.constBegin();
      typename QSet<K>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        typename QList<T>::iterator ri = result.begin();
        typename QList<T>::iterator re = result.end();
        T transmorfed = transformer(*iterator);
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, transmorfed ) ) break;
        }
        result.insert(ri,transmorfed);
      }

      return result;
    }

    // Takes Set MemoryIdentifier transform them and sorted them using insertion sort
    template <typename T, typename K, typename Comparator>
    inline QList<T> sortedTransformedList( const QList<K> & hash,
                                          const Comparator & comparator,
                                          const Transformer<T,K> & transformer ) const
    {
      QList<T> result;

      typename QList<K>::const_iterator iterator = hash.constBegin();
      typename QList<K>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        typename QList<T>::iterator ri = result.begin();
        typename QList<T>::iterator re = result.end();
        T transmorfed = transformer(*iterator);
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, transmorfed ) ) break;
        }
        result.insert(ri,transmorfed);
      }

      return result;
    }

    template <typename T, typename K, typename V, typename Comparator, typename Filter>
    inline QList<T> sortedTransformedFilteredList( const QHash<K,V> & hash,
                                                  Comparator comparator,
                                                  Transformer<T,K> transformer,
                                                  Filter filter ) const
    {
      QList<T> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        if ( !filter(iterator.value()) ) continue;

        typename QList<T>::iterator ri = result.begin();
        typename QList<T>::iterator re = result.end();
        T transmorfed = transformer(iterator.key());
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, transmorfed ) ) break;
        }
        result.insert(ri,transmorfed);
      }

      return result;
    }

    template <typename T, typename K, typename V>
    inline QList<T> transformedList( const QHash<K,V> & hash,
                                    const Transformer<T,K> & transformer ) const
    {
      QList<T> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        result.append(transformer(iterator.key()));
      }

      return result;
    }

    template <typename T, typename K>
    inline QList<T> transformedList( const QSet<K> & hash,
                                     const Transformer<T,K> & transformer ) const
    {
      QList<T> result;

      typename QSet<K>::const_iterator iterator = hash.constBegin();
      typename QSet<K>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        result.append(transformer(*iterator));
      }

      return result;
    }

    template <typename T, typename K>
    inline QList<T> transformedList( const QList<K> & hash,
                                     const Transformer<T,K> & transformer ) const
    {
      QList<T> result;

      typename QList<K>::const_iterator iterator = hash.constBegin();
      typename QList<K>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        result.append(transformer(*iterator));
      }

      return result;
    }

    template <typename T, typename K, typename V, typename Filter>
    inline QList<T> transformedFilteredList( const QHash<K,V> & hash,
                                            const Transformer<T,K> & transformer,
                                            Filter filter) const
    {
      QList<T> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        if ( filter(iterator.value()) )
          result.append(transformer(iterator.key()));
      }

      return result;
    }

    template <typename T, typename K, typename V, typename Filter>
    inline QList<T> transformedFilteredList( const QSet<K> & hash,
                                            const Transformer<T,K> & transformer,
                                            Filter filter) const
    {
      QList<T> result;

      typename QSet<K>::const_iterator iterator = hash.constBegin();
      typename QSet<K>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        if ( filter(iterator.value()) )
          result.append(transformer(iterator.key()));
      }

      return result;
    }

    template <typename K, typename V, typename Tester>
    inline QList<K> filteredList( const QHash<K,V> & hash,
                                  Tester tester ) const
    {
      QList<K> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        if ( tester(iterator.value()) )
          result.append( iterator.key() );
      }

      return result;
    }

    template <typename K, typename V, typename Tester>
    inline int countIf( const QHash<K,V> & hash,
                        Tester tester ) const
    {
      int count = 0;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        if ( tester(iterator.value()) )
          ++count;
      }

      return count;
    }

    template <typename K, typename V, typename Comparator, typename Filter>
    inline QList<K> sortedFilteredList( const QHash<K,V> & hash,
                                        Comparator comparator,
                                        Filter filter ) const
    {
      QList<K> result;

      typename QHash<K,V>::const_iterator iterator = hash.constBegin();
      typename QHash<K,V>::const_iterator end      = hash.constEnd();
      for( ; iterator != end; ++iterator )
      {
        if ( !filter(iterator.value()) ) continue;

        typename QList<K>::iterator ri = result.begin();
        typename QList<K>::iterator re = result.end();
        for( ; ri != re; ++ri )
        {
          if ( comparator( *ri, iterator.key() ) ) break;
        }
        result.insert(ri,iterator.key());
      }

      return result;
    }

public:

    SpacePacked();
    virtual ~SpacePacked();

    static const QString StaticClassName;

    QString componentClassName() const;

    bool componentHasProperty( const QString & name ) const;

//    bool componentIsStaticProperty( const QString & name ) const;

    int componentPropertyCount() const;

    QStringList componentPropertyNames() const;

    QVariant componentProperty( const QString & name,
                                const QVariant & defaultValue =
                                QVariant() ) const;

    int componentSetProperty( const QString & name,
                              const QVariant & value );

    SRPS::Space * clone() const;

    SRPS::Space * cloneUndistributed() const;

    void undistribute() ;

    void clear();

    // Individual Space interface

    int individualCount() const;

    MIDList individualMIDs( const SortOrder & so = Unordered ) const;

    ConstIndividualList constIndividuals( const SortOrder & so = Unordered ) const;

    IndividualList individuals( const SortOrder & so = Unordered ) const;

    int distributedIndividualCount() const;

    MIDList distributedIndividualMIDs( const SortOrder & so = Unordered ) const ;

    ConstIndividualList constDistributedIndividuals( const SortOrder & so = Unordered ) const;

    IndividualList distributedIndividuals( const SortOrder & so = Unordered ) const;

    int undistributedIndividualCount() const;

    MIDList undistributedIndividualMIDs( const SortOrder & so = Unordered ) const ;

    ConstIndividualList constUndistributedIndividuals( const SortOrder & so = Unordered ) const;

    IndividualList undistributedIndividuals( const SortOrder & so = Unordered ) const;

    // Invidual Reference Interface

    bool individualIsValid( const MemoryIdentifier &) const;

    bool individualHasProperty( const MemoryIdentifier & mid,
                                const QString & name ) const;

    int individualPropertyCount( const MemoryIdentifier & mid) const;

    QStringList individualPropertyNames( const MemoryIdentifier & mid ) const;

    QVariant individualProperty( const MemoryIdentifier & mid,
                                 const QString & name,
                                 const QVariant & defaultValue =
                                 QVariant() ) const;

    MemoryIdentifier individualSpeciesMID( const MemoryIdentifier & mid ) const;

    ConstSpecies individualConstSpecies( const MemoryIdentifier & mid ) const;

    MemoryIdentifier individualCellMID( const MemoryIdentifier & mid ) const;
    ConstCell individualConstCell( const MemoryIdentifier & mid ) const;

    bool individualIsDistributed(const MemoryIdentifier & mid) const ;

    int individualSetProperty( const MemoryIdentifier & mid,
                               const QString & name,
                               const QVariant & value );

    // Species Space Interface

    int speciesCount() const;

    MIDList speciesMIDs( const SortOrder & so = Unordered ) const ;

    ConstSpeciesList constSpecies( const SortOrder & so = Unordered ) const;

    SpeciesList species( const SortOrder & so = Unordered ) const;

    int distributedSpeciesCount() const;

    MIDList distributedSpeciesMIDs( const SortOrder & so = Unordered ) const ;

    ConstSpeciesList constDistributedSpecies( const SortOrder & so = Unordered ) const;

    SpeciesList distributedSpecies( const SortOrder & so = Unordered ) const;

    int undistributedSpeciesCount() const;

    MIDList undistributedSpeciesMIDs( const SortOrder & so = Unordered ) const ;

    ConstSpeciesList constUndistributedSpecies( const SortOrder & so = Unordered ) const;

    SpeciesList undistributedSpecies( const SortOrder & so = Unordered ) const;

    int emptySpeciesCount() const;

    MIDList emptySpeciesMIDs( const SortOrder & so = Unordered ) const ;

    ConstSpeciesList constEmptySpecies( const SortOrder & so = Unordered ) const;

    SpeciesList emptySpecies( const SortOrder & so = Unordered ) const;

    int nonEmptySpeciesCount() const;

    MIDList nonEmptySpeciesMIDs( const SortOrder & so = Unordered ) const ;

    ConstSpeciesList constNonEmptySpecies( const SortOrder & so = Unordered ) const;

    SpeciesList nonEmptySpecies( const SortOrder & so = Unordered ) const;

    MemoryIdentifier createSpeciesMID();

    Species createSpecies();

    bool deleteSpecies( const MemoryIdentifier & imid );

    bool deleteSpecies( const ConstSpecies & species );

    int deleteAllSpecies();

    int deleteDistributedSpecies();

    int deleteUndistributedSpecies();

    int deleteEmptySpecies();

    int deleteNonEmptySpecies();

    // Species Reference Interface

    bool speciesIsValid( const MemoryIdentifier &) const;

    bool speciesHasProperty( const MemoryIdentifier & mid,
                             const QString & name ) const;

    int speciesPropertyCount( const MemoryIdentifier & mid) const;

    QStringList speciesPropertyNames( const MemoryIdentifier & mid ) const;

    QVariant speciesProperty( const MemoryIdentifier & mid,
                              const QString & name,
                              const QVariant & defaultValue =
                              QVariant() ) const;

    int speciesIndividualCount(const MemoryIdentifier & mid) const;

    MIDList speciesIndividualMIDs( const MemoryIdentifier & mid,
                                   const SortOrder & so = Unordered ) const ;

    ConstIndividualList
        speciesConstIndividuals( const MemoryIdentifier & mid,
                                 const SortOrder & so = Unordered ) const ;

    IndividualList
        speciesIndividuals( const MemoryIdentifier & mid,
                            const SortOrder & so = Unordered ) const ;

    int speciesDistributedIndividualCount( const MemoryIdentifier & mid ) const;

    MIDList speciesDistributedIndividualMIDs( const MemoryIdentifier & mid,
                                              const SortOrder & so = Unordered ) const ;

    ConstIndividualList
        speciesDistributedConstIndividuals( const MemoryIdentifier & mid,
                                            const SortOrder & so = Unordered ) const ;

    int speciesUndistributedIndividualCount( const MemoryIdentifier & mid ) const;

    MIDList speciesUndistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                const SortOrder & so = Unordered ) const ;

    ConstIndividualList
        speciesUndistributedConstIndividuals( const MemoryIdentifier & mid,
                                              const SortOrder & so = Unordered ) const ;

    bool speciesIsDistributed(const MemoryIdentifier & mid) const ;

    bool speciesIsUndistributed(const MemoryIdentifier & mid) const ;

    int speciesDistributedCellCount( const MemoryIdentifier & mid ) const;

    MIDList speciesDistributedCellMIDs( const MemoryIdentifier & mid,
                                        const SortOrder & so = Unordered ) const;

    ConstCellList
        speciesDistributedConstCells( const MemoryIdentifier & mid,
                                      const SortOrder & so = Unordered ) const ;

    int speciesSetProperty( const MemoryIdentifier & mid,
                             const QString & name,
                             const QVariant & value );

    MemoryIdentifier speciesCreateIndividualMID( const MemoryIdentifier & smid );

    Individual speciesCreateIndividual( const MemoryIdentifier & smid );

    bool speciesDeleteIndividual( const MemoryIdentifier & smid,
                                  const MemoryIdentifier & imid );

    int speciesDeleteIndividuals( const MemoryIdentifier & smid );

    int speciesDeleteDistributedIndividuals( const MemoryIdentifier & smid );

    int speciesDeleteUndistributedIndividuals( const MemoryIdentifier & smid );

    IndividualList
        speciesDistributedIndividuals( const MemoryIdentifier & mid,
                                       const SortOrder & so = Unordered ) const ;

    IndividualList
        speciesUndistributedIndividuals( const MemoryIdentifier & mid,
                                         const SortOrder & so = Unordered ) const ;

    int speciesUndistributeCells( const MemoryIdentifier & smid );

    int speciesUndistributeIndividuals( const MemoryIdentifier & smid );

    // Cell Sapce Interface

    int cellCount() const;

    MIDList cellMIDs( const SortOrder & so = Unordered ) const;

    ConstCellList constCells( const SortOrder & so = Unordered ) const;

    CellList cells( const SortOrder & so = Unordered ) const;

    int emptyCellCount() const;

    MIDList emptyCellMIDs( const SortOrder & so = Unordered ) const;

    ConstCellList emptyConstCells( const SortOrder & so = Unordered ) const;

    CellList emptyCells( const SortOrder & so = Unordered ) const;

    int nonEmptyCellCount() const;

    MIDList nonEmptyCellMIDs( const SortOrder & so = Unordered ) const;

    ConstCellList nonEmptyConstCells( const SortOrder & so = Unordered ) const;

    CellList nonEmptyCells( const SortOrder & so = Unordered ) const;

    MemoryIdentifier createCellMID();

    Cell createCell();

    bool deleteCell(const MemoryIdentifier & mid);

    bool deleteCell( const ConstCell & cell );

    int deleteAllCells();

    int deleteEmptyCells();

    int deleteNonEmptyCells();

    // Cell Reference Interface

    bool cellIsValid( const MemoryIdentifier &) const;

    bool cellHasProperty( const MemoryIdentifier & mid,
                          const QString & name ) const;

    int cellPropertyCount( const MemoryIdentifier & mid) const;

    QStringList cellPropertyNames( const MemoryIdentifier & mid ) const;

    QVariant cellProperty( const MemoryIdentifier & mid,
                           const QString & name,
                           const QVariant & defaultValue =
                           QVariant() ) const;

    int cellIndividualCount( const MemoryIdentifier & mid ) const;

    int cellSpeciesIndividualCount( const MemoryIdentifier & cmid,
                                    const MemoryIdentifier & smid ) const;

    MIDList cellIndividualMIDs( const MemoryIdentifier & mid,
                                const SortOrder & so = Unordered ) const;
    ConstIndividualList cellConstIndividuals( const MemoryIdentifier & mid,
                                              const SortOrder & so = Unordered ) const;

    MIDList cellSpeciesIndividualMIDs( const MemoryIdentifier & cmid,
                                       const MemoryIdentifier & smid,
                                       const SortOrder & so = Unordered) const;

    ConstIndividualList cellSpeciesConstIndividuals( const MemoryIdentifier & cmid,
                                                     const MemoryIdentifier & smid,
                                                     const SortOrder & so = Unordered) const;

    int cellSpeciesCount( const MemoryIdentifier & cmid ) const;

    bool cellHasSpecies( const MemoryIdentifier & cmid,
                         const MemoryIdentifier & smid ) const;

    MIDList cellSpeciesMIDs( const MemoryIdentifier & cmid,
                             const SortOrder & so = Unordered ) const;
    ConstSpeciesList cellConstSpecies( const MemoryIdentifier & cmid,
                                       const SortOrder & so = Unordered ) const;

    int cellSetProperty( const MemoryIdentifier & mid,
                          const QString & name,
                          const QVariant & value );

    IndividualList cellIndividuals( const MemoryIdentifier & mid,
                                    const SortOrder & so = Unordered ) const;

    IndividualList cellSpeciesIndividuals( const MemoryIdentifier & cmid,
                                           const MemoryIdentifier & smid,
                                           const SortOrder & so = Unordered ) const;

    bool cellInsertIndividual( const MemoryIdentifier & cmid,
                               const MemoryIdentifier & smid,
                               const MemoryIdentifier & imid );

    bool cellRemoveIndividual( const MemoryIdentifier & cmid,
                               const MemoryIdentifier & smid,
                               const MemoryIdentifier & imid );

    int cellRemoveIndividuals(const MemoryIdentifier & cmid);

    int cellRemoveSpecies(const MemoryIdentifier & cmid,
                          const MemoryIdentifier & smid );

    int cellRemoveAllSpecies(const MemoryIdentifier & cmid );
};

} // namespace Implementation
} // namespace SRPS

#endif // SPACES_SPACEPACKED_H
