/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spaces_space_packed.h"

#include <QtCore/QVariantHash>
#include <QtCore/QSet>
#include <QtCore/QMutableHashIterator>

#include <algorithm>
#include <functional>

namespace SRPS {
namespace Implementation {

// base data class, manage properties

class PropertyData
{
private:

  QVariantHash properties;

public:

  bool hasProperty( const QString & name ) const
  {
    return properties.contains(name);
  }

  int propertyCount() const
  {
    return properties.count();
  }

  QStringList propertyNames() const
  {
    return properties.keys();
  }

  QVariant property( const QString & name, const QVariant & defaultValue ) const
  {
    return properties.value(name,defaultValue);
  }

  int setProperty( const QString & name, const QVariant & value )
  {
    if ( !value.isValid() )
      properties.remove(name);
    else
      properties.insert(name,value);
    return 0;
  }

  void clear()
  {
    properties.clear();
  }

};

// individual data class

class IndividualData : public PropertyData
{
private:

  Space::MemoryIdentifier species;
  Space::MemoryIdentifier cell;

  bool inCell;
  bool valid;

public:

  IndividualData():
      PropertyData(),
      species(0),
      cell(),
      inCell(false),
      valid(false)
  {}

  IndividualData( const Space::MemoryIdentifier & smid ):
      PropertyData(),
      species(smid),
      cell(),
      inCell(false),
      valid(true)
  {}

  Space::MemoryIdentifier speciesMID() const {
    return species;
  }

  bool isValid() const
  {
    return valid;
  }

  Space::MemoryIdentifier cellMID() const
  {
    return cell;
  }

  bool isDistributed() const
  {
    return inCell;
  }

  void setDistributed( const Space::MemoryIdentifier & cmid )
  {
    inCell = true;
    cell = cmid;
  }

  void setUndistributed()
  {
    inCell = false;
    cell = Space::MemoryIdentifier(0);
  }

};

// species data class

class SpeciesData : public PropertyData
{
private:
public:

  SpeciesData():PropertyData(){}
};

// cell data class

class CellData : public PropertyData
{
private:

  QHash<Space::MemoryIdentifier,Space::MIDList> _table;
  Space::MIDList _species;
  Space::MIDList _individuals;

public:

  CellData():
      PropertyData()
  {}

  bool isEmpty() const
  {
    return _individuals.isEmpty();
  }

  int individualCount() const
  {
    return _individuals.count();
  }

  Space::MIDList individuals() const
  {
    return _individuals;
  }

  int individualCount( const Space::MemoryIdentifier & smid ) const
  {
    if ( !_species.contains(smid) ) return 0;

    return _table[smid].count();
  }

  Space::MIDList individuals( const Space::MemoryIdentifier & smid ) const
  {
    return _table.value(smid);
  }

  int speciesCount() const
  {
    return _species.count();
  }

  Space::MIDList species() const
  {
    return _species;
  }

  bool hasSpecies( const Space::MemoryIdentifier & smid )
  {
    return _table.contains(smid);
  }

  void insertIndividual( const Space::MemoryIdentifier & smid,
                         const Space::MemoryIdentifier & imid )
  {
    if ( !_table.contains(smid) )
      _species.append( smid );
    _individuals.append(imid);
    _table[smid].append(imid);
    Q_ASSERT( _species.count() == _table.count() );
  }

  bool removeIndividual( const Space::MemoryIdentifier & smid,
                         const Space::MemoryIdentifier & imid )
  {
    if ( !_table.contains(smid) ) return false;

    _table[smid].removeAll(imid);
    int c = _individuals.removeAll(imid);
    if ( !_table[smid].count() )
    {
      _table.remove(smid);
      _species.removeAll(smid);
    }
    Q_ASSERT( _species.count() == _table.count() );
    Q_ASSERT( c == 1 || c == 0 );
    return c > 0;
  }

  int removeIndividuals()
  {
    int count = _individuals.count();

    _table.clear();
    _species.clear();
    _individuals.clear();

    return count;
  }

  int removeAllSpecies()
  {
    int count = _species.count();

    _table.clear();
    _species.clear();
    _individuals.clear();

    return count;
  }

  int removeSpecies( const Space::MemoryIdentifier & smid )
  {
    if ( !_table.contains(smid) ) return 0;

    _species.removeAll(smid);
    QSet<Space::MemoryIdentifier> removed_individual_set = _table.take(smid).toSet();
    Space::MIDList copy = _individuals;
    _individuals = Space::MIDList();

    // filter
    Space::MIDList::const_iterator iterator = copy.constBegin();
    Space::MIDList::const_iterator end      = copy.constEnd();
    for( ; iterator != end ; ++iterator )
      if( !removed_individual_set.contains(*iterator) )
        _individuals.append( *iterator );

    return removed_individual_set.count();
  }

  void undistribute()
  {
    _table.clear();
    _species = Space::MIDList();
    _individuals = Space::MIDList();
  }
};

typedef QHash<Space::MemoryIdentifier,IndividualData> IndividualContainer;
typedef QMutableHashIterator<Space::MemoryIdentifier,IndividualData> MutableIndividualContainerIterator;

typedef QHash<Space::MemoryIdentifier,SpeciesData> SpeciesContainer;

typedef QHash<Space::MemoryIdentifier,CellData> CellContainer;
typedef QMutableHashIterator<Space::MemoryIdentifier,CellData> MutableCellContainerIterator;

class Implementation::SpacePacked::Private
{
  public:
    Private():counter(0){}

    Space::MemoryIdentifier counter;

    PropertyData properties;

    IndividualContainer individuals;
//    QHash<Space::MemoryIdentifier,IndividualData> individuals;

    SpeciesContainer species;

    CellContainer cells;
};

SpacePacked::SpacePacked()
    : SRPS::Space(),
    p ( new Private )
{
}

SpacePacked::~SpacePacked()
{
    delete p;
}

const QString SpacePacked::StaticClassName = QLatin1String("srps.space.packed");

QString SpacePacked::SpacePacked::componentClassName() const
{
  return StaticClassName;
}

bool SpacePacked::componentHasProperty( const QString & name ) const
{
  return p->properties.hasProperty(name);
}

int SpacePacked::componentPropertyCount() const
{
  return p->properties.propertyCount();
}

QStringList SpacePacked::componentPropertyNames() const
{
  return p->properties.propertyNames();
}

QVariant SpacePacked::componentProperty( const QString & name,
                            const QVariant & defaultValue ) const
{
  return p->properties.property(name,defaultValue);
}

int SpacePacked::componentSetProperty( const QString & name,
                          const QVariant & value )
{
  return p->properties.setProperty(name,value);
}

SRPS::Space * SpacePacked::clone() const
{
  SpacePacked * cloned = new SpacePacked();

  cloned->p->counter = p->counter;
  cloned->p->properties = p->properties;
  cloned->p->individuals = p->individuals;
  cloned->p->species = p->species;
  cloned->p->cells = p->cells;

  return cloned;
}

SRPS::Space * SpacePacked::cloneUndistributed() const
{
  SpacePacked * cloned = new SpacePacked();

  cloned->p->counter = p->counter;
  cloned->p->properties = p->properties;
  cloned->p->individuals = p->individuals;
  cloned->p->species = p->species;
  cloned->p->cells = p->cells;

  cloned->undistribute();

  return cloned;
}

void SpacePacked::undistribute()
{
  // individuals
  {
    QHash<Space::MemoryIdentifier,IndividualData>::iterator iterator = p->individuals.begin();
    QHash<Space::MemoryIdentifier,IndividualData>::iterator end      = p->individuals.end();
    for( ; iterator != end; ++iterator )
        iterator->setUndistributed();
  }

  // celss
  {
    CellContainer::iterator iterator = p->cells.begin();
    CellContainer::iterator end      = p->cells.end();
    for( ; iterator != end; ++iterator )
        iterator->undistribute();
  }
}

void SpacePacked::clear()
{
  p->properties.clear();
  p->individuals.clear();
  p->species.clear();
  p->cells.clear();
  p->counter = 0;
}

// Individual Space interface

int SpacePacked::individualCount() const
{
  return p->individuals.count();
}

SpacePacked::MIDList SpacePacked::individualMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedList(p->individuals,MILess<Space::MemoryIdentifier>());
  else if ( so == InverseCreationOrder )
    return sortedList(p->individuals,MIGreater<Space::MemoryIdentifier>());
  else
    return p->individuals.keys();
}

SpacePacked::ConstIndividualList
    SpacePacked::constIndividuals( const SpacePacked::SortOrder & so ) const
{

  if ( so == CreationOrder )
    return sortedTransformedList(p->individuals,
                                RLess<ConstIndividual>(),
                                Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));

  else if ( so == InverseCreationOrder )
    return sortedTransformedList(p->individuals,
                                RGreater<ConstIndividual>(),
                                Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));

  else
    return transformedList(p->individuals,
                          Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));

}

SpacePacked::IndividualList
    SpacePacked::individuals( const SpacePacked::SortOrder & so ) const
{

  if ( so == CreationOrder )
    return sortedTransformedList(p->individuals,
                                RLess<Individual>(),
                                Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));

  else if ( so == InverseCreationOrder )
    return sortedTransformedList(p->individuals,
                                RGreater<ConstIndividual>(),
                                Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));

  else
    return transformedList(p->individuals,
                          Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));

}

struct FilterDistributedIndividual
{
  bool operator()( const IndividualData & data ) const
  {
    return data.isDistributed();
  }
};

int SpacePacked::distributedIndividualCount() const
{
  return countIf(p->individuals,FilterDistributedIndividual());
}

SpacePacked::MIDList SpacePacked::distributedIndividualMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedFilteredList(p->individuals,MILess<MemoryIdentifier>(),FilterDistributedIndividual());

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->individuals,MIGreater<MemoryIdentifier>(),FilterDistributedIndividual());

  else
    return filteredList(p->individuals,FilterDistributedIndividual());
}

SpacePacked::ConstIndividualList
    SpacePacked::constDistributedIndividuals( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterDistributedIndividual());

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterDistributedIndividual());

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                   FilterDistributedIndividual());
}

SpacePacked::IndividualList
    SpacePacked::distributedIndividuals( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterDistributedIndividual());

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterDistributedIndividual());

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                   FilterDistributedIndividual());
}

struct FilterUndistributedIndividual
{
  bool operator()( const IndividualData & data ) const
  {
    return !data.isDistributed();
  }
};

int SpacePacked::undistributedIndividualCount() const
{
  return countIf(p->individuals,FilterUndistributedIndividual());
}

SpacePacked::MIDList SpacePacked::undistributedIndividualMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedFilteredList(p->individuals,MILess<MemoryIdentifier>(),FilterUndistributedIndividual());

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->individuals,MIGreater<MemoryIdentifier>(),FilterUndistributedIndividual());

  else
    return filteredList(p->individuals,FilterUndistributedIndividual());
}

SpacePacked::ConstIndividualList
    SpacePacked::constUndistributedIndividuals( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterUndistributedIndividual());

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterUndistributedIndividual());

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                   FilterUndistributedIndividual());
}

SpacePacked::IndividualList
    SpacePacked::undistributedIndividuals( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterUndistributedIndividual());

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterUndistributedIndividual());

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                   FilterUndistributedIndividual());
}

// Invidual Reference Interface

bool SpacePacked::individualIsValid( const SpacePacked::MemoryIdentifier & imid) const
{
  return p->individuals.contains(imid);
}

bool SpacePacked::individualHasProperty( const SpacePacked::MemoryIdentifier & mid,
                                         const QString & name ) const
{
  if ( !individualIsValid(mid) ) return false;

  return p->individuals[mid].hasProperty(name);
}

int SpacePacked::individualPropertyCount( const SpacePacked::MemoryIdentifier & mid) const
{
  if ( !individualIsValid(mid) ) return 0;

  return p->individuals[mid].propertyCount();
}

QStringList SpacePacked::individualPropertyNames( const SpacePacked::MemoryIdentifier & mid ) const
{
  if ( !individualIsValid(mid) ) return QStringList();

  return p->individuals[mid].propertyNames();
}

QVariant SpacePacked::individualProperty( const SpacePacked::MemoryIdentifier & mid,
                                          const QString & name,
                                          const QVariant & defaultValue ) const
{
  if ( !individualIsValid(mid) ) return defaultValue;

  return p->individuals[mid].property(name,defaultValue);
}

SpacePacked::MemoryIdentifier
    SpacePacked::individualSpeciesMID( const SpacePacked::MemoryIdentifier & mid ) const
{
  if ( !individualIsValid(mid) ) return MemoryIdentifier(0);

  return p->individuals[mid].speciesMID();
}

SpacePacked::ConstSpecies SpacePacked::individualConstSpecies( const SpacePacked::MemoryIdentifier & mid ) const
{
  return toConstSpecies(individualSpeciesMID(mid));
}

SpacePacked::MemoryIdentifier SpacePacked::individualCellMID( const SpacePacked::MemoryIdentifier & mid ) const
{
  if ( !individualIsValid(mid) ) return MemoryIdentifier(0);

  return p->individuals[mid].cellMID();
}

SpacePacked::ConstCell SpacePacked::individualConstCell( const SpacePacked::MemoryIdentifier & mid ) const
{
  return toConstCell(individualCellMID(mid));
}

bool SpacePacked::individualIsDistributed(const MemoryIdentifier & mid) const
{
  if ( !individualIsValid(mid) ) return false;

  return p->individuals[mid].isDistributed();
}

int SpacePacked::individualSetProperty( const SpacePacked::MemoryIdentifier & mid,
                                        const QString & name,
                                        const QVariant & value )
{
  if ( !individualIsValid(mid) ) return -1;

  return !p->individuals[mid].setProperty(name,value);
}

// Species Space Interface

int SpacePacked::speciesCount() const
{
  return p->species.count();
}

SpacePacked::MIDList SpacePacked::speciesMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedList(p->species,MILess<MemoryIdentifier>());

  else if ( so == InverseCreationOrder )
    return sortedList(p->species,MIGreater<MemoryIdentifier>());

  else
    return p->species.keys();
}

SpacePacked::ConstSpeciesList SpacePacked::constSpecies( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedList(p->species,
                                RLess<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else if( so == InverseCreationOrder )
    return sortedTransformedList(p->species,
                                RGreater<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else
    return transformedList(p->species,Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));
}

SpacePacked::SpeciesList SpacePacked::species( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedList(p->species,
                                RLess<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else if( so == InverseCreationOrder )
    return sortedTransformedList(p->species,
                                RGreater<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else
    return transformedList(p->species,Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));
}

static inline QHash<Space::MemoryIdentifier,Space::MemoryIdentifier>
    filter_distributed_species( const IndividualContainer & individuals )
{
  // MemoryIdentifier -> dummy
  QHash<Space::MemoryIdentifier,Space::MemoryIdentifier> distributed_species;

  IndividualContainer::const_iterator iterator = individuals.constBegin();
  IndividualContainer::const_iterator end      = individuals.constEnd();

  for( ; iterator != end; ++iterator )
  {
    if ( iterator.value().isDistributed() )
      distributed_species.insert(iterator.value().speciesMID(),0);
  }

  return distributed_species;
}

int SpacePacked::distributedSpeciesCount() const
{
  return filter_distributed_species(p->individuals).count();
}

SpacePacked::MIDList SpacePacked::distributedSpeciesMIDs( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier -> dummy
  QHash<MemoryIdentifier,MemoryIdentifier> distributed_species
      = filter_distributed_species(p->individuals);

  if ( so == CreationOrder )
    return sortedList(distributed_species,MILess<MemoryIdentifier>());

  else if( so == InverseCreationOrder )
    return sortedList(distributed_species,MIGreater<MemoryIdentifier>());

  else
    return distributed_species.keys();
}

SpacePacked::ConstSpeciesList
    SpacePacked::constDistributedSpecies( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier -> dummy
  QHash<MemoryIdentifier,MemoryIdentifier> distributed_species
      = filter_distributed_species(p->individuals);

  if ( so == CreationOrder )
    return sortedTransformedList(distributed_species,
                                RLess<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else if( so == InverseCreationOrder )
    return sortedTransformedList(distributed_species,
                                RGreater<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else
    return transformedList(distributed_species,
                          Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));
}

SpacePacked::SpeciesList SpacePacked::distributedSpecies( const SortOrder & so ) const
{
  // MemoryIdentifier -> dummy
  QHash<MemoryIdentifier,MemoryIdentifier> distributed_species
      = filter_distributed_species(p->individuals);

  if ( so == CreationOrder )
    return sortedTransformedList(distributed_species,
                                RLess<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else if( so == InverseCreationOrder )
    return sortedTransformedList(distributed_species,
                                RGreater<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else
    return transformedList(distributed_species,
                          Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));
}

static inline QHash<Space::MemoryIdentifier,Space::MemoryIdentifier>
    filter_undistributed_species( const IndividualContainer & individuals )
{
  // MemoryIdentifier -> dummy
  QHash<Space::MemoryIdentifier,Space::MemoryIdentifier> undistributed_species;

  IndividualContainer::const_iterator iterator = individuals.constBegin();
  IndividualContainer::const_iterator end      = individuals.constEnd();

  for( ; iterator != end; ++iterator )
  {
    if ( !iterator.value().isDistributed() )
      undistributed_species.insert(iterator.value().speciesMID(),0);
  }

  return undistributed_species;
}

int SpacePacked::undistributedSpeciesCount() const
{
  return filter_undistributed_species(p->individuals).count();
}

SpacePacked::MIDList SpacePacked::undistributedSpeciesMIDs( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier -> dummy
  QHash<MemoryIdentifier,MemoryIdentifier> undistributed_species
      = filter_undistributed_species(p->individuals);

  if ( so == CreationOrder )
    return sortedList(undistributed_species,MILess<MemoryIdentifier>());

  else if( so == InverseCreationOrder )
    return sortedList(undistributed_species,MIGreater<MemoryIdentifier>());

  else
    return undistributed_species.keys();
}

SpacePacked::ConstSpeciesList
    SpacePacked::constUndistributedSpecies( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier -> dummy
  QHash<MemoryIdentifier,MemoryIdentifier> undistributed_species
      = filter_undistributed_species(p->individuals);

  if ( so == CreationOrder )
    return sortedTransformedList(undistributed_species,
                                RLess<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else if( so == InverseCreationOrder )
    return sortedTransformedList(undistributed_species,
                                RGreater<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else
    return transformedList(undistributed_species,
                          Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));
}

SpacePacked::SpeciesList SpacePacked::undistributedSpecies( const SortOrder & so ) const
{
  // MemoryIdentifier -> dummy
  QHash<MemoryIdentifier,MemoryIdentifier> undistributed_species
      = filter_undistributed_species(p->individuals);

  if ( so == CreationOrder )
    return sortedTransformedList(undistributed_species,
                                RLess<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else if( so == InverseCreationOrder )
    return sortedTransformedList(undistributed_species,
                                RGreater<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else
    return transformedList(undistributed_species,
                          Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));
}

template <typename K, typename V>
QSet<K> hashSet( const QHash<K,V> & hash )
{
  QSet<K> set;

  typename QHash<K,V>::const_iterator i = hash.constBegin();
  typename QHash<K,V>::const_iterator e = hash.constEnd();
  for( ; i != e; ++i )
    set.insert( i.key() );

  return set;
}

static QSet<Space::MemoryIdentifier>
    filter_empty_species_set( QSet<Space::MemoryIdentifier> & species,
                              const IndividualContainer & individuals )
{
  IndividualContainer::const_iterator i = individuals.constBegin();
  IndividualContainer::const_iterator e = individuals.constEnd();
  for( ; i != e; ++i )
  {
    species.remove(i.value().speciesMID() );
  }

  return species;
}

int SpacePacked::emptySpeciesCount() const
{
  // MemoryIdentifier
  QSet<MemoryIdentifier> work = hashSet(p->species);
  return filter_empty_species_set(work,p->individuals).count();
}

SpacePacked::MIDList
    SpacePacked::emptySpeciesMIDs( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier
  QSet<MemoryIdentifier> work = hashSet(p->species);
  work = filter_empty_species_set(work,p->individuals);

  if ( so == CreationOrder )
    return sortedList(work,MILess<MemoryIdentifier>());

  else if ( so == InverseCreationOrder )
    return sortedList(work,MIGreater<MemoryIdentifier>());

  else
    return work.toList();
}

SpacePacked::ConstSpeciesList
    SpacePacked::constEmptySpecies( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier
  QSet<MemoryIdentifier> work = hashSet(p->species);
  work = filter_empty_species_set(work,p->individuals);

  if ( so == CreationOrder )
    return sortedTransformedList(work,
                                RLess<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else if ( so == InverseCreationOrder )
    return sortedTransformedList(work,
                                RGreater<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else
    return transformedList(work,
                           Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));
}

SpacePacked::SpeciesList
    SpacePacked::emptySpecies( const SpacePacked::SortOrder & so ) const
{
  // MemoryIdentifier
  QSet<MemoryIdentifier> work = hashSet(p->species);
  work = filter_empty_species_set(work,p->individuals);

  if ( so == CreationOrder )
    return sortedTransformedList(work,
                                RLess<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else if ( so == InverseCreationOrder )
    return sortedTransformedList(work,
                                RGreater<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else
    return transformedList(work,
                           Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));
}

static QSet<Space::MemoryIdentifier>
    filter_nonempty_species_set( const IndividualContainer & individuals )
{
  QSet<Space::MemoryIdentifier> set;

  IndividualContainer::const_iterator i = individuals.constBegin();
  IndividualContainer::const_iterator e = individuals.constEnd();
  for( ; i != e; ++i )
  {
    set.insert(i.value().speciesMID() );
  }

  return set;
}

int SpacePacked::nonEmptySpeciesCount() const
{
  return filter_nonempty_species_set(p->individuals).count();
}

SpacePacked::MIDList SpacePacked::nonEmptySpeciesMIDs( const SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedList(filter_nonempty_species_set(p->individuals),
                      MILess<MemoryIdentifier>());

  else if ( so == InverseCreationOrder  )
    return sortedList(filter_nonempty_species_set(p->individuals),
                      MIGreater<MemoryIdentifier>());

  else
    return filter_nonempty_species_set(p->individuals).toList();
}

SpacePacked::ConstSpeciesList
    SpacePacked::constNonEmptySpecies( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedList(filter_nonempty_species_set(p->individuals),
                                RLess<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else if ( so == InverseCreationOrder  )
    return sortedTransformedList(filter_nonempty_species_set(p->individuals),
                                RGreater<ConstSpecies>(),
                                Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  else
    return transformedList(filter_nonempty_species_set(p->individuals),
                          Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));
}

SpacePacked::SpeciesList
    SpacePacked::nonEmptySpecies( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedList(filter_nonempty_species_set(p->individuals),
                                RLess<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else if ( so == InverseCreationOrder  )
    return sortedTransformedList(filter_nonempty_species_set(p->individuals),
                                RGreater<Species>(),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));

  else
    return transformedList(filter_nonempty_species_set(p->individuals),
                                Transformer<Species,MemoryIdentifier>(this,&SpacePacked::toSpecies));
}

SpacePacked::MemoryIdentifier SpacePacked::createSpeciesMID()
{
  MemoryIdentifier smid = ++(p->counter);

  SpeciesData sd;
  p->species.insert(smid,sd);
  return smid;
}


SpacePacked::Species SpacePacked::createSpecies()
{
  return toSpecies(createSpeciesMID());
}

bool SpacePacked::deleteSpecies( const SpacePacked::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  MutableIndividualContainerIterator iiterator(p->individuals);

  while( iiterator.hasNext() )
  {
    iiterator.next();
    if( iiterator.value().speciesMID() == smid )
      iiterator.remove();
  }

  MutableCellContainerIterator citerator(p->cells);

  while( citerator.hasNext() )
  {
    citerator.next();
    citerator.value().removeSpecies(smid);
  }

  p->species.remove(smid);

  return true;
}


bool SpacePacked::deleteSpecies( const SpacePacked::ConstSpecies & species )
{
  if (!isReferenceFromThis(species)) return false;


  return deleteSpecies(species.mid());
}

static void cell_undistribute( CellData & cell )
{
  cell.undistribute();
}

int SpacePacked::deleteAllSpecies()
{
  p->individuals.clear();

  int count = p->species.count();
  p->species.clear();

  std::for_each( p->cells.begin(),p->cells.end(), cell_undistribute );

  return count;

}

int SpacePacked::deleteDistributedSpecies()
{
  int count = 0;

  foreach( MemoryIdentifier smid, distributedSpeciesMIDs() )
    if ( deleteSpecies(smid) )
      ++count;

  return count;
}


int SpacePacked::deleteUndistributedSpecies()
{
  int count = 0;

  foreach( MemoryIdentifier smid, undistributedSpeciesMIDs() )
    if ( deleteSpecies(smid) )
      ++count;

  return count;
}

int SpacePacked::deleteEmptySpecies()
{
  int count = 0;

  foreach( MemoryIdentifier smid, emptySpeciesMIDs() )
    if ( deleteSpecies(smid) )
      ++count;

  return count;
}

int SpacePacked::deleteNonEmptySpecies()
{
  int count = 0;

  foreach( MemoryIdentifier smid, nonEmptySpeciesMIDs() )
    if ( deleteSpecies(smid) )
      ++count;

  return count;
}

// Species Reference Interface

bool SpacePacked::speciesIsValid( const SpacePacked::MemoryIdentifier & smid ) const
{
  return p->species.contains(smid);
}

bool SpacePacked::speciesHasProperty( const SpacePacked::MemoryIdentifier & smid,
                                      const QString & name ) const
{
  if ( !speciesIsValid(smid) ) return false;

  return p->species[smid].hasProperty(name);
}

int SpacePacked::speciesPropertyCount( const SpacePacked::MemoryIdentifier & smid) const
{
  if ( !speciesIsValid(smid) ) return 0;

  return p->species[smid].propertyCount();
}

QStringList SpacePacked::speciesPropertyNames( const SpacePacked::MemoryIdentifier & smid ) const
{
  if ( !speciesIsValid(smid ) ) return QStringList();

  return p->species[smid].propertyNames();
}

QVariant SpacePacked::speciesProperty( const SpacePacked::MemoryIdentifier & smid,
                                       const QString & name,
                                       const QVariant & defaultValue ) const
{
  if ( !speciesIsValid(smid) ) return defaultValue;

  return p->species[smid].property(name,defaultValue);
}

struct FilterSpeciesIndividual
{
  Space::MemoryIdentifier smid;
  FilterSpeciesIndividual(const Space::MemoryIdentifier & m ):smid(m){}

  bool operator()( const IndividualData & id ) const
  {
    return id.speciesMID() == smid;
  }
};

int SpacePacked::speciesIndividualCount(const SpacePacked::MemoryIdentifier & smid) const
{
  if ( !speciesIsValid(smid ) ) return 0;

  return countIf(p->individuals,FilterSpeciesIndividual(smid));
}

SpacePacked::MIDList
    SpacePacked::speciesIndividualMIDs(
        const SpacePacked::MemoryIdentifier & mid,
        const SpacePacked::SortOrder & so  ) const
{
  if (!speciesIsValid(mid)) return MIDList();

  if ( so == CreationOrder )
    return sortedFilteredList(p->individuals,
                              MILess<MemoryIdentifier>(),
                              FilterSpeciesIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->individuals,
                              MIGreater<MemoryIdentifier>(),
                              FilterSpeciesIndividual(mid));

  else
    return filteredList(p->individuals,
                        FilterSpeciesIndividual(mid));
}

SpacePacked::ConstIndividualList
    SpacePacked::speciesConstIndividuals( const SpacePacked::MemoryIdentifier & mid,
                                          const SpacePacked::SortOrder & so ) const
{
  if (!speciesIsValid(mid)) return ConstIndividualList();

  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterSpeciesIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterSpeciesIndividual(mid));

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                   FilterSpeciesIndividual(mid));
}

SpacePacked::IndividualList
    SpacePacked::speciesIndividuals( const SpacePacked::MemoryIdentifier & mid,
                                          const SpacePacked::SortOrder & so ) const
{
  if (!speciesIsValid(mid)) return IndividualList();

  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterSpeciesIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterSpeciesIndividual(mid));

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                   FilterSpeciesIndividual(mid));
}

struct FilterSpeciesDistributedIndividual
{
  Space::MemoryIdentifier smid;
  FilterSpeciesDistributedIndividual(const Space::MemoryIdentifier & m ):smid(m){}

  bool operator()( const IndividualData & id ) const
  {
    return id.speciesMID() == smid && id.isDistributed();
  }
};

int SpacePacked::speciesDistributedIndividualCount( const SpacePacked::MemoryIdentifier & mid ) const
{
  if (!speciesIsValid(mid)) return 0;

  return countIf(p->individuals,FilterSpeciesDistributedIndividual(mid));
}

SpacePacked::MIDList
    SpacePacked::speciesDistributedIndividualMIDs(
        const SpacePacked::MemoryIdentifier & mid,
        const SpacePacked::SortOrder & so  ) const
{
  if (!speciesIsValid(mid)) return MIDList();

  if ( so == CreationOrder )
    return sortedFilteredList(p->individuals,
                              MILess<MemoryIdentifier>(),
                              FilterSpeciesDistributedIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->individuals,
                              MIGreater<MemoryIdentifier>(),
                              FilterSpeciesDistributedIndividual(mid));

  else
    return filteredList(p->individuals,
                        FilterSpeciesDistributedIndividual(mid));
}

SpacePacked::ConstIndividualList
    SpacePacked::speciesDistributedConstIndividuals( const SpacePacked::MemoryIdentifier & mid,
                                          const SpacePacked::SortOrder & so ) const
{
  if (!speciesIsValid(mid)) return ConstIndividualList();

  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterSpeciesDistributedIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterSpeciesDistributedIndividual(mid));

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                   FilterSpeciesDistributedIndividual(mid));
}

struct FilterSpeciesUndistributedIndividual
{
  Space::MemoryIdentifier smid;
  FilterSpeciesUndistributedIndividual(const Space::MemoryIdentifier & m ):smid(m){}

  bool operator()( const IndividualData & id ) const
  {
    return id.speciesMID() == smid && !id.isDistributed();
  }
};

int SpacePacked::speciesUndistributedIndividualCount( const SpacePacked::MemoryIdentifier & mid ) const
{
  if (!speciesIsValid(mid)) return 0;

  return countIf(p->individuals,FilterSpeciesUndistributedIndividual(mid));
}

SpacePacked::MIDList
    SpacePacked::speciesUndistributedIndividualMIDs(
        const SpacePacked::MemoryIdentifier & mid,
        const SpacePacked::SortOrder & so  ) const
{
  if (!speciesIsValid(mid)) return MIDList();

  if ( so == CreationOrder )
    return sortedFilteredList(p->individuals,
                              MILess<MemoryIdentifier>(),
                              FilterSpeciesUndistributedIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->individuals,
                              MIGreater<MemoryIdentifier>(),
                              FilterSpeciesUndistributedIndividual(mid));

  else
    return filteredList(p->individuals,
                        FilterSpeciesUndistributedIndividual(mid));
}

SpacePacked::ConstIndividualList
    SpacePacked::speciesUndistributedConstIndividuals( const SpacePacked::MemoryIdentifier & mid,
                                          const SpacePacked::SortOrder & so ) const
{
  if (!speciesIsValid(mid)) return ConstIndividualList();

  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterSpeciesUndistributedIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<ConstIndividual>(),
                                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                         FilterSpeciesUndistributedIndividual(mid));

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual),
                                   FilterSpeciesUndistributedIndividual(mid));
}



bool SpacePacked::speciesIsDistributed(const SpacePacked::MemoryIdentifier & mid) const
{
  if (!speciesIsValid(mid)) return false;

  return std::find_if(p->individuals.constBegin(),
                      p->individuals.constEnd(),
                      FilterSpeciesDistributedIndividual(mid)) != p->individuals.constEnd();
}

bool SpacePacked::speciesIsUndistributed(const SpacePacked::MemoryIdentifier & mid) const
{
  if (!speciesIsValid(mid)) return false;

  return std::find_if(p->individuals.constBegin(),
                      p->individuals.constEnd(),
                      FilterSpeciesDistributedIndividual(mid)) == p->individuals.constEnd();
}

static QSet<Space::MemoryIdentifier> filter_species_cells( const IndividualContainer & ic,
                                                           const Space::MemoryIdentifier & smid )
{
  QSet<Space::MemoryIdentifier> result;

  IndividualContainer::const_iterator i = ic.constBegin();
  IndividualContainer::const_iterator e = ic.constEnd();
  for( ; i != e; ++i )
    if( i.value().speciesMID() == smid && i.value().isDistributed() )
      result.insert( i.value().cellMID() );

  return result;
}

int SpacePacked::speciesDistributedCellCount( const SpacePacked::MemoryIdentifier & mid ) const
{
  if ( !speciesIsValid(mid) ) return 0;

  return filter_species_cells(p->individuals,mid).count();
}

SpacePacked::MIDList
    SpacePacked::speciesDistributedCellMIDs( const SpacePacked::MemoryIdentifier & mid,
                                             const SpacePacked::SortOrder & so ) const
{
  if ( !speciesIsValid(mid) ) return MIDList();

  if ( so == CreationOrder )
    return sortedList(filter_species_cells(p->individuals,mid),
                      MILess<MemoryIdentifier>());

  else if ( so == InverseCreationOrder )
    return sortedList(filter_species_cells(p->individuals,mid),
                      MIGreater<MemoryIdentifier>());

  else
    return filter_species_cells(p->individuals,mid).toList();
}

SpacePacked::ConstCellList
    SpacePacked::speciesDistributedConstCells(
        const SpacePacked::MemoryIdentifier & mid,
        const SpacePacked::SortOrder & so ) const
{
  if ( !speciesIsValid(mid) ) return ConstCellList();

  if ( so == CreationOrder )
    return sortedTransformedList(filter_species_cells(p->individuals,mid),
                                 RLess<ConstCell>(),
                                 Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell));

  else if ( so == InverseCreationOrder )
    return sortedTransformedList(filter_species_cells(p->individuals,mid),
                                 RGreater<ConstCell>(),
                                 Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell));

  else
    return transformedList(filter_species_cells(p->individuals,mid),
                           Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell));
}

int SpacePacked::speciesSetProperty( const SpacePacked::MemoryIdentifier & mid,
                                      const QString & name,
                                      const QVariant & value )
{
  if (!speciesIsValid(mid)) return -1;

  return p->species[mid].setProperty(name,value);
}

SpacePacked::MemoryIdentifier
    SpacePacked::speciesCreateIndividualMID(
        const SpacePacked::MemoryIdentifier & smid )
{
  if (!speciesIsValid(smid)) return MemoryIdentifier();

  MemoryIdentifier imid = ++(p->counter);

  IndividualData id(smid);

  p->individuals.insert(imid,id);

  return imid;
}

SpacePacked::Individual
    SpacePacked::speciesCreateIndividual(
        const SpacePacked::MemoryIdentifier & smid )
{
  return toIndividual(speciesCreateIndividualMID(smid));
}

bool SpacePacked::speciesDeleteIndividual(
    const SpacePacked::MemoryIdentifier & smid,
    const SpacePacked::MemoryIdentifier & imid )
{
  if ( !speciesIsValid(smid) ) return false;

  if ( !individualIsValid(imid) ) return false;

  if ( p->individuals[imid].speciesMID() != smid ) return false;

  IndividualData id = p->individuals.take(imid);
  if ( id.isDistributed() && cellIsValid(id.cellMID()) )
  {
    p->cells[id.cellMID()].removeIndividual(smid,imid);
  }

  return true;
}

int SpacePacked::speciesDeleteIndividuals( const SpacePacked::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  MutableIndividualContainerIterator iterator(p->individuals);

  int count = 0;

  while(iterator.hasNext())
  {
    iterator.next();
    IndividualData id = iterator.value();
    if ( id.speciesMID() == smid )
    {
      ++count;
      iterator.remove();
      if ( id.isDistributed() && cellIsValid(id.cellMID()) )
      {
        p->cells[id.cellMID()].removeIndividual(smid,iterator.key());
      }
    }
  }

  return count;
}

int SpacePacked::speciesDeleteDistributedIndividuals( const SpacePacked::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  MutableIndividualContainerIterator iterator(p->individuals);

  int count = 0;

  while(iterator.hasNext())
  {
    iterator.next();
    IndividualData id = iterator.value();
    if ( id.speciesMID() == smid && id.isDistributed() )
    {
      ++count;
      iterator.remove();
      if ( cellIsValid(id.cellMID()) )
      {
        p->cells[id.cellMID()].removeIndividual(smid,iterator.key());
      }
    }
  }

  return count;
}

int SpacePacked::speciesDeleteUndistributedIndividuals( const SpacePacked::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  MutableIndividualContainerIterator iterator(p->individuals);

  int count = 0;

  while(iterator.hasNext())
  {
    iterator.next();
    IndividualData id = iterator.value();
    if ( id.speciesMID() == smid && !id.isDistributed() )
    {
      ++count;
      iterator.remove();
    }
  }

  return count;
}

SpacePacked::IndividualList
    SpacePacked::speciesDistributedIndividuals( const SpacePacked::MemoryIdentifier & mid,
                                          const SpacePacked::SortOrder & so ) const
{
  if (!speciesIsValid(mid)) return IndividualList();

  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterSpeciesDistributedIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterSpeciesDistributedIndividual(mid));

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                   FilterSpeciesDistributedIndividual(mid));
}

SpacePacked::IndividualList
    SpacePacked::speciesUndistributedIndividuals( const SpacePacked::MemoryIdentifier & mid,
                                          const SpacePacked::SortOrder & so ) const
{
  if (!speciesIsValid(mid)) return IndividualList();

  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RLess<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterSpeciesUndistributedIndividual(mid));

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->individuals,
                                         RGreater<Individual>(),
                                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                         FilterSpeciesUndistributedIndividual(mid));

  else
    return transformedFilteredList(p->individuals,
                                   Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual),
                                   FilterSpeciesUndistributedIndividual(mid));
}

int SpacePacked::speciesUndistributeCells( const SpacePacked::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  QSet<MemoryIdentifier> set;

  IndividualContainer::iterator i = p->individuals.begin();
  IndividualContainer::iterator e = p->individuals.end();
  for( ; i != e; ++i )
  {
    if ( i->speciesMID() == smid && i->isDistributed() )
    {
      if ( cellIsValid(i->cellMID()) )
      {
        set.insert(i->cellMID());
        p->cells[i->cellMID()].removeIndividual(smid,i.key());
      }
      i->setUndistributed();
    }
  }

  return set.count();
}

int SpacePacked::speciesUndistributeIndividuals( const SpacePacked::MemoryIdentifier & smid )
{
  if ( !speciesIsValid(smid) ) return false;

  int count = 0;

  IndividualContainer::iterator i = p->individuals.begin();
  IndividualContainer::iterator e = p->individuals.end();
  for( ; i != e; ++i )
  {
    if ( i->speciesMID() == smid && i->isDistributed() )
    {
      if ( cellIsValid(i->cellMID()) )
      {
        p->cells[i->cellMID()].removeIndividual(smid,i.key());
        ++count;
      }
      i->setUndistributed();
    }
  }

  return count;
}

// Cell Sapce Interface

int SpacePacked::cellCount() const
{
  return p->cells.count();
}

SpacePacked::MIDList SpacePacked::cellMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedList(p->cells,
                      MILess<MemoryIdentifier>());

  else if ( so == InverseCreationOrder )
    return sortedList(p->cells,
                      MIGreater<MemoryIdentifier>());

  else
    return p->cells.keys();
}

SpacePacked::ConstCellList
    SpacePacked::constCells( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedList(p->cells,
                                 RLess<ConstCell>(),
                                 Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell));

  if ( so == InverseCreationOrder )
    return sortedTransformedList(p->cells,
                                 RGreater<ConstCell>(),
                                 Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell));

  else
    return transformedList(p->cells,
                           Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell));
}

SpacePacked::CellList
    SpacePacked::cells( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedList(p->cells,
                                 RLess<Cell>(),
                                 Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell));

  if ( so == InverseCreationOrder )
    return sortedTransformedList(p->cells,
                                 RGreater<Cell>(),
                                 Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell));

  else
    return transformedList(p->cells,
                           Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell));
}

static bool FilterEmptyCell( const CellData & cell )
{
  return cell.isEmpty();
}

int SpacePacked::emptyCellCount() const
{
  return countIf(p->cells,FilterEmptyCell);
}

SpacePacked::MIDList SpacePacked::emptyCellMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedFilteredList(p->cells,
                            MILess<MemoryIdentifier>(),
                            FilterEmptyCell);

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->cells,
                            MIGreater<MemoryIdentifier>(),
                            FilterEmptyCell);

  else
    return filteredList(p->cells,
                        FilterEmptyCell);
}

SpacePacked::ConstCellList SpacePacked::emptyConstCells( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RLess<ConstCell>(),
                            Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell),
                            FilterEmptyCell);

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RGreater<ConstCell>(),
                            Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell),
                            FilterEmptyCell);

  else
    return transformedFilteredList(p->cells,
                                   Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell),
                                   FilterEmptyCell);
}

SpacePacked::CellList SpacePacked::emptyCells( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RLess<Cell>(),
                            Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell),
                            FilterEmptyCell);

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RGreater<Cell>(),
                            Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell),
                            FilterEmptyCell);

  else
    return transformedFilteredList(p->cells,
                                   Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell),
                                   FilterEmptyCell);
}

static bool FilterNonEmptyCell( const CellData & cell )
{
  return !cell.isEmpty();
}

int SpacePacked::nonEmptyCellCount() const
{
  return countIf(p->cells,FilterNonEmptyCell);
}

SpacePacked::MIDList SpacePacked::nonEmptyCellMIDs( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedFilteredList(p->cells,
                            MILess<MemoryIdentifier>(),
                            FilterNonEmptyCell);

  else if ( so == InverseCreationOrder )
    return sortedFilteredList(p->cells,
                            MIGreater<MemoryIdentifier>(),
                            FilterNonEmptyCell);

  else
    return filteredList(p->cells,
                        FilterNonEmptyCell);
}

SpacePacked::ConstCellList SpacePacked::nonEmptyConstCells( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RLess<ConstCell>(),
                            Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell),
                            FilterNonEmptyCell);

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RGreater<ConstCell>(),
                            Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell),
                            FilterNonEmptyCell);

  else
    return transformedFilteredList(p->cells,
                                   Transformer<ConstCell,MemoryIdentifier>(this,&SpacePacked::toConstCell),
                                   FilterNonEmptyCell);
}

SpacePacked::CellList SpacePacked::nonEmptyCells( const SpacePacked::SortOrder & so ) const
{
  if ( so == CreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RLess<Cell>(),
                            Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell),
                            FilterNonEmptyCell);

  else if ( so == InverseCreationOrder )
    return sortedTransformedFilteredList(p->cells,
                            RGreater<Cell>(),
                            Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell),
                            FilterNonEmptyCell);

  else
    return transformedFilteredList(p->cells,
                                   Transformer<Cell,MemoryIdentifier>(this,&SpacePacked::toCell),
                                   FilterNonEmptyCell);
}

SpacePacked::MemoryIdentifier SpacePacked::createCellMID()
{
  MemoryIdentifier cmid = ++(p->counter);

  CellData cd;
  p->cells.insert(cmid,cd);

  return cmid;
}

SpacePacked::Cell SpacePacked::createCell()
{
  return toCell(createCellMID());
}

bool SpacePacked::deleteCell(const SpacePacked::MemoryIdentifier & mid)
{
  if ( !cellIsValid(mid) ) return false;

  p->cells.remove(mid);

  IndividualContainer::iterator  i = p->individuals.begin();
  IndividualContainer::iterator  e = p->individuals.end();

  for( ; i != e; ++i )
    if ( i->isDistributed() && i->cellMID() == mid )
      i->setUndistributed();

  return true;
}

bool SpacePacked::deleteCell( const SpacePacked::ConstCell & cell )
{
  if (!isReferenceFromThis(cell) ) return false;

  return deleteCell(cell.mid());
}

int SpacePacked::deleteAllCells()
{
  int count = 0;

  IndividualContainer::iterator  i = p->individuals.begin();
  IndividualContainer::iterator  e = p->individuals.end();

  for( ; i != e; ++i )
    i->setUndistributed();

  return count;
}

int SpacePacked::deleteEmptyCells()
{
  int count = 0;

  MutableCellContainerIterator iterator( p->cells );
  while( iterator.hasNext() )
  {
    iterator.next();
    if( iterator.value().isEmpty() )
    {
      ++count;
      iterator.remove();
    }
  }

  return count;
}

int SpacePacked::deleteNonEmptyCells()
{
  int count = 0;

  MutableCellContainerIterator iterator( p->cells );
  while( iterator.hasNext() )
  {
    iterator.next();
    if( !iterator.value().isEmpty() )
    {
      ++count;
      iterator.remove();
    }
  }

  IndividualContainer::iterator i = p->individuals.begin();
  IndividualContainer::iterator e = p->individuals.end();

  for( ; i != e; ++i )
    i->setUndistributed();

  return count;
}

// Cell Reference Interface

bool SpacePacked::cellIsValid( const MemoryIdentifier & cmid) const
{
  return p->cells.contains(cmid);
}

bool SpacePacked::cellHasProperty( const SpacePacked::MemoryIdentifier & cmid,
                                   const QString & name ) const
{
  if (!cellIsValid(cmid) ) return false;

  return p->cells[cmid].hasProperty(name);
}

int SpacePacked::cellPropertyCount( const SpacePacked::MemoryIdentifier & cmid) const
{
  if (!cellIsValid(cmid)) return 0;

  return p->cells[cmid].propertyCount();
}

QStringList SpacePacked::cellPropertyNames( const SpacePacked::MemoryIdentifier & cmid ) const
{
  if (!cellIsValid(cmid)) return QStringList();

  return p->cells[cmid].propertyNames();
}

QVariant SpacePacked::cellProperty( const SpacePacked::MemoryIdentifier & cmid,
                                    const QString & name,
                                    const QVariant & defaultValue  ) const
{
  if (!cellIsValid(cmid)) return defaultValue;

  return p->cells[cmid].property(name,defaultValue);
}

int SpacePacked::cellIndividualCount( const MemoryIdentifier & cmid ) const
{
  if ( !cellIsValid(cmid) ) return 0;

  return p->cells[cmid].individualCount();
}

int SpacePacked::cellSpeciesIndividualCount( const SpacePacked::MemoryIdentifier & cmid,
                                             const SpacePacked::MemoryIdentifier & smid ) const
{
  if ( !cellIsValid(cmid) ) return 0;

  if ( !speciesIsValid(smid) ) return 0;

  return p->cells[cmid].individualCount(smid);
}

SpacePacked::MIDList SpacePacked::cellIndividualMIDs(
    const SpacePacked::MemoryIdentifier & cmid,
    const SpacePacked::SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return MIDList();

  if( so == InverseInsertionOrder )
    return sortedList(p->cells[cmid].individuals(),
                      MILess<MemoryIdentifier>());

  return
      p->cells[cmid].individuals();
}

SpacePacked::ConstIndividualList SpacePacked::cellConstIndividuals( const MemoryIdentifier & cmid,
                                          const SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return ConstIndividualList();

  // cell stored in insertion order

  if( so == InverseInsertionOrder )
    return sortedTransformedList(p->cells[cmid].individuals(),
                                 RLess<ConstIndividual>(),
                                 Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));

  return transformedList(p->cells[cmid].individuals(),
                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));
}

SpacePacked::MIDList SpacePacked::cellSpeciesIndividualMIDs( const MemoryIdentifier & cmid,
                                   const MemoryIdentifier & smid,
                                   const SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return MIDList();

  if (!speciesIsValid(smid)) return MIDList();

  if( so == InverseInsertionOrder )
    return sortedList(p->cells[cmid].individuals(smid),
                      MILess<MemoryIdentifier>());

  return
      p->cells[cmid].individuals(smid);
}

SpacePacked::ConstIndividualList
    SpacePacked::cellSpeciesConstIndividuals( const SpacePacked::MemoryIdentifier & cmid,
                                              const SpacePacked::MemoryIdentifier & smid,
                                              const SpacePacked::SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return ConstIndividualList();

  if (!speciesIsValid(smid)) return ConstIndividualList();

  // cell stored in insertion order

  if( so == InverseInsertionOrder )
    return sortedTransformedList(p->cells[cmid].individuals(smid),
                                 RLess<ConstIndividual>(),
                                 Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));

  return transformedList(p->cells[cmid].individuals(smid),
                         Transformer<ConstIndividual,MemoryIdentifier>(this,&SpacePacked::toConstIndividual));
}

int SpacePacked::cellSpeciesCount( const SpacePacked::MemoryIdentifier & cmid ) const
{
  if (!cellIsValid(cmid)) return 0;

  return p->cells[cmid].speciesCount();
}

bool SpacePacked::cellHasSpecies( const SpacePacked::MemoryIdentifier & cmid,
                                  const SpacePacked::MemoryIdentifier & smid ) const
{
  if(!cellIsValid(cmid)) return false;

  return p->cells[cmid].hasSpecies(smid);
}

SpacePacked::MIDList
    SpacePacked::cellSpeciesMIDs( const SpacePacked::MemoryIdentifier & cmid,
                                  const SpacePacked::SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return MIDList();

  if( so == InverseInsertionOrder )
    return sortedList(p->cells[cmid].species(),
                      MIGreater<MemoryIdentifier>());

  return p->cells[cmid].species();
}

SpacePacked::ConstSpeciesList
    SpacePacked::cellConstSpecies( const SpacePacked::MemoryIdentifier & cmid,
                                   const SpacePacked::SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return ConstSpeciesList();

  if( so == InverseInsertionOrder )
    return sortedTransformedList(p->cells[cmid].species(),
                                 RGreater<ConstSpecies>(),
                                 Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));

  return
      transformedList(p->cells[cmid].species(),
                      Transformer<ConstSpecies,MemoryIdentifier>(this,&SpacePacked::toConstSpecies));
}

int SpacePacked::cellSetProperty( const SpacePacked::MemoryIdentifier & mid,
                                   const QString & name,
                                   const QVariant & value )
{
  if (!cellIsValid(mid)) return -1;

  return p->cells[mid].setProperty(name,value);
}

SpacePacked::IndividualList
    SpacePacked::cellIndividuals( const SpacePacked::MemoryIdentifier & cmid,
                                  const SpacePacked::SortOrder & so ) const
{
    if(!cellIsValid(cmid)) return IndividualList();

  // cell stored in insertion order

  if( so == InverseInsertionOrder )
    return sortedTransformedList(p->cells[cmid].individuals(),
                                 RLess<Individual>(),
                                 Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));

  return transformedList(p->cells[cmid].individuals(),
                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));
}

SpacePacked::IndividualList
    SpacePacked::cellSpeciesIndividuals( const SpacePacked::MemoryIdentifier & cmid,
                                              const SpacePacked::MemoryIdentifier & smid,
                                              const SpacePacked::SortOrder & so ) const
{
  if(!cellIsValid(cmid)) return IndividualList();

  if (!speciesIsValid(smid)) return IndividualList();

  // cell stored in insertion order

  if( so == InverseInsertionOrder )
    return sortedTransformedList(p->cells[cmid].individuals(smid),
                                 RLess<Individual>(),
                                 Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));

  return transformedList(p->cells[cmid].individuals(smid),
                         Transformer<Individual,MemoryIdentifier>(this,&SpacePacked::toIndividual));
}

bool SpacePacked::cellInsertIndividual( const SpacePacked::MemoryIdentifier & cmid,
                                        const SpacePacked::MemoryIdentifier & smid,
                                        const SpacePacked::MemoryIdentifier & imid )
{
  if(!cellIsValid(cmid)) return false;
  if(!speciesIsValid(smid)) return false;
  if(!individualIsValid(imid)) return false;

  if(p->individuals[imid].speciesMID()!=smid) return false;

  p->cells[cmid].insertIndividual(smid,imid);
  p->individuals[imid].setDistributed(cmid);

  return true;
}

bool SpacePacked::cellRemoveIndividual( const SpacePacked::MemoryIdentifier & cmid,
                                        const SpacePacked::MemoryIdentifier & smid,
                                        const SpacePacked::MemoryIdentifier & imid )
{
  if(!cellIsValid(cmid)) return false;
  if(!speciesIsValid(smid)) return false;
  if(!individualIsValid(imid)) return false;

  if(p->individuals[imid].speciesMID()!=smid) return false;

  bool ok = p->cells[cmid].removeIndividual(smid,imid);
  if ( ok )
    p->individuals[imid].setUndistributed();
  return ok;
}

int SpacePacked::cellRemoveIndividuals(const SpacePacked::MemoryIdentifier & cmid)
{
  if(!cellIsValid(cmid)) return 0;

  int ccount = p->cells[cmid].removeIndividuals();

  int count = 0;

  IndividualContainer::iterator i = p->individuals.begin();
  IndividualContainer::iterator e = p->individuals.begin();
  for( ; i != e; ++i )
  {
    if( i->isDistributed() && i->cellMID() == cmid )
    {
      ++count;
      i->setUndistributed();
    }
  }

  Q_ASSERT(count == ccount);

  return count;
}

int SpacePacked::cellRemoveSpecies(const SpacePacked::MemoryIdentifier & cmid,
                                   const SpacePacked::MemoryIdentifier & smid )
{
  if(!cellIsValid(cmid)) return 0;

  if(!speciesIsValid(smid)) return 0;

  int ccount = p->cells[cmid].removeSpecies(smid);

  int count = 0;

  IndividualContainer::iterator i = p->individuals.begin();
  IndividualContainer::iterator e = p->individuals.begin();
  for( ; i != e; ++i )
  {
    if( i->isDistributed() && i->cellMID() == cmid && i->speciesMID() == smid )
    {
      ++count;
      i->setUndistributed();
    }
  }

  Q_ASSERT(count == ccount);

  return count;
}

int SpacePacked::cellRemoveAllSpecies(const SpacePacked::MemoryIdentifier & cmid )
{
  if(!cellIsValid(cmid)) return 0;

  int scount = p->cells[cmid].removeAllSpecies();

  QSet<MemoryIdentifier> set;

  IndividualContainer::iterator i = p->individuals.begin();
  IndividualContainer::iterator e = p->individuals.begin();
  for( ; i != e; ++i )
  {
    if( i->isDistributed() && i->cellMID() == cmid )
    {
      set.insert(i->speciesMID());
      i->setUndistributed();
    }
  }

  Q_ASSERT(scount == set.count());

  return scount;
}

} // namespace Implementation
} // namespace SRPS
