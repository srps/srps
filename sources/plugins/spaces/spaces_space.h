/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SPACES_SPACE_H
#define SPACES_SPACE_H

#include <SRPS/Space>

namespace SRPS {
namespace Implementation {

class Space : public SRPS::Space
{
    class Private;
    Private * p;

public:
    Space( const QVariantHash & = QVariantHash());
    virtual ~Space();

    static const QString StaticClassName;

    QString componentClassName() const;

    bool componentHasProperty( const QString & name ) const;

    int componentPropertyCount() const;

    QStringList componentPropertyNames() const;

    QVariant componentProperty( const QString & name,
                                        const QVariant & defaultValue =
                                          QVariant() ) const;

    int componentSetProperty( const QString & name,const QVariant & value );

    SRPS::Space * clone() const;
    SRPS::Space * cloneUndistributed() const;
    bool undistribute();

    // Invidual MID Interface

    bool individualIsValid( const MemoryIdentifier &) const;

    bool individualIsDistributed(const MemoryIdentifier & mid) const;

    MemoryIdentifier individualSpeciesMID( const MemoryIdentifier & mid ) const;

    MemoryIdentifier individualCellMID( const MemoryIdentifier & mid ) const;

    int individualCount() const;

    MIDList individualMIDs( const SortOrder & so = Unordered ) const;

    int distributedIndividualCount() const;

    MIDList distributedIndividualMIDs( const SortOrder & so = Unordered ) const;

    int undistributedIndividualCount() const;

    MIDList undistributedIndividualMIDs( const SortOrder & so = Unordered ) const;

    bool individualHasProperty( const MemoryIdentifier & mid,
                                   const QString & name ) const;

    int individualPropertyCount( const MemoryIdentifier & mid) const;

    QStringList individualPropertyNames( const MemoryIdentifier & mid ) const;

    QVariant individualProperty( const MemoryIdentifier & mid,
                                         const QString & name,
                                         const QVariant & defaultValue =
                                            QVariant() ) const;

    bool individualSetProperty( const MemoryIdentifier & mid,
                                        const QString & name,
                                        const QVariant & value );

//    MemoryIdentifier createIndividualMID( const MemoryIdentifier & smid );

//    bool deleteIndividual( const MemoryIdentifier & imid );

//    int deleteAllIndividuals();

//    int deleteSpeciesIndividuals(const MemoryIdentifier & smid);
//
//    int deleteDistributedIndividuals();
//
//    int deleteUndistributedIndividuals();
//
//    int deleteSpeciesDistributedIndividuals(const MemoryIdentifier & smid);
//
//    int deleteSpeciesUndistributedIndividuals(const MemoryIdentifier & smid);

    // Species MID Interface

    bool speciesIsValid( const MemoryIdentifier &) const;

    bool speciesIsDistributed(const MemoryIdentifier & mid) const;

    bool speciesIsUndistributed(const MemoryIdentifier & mid) const;

    int speciesDistributedCellCount( const MemoryIdentifier & mid ) const;

    MIDList speciesDistributedCellMIDs( const MemoryIdentifier & mid,
                                        const SortOrder & so = Unordered ) const;

    int speciesUndistributedCellCount( const MemoryIdentifier & mid ) const;

    MIDList speciesUndistributedCellMIDs( const MemoryIdentifier & mid,
                                          const SortOrder & so = Unordered ) const;

    int speciesCount() const;

    MIDList speciesMIDs( const SortOrder & so = Unordered ) const;

    int distributedSpeciesCount() const;

    MIDList distributedSpeciesMIDs( const SortOrder & so = Unordered ) const;

    int undistributedSpeciesCount() const;

    MIDList undistributedSpeciesMIDs( const SortOrder & so = Unordered ) const;

    int speciesIndividualCount(const MemoryIdentifier & mid) const;

    MIDList speciesIndividualMIDs( const MemoryIdentifier & mid,
                                           const SortOrder & so = Unordered ) const;

    int speciesDistributedIndividualCount( const MemoryIdentifier & mid ) const;


    MIDList speciesDistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                      const SortOrder & so = Unordered ) const;

    int speciesUndistributedIndividualCount( const MemoryIdentifier & mid ) const;

    MIDList speciesUndistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                const SortOrder & so = Unordered ) const;

    int emptySpeciesCount() const;

    MIDList emptySpeciesMIDs( const SortOrder & so = Unordered ) const;

    int nonEmptySpeciesCount() const;

    MIDList nonEmptySpeciesMIDs( const SortOrder & so = Unordered ) const;

    int speciesUndistribute( const MemoryIdentifier & mid );

    bool speciesHasProperty( const MemoryIdentifier & mid,
                                   const QString & name ) const;

    int speciesPropertyCount( const MemoryIdentifier & mid) const;

    QStringList speciesPropertyNames( const MemoryIdentifier & mid ) const;

    QVariant speciesProperty( const MemoryIdentifier & mid,
                                         const QString & name,
                                         const QVariant & defaultValue =
                                            QVariant() ) const;

    bool speciesSetProperty( const MemoryIdentifier & mid,
                                        const QString & name,
                                        const QVariant & value );

    MemoryIdentifier createSpeciesMID();

    bool deleteSpecies( const MemoryIdentifier & imid );

    int deleteAllSpecies();

    // Cell MID Interface

    bool cellIsValid( const MemoryIdentifier &) const;

    int cellCount() const;

    MIDList cellMIDs( const SortOrder & so = Unordered ) const;

    int emptyCellCount() const;

    MIDList emptyCellMIDs( const SortOrder & so = Unordered ) const;

    int nonEmptyCellCount() const;

    MIDList nonEmptyCellMIDs( const SortOrder & so = Unordered ) const;

    bool cellHasProperty( const MemoryIdentifier & mid,
                                   const QString & name ) const;

    int cellPropertyCount( const MemoryIdentifier & mid) const;

    QStringList cellPropertyNames( const MemoryIdentifier & mid ) const;

    QVariant cellProperty( const MemoryIdentifier & mid,
                                    const QString & name,
                                    const QVariant & defaultValue =
                                      QVariant() ) const;

    bool cellSetProperty( const MemoryIdentifier & mid,
                                   const QString & name,
                                   const QVariant & value );

    MemoryIdentifier createCellMID();

    bool deleteCell(const MemoryIdentifier & mid);

    int deleteAllCells();

    int cellIndividualCount( const MemoryIdentifier & mid ) const;

    MIDList cellIndividualMIDs( const MemoryIdentifier & mid,
                                        const SortOrder & so = Unordered ) const;

    bool cellInsertIndividual( const MemoryIdentifier & cmid,
                                       const MemoryIdentifier & smid,
                                       const MemoryIdentifier & imid );

    bool cellRemoveIndividual( const MemoryIdentifier & cmid,
                                       const MemoryIdentifier & smid,
                                       const MemoryIdentifier & imid );

    int cellRemoveIndividuals(const MemoryIdentifier & cmid);

    int cellSpeciesCount( const MemoryIdentifier & cmid ) const;

    MIDList cellSpeciesMIDs( const MemoryIdentifier & cmid,
                             const SortOrder & so = Unordered ) const;

    bool cellHasSpecies( const MemoryIdentifier & cmid,
                         const MemoryIdentifier & smid ) const;

    int cellSpeciesIndividualCount( const MemoryIdentifier & cmid,
                                    const MemoryIdentifier & smid ) const;

    MIDList cellSpeciesIndividualMIDs( const MemoryIdentifier & cmid,
                                       const MemoryIdentifier & smid,
                                       const SortOrder & so = Unordered) const;

    int cellRemoveSpecies(const MemoryIdentifier & cmid,
                          const MemoryIdentifier & smid );

    int cellRemoveAllSpecies(const MemoryIdentifier & cmid );
};

} // namespace Implementation
} // namespace SRPS

#endif // SPACES_SPACE_H
