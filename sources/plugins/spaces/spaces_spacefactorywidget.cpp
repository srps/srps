#include "spaces_spacefactorywidget.h"
#include "ui_spaces_spacefactorywidget.h"

#include "spaces_space.h"

namespace SRPS {
namespace Implementation {

SpaceFactoryWidget::SpaceFactoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpaceFactoryWidget)
{
    ui->setupUi(this);

    ui->setupScript->setPlainText(
        Implementation::Space::defaultSetupScriptValue()
        );

    ui->cellScript->setPlainText(
        Implementation::Space::defaultCellScriptValue()
        );

    ui->speciesScript->setPlainText(
        Implementation::Space::defaultSpeciesScriptValue()
        );

    ui->individualScript->setPlainText(
        Implementation::Space::defaultIndividualScriptValue()
        );

    ui->randomDistributedCheck->setChecked(Implementation::Space::defaultRandomDistributionValue());
}

SpaceFactoryWidget::~SpaceFactoryWidget()
{
    delete ui;
}

QVariantHash SpaceFactoryWidget::componentFactoryWidgetParameters() const
{
    QVariantHash r;

    r.insert(Implementation::Space::defaultSetupScriptName(),
             ui->setupCheck->isChecked()
                ?ui->setupScript->toPlainText()
                :Implementation::Space::defaultSetupScriptValue());

    r.insert(Implementation::Space::defaultCellScriptName(),
             ui->cellScript->toPlainText());

    r.insert(Implementation::Space::defaultSpeciesScriptName(),
             ui->speciesScript->toPlainText());

    r.insert(Implementation::Space::defaultIndividualScriptName(),
             ui->individualScript->toPlainText());

    r.insert(Implementation::Space::defaultRandomDistributionName(),
             ui->randomDistributedCheck->isChecked());

    return r;
}

bool SpaceFactoryWidget::componentFactoryWidgetSetParameters( const QVariantHash & parameters )
{
  ui->setupScript->setPlainText(
      parameters.value(
          Implementation::Space::defaultSetupScriptName(),
          Implementation::Space::defaultSetupScriptValue()
      ).toString()
  );

  ui->cellScript->setPlainText(
      parameters.value(
          Implementation::Space::defaultCellScriptName(),
          Implementation::Space::defaultCellScriptValue()
      ).toString()
  );

  ui->speciesScript->setPlainText(
      parameters.value(
          Implementation::Space::defaultSpeciesScriptName(),
          Implementation::Space::defaultSpeciesScriptValue()
      ).toString()
  );

  ui->individualScript->setPlainText(
      parameters.value(
          Implementation::Space::defaultIndividualScriptName(),
          Implementation::Space::defaultIndividualScriptValue()
      ).toString()
  );

  ui->randomDistributedCheck->setChecked(
      parameters.value(
          Implementation::Space::defaultRandomDistributionName(),
          Implementation::Space::defaultRandomDistributionValue()
      ).toBool()
  );

  return true;
}

void SpaceFactoryWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS
