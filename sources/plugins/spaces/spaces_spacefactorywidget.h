#ifndef SPACES_SPACEFACTORYWIDGET_H
#define SPACES_SPACEFACTORYWIDGET_H

#include <SRPS/ComponentFactoryWidget>
#include <QtGui/QWidget>

namespace SRPS {
namespace Implementation {

namespace Ui {
    class SpaceFactoryWidget;
}

class SpaceFactoryWidget : public QWidget, public SRPS::ComponentFactoryWidget
{
    Q_OBJECT
    Q_INTERFACES(SRPS::ComponentFactoryWidget)

public:
    explicit SpaceFactoryWidget(QWidget *parent = 0);
    virtual ~SpaceFactoryWidget();

    QVariantHash componentFactoryWidgetParameters() const;
    bool componentFactoryWidgetSetParameters(const QVariantHash & parameters );

protected:
    void changeEvent(QEvent *e);

private:
    Ui::SpaceFactoryWidget *ui;
};


} // namespace Implementation
} // namespace SRPS
#endif // SPACES_SPACEFACTORYWIDGET_H
