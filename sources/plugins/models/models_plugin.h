/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_MODELS_PLUGIN_H
#define SRPS_MODELS_PLUGIN_H

#include <QtCore/QObject>
#include <SRPS/Plugin>
#include <SRPS/ComponentFactory>
#include <SRPS/ModelFactory>

namespace SRPS {
namespace Implementation {

class ModelFactory :
    public QObject,
    public SRPS::Plugin,
    public SRPS::ComponentFactory,
    public SRPS::ModelFactory
{
    Q_OBJECT
    Q_INTERFACES(SRPS::Plugin SRPS::ModelFactory SRPS::ComponentFactory)

    class Private;
    Private * p;

public:
    explicit ModelFactory(QObject *parent = 0);
    virtual ~ModelFactory();

    // Plugin
    QString errorString() const;
    bool isInitialized() const;
    bool isFinalized() const;
    bool pluginReadSettings( const SettingsManager * sm );
    bool pluginInitialize();
    bool pluginFinalize();
    bool pluginWriteSettings( SettingsManager * sm );

    // Factory
//    QWidget * componentFactoryWidgetFor(const QString &key) const;
    QVariantHash componentFactoryDefaultParametersFor(const QString &key) const;
    bool componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters);

    // ModelFactory
    QStringList modelFactoryKeys() const;
    SRPS::Model * modelFactoryCreate (const QString &model,
                                          const QVariantHash &parameters=
                                                    QVariantHash());
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_MODELS_PLUGIN_H
