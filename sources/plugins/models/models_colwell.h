/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_COLWELL_H
#define SRPS_PLUGIN_COLWELL_H

#include <SRPS/Model>

namespace SRPS {
namespace Implementation {

class ColwellModel : public SRPS::Model
{
  SRPS_DISABLE_COPY(ColwellModel)
  class Private;
  Private * p;
  public:
    explicit ColwellModel();
    virtual ~ColwellModel();

    static const QString StaticClassName;

    QString componentClassName() const;

    // Space Needs

    bool needSourceSpace() const;
    void setSourceSpace( const SRPS::Space * space );

    bool needSourceCellMIDs() const;
    void setSourceCellMIDs( const SRPS::Space::MIDList & cells );

    bool needSourceSpeciesMIDs() const;
    void setSourceSpeciesMIDs( const SRPS::Space::MIDList & species );

    bool needTargetSpace() const;
    void setTargetSpace( SRPS::Space * space );

    bool needTargetCellMIDs() const;
    void setTargetCellMIDs( const SRPS::Space::MIDList & cells );

    bool needTargetSpeciesMIDs() const;
    void setTargetSpeciesMIDs( const SRPS::Space::MIDList & species );

    // DPRNG Needs

    bool needDPRNG() const;
    void setDPRNG( SRPS::DPRNG * dprng );

    // Model

    bool setup();
    bool finished() const;
    void model();
};

} // namespace Implementation
} // namespace SRPS

#endif // PLUGIN_SPECIESRICHNESS_H
