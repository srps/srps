/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_ZAPATA_H
#define SRPS_PLUGIN_ZAPATA_H

#include <SRPS/Model>

namespace SRPS {
namespace Implementation {

class ZapataModel : public SRPS::Model
{
  SRPS_DISABLE_COPY(ZapataModel)
  class Private;
  Private * p;
  public:
    explicit ZapataModel();
    virtual ~ZapataModel();

    static const QString StaticClassName;

    QString componentClassName() const;

    // Space Needs

    bool needTargetSpace() const;
    void setTargetSpace( SRPS::Space * space );

//    bool needCells() const;
//    void setCells( const SRPS::Space::CellList & cells );

    bool needTargetCellMIDs() const;
    void setTargetCellMIDs( const SRPS::Space::MIDList & cells );

//    bool needSpecies() const;
//    void setSpeciess( const SRPS::Space::SpeciesList & species );

//    bool needSpeciesMIDs() const;
//    void setSpeciesMIDs( const SRPS::Space::MIDList & species );

//    bool needIndividuals() const;
//    void setIndividuals( const SRPS::Space::IndividualList & individuals );

    bool needTargetIndividualMIDs() const;
    void setTargetIndividualsMIDs( const SRPS::Space::MIDList & individuals );

    // PRNG Needs

//    bool needPRNG() const;
//    void setPRNG( SRPS::PRNG * prng );

    // DPRNG Needs

    bool needDPRNG() const;
    void setDPRNG( SRPS::DPRNG * dprng );

    // Model

    bool setup();
    bool finished() const;
    void model();
};

} // namespace Implementation
} // namespace SRPS

#endif // PLUGIN_SPECIESRICHNESS_H
