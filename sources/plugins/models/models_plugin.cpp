/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <SRPS/SettingsManager>
#include "models_plugin.h"
#include "models_colwell.h"
#include "models_zapata.h"
//#include "models_modelfactorywidget.h"

using SRPS::Implementation::ModelFactory;

class SRPS::Implementation::ModelFactory::Private
{
public:
  Private():initialized(false),finalized(false){}
  bool initialized;
  bool finalized;
  QString lastError;
  QVariantHash defaultProperties;
};

ModelFactory::ModelFactory(QObject *parent) :
  QObject(parent),
  SRPS::ModelFactory(),
  p( new Private )
{
//  p->defaultProperties.insert(Implementation::Model::StaticDefaultVariableCountParameterName,
//                              Implementation::Model::StaticDefaultVariableCountParameterValue);
//  p->defaultProperties.insert(Implementation::Model::StaticDefaultTupleCountParameterName,
//                              Implementation::Model::StaticDefaultTupleCountParameterValue);
}

ModelFactory::~ModelFactory()
{
  delete p;
}

// Plugin
QString ModelFactory::errorString() const
{
  return p->lastError;
}

bool ModelFactory::isInitialized() const
{
  return p->initialized;
}

bool ModelFactory::isFinalized() const
{
  return p->finalized;
}

bool ModelFactory::pluginReadSettings( const SRPS::SettingsManager * sm )
{
//  if( !sm->isValid() )
    return false;

//  QVariantHash ps;
//  ps[Implementation::Model::StaticDefaultVariableCountParameterName]=
//          sm->value(Implementation::Model::StaticDefaultVariableCountParameterName,
//                   Implementation::Model::StaticDefaultVariableCountParameterValue);
//  ps[Implementation::Model::StaticDefaultTupleCountParameterName]=
//          sm->value(Implementation::Model::StaticDefaultTupleCountParameterName,
//                   Implementation::Model::StaticDefaultTupleCountParameterValue);
//  return componentFactorySetDefaultParametersFor(Implementation::Model::StaticClassName,ps);
}

bool ModelFactory::pluginInitialize()
{
  return true;
}

bool ModelFactory::pluginFinalize()
{
  return true;
}

bool ModelFactory::pluginWriteSettings( SRPS::SettingsManager * sm )
{
//  if ( !sm->isValid() )
//    return false;

//  sm->setValue(Implementation::Model::StaticDefaultVariableCountParameterName,
//                p->defaultProperties.value(
//                    Implementation::Model::StaticDefaultVariableCountParameterName).toInt());
//  sm->setValue(Implementation::Model::StaticDefaultTupleCountParameterName,
//                p->defaultProperties.value(
//                    Implementation::Model::StaticDefaultTupleCountParameterName).toInt());
  return true;
}

// ComponentFactory
//QWidget * ModelFactory::componentFactoryWidgetFor(const QString &key) const
//{
//  if ( key == Implementation::Model::StaticClassName )
//  {
//      ModelFactoryWidget * w = new ModelFactoryWidget;
//      w->componentFactoryWidgetSetParameters(p->defaultProperties);
//      return w;
//  }
//  return 0;
//}

QVariantHash ModelFactory::componentFactoryDefaultParametersFor(const QString &key) const
{
//  if ( key == Implementation::Model::StaticClassName )
//      return p->defaultProperties;
  return QVariantHash();
}

bool ModelFactory::componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters)
{
//  if ( key != Implementation::Model::StaticClassName )
//    return false;

//  int t = parameters.value(Implementation::Model::StaticDefaultTupleCountParameterName,-1).toInt();
//  if ( t < 0 )
//    return false;

//  int v = parameters.value(Implementation::Model::StaticDefaultVariableCountParameterName,-1).toInt();
//  if ( v < 0 )
//    return false;

//  p->defaultProperties[Implementation::Model::StaticDefaultTupleCountParameterName] = t;
//  p->defaultProperties[Implementation::Model::StaticDefaultVariableCountParameterName] = v;
  return true;
}

// ModelFactory
QStringList ModelFactory::modelFactoryKeys() const
{
  return QStringList(SRPS::Implementation::ColwellModel::StaticClassName)
      << SRPS::Implementation::ZapataModel::StaticClassName;
}

SRPS::Model * ModelFactory::modelFactoryCreate (const QString &model,
                                                const QVariantHash &/*parameters*/)
{
  if( model == SRPS::Implementation::ColwellModel::StaticClassName )
  {
//      QVariantHash p = parameters;
//      if ( p.value(Implementation::Model::StaticDefaultTupleCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Model::StaticDefaultTupleCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Model::StaticDefaultTupleCountParameterName);
//      if ( p.value(Implementation::Model::StaticDefaultVariableCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Model::StaticDefaultVariableCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Model::StaticDefaultVariableCountParameterName);
    return new SRPS::Implementation::ColwellModel;
  }
  if( model == SRPS::Implementation::ZapataModel::StaticClassName )
  {
//      QVariantHash p = parameters;
//      if ( p.value(Implementation::Model::StaticDefaultTupleCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Model::StaticDefaultTupleCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Model::StaticDefaultTupleCountParameterName);
//      if ( p.value(Implementation::Model::StaticDefaultVariableCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Model::StaticDefaultVariableCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Model::StaticDefaultVariableCountParameterName);
    return new SRPS::Implementation::ZapataModel;
  }
  return 0;
}

Q_EXPORT_PLUGIN2(SRPSModels,SRPS::Implementation::ModelFactory)
