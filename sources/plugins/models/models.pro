include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}Models
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += models_plugin.cpp \
    models_zapata.cpp \
    models_colwell.cpp
HEADERS += models_plugin.h \
    models_zapata.h \
    models_colwell.h

RESOURCES += \
    resources.qrc
