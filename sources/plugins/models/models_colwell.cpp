/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "models_colwell.h"

#include <SRPS/DPRNG>
#include <SRPS/Model>
#include <SRPS/Space>

#include <QtCore/QDebug>

#include <cmath>

namespace SRPS {
namespace Implementation {

struct SpeciesData
{
  SpeciesData()
  {
    valid = false;
  }

  int lower;
  int upper;
  int size;
  int midpoint;
  bool valid;
};

class ColwellModel::Private
{
  public:
    Private():dprng(0),space(0),sourceSpace(0){}
    DPRNG * dprng;

    SRPS::Space * space;
    const SRPS::Space * sourceSpace;

    SRPS::Space::MIDList cellMIDs;
    SRPS::Space::MIDList speciesMIDs;

    SRPS::Space::MIDList sourceCellMIDs;
    SRPS::Space::MIDList sourceSpeciesMIDs;

    QHash<SRPS::Space::MemoryIdentifier,SpeciesData> speciesDataTable;
};

ColwellModel::ColwellModel()
  : SRPS::Model( ),
    p( new Private )
{
}

ColwellModel::~ColwellModel()
{
  delete p;
}

const QString ColwellModel::StaticClassName = QLatin1String("srps.model.colwell");

QString ColwellModel::componentClassName() const
{
  return StaticClassName;
}

// Space Needs

bool ColwellModel::needSourceSpace() const
{
  return true;
}

void ColwellModel::setSourceSpace( const SRPS::Space * space )
{
  p->sourceSpace = space;
}

bool ColwellModel::needSourceCellMIDs() const
{
  return true;
}

void ColwellModel::setSourceCellMIDs( const SRPS::Space::MIDList & cells )
{
  p->sourceCellMIDs = cells;
}

bool ColwellModel::needSourceSpeciesMIDs() const
{
  return true;
}

void ColwellModel::setSourceSpeciesMIDs( const SRPS::Space::MIDList & species )
{
  p->sourceSpeciesMIDs = species;
}

bool ColwellModel::needTargetSpace() const
{
  return true;
}

void ColwellModel::setTargetSpace( SRPS::Space * space )
{
  p->space = space;
}

//    bool needCells() const;
//    void setCells( const SRPS::Space::CellList & cells );

bool ColwellModel::needTargetCellMIDs() const
{
  return true;
}

void ColwellModel::setTargetCellMIDs( const SRPS::Space::MIDList & cells )
{
  p->cellMIDs = cells;
}

//    bool needSpecies() const;
//    void setSpeciess( const SRPS::Space::SpeciesList & species );

bool ColwellModel::needTargetSpeciesMIDs() const
{
  return true;
}

void ColwellModel::setTargetSpeciesMIDs( const SRPS::Space::MIDList & species )
{
  p->speciesMIDs = species;
}

bool ColwellModel::needDPRNG() const
{
  return true;
}

void ColwellModel::setDPRNG( SRPS::DPRNG * dprng )
{
  p->dprng = dprng;
}

// Model

bool ColwellModel::setup()
{
  if ( !p->space ) return false;

  if ( !p->sourceSpace ) return false;

  if ( !p->dprng ) return false;

  p->dprng->setRange(SRPS::DPRNG::Range(0,p->cellMIDs.count()-1));

  foreach( SRPS::Space::MemoryIdentifier smid, p->sourceSpeciesMIDs )
  {
    SpeciesData data;
    data.valid = false;

    // lower
    for( int i = 0; i < p->sourceCellMIDs.count(); ++i )
    {
      if ( p->sourceSpace->cellHasSpecies(p->sourceCellMIDs.at(i), smid) )
      {
        data.lower = i;
        data.valid = true;
        break;
      }
    }

    // upper
    for( int i = p->sourceCellMIDs.count()-1; i >= 0; --i )
    {
      if ( p->sourceSpace->cellHasSpecies(p->sourceCellMIDs.at(i), smid) )
      {
        data.upper = i;
        data.valid = true;
        break;
      }
    }

    if ( data.valid )
    {
      data.size = data.upper-data.lower+1;
    }

    p->speciesDataTable.insert(smid,data);
  }

  return true;
}

bool ColwellModel::finished() const
{
  return p->speciesMIDs.count() == 0;
}

void ColwellModel::model()
{
  SRPS::Space::MemoryIdentifier specieMID = p->speciesMIDs.takeFirst();

  SpeciesData data = p->speciesDataTable.value(specieMID);
  if ( !data.valid ) return;

  DPRNG::Range drange = p->dprng->range();
  p->dprng->setRange( DPRNG::Range(0, p->cellMIDs.count()-data.size) );
  int pos = int(round(p->dprng->nextCC()));
  p->dprng->setRange(drange);

  Space::MIDList individualList = p->space->speciesIndividualMIDs(specieMID);

  int idiff = individualList.count()-data.size;
  if ( idiff < 0 )
    while( idiff++ )
      individualList << p->space->speciesCreateIndividualMID(specieMID);
  for( int c = pos; c < data.size+pos; ++c )
    p->space->cellInsertIndividual(p->cellMIDs.at(c),specieMID,individualList.takeFirst());
}

} // namespace Implementation
} // namespace SRPS
