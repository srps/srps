/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "models_zapata.h"

#include <SRPS/DPRNG>
#include <SRPS/Model>
#include <SRPS/Space>

#include <QtCore/QDebug>

#include <cmath>

namespace SRPS {
namespace Implementation {

class ZapataModel::Private
{
  public:
    Private():dprng(0),space(0){}
    DPRNG * dprng;

    SRPS::Space * space;
    SRPS::Space::MIDList cellMIDs;
    SRPS::Space::MIDList individualMIDs;
};

ZapataModel::ZapataModel()
  : SRPS::Model( ),
    p( new Private )
{
}

ZapataModel::~ZapataModel()
{
  delete p;
}

const QString ZapataModel::StaticClassName = QLatin1String("srps.model.zapata");

QString ZapataModel::componentClassName() const
{
  return StaticClassName;
}

// Space Needs

bool ZapataModel::needTargetSpace() const
{
  return true;
}

void ZapataModel::setTargetSpace( SRPS::Space * space )
{
  p->space = space;
}

//    bool needCells() const;
//    void setCells( const SRPS::Space::CellList & cells );

bool ZapataModel::needTargetCellMIDs() const
{
  return true;
}

void ZapataModel::setTargetCellMIDs( const SRPS::Space::MIDList & cells )
{
  p->cellMIDs = cells;
}

//    bool needSpecies() const;
//    void setSpeciess( const SRPS::Space::SpeciesList & species );

//bool ZapataModel::needSpeciesMIDs() const
//{
//  return true;
//}

//void ZapataModel::setSpeciesMIDs( const SRPS::Space::MIDList & species )
//{
//  p->speciesMIDs = species;
//}

bool ZapataModel::needTargetIndividualMIDs() const
{
  return true;
}

void ZapataModel::setTargetIndividualsMIDs( const SRPS::Space::MIDList & individuals )
{
  p->individualMIDs = individuals;
}

bool ZapataModel::needDPRNG() const
{
  return true;
}

void ZapataModel::setDPRNG( SRPS::DPRNG * dprng )
{
  p->dprng = dprng;
}

// Model

bool ZapataModel::setup()
{
  if ( !p->space ) return false;

  if ( !p->dprng ) return false;

  p->dprng->setRange(SRPS::DPRNG::Range(0,p->cellMIDs.count()));
  Q_ASSERT(p->cellMIDs.count());

  return true;
}

bool ZapataModel::finished() const
{
  return p->individualMIDs.count() == 0;
}

void ZapataModel::model()
{
  SRPS::Space::MemoryIdentifier individual = p->individualMIDs.takeFirst();

  int pos = int(floor(p->dprng->nextCC()));
//  while( pos < 0 || pos >= p->cellMIDs.count() )
//    pos = int(floor(p->dprng->nextCC()));

  p->space->cellInsertIndividual(
      p->cellMIDs.at(pos),
      p->space->individualSpeciesMID(individual),
      individual
      );
}

} // namespace Implementation
} // namespace SRPS
