include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}Accumulators
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += accumulators_plugin.cpp \
    accumulators_speciesrichness.cpp
    #accumulators_shannonwiener.cpp
HEADERS += accumulators_plugin.h \
    accumulators_speciesrichness.h
    #accumulators_shannonwiener.h

RESOURCES += \
    resources.qrc
