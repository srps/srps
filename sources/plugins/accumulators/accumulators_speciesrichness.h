/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_SPECIESRICHNESS_H
#define SRPS_PLUGIN_SPECIESRICHNESS_H

#include <SRPS/Accumulator>

namespace SRPS {
namespace Implementation {

class SpeciesRichness : public SRPS::Accumulator
{

  class Private;
  Private * p;
  public:
    SpeciesRichness( );
    virtual ~SpeciesRichness();

    static const QString StaticClassName;

    QString componentClassName() const;

    // Space Needs

    bool needSpace() const;
    void setSpace( const SRPS::Space * space );

//    virtual bool needCells() const;
//    virtual void setCells( const SRPS::Space::CellList & cells );

    bool needCellMIDs() const;
    void setCellMIDs( const SRPS::Space::MIDList & cells );

//    virtual bool needSpecies() const;
//    virtual void setSpeciess( const SRPS::Space::SpeciesList & species );

//    virtual bool needSpeciesMIDs() const;
//    virtual void setSpeciesMIDs( const SRPS::Space::MIDList & species );

//    virtual bool needIndividuals() const;
//    virtual void setIndividuals( const SRPS::Space::IndividualList & individuals );

//    virtual bool needIndividualMIDs() const;
//    virtual void setIndividualsMIDs( const SRPS::Space::MIDList & species );

    // Accumulator

    bool setup();
    void accumulate();
    void finalize( SRPS::Curve * curve );
};

} // namespace Implementation
} // namespace SRPS

#endif // PLUGIN_SPECIESRICHNESS_H
