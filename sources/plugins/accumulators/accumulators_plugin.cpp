/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <SRPS/SettingsManager>
#include "accumulators_plugin.h"
#include "accumulators_speciesrichness.h"

using SRPS::Implementation::AccumulatorFactory;

class SRPS::Implementation::AccumulatorFactory::Private
{
public:
  Private():initialized(false),finalized(false){}
  bool initialized;
  bool finalized;
  QString lastError;
  QVariantHash defaultProperties;
};

AccumulatorFactory::AccumulatorFactory(QObject *parent) :
  QObject(parent),
  SRPS::AccumulatorFactory(),
  p( new Private )
{
//  p->defaultProperties.insert(Implementation::Accumulator::StaticDefaultVariableCountParameterName,
//                              Implementation::Accumulator::StaticDefaultVariableCountParameterValue);
//  p->defaultProperties.insert(Implementation::Accumulator::StaticDefaultTupleCountParameterName,
//                              Implementation::Accumulator::StaticDefaultTupleCountParameterValue);
}

AccumulatorFactory::~AccumulatorFactory()
{
  delete p;
}

// Plugin
QString AccumulatorFactory::errorString() const
{
  return p->lastError;
}

bool AccumulatorFactory::isInitialized() const
{
  return p->initialized;
}

bool AccumulatorFactory::isFinalized() const
{
  return p->finalized;
}

bool AccumulatorFactory::pluginReadSettings( const SRPS::SettingsManager * sm )
{
//  if( !sm->isValid() )
    return false;

//  QVariantHash ps;
//  ps[Implementation::Accumulator::StaticDefaultVariableCountParameterName]=
//          sm->value(Implementation::Accumulator::StaticDefaultVariableCountParameterName,
//                   Implementation::Accumulator::StaticDefaultVariableCountParameterValue);
//  ps[Implementation::Accumulator::StaticDefaultTupleCountParameterName]=
//          sm->value(Implementation::Accumulator::StaticDefaultTupleCountParameterName,
//                   Implementation::Accumulator::StaticDefaultTupleCountParameterValue);
//  return componentFactorySetDefaultParametersFor(Implementation::Accumulator::StaticClassName,ps);
}

bool AccumulatorFactory::pluginInitialize()
{
  return true;
}

bool AccumulatorFactory::pluginFinalize()
{
  return true;
}

bool AccumulatorFactory::pluginWriteSettings( SRPS::SettingsManager * sm )
{
//  if ( !sm->isValid() )
//    return false;

//  sm->setValue(Implementation::Accumulator::StaticDefaultVariableCountParameterName,
//                p->defaultProperties.value(
//                    Implementation::Accumulator::StaticDefaultVariableCountParameterName).toInt());
//  sm->setValue(Implementation::Accumulator::StaticDefaultTupleCountParameterName,
//                p->defaultProperties.value(
//                    Implementation::Accumulator::StaticDefaultTupleCountParameterName).toInt());
  return true;
}

// ComponentFactory
//QWidget * AccumulatorFactory::componentFactoryWidgetFor(const QString &key) const
//{
//  if ( key == Implementation::Accumulator::StaticClassName )
//  {
//      AccumulatorFactoryWidget * w = new AccumulatorFactoryWidget;
//      w->componentFactoryWidgetSetParameters(p->defaultProperties);
//      return w;
//  }
//  return 0;
//}

QVariantHash AccumulatorFactory::componentFactoryDefaultParametersFor(const QString &key) const
{
//  if ( key == Implementation::Accumulator::StaticClassName )
//      return p->defaultProperties;
  return QVariantHash();
}

bool AccumulatorFactory::componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters)
{
//  if ( key != Implementation::Accumulator::StaticClassName )
//    return false;

//  int t = parameters.value(Implementation::Accumulator::StaticDefaultTupleCountParameterName,-1).toInt();
//  if ( t < 0 )
//    return false;

//  int v = parameters.value(Implementation::Accumulator::StaticDefaultVariableCountParameterName,-1).toInt();
//  if ( v < 0 )
//    return false;

//  p->defaultProperties[Implementation::Accumulator::StaticDefaultTupleCountParameterName] = t;
//  p->defaultProperties[Implementation::Accumulator::StaticDefaultVariableCountParameterName] = v;
  return true;
}

// AccumulatorFactory
QStringList AccumulatorFactory::accumulatorFactoryKeys() const
{
  return QStringList(SRPS::Implementation::SpeciesRichness::StaticClassName);
//      << SRPS::Implementation::ZapataAccumulator::StaticClassName;
}

SRPS::Accumulator * AccumulatorFactory::accumulatorFactoryCreate (const QString &accumulator,
                                                const QVariantHash &/*parameters*/)
{
  if( accumulator == SRPS::Implementation::SpeciesRichness::StaticClassName )
  {
//      QVariantHash p = parameters;
//      if ( p.value(Implementation::Accumulator::StaticDefaultTupleCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Accumulator::StaticDefaultTupleCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Accumulator::StaticDefaultTupleCountParameterName);
//      if ( p.value(Implementation::Accumulator::StaticDefaultVariableCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Accumulator::StaticDefaultVariableCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Accumulator::StaticDefaultVariableCountParameterName);
    return new SRPS::Implementation::SpeciesRichness;
  }
//  if( accumulator == SRPS::Implementation::ZapataAccumulator::StaticClassName )
//  {
//      QVariantHash p = parameters;
//      if ( p.value(Implementation::Accumulator::StaticDefaultTupleCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Accumulator::StaticDefaultTupleCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Accumulator::StaticDefaultTupleCountParameterName);
//      if ( p.value(Implementation::Accumulator::StaticDefaultVariableCountParameterName,-1).toInt() < 0 )
//          p[Implementation::Accumulator::StaticDefaultVariableCountParameterName] =
//                  this->p->defaultProperties.value(Implementation::Accumulator::StaticDefaultVariableCountParameterName);
//    return new SRPS::Implementation::ZapataAccumulator;
//  }
  return 0;
}

Q_EXPORT_PLUGIN2(SRPSAccumulators,SRPS::Implementation::AccumulatorFactory)
