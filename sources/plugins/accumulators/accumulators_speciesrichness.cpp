/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "accumulators_speciesrichness.h"

#include <QtCore/QVector>
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

class SpeciesRichness::Private
{
  public:
    Private():counter(0),space(0){}
    int counter;
    const SRPS::Space  * space;
    QVector<double> richness;
    SRPS::Space::MIDList cellMIDs;
};

SpeciesRichness::SpeciesRichness()
  : SRPS::Accumulator( ),
    p( new Private )
{
}

SpeciesRichness::~SpeciesRichness()
{
  delete p;
}

const QString SpeciesRichness::StaticClassName = QLatin1String("srps.accumulator.species.richness");

QString SpeciesRichness::componentClassName() const
{
  return StaticClassName;
}

bool SpeciesRichness::needSpace() const
{
  return true;
}

void SpeciesRichness::setSpace( const SRPS::Space * space )
{
  p->space = space;
}

bool SpeciesRichness::needCellMIDs() const
{
  return true;
}

void SpeciesRichness::setCellMIDs( const SRPS::Space::MIDList & cells )
{
  p->cellMIDs = cells;
}

bool SpeciesRichness::setup()
{
  if ( !p->space ) return false;

  if ( p->cellMIDs.count() == 0 ) return false;

  if ( p->cellMIDs.count() > p->richness.count() )
    p->richness.resize(p->cellMIDs.count());

  return true;
}

void SpeciesRichness::accumulate()
{
  for( int i = 0; i < p->cellMIDs.count(); ++i )
  {
    p->richness[i] += p->space->cellSpeciesCount(p->cellMIDs.at(i));
  }
  ++(p->counter);
}

void SpeciesRichness::finalize( SRPS::Curve * curve )
{
  if ( curve )
  {
    Curve::Variable variable = curve->createVariable();
    bool valid = variable.isValid();
    variable.setProperty("srps.name",QObject::tr("Species Richness"));
    variable.setProperty("srps.description",QObject::tr("Steven's Species Richness"));

    for( int i = 0; i < p->richness.count(); ++i )
    {
      Curve::Tuple tuple = curve->createTuple();
      valid = tuple.isValid();
      tuple.setValue(variable, p->richness[i]/double(p->counter) );
      if ( i == p->richness.count() -1 )
      {
        qDebug() << "SR" <<tuple.value(variable);
      }
    }
  }
  p->richness.clear();
  p->counter = 0;

}

} // namespace Implementation
} // namespace SRPS
