include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}Curves
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += \
    curves_plugin.cpp \
    curves_curve.cpp
HEADERS += \
    curves_plugin.h \
    curves_curve.h

RESOURCES += \
    resources.qrc
