/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVES_CURVE_H
#define SRPS_CURVES_CURVE_H

#include <SRPS/Curve>

namespace SRPS {
namespace Implementation {

class Curve : public SRPS::Curve
{
    class Private;
    Private * p;
public:
    Curve( const QVariantHash & parameters = QVariantHash() );
    virtual ~Curve();

    static const QString StaticClassName;
    static const QString StaticDefaultVariableCountParameterName;
    static const int StaticDefaultVariableCountParameterValue;
    static const QString StaticDefaultTupleCountParameterName;
    static const int StaticDefaultTupleCountParameterValue;

    // Component

    QString componentClassName() const;

    bool componentHasProperty( const QString & name ) const;
    int componentPropertyCount() const;
    QStringList componentPropertyNames() const;
    QVariant componentProperty( const QString & name, const QVariant & defaultValue = QVariant() ) const;
    int componentSetProperty( const QString & name, const QVariant & value );

    // SRPS::Curve

    Curve * clone() const;

    void clear();

    // Variable Curve API

    int variableCount() const;

    MIDList variableMIDs( const SortOrder & ord = Unordered ) const;

    ConstVariableList constVariables( const SortOrder &  so = Unordered ) const;

    VariableList variables( const SortOrder & so = Unordered ) const;

    MemoryIdentifier createVariableMID();

    Variable createVariable();

    bool deleteVariable(const MemoryIdentifier &);

    bool deleteVariable( const ConstVariable & variable );

    int deleteAllVariables();

    // Variable Reference API

    bool variableIsValid( const MemoryIdentifier & mid) const;

    bool variableHasProperty( const MemoryIdentifier & mid,
                              const QString & name ) const;

    int variablePropertyCount( const MemoryIdentifier & mid) const;

    QStringList variablePropertyNames( const MemoryIdentifier & mid) const;

    QVariant variableProperty( const MemoryIdentifier & mid,
                               const QString & name,
                               const QVariant & defaultValue =
                               QVariant() ) const;

    int variableSetProperty( const MemoryIdentifier & mid,
                              const QString & name,
                              const QVariant & value );

    // Tuple Space API

    int tupleCount() const;

    MIDList tupleMIDs( const SortOrder & so = Unordered ) const;

    ConstTupleList constTuples( const SortOrder & so = Unordered ) const;

    TupleList tuples( const SortOrder & so = Unordered ) const;

    MemoryIdentifier createTupleMID();

    Tuple createTuple();

    bool deleteTuple(const MemoryIdentifier & mid);

    bool deleteTuple( const ConstTuple & tuple );

    int deleteAllTuples();

    // Tuple Reference API

    bool tupleIsValid( const MemoryIdentifier &) const;

    bool tupleHasProperty( const MemoryIdentifier & mid,
                           const QString & name ) const;

    int tuplePropertyCount( const MemoryIdentifier & mid) const;

    QStringList tuplePropertyNames( const MemoryIdentifier & mid ) const;

    QVariant tupleProperty( const MemoryIdentifier & mid,
                            const QString & name,
                            const QVariant & defaultValue =
                            QVariant() ) const;

    QVariant tupleValue( const MemoryIdentifier & tmid,
                         const MemoryIdentifier & vmid,
                         const QVariant & defaultValue =
                         QVariant()) const;

    int tupleVariableCount( const MemoryIdentifier & tmid ) const;

    MIDList tupleVariableMIDs( const MemoryIdentifier & tmid,
                               const SortOrder & so = Unordered ) const;

    ConstVariableList tupleConstVariables( const MemoryIdentifier & tmid,
                                           const SortOrder & so = Unordered ) const;

    int tupleSetProperty( const MemoryIdentifier & mid,
                           const QString & name,
                           const QVariant & value );

    bool tupleSetValue( const MemoryIdentifier & tmid,
                        const MemoryIdentifier & vmid,
                        const QVariant & value = QVariant());

    int tupleClearValues( const MemoryIdentifier & tmid );
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_CURVES_CURVE_H
