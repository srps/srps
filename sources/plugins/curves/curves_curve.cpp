/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curves_curve.h"

#include <QtCore/QHash>
#include <QtCore/QVariantHash>

namespace SRPS {
namespace Implementation {

template <class T>
class RLess
{
public:
  inline bool operator()(const T &t1,
                         const T &t2) const
  {
      return (t1.mid() < t2.mid());
  }
};

template < class T>
class RGreater
{
public:
  inline bool operator()(const T &t1,
                         const T &t2) const
  {
      return (t2.mid() < t1.mid());
  }
};

struct VariableData
{
  QVariantHash properties;
};

struct TupleData
{
  QVariantHash properties;
  QHash<Curve::MemoryIdentifier,QVariant> values;
};

typedef QHash<Curve::MemoryIdentifier,VariableData> VariableTable;
typedef QHash<Curve::MemoryIdentifier,TupleData> TupleTable;

class Implementation::Curve::Private
{
  public:
    Private():counter(0){}
    Curve::MemoryIdentifier counter;
    VariableTable variables;
    TupleTable tuples;
    QVariantHash properties;
};

const QString Curve::StaticClassName = QLatin1String("srps.curve");
const QString Curve::StaticDefaultVariableCountParameterName = QLatin1String("srps.curve.variable.count");
const int Curve::StaticDefaultVariableCountParameterValue = 0;
const QString Curve::StaticDefaultTupleCountParameterName = QLatin1String("srps.curve.tuple.count");
const int Curve::StaticDefaultTupleCountParameterValue = 0;

Curve::Curve( const QVariantHash & parameters )
    : SRPS::Curve(),
    p( new Private )
{
    int tc = parameters.value(StaticDefaultTupleCountParameterName,StaticDefaultTupleCountParameterValue).toInt();
    int vc = parameters.value(StaticDefaultVariableCountParameterName,StaticDefaultVariableCountParameterValue).toInt();

    while( vc-- )
        createVariableMID();

    while( tc-- )
        createTupleMID();

}

Curve::~Curve()
{
    delete p;
}


// Component
QString Curve::componentClassName() const
{
    return StaticClassName;
}

int Curve::componentPropertyCount() const
{
    return p->properties.count();
}

bool Curve::componentHasProperty( const QString & name ) const
{
    return p->properties.contains(name);
}

QStringList Curve::componentPropertyNames() const
{
    return p->properties.keys();
}

QVariant Curve::componentProperty(const QString & name, const QVariant & defaultResult) const
{
    return p->properties.value(name,defaultResult);
}

int Curve::componentSetProperty(const QString & name, const QVariant & value )
{
    if ( name.isEmpty() )
    {
        qWarning("%s empty property name",Q_FUNC_INFO);
        return -1;
    }
    if ( !value.isValid() )
      p->properties.remove(name);
    else
      p->properties.insert(name,value);
    return 0;
}

// SRPS::Curve
Curve * Curve::clone() const
{
    Implementation::Curve * c = new Implementation::Curve;
    c->p->counter = p->counter;
    c->p->tuples = p->tuples;
    c->p->variables = p->variables;
    c->p->properties = p->properties;
    return c;
}

void Curve::clear()
{
  p->properties.clear();
  p->variables.clear();
  p->tuples.clear();
  p->counter = 0;
}

int Curve::variableCount() const
{
    return p->variables.count();
}

Curve::MIDList Curve::variableMIDs( const Curve::SortOrder & so ) const
{
    Curve::MIDList list = p->variables.keys();

    if ( so == Curve::CreationOrder )
      qStableSort(list);
    else if ( so == Curve::InverseCreationOrder )
      qStableSort(list.begin(),list.end(),qGreater<Curve::MemoryIdentifier>());

    return list;
}

Curve::ConstVariableList Curve::constVariables( const SortOrder &  so ) const
{
  Curve::ConstVariableList list;

  VariableTable::const_iterator iterator = p->variables.constBegin();
  VariableTable::const_iterator end      = p->variables.constEnd();
  for( ; iterator != end ; ++iterator )
    list << toConstVariable(iterator.key());

  if ( so == Curve::CreationOrder )
    qStableSort(list.begin(),list.end(),RLess<ConstVariable>());
  else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),RGreater<ConstVariable>());

  return list;
}

Curve::VariableList Curve::variables( const Curve::SortOrder & so ) const
{
  Curve::VariableList list;

  VariableTable::const_iterator iterator = p->variables.constBegin();
  VariableTable::const_iterator end      = p->variables.constEnd();
  for( ; iterator != end ; ++iterator )
    list << toVariable(iterator.key());

  if ( so == Curve::CreationOrder )
    qStableSort(list.begin(),list.end(),RLess<Variable>());
  else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),RGreater<Variable>());

  return list;
}

Curve::MemoryIdentifier Curve::createVariableMID()
{
  p->variables.insert( ++p->counter, VariableData() );

  return p->counter;
}

Curve::Variable Curve::createVariable()
{
  return toVariable(createVariableMID());
}

bool Curve::deleteVariable( const Curve::MemoryIdentifier & i )
{

  if ( !p->variables.contains( i ) )
    return false;

  p->variables.remove( i );

  TupleTable::iterator  iter = p->tuples.begin();
  TupleTable::iterator   end = p->tuples.end();
  for( ; iter != end; ++iter )
  {
    iter->values.remove( i );
  }

  return true;
}

bool Curve::deleteVariable( const Curve::ConstVariable & i )
{
  if ( isReferenceFromThis(i) ) return false;

  return deleteVariable(i.mid());
}

int Curve::deleteAllVariables()
{
  int count = p->variables.count();

  p->variables.clear();

  TupleTable::iterator iter = p->tuples.begin();
  TupleTable::iterator end  = p->tuples.end();
  for( ; iter != end; ++iter )
  {
    iter->values.clear();
  }

  return count;
}


// Variable Reference API

bool Curve::variableIsValid( const Curve::MemoryIdentifier & i ) const
{
  return p->variables.contains( i );
}

bool Curve::variableHasProperty( const MemoryIdentifier & i, const QString & n) const
{
    return p->variables.value(i).properties.contains(n);
}

int Curve::variablePropertyCount( const Curve::MemoryIdentifier & i ) const
{
  return p->variables.value( i ).properties.count();
}

QStringList Curve::variablePropertyNames( const Curve::MemoryIdentifier & i ) const
{
  return p->variables.value( i ).properties.keys();
}

QVariant Curve::variableProperty( const Curve::MemoryIdentifier & i, const QString & n, const QVariant & dv ) const
{
  return p->variables.value( i ).properties.value(n,dv);
}

int Curve::variableSetProperty( const Curve::MemoryIdentifier & i, const QString & n, const QVariant & v)
{
  if ( !p->variables.contains(i) )
    return -1;

  if ( !v.isValid() )
    p->variables[ i ].properties.remove( n );
  else
    p->variables[ i ].properties.insert( n, v);
  return 0;
}

// Tuple Space API

int Curve::tupleCount() const
{
  return p->tuples.count();
}

Curve::MIDList Curve::tupleMIDs( const Curve::SortOrder & so ) const
{
  Curve::MIDList list = p->tuples.keys();

  if ( so == Curve::CreationOrder )
    qStableSort(list);
  else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),qGreater<Curve::MemoryIdentifier>());

  return list;
}

Curve::ConstTupleList Curve::constTuples(const Curve::SortOrder & so ) const
{
  Curve::ConstTupleList list;

  TupleTable::const_iterator iterator = p->tuples.constBegin();
  TupleTable::const_iterator end      = p->tuples.constEnd();
  for( ; iterator != end ; ++iterator )
    list << toConstTuple(iterator.key());

  if ( so == Curve::CreationOrder )
    qStableSort(list.begin(),list.end(),RLess<ConstTuple>());
    else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),RGreater<ConstTuple>());

  return list;
}

Curve::TupleList Curve::tuples(const Curve::SortOrder & so ) const
{
  Curve::TupleList list;

  TupleTable::const_iterator iterator = p->tuples.constBegin();
  TupleTable::const_iterator end      = p->tuples.constEnd();
  for( ; iterator != end ; ++iterator )
    list << toTuple(iterator.key());

  if ( so == Curve::CreationOrder )
    qStableSort(list.begin(),list.end(),RLess<Tuple>());
  else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),RGreater<Tuple>());

  return list;
}

Curve::MemoryIdentifier Curve::createTupleMID()
{
  p->tuples.insert( ++p->counter, TupleData() );

  return p->counter;
}

Curve::Tuple Curve::createTuple()
{
  return toTuple(createTupleMID());
}

bool Curve::deleteTuple( const MemoryIdentifier & i )
{
    if ( !p->tuples.contains(i))
        return false;

    p->tuples.remove(i);
    return true;
}

bool Curve::deleteTuple(const ConstTuple &tuple)
{
  if ( !isReferenceFromThis(tuple) ) return false;

  return deleteTuple(tuple.mid());
}

int Curve::deleteAllTuples()
{
  int count = p->tuples.count();

  p->tuples.clear();

  return count;
}

// Tuple Reference API

bool Curve::tupleIsValid( const Curve::MemoryIdentifier & i ) const
{
  return p->tuples.contains( i );
}

bool Curve::tupleHasProperty(const MemoryIdentifier & i, const QString & n) const
{
    return p->tuples.value(i).properties.contains(n);
}

int Curve::tuplePropertyCount( const Curve::MemoryIdentifier & i) const
{
  return p->tuples.value( i ).properties.count();
}

QStringList Curve::tuplePropertyNames( const Curve::MemoryIdentifier & i ) const
{
  return p->tuples.value( i ).properties.keys();
}

QVariant Curve::tupleProperty( const Curve::MemoryIdentifier & i, const QString & n, const QVariant & dv ) const
{
  return p->tuples.value( i ).properties.value(n,dv);
}

QVariant Curve::tupleValue( const Curve::MemoryIdentifier & i, const Curve::MemoryIdentifier & v, const QVariant & dv ) const
{
  return p->tuples.value(i).values.value(v,dv);
}

int Curve::tupleVariableCount(const MemoryIdentifier &tmid) const
{
  return p->tuples.value(tmid).values.count();
}

Curve::MIDList Curve::tupleVariableMIDs(const MemoryIdentifier &tmid, const SortOrder &so) const
{
  Curve::MIDList list = p->tuples.value(tmid).values.keys();

  if ( so == Curve::CreationOrder )
    qStableSort(list);
  else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),qGreater<MemoryIdentifier>());

  return list;
}

Curve::ConstVariableList Curve::tupleConstVariables(const MemoryIdentifier &tmid, const SortOrder &so) const
{
  Curve::ConstVariableList list;

  if ( !p->tuples.contains(tmid) ) return list;

  VariableTable::const_iterator iterator = p->variables.constBegin();
  VariableTable::const_iterator end      = p->variables.constEnd();
  for( ; iterator != end ; ++iterator )
    list << toConstVariable(iterator.key());

  if ( so == Curve::CreationOrder )
    qStableSort(list.begin(),list.end(),RLess<ConstVariable>());
  else if ( so == Curve::InverseCreationOrder )
    qStableSort(list.begin(),list.end(),RGreater<ConstVariable>());

  return list;
}

int Curve::tupleSetProperty( const Curve::MemoryIdentifier & i, const QString & n, const QVariant & v )
{
  if ( !p->tuples.contains(i))
    return -1;

  if ( !v.isValid() )
    p->tuples[ i ].properties.remove( n );
  else
    p->tuples[ i ].properties.insert( n,v );
  return 0;
}

bool Curve::tupleSetValue( const Curve::MemoryIdentifier & i, const Curve::MemoryIdentifier & v, const QVariant & val )
{
  if ( !p->tuples.contains(i) )
    return false;

  if ( !val.isValid() )
    p->tuples[ i ].values.remove(v);
  else
    p->tuples[ i ].values.insert(v,val);
  return true;
}

int Curve::tupleClearValues(const MemoryIdentifier &tmid)
{
  if ( !p->tuples.contains(tmid) ) return 0;

  int count = p->tuples[tmid].values.count();

  p->tuples[tmid].values.clear();

  return count;
}

} // namespace Implementation
} // namespace SRPS
