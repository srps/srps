/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <SRPS/SettingsManager>
#include "curves_plugin.h"
#include "curves_curve.h"

using SRPS::Implementation::CurveFactory;

class SRPS::Implementation::CurveFactory::Private
{
public:
  Private():initialized(false),finalized(false){}
  bool initialized;
  bool finalized;
  QString lastError;
  QVariantHash defaultProperties;
};

CurveFactory::CurveFactory(QObject *parent) :
  QObject(parent),
  SRPS::CurveFactory(),
  p( new Private )
{
  p->defaultProperties.insert(Implementation::Curve::StaticDefaultVariableCountParameterName,
                              Implementation::Curve::StaticDefaultVariableCountParameterValue);
  p->defaultProperties.insert(Implementation::Curve::StaticDefaultTupleCountParameterName,
                              Implementation::Curve::StaticDefaultTupleCountParameterValue);
}

CurveFactory::~CurveFactory()
{
  delete p;
}

// Plugin
QString CurveFactory::errorString() const
{
  return p->lastError;
}

bool CurveFactory::isInitialized() const
{
  return p->initialized;
}

bool CurveFactory::isFinalized() const
{
  return p->finalized;
}

bool CurveFactory::pluginReadSettings( const SRPS::SettingsManager * sm )
{
  if( !sm->isValid() )
    return false;

  QVariantHash ps;
  ps[Implementation::Curve::StaticDefaultVariableCountParameterName]=
          sm->value(Implementation::Curve::StaticDefaultVariableCountParameterName,
                   Implementation::Curve::StaticDefaultVariableCountParameterValue);
  ps[Implementation::Curve::StaticDefaultTupleCountParameterName]=
          sm->value(Implementation::Curve::StaticDefaultTupleCountParameterName,
                   Implementation::Curve::StaticDefaultTupleCountParameterValue);
  return componentFactorySetDefaultParametersFor(Implementation::Curve::StaticClassName,ps);
}

bool CurveFactory::pluginInitialize()
{
  return true;
}

bool CurveFactory::pluginFinalize()
{
  return true;
}

bool CurveFactory::pluginWriteSettings( SRPS::SettingsManager * sm )
{
  if ( !sm->isValid() )
    return false;

  sm->setValue(Implementation::Curve::StaticDefaultVariableCountParameterName,
                p->defaultProperties.value(
                    Implementation::Curve::StaticDefaultVariableCountParameterName).toInt());
  sm->setValue(Implementation::Curve::StaticDefaultTupleCountParameterName,
                p->defaultProperties.value(
                    Implementation::Curve::StaticDefaultTupleCountParameterName).toInt());
  return true;
}

QVariantHash CurveFactory::componentFactoryDefaultParametersFor(const QString &key) const
{
  if ( key == Implementation::Curve::StaticClassName )
      return p->defaultProperties;
  return QVariantHash();
}

bool CurveFactory::componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters)
{
  if ( key != Implementation::Curve::StaticClassName )
    return false;

  int t = parameters.value(Implementation::Curve::StaticDefaultTupleCountParameterName,-1).toInt();
  if ( t < 0 )
    return false;

  int v = parameters.value(Implementation::Curve::StaticDefaultVariableCountParameterName,-1).toInt();
  if ( v < 0 )
    return false;

  p->defaultProperties[Implementation::Curve::StaticDefaultTupleCountParameterName] = t;
  p->defaultProperties[Implementation::Curve::StaticDefaultVariableCountParameterName] = v;
  return true;
}

// CurveFactory
QStringList CurveFactory::curveFactoryKeys() const
{
  return QStringList(SRPS::Implementation::Curve::StaticClassName);
}

SRPS::Curve * CurveFactory::curveFactoryCreate (const QString &curve,
                                                const QVariantHash &parameters)
{
  if( curve == Implementation::Curve::StaticClassName )
  {
      return new Implementation::Curve(parameters);
  }
  return 0;
}

Q_EXPORT_PLUGIN2(SRPSCurves,SRPS::Implementation::CurveFactory)
