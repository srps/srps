/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVES_PLUGIN_H
#define SRPS_CURVES_PLUGIN_H

#include <QtCore/QObject>
#include <SRPS/Plugin>
#include <SRPS/ComponentFactory>
#include <SRPS/CurveFactory>

namespace SRPS {
namespace Implementation {

class CurveFactory :
    public QObject,
    public SRPS::Plugin,
    public SRPS::ComponentFactory,
    public SRPS::CurveFactory
{
    Q_OBJECT
    Q_INTERFACES(SRPS::Plugin SRPS::CurveFactory SRPS::ComponentFactory)

    class Private;
    Private * p;

public:
    explicit CurveFactory(QObject *parent = 0);
    virtual ~CurveFactory();

    // Plugin
    QString errorString() const;
    bool isInitialized() const;
    bool isFinalized() const;
    bool pluginReadSettings( const SettingsManager * sm );
    bool pluginInitialize();
    bool pluginFinalize();
    bool pluginWriteSettings( SettingsManager * sm );

    // Factory
    QVariantHash componentFactoryDefaultParametersFor(const QString &key) const;
    bool componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters);

    // CurveFactory
    QStringList curveFactoryKeys() const;
    SRPS::Curve * curveFactoryCreate (const QString &curve,
                                          const QVariantHash &parameters=
                                                    QVariantHash());
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_CURVES_PLUGIN_H
