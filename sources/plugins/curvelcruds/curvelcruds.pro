include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}CurveLCRUDs
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += curvelcruds_plugin.cpp \
    curvelcruds_sqlite.cpp \
    curvelcruds_sqlitefactorywidget.cpp
HEADERS += curvelcruds_plugin.h \
    curvelcruds_sqlite.h \
    curvelcruds_sqlitefactorywidget.h
QT += sql

FORMS += \
    curvelcruds_sqlitefactorywidget.ui
