/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef CURVEXMLLCRUD_H
#define CURVEXMLLCRUD_H

#include <SRPS/Curve>
#include <SRPS/CurveLCRUD>

#include <QtSql>

namespace SRPS {
namespace Implementation {

class CurveSqliteLCRUD : public SRPS::CurveLCRUD
{
  Q_DISABLE_COPY(CurveSqliteLCRUD)

  QSqlDatabase database;

  public:
    explicit CurveSqliteLCRUD();
    virtual ~CurveSqliteLCRUD();

    static const QString StaticClassName;

    QString componentClassName() const;

    int prepareDatabase( QSqlDatabase & database );

    QStringList curveList() const;

    QString curveCreate( const SRPS::Curve * curve );

    int curveCreate( const QString & key, SRPS::Curve * curve );

    int curveUpdate( const QString & key, const SRPS::Curve * curve );

    int curveDelete( const QString & key );
};

} // namespace Implementation
} // namespace SRPS

#endif // CURVEXMLLCRUD_H
