/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curvelcruds_plugin.h"

#include <QtCore/QStringList>
#include "curvelcruds_sqlite.h"

namespace SRPS {
namespace Implementation {

CurveLCRUDFactory::CurveLCRUDFactory( QObject * parent )
  : QObject( parent )
{
}

CurveLCRUDFactory::~CurveLCRUDFactory()
{
}

QStringList CurveLCRUDFactory::curveLCRUDFactoryKeys() const
{
    return QStringList(Implementation::CurveSqliteLCRUD::StaticClassName);
}

SRPS::CurveLCRUD * CurveLCRUDFactory::curveLCRUDFactoryCreate( const QString & curveLCRUDClass,
                                               const QVariantHash & parameters)
{
//    if ( curveLCRUDClass == Implementation::CurveSqliteLCRUD::StaticClassName)
//      return new CurveSqliteLCRUD(parameters);
    return 0;
}

} // namespace Implementation
} // namespace SRPS

Q_EXPORT_PLUGIN2( SRPSCurveLCRUDs, SRPS::Implementation::CurveLCRUDFactory );
