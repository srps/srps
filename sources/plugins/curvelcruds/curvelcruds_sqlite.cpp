/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curvelcruds_sqlite.h"

#include <QtCore/QDebug>
#include <QtCore/QDateTime>

namespace SRPS {
namespace Implementation {

CurveSqliteLCRUD::CurveSqliteLCRUD()
  : CurveLCRUD()
{
}

CurveSqliteLCRUD::~CurveSqliteLCRUD()
{
}

const QString CurveSqliteLCRUD::StaticClassName =  QLatin1String("srps.curve.lcrud.sqlite");

QString CurveSqliteLCRUD::componentClassName() const
{
    return StaticClassName;
}

int CurveSqliteLCRUD::prepareDatabase( QSqlDatabase & database )
{
  if ( !database.isOpen() )
    return registerLastOperationResult(-1,tr("Database not opened"));

  QStringList tables = database.tables();

  if ( !tables.contains("srps") )
    return registerLastOperationResult(-1,tr("No srps table"));

  if ( !tables.contains("srps_curve") )
    return registerLastOperationResult(-1,tr("No srps_curve table"));

  if ( !tables.contains("srps_curve_property") )
    return registerLastOperationResult(-1,tr("No srps_curve_property table"));

  if ( !tables.contains("srps_curve_variable") )
    return registerLastOperationResult(-1,tr("No srps_curve_variable table"));

  if ( !tables.contains("srps_curve_variable_property") )
    return registerLastOperationResult(-1,tr("No srps_curve_variable_property table"));

  if ( !tables.contains("srps_curve_tuple") )
    return registerLastOperationResult(-1,tr("No srps_curve_tuple table"));

  if ( !tables.contains("srps_curve_tuple_property") )
    return registerLastOperationResult(-1,tr("No srps_curve_tuple_property table"));

  this->database = database;

  return registerLastOperationResult(0,tr("Database prepared"));
}

QStringList CurveSqliteLCRUD::curveList() const
{
  if ( !database.isOpen() ) return QStringList();

  QSqlQuery q = database.exec("SELECT id FROM srps");

  if( !q.isActive() ) return QStringList();

  q.setForwardOnly(true);

  QStringList r;
  while(q.next())
  {
    QVariant v =  q.value(0);
    if ( v.isValid() && v.toString().size() )
      r.append(v.toString());
  }

  return r;
}

QString CurveSqliteLCRUD::curveCreate( const SRPS::Curve * curve )
{
  if (!curve) return QString();

  if (!database.isOpen()) return QString();

  QString key = QDateTime::currentDateTime().toString("yyMMddhhmmsszzz");

  QSqlQuery creation =
      database.exec(QString("INSERT INTO srps_curve VALUES (NULL,'%1')").arg(key));
  Q_UNUSED(creation);

  QSqlQuery check_creation =
      database.exec(QString("SELECT id FROM srps_curve WHERE key='%1").arg(key));

  bool ok;
  int ikey = -1;
  while(check_creation.next())
    ikey = check_creation.value(0).toInt(&ok);

  if (!ok) return QString();

  // created.
}

int curveCreate( const QString & key, SRPS::Curve * curve )
{

}

int curveUpdate( const QString & key, const SRPS::Curve * curve );

int curveDelete( const QString & key );

} // namespace Implementation
} // namespace SRPS
