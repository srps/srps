include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}CurveRWs
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += curverws_plugin.cpp \
    curverws_xsv.cpp \
    curverws_xml.cpp
HEADERS += curverws_plugin.h \
    curverws_xsv.h \
    curverws_xml.h
QT += xml

RESOURCES += \
    resources.qrc
