/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef CURVEXMLRW_H
#define CURVEXMLRW_H

#include <SRPS/Curve>
#include <SRPS/CurveRW>

namespace SRPS {
namespace Implementation {

class CurveXmlRW : public SRPS::CurveRW
{
  Q_DISABLE_COPY(CurveXmlRW)
  QString xmlError;
  SRPS::Curve * read_curve;
  QIODevice * read_device;
  const SRPS::Curve * write_curve;
  QIODevice * write_device;
  public:
    explicit CurveXmlRW();
    virtual ~CurveXmlRW();

    static const QString StaticClassName;

    QString componentClassName() const;

    int prepareCurveRead( QIODevice * device, SRPS::Curve * curve );

    int readCurve();

    int prepareCurveWrite( const SRPS::Curve * curve, QIODevice * device );

    int writeCurve();
};

} // namespace Implementation
} // namespace SRPS

#endif // CURVEXMLRW_H
