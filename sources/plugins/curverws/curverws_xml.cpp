/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curverws_xml.h"

#include <QtCore/QBuffer>
#include <QtXml/QDomDocument>
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

static const QString NAMESPACE = QString("srps");
static const QString NAMESPACEURI = QString(":/srps/curve/rw/xml");
static const QString NAMESPACEPREFIX = QString("srps");
static const QString ROOT = QString("curve");
static const QString VARIABLE = QString("variable");
static const QString TUPLE = QString("tuple");
static const QString VALUE = QString("value");
static const QString NAME = QString("srps.name");
static const QString DESCRIPTION = QString("description");
static const QString PROPERTY = QString("property");

CurveXmlRW::CurveXmlRW()
  : CurveRW()
{
  read_curve = 0;
  read_device = 0;

  write_curve = 0;
  write_device = 0;
}

CurveXmlRW::~CurveXmlRW()
{
}

const QString CurveXmlRW::StaticClassName =  QLatin1String("srps.curve.rw.txt.xml");

QString CurveXmlRW::componentClassName() const
{
    return StaticClassName;
}

int CurveXmlRW::prepareCurveRead( QIODevice * _device, SRPS::Curve * _curve )
{
  if ( !_curve )
    return 100; //tr("SRPS::Curve is null.");

  if ( ! _device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( _device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  read_curve = _curve;
  read_device = _device;

  return 0;
}

int CurveXmlRW::readCurve()
{
  if ( !read_curve )
    return 100; //tr("SRPS::Curve is null.");

  if ( ! read_device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( read_device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  QDomDocument document;
  xmlError.clear();
  if ( !document.setContent(read_device,&xmlError) )
    return 300;

  QDomElement root = document.documentElement();
  if ( root.tagName() != ROOT )
    return 301;

  QDomElement element = root.firstChildElement();

  Curve::MIDList variables;
  Curve::MIDList tuples;

  while(!element.isNull())
  {
    if ( element.tagName() == PROPERTY )
    {
      QString name = element.attribute("srps.name");
      if ( !name.isEmpty() )
        read_curve->componentSetProperty(name,element.text());
    }
    else if ( element.tagName() == VARIABLE )
    {
      QDomElement variableElement = element.firstChildElement();
      Curve::MemoryIdentifier variable = read_curve->createVariableMID();
      variables << variable;
      while( !variableElement.isNull() )
      {
        if ( variableElement.tagName() == PROPERTY )
        {
          QString name = variableElement.attribute("srps.name");
          if ( !name.isEmpty() )
            read_curve->variableSetProperty(variable,name,variableElement.text());
        }
        variableElement = variableElement.nextSiblingElement();
      }
    }
    else if ( element.tagName() == TUPLE )
    {
      QDomElement tupleElement = element.firstChildElement();
      Curve::MemoryIdentifier tuple = read_curve->createTupleMID();
      tuples << tuple;
      while( !tupleElement.isNull() )
      {
        if ( tupleElement.tagName() == PROPERTY )
        {
          QString name = tupleElement.attribute("srps.name");
          if ( !name.isEmpty() )
            read_curve->tupleSetProperty(tuple,name,tupleElement.text());
        }
        else if ( tupleElement.tagName() == VALUE )
        {
          bool ok;
          int index = tupleElement.attribute(VARIABLE).toInt(&ok);
          if ( ok && index >= 0 && index < variables.count() )
            read_curve->tupleSetValue(tuple,variables.at(index),tupleElement.text());
        }
        tupleElement = tupleElement.nextSiblingElement();
      }
    }

    element = element.nextSiblingElement();
  }

  read_device = 0;
  read_curve = 0;

  return 0;
}

int CurveXmlRW::prepareCurveWrite( const SRPS::Curve * curve, QIODevice * device )
{
  if ( !curve )
    return 100;//tr("SRPS::Curve is null.");

  if ( ! device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  write_device = device;
  write_curve = curve;

  return 0;
}

int CurveXmlRW::writeCurve( )
{
  if ( !write_curve )
    return 100;//tr("SRPS::Curve is null.");

  if ( ! write_device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( write_device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  QBuffer buffer;
  if ( ! buffer.open( QIODevice::WriteOnly ) )
    return 400;//tr("Interal QBuffer cant write.");
  {
    QDomDocument document;
    QDomElement root = document.createElement(ROOT);
    document.appendChild(root);

    foreach( QString pname, write_curve->componentPropertyNames() )
    {
      QDomElement property = document.createElement(PROPERTY);
      property.setAttribute("srps.name",pname);
      QDomText text = document.createTextNode(write_curve->componentProperty(pname).toString());
      property.appendChild(text);
      root.appendChild(property);
    }

    Curve::MIDList variables = write_curve->variableMIDs(Curve::CreationOrder); // lista con orden
    foreach( Curve::MemoryIdentifier variable, variables )
    {
      QDomElement variableElement = document.createElement(VARIABLE);
      foreach( QString pname, write_curve->variablePropertyNames(variable) )
      {
        QDomText text = document.createTextNode(write_curve->variableProperty(variable,
                                                                        pname).toString());
        QDomElement property = document.createElement(PROPERTY);
        property.setAttribute("srps.name",pname);
        property.appendChild(text);
        variableElement.appendChild(property);
      }
      root.appendChild(variableElement);
    }

    Curve::MIDList tuples = write_curve->tupleMIDs(Curve::CreationOrder); // tuplas con orden

    foreach( Curve::MemoryIdentifier tuple, tuples )
    {
      QDomElement tupleElement = document.createElement(TUPLE);
      foreach( QString pname, write_curve->tuplePropertyNames(tuple) )
      {
        QDomText text = document.createTextNode(write_curve->tupleProperty(tuple,
                                                                     pname).toString());
        QDomElement property = document.createElement(PROPERTY);
        property.setAttribute("srps.name",pname);
        property.appendChild(text);
        tupleElement.appendChild(property);
      }
      foreach( Curve::MemoryIdentifier variable, write_curve->tupleVariableMIDs(tuple,Curve::CreationOrder) )
      {
        QDomElement valueElement = document.createElement(VALUE);
        valueElement.setAttribute("variable",variables.indexOf(variable));
        QDomText text = document.createTextNode(
            write_curve->tupleValue(tuple,variable).toString());
        valueElement.appendChild(text);
        tupleElement.appendChild(valueElement);
      }
      root.appendChild(tupleElement);
    }

    QTextStream stream(&buffer);
    stream << document.toString();
  }

  if ( write_device->write( buffer.data() ) != buffer.data().length() )
    return 401;//tr("Cant write buffer to write_device");

  return 0;//;QString();
}

} // namespace Implementation
} // namespace SRPS
