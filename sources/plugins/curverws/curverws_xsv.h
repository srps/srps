/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_IMPLEMENTATION_CURVEXSVRW_H
#define SRPS_IMPLEMENTATION_CURVEXSVRW_H

#include <SRPS/Curve>
#include <SRPS/CurveRW>

namespace SRPS {
namespace Implementation {

class CurveXSVRW : public SRPS::CurveRW
{
    Q_DISABLE_COPY(CurveXSVRW)

    QString p_separator;
    SRPS::Curve * read_curve;
    QIODevice * read_device;
    const SRPS::Curve * write_curve;
    QIODevice * write_device;

public:

    CurveXSVRW();
    virtual ~CurveXSVRW();

    QString separator() const;
    void setSeparator( const QString & );

    static const QString StaticClassName;

    QString componentClassName() const;

    int prepareCurveRead( QIODevice * device, SRPS::Curve * curve );

    int readCurve();

    int prepareCurveWrite( const SRPS::Curve * curve, QIODevice * device );

    int writeCurve();
};

} // namespace Implementation
} // namespace SRPS

#endif // CURVEXSVRW_H
