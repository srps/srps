/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curverws_plugin.h"

#include <QtCore/QStringList>
#include "curverws_xsv.h"
#include "curverws_xml.h"

namespace SRPS {
namespace Implementation {

CurveRWFactory::CurveRWFactory( QObject * parent )
  : QObject( parent )
{
}

CurveRWFactory::~CurveRWFactory()
{
}

QStringList CurveRWFactory::curveRWFactoryKeys() const
{
    return QStringList(Implementation::CurveXmlRW::StaticClassName)
        <<Implementation::CurveXSVRW::StaticClassName;
}

SRPS::CurveRW * CurveRWFactory::curveRWFactoryCreate( const QString & curveRWClass,
                                               const QVariantHash & /*parameters*/)
{
    if ( curveRWClass == Implementation::CurveXSVRW::StaticClassName)
      return new CurveXSVRW;
    if ( curveRWClass == Implementation::CurveXmlRW::StaticClassName )
      return new CurveXmlRW;
    return 0;
}

} // namespace Implementation
} // namespace SRPS

Q_EXPORT_PLUGIN2( SRPSCurveRWs, SRPS::Implementation::CurveRWFactory );
