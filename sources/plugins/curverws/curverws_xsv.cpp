/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curverws_xsv.h"

#include <QtCore/QBuffer>
#include <QtCore/QTextStream>
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

CurveXSVRW::CurveXSVRW():
    p_separator("|")
{
  read_curve = 0;
  read_device = 0;

  write_curve = 0;
  write_device = 0;
}

CurveXSVRW::~CurveXSVRW()
{
}

QString CurveXSVRW::separator() const
{
  return p_separator;
}

void CurveXSVRW::setSeparator( const QString & s )
{
  if ( !s.isEmpty() )
    p_separator = s;
}

const QString CurveXSVRW::StaticClassName = QString("srps.curve.rw.xsv");

QString CurveXSVRW::componentClassName() const
{
    return StaticClassName;
}

int CurveXSVRW::prepareCurveRead(QIODevice * device, SRPS::Curve * curve )
{
  if ( !curve )
    return 100;//tr("SRPS::Curve is null.");

  if ( ! device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  read_device = device;
  read_curve = curve;

  return 0;
}

int CurveXSVRW::readCurve()
{
  if ( !read_curve )
    return 100;//tr("SRPS::Curve is null.");

  if ( ! read_device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( read_device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  QTextStream reader( read_device );
  QString f_separator = reader.readLine();
  if ( f_separator.isEmpty() )
    return 300; //tr("No separator found.");
  QStringList vars = reader.readLine().split(f_separator,QString::KeepEmptyParts);
  Curve::MIDList variables;
  foreach( QString var, vars )
  {
    Curve::MemoryIdentifier myvar =read_curve->createVariableMID();
    variables << myvar;
    read_curve->variableSetProperty(myvar,"name",var);
  }
  QString line = reader.readLine();
  while( !line.isEmpty() )
  {
    QStringList values = line.split(f_separator,QString::KeepEmptyParts);
    int min = qMin( variables.count(),values.count());
    Curve::MemoryIdentifier tuple = read_curve->createTupleMID();
    for( int v = 0; v < min; ++v )
      qDebug() << Q_FUNC_INFO << "103" << read_curve->tupleSetValue(tuple,variables.at(v), values.at(v));
    line = reader.readLine();
  }

  read_device = 0;
  read_curve = 0;

  return 0; //QString();
}

int CurveXSVRW::prepareCurveWrite(const SRPS::Curve *curve, QIODevice *device)
{
  if ( !curve )
    return 100;//tr("SRPS::Curve is null.");

  if ( ! device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  write_curve = curve;
  write_device = device;

  return 0;
}

int CurveXSVRW::writeCurve()
{
  if ( !write_curve )
    return 100;//tr("SRPS::Curve is null.");

  if ( ! write_device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( write_device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  QBuffer buffer;
  if ( ! buffer.open( QIODevice::WriteOnly ) )
    return 400;//tr("Interal QBuffer cant write.");
  {
    QTextStream writer( &buffer );
    writer.setCodec("UTF-8");

    writer << separator() << endl;
    Curve::MIDList variables = write_curve->variableMIDs(Curve::CreationOrder);
    for( int v = 0; v < variables.count(); ++v )
    {
      writer << write_curve->variableProperty(variables.at(v),"name").toString();
      if ( v < variables.count() - 1 )
        writer << separator();
    }
    writer << endl;
    Curve::MIDList tuples = write_curve->tupleMIDs(Curve::CreationOrder);
    for( int t = 0; t <  tuples.count(); ++t )
    {
      for( int v = 0; v < variables.count(); ++v )
      {
        writer << write_curve->tupleValue(tuples.at(t),variables.at(v)).toString();
        if ( v < variables.count() - 1 )
          writer << separator();
      }
      if ( t < tuples.count() -1 )
      writer << endl;
    }
  }

  if ( write_device->write( buffer.data() ) != buffer.data().length() )
    return 203;//tr("Cant write buffer to write_device");

  write_curve = 0;
  write_device = 0;
  return 0;//QString();
}

} // namespace Implementation
} // namespace SRPS
