TEMPLATE = subdirs
CONFIG  += ordered
SUBDIRS += \
    curves \
    curveeditors \
    curvevisualizers \
    curverws \
    spaces \
    spacegenerators \
    spaceeditors \
    spacerws \
    prngs \
    dprngs \
    simulators \
    models \
    accumulators \
    gui
