#ifndef DPRNGS_NORMALDPRNGFACTORYWIDGET_H
#define DPRNGS_NORMALDPRNGFACTORYWIDGET_H

#include <QtGui/QWidget>
#include <SRPS/ComponentFactoryWidget>

#include "qwt_plot_curve.h"

namespace SRPS {
namespace Implementation {

namespace Ui {
    class NormalDPRNGFactoryWidget;
}

class NormalDPRNGFactoryWidget : public QWidget, public ComponentFactoryWidget
{
    Q_OBJECT
  Q_DISABLE_COPY(NormalDPRNGFactoryWidget)
      Q_INTERFACES(SRPS::ComponentFactoryWidget)

      QwtPlotCurve curve;
      QVector<double> x;
      QVector<double> y;


public:
    explicit NormalDPRNGFactoryWidget(QWidget *parent = 0);
    ~NormalDPRNGFactoryWidget();

    QVariantHash componentFactoryWidgetParameters() const;
    bool componentFactoryWidgetSetParameters(const QVariantHash & parameters);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::NormalDPRNGFactoryWidget *ui;

private slots:
    void on_horizontalSlider_valueChanged(int value);
    void on_doubleSpinBox_valueChanged(double );

    void updatePlot();
};


} // namespace Implementation
} // namespace SRPS
#endif // DPRNGS_NORMALDPRNGFACTORYWIDGET_H
