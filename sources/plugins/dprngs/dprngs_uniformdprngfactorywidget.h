#ifndef DPRNGS_UNIFORMDPRNGFACTORYWIDGET_H
#define DPRNGS_UNIFORMDPRNGFACTORYWIDGET_H

#include <SRPS/ComponentFactoryWidget>
#include <QtGui/QWidget>

namespace SRPS {
namespace Implementation {

namespace Ui {
    class UniformDPRNGFactoryWidget;
}

class UniformDPRNGFactoryWidget : public QWidget, public SRPS::ComponentFactoryWidget
{
    Q_OBJECT
  Q_DISABLE_COPY(UniformDPRNGFactoryWidget)
      Q_INTERFACES(SRPS::ComponentFactoryWidget)

public:
    explicit UniformDPRNGFactoryWidget(QWidget *parent = 0);
    ~UniformDPRNGFactoryWidget();

    QVariantHash componentFactoryWidgetParameters() const;
    bool componentFactoryWidgetSetParameters(const QVariantHash & parameters);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::UniformDPRNGFactoryWidget *ui;
};


} // namespace Implementation
} // namespace SRPS
#endif // DPRNGS_UNIFORMDPRNGFACTORYWIDGET_H
