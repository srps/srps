#include "dprngs_normaldprngfactorywidget.h"
#include "ui_dprngs_normaldprngfactorywidget.h"

#include "dprngs_normal.h"

#include <cmath>

namespace SRPS {
namespace Implementation {

NormalDPRNGFactoryWidget::NormalDPRNGFactoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NormalDPRNGFactoryWidget)
{
    ui->setupUi(this);

    curve.attach(ui->qwtPlot);

    connect(ui->doubleSpinBox,SIGNAL(valueChanged(double)),
            this,SLOT(updatePlot()));
    connect(ui->varianceDoubleSpinBox,SIGNAL(valueChanged(double)),
            this,SLOT(updatePlot()));
    connect(ui->intervalSpin,SIGNAL(valueChanged(double)),
            this,SLOT(updatePlot()));

    updatePlot();
}

NormalDPRNGFactoryWidget::~NormalDPRNGFactoryWidget()
{
  curve.detach();
    delete ui;
}

QVariantHash NormalDPRNGFactoryWidget::componentFactoryWidgetParameters() const
{
  QVariantHash p;
  p.insert(NormalDPRNG::DOName,ui->varianceDoubleSpinBox->value());
  p.insert(NormalDPRNG::RUName,ui->doubleSpinBox->value()/100);
  p.insert(NormalDPRNG::ICName,ui->intervalSpin->value());
  return p;
}

bool NormalDPRNGFactoryWidget::componentFactoryWidgetSetParameters(
    const QVariantHash & p)
{
  if( !p.contains(NormalDPRNG::DOName) )
    return false;

  ui->varianceDoubleSpinBox->setValue(
      p.value(NormalDPRNG::DOName,NormalDPRNG::DOVal).toDouble());

  ui->doubleSpinBox->setValue(
      p.value(NormalDPRNG::RUName,NormalDPRNG::RUVal).toDouble()*100);

  ui->intervalSpin->setValue(
      p.value(NormalDPRNG::ICName,NormalDPRNG::ICVal).toDouble());

  return true;
}

void NormalDPRNGFactoryWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SRPS::Implementation::NormalDPRNGFactoryWidget::on_doubleSpinBox_valueChanged(double d)
{
    ui->horizontalSlider->setValue( int(d*100) );
}

void SRPS::Implementation::NormalDPRNGFactoryWidget::on_horizontalSlider_valueChanged(int value)
{
  double val = value;
  val/=100;
  ui->doubleSpinBox->setValue(val);
}

double evalNormal( double sigma_1, double mean, double x )
{
  static double const PI = 3.14159265359;
  return 1.0/(sigma_1*std::sqrt(2*PI))*std::exp((-0.5)*pow((x-mean)/sigma_1,2));
}

void NormalDPRNGFactoryWidget::updatePlot()
{
  double relative = ui->doubleSpinBox->value();
  double sigma_1 =std::sqrt(ui->varianceDoubleSpinBox->value());
  double left = -sigma_1*ui->intervalSpin->value();
  double right = -1*left;
  double length = right - left;

  double mean = length*relative/100+left;

  x.resize(200);;
  y.resize(200);
  for( int  i = 0; i < 200; ++i )
  {
    x[i] = left + length/200*i;
    y[i] = evalNormal(sigma_1,mean,x[i]);
  }

  curve.setData(x,y);
  ui->qwtPlot->setAxisScale(QwtPlot::xBottom,x.first(),x.last());
  ui->qwtPlot->replot();
}

} // namespace Implementation
} // namespace SRPS
