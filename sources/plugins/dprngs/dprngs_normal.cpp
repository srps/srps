/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "dprngs_normal.h"

#include <SRPS/PRNG>
#include <cmath>

#include <QDebug>

namespace SRPS {
namespace Implementation {

const QString NormalDPRNG::RUName = QLatin1String("srps.dprng.normal.relative_mean");
const QString NormalDPRNG::DOName = QLatin1String("srps.dprng.normal.variance");
const QString NormalDPRNG::ICName = QLatin1String("srps.dprng.normal.confidence_interval");

const double NormalDPRNG::RUVal = .5;
const double NormalDPRNG::DOVal = 1;
const double NormalDPRNG::ICVal = 3;

NormalDPRNG::NormalDPRNG(const QVariantHash & p)
  : p(0)
{
  n[0]=0;
  n[1]=0;
  r[0]=0;
  r[1]=0;
  y = 0;
  w = 0;

  counter = 2;

  p_o = 1;
  r_u = .5;
  c_i = 3;
  recalculate_interval();

  bool ok;
  double val = p.value(DOName,"na").toDouble(&ok);
  if ( ok ) setO(std::sqrt(val));

  double R = p.value(RUName,"na").toDouble(&ok);
  if( ok ) setRU(R);

  double IC = p.value(ICName,"na").toDouble(&ok);
  if( ok ) setIC(IC);
}

NormalDPRNG::~NormalDPRNG()
{
}

const QString NormalDPRNG::StaticClassName = QLatin1String("srps.dprng.normal");


QString NormalDPRNG::componentClassName() const
{
  return StaticClassName;
}

bool NormalDPRNG::componentHasProperty( const QString & name ) const
{
  if ( name == QLatin1String("mean") || name == QLatin1String("variance") )
    return true;

  return false;
}

int NormalDPRNG::componentPropertyCount() const
{
  return 2;
}

QStringList NormalDPRNG::componentPropertyNames() const
{
  return QStringList() << QLatin1String("mean") << QLatin1String("variance");
}

QVariant NormalDPRNG::componentProperty( const QString & name,
                                         const QVariant & defaultValue ) const
{
  if ( name == QLatin1String("variance") )
    return o();
  return defaultValue;
}

int NormalDPRNG::componentSetProperty( const QString & name,
                                        const QVariant & value )
{
  bool ok;
  double val = value.toDouble(&ok);

  if ( name == QLatin1String("variance") && ok )
  {
    setO(val);
    return 0;
  }
  return -1;
}

SRPS::PRNG * NormalDPRNG::prng() const
{
  return p;
}

bool NormalDPRNG::setPRNG( SRPS::PRNG * pr )
{
  p = pr;
  return true;
}

NormalDPRNG::Range NormalDPRNG::range() const
{
  return Range(r_l,r_r);
}

bool NormalDPRNG::setRange( const Range & range )
{
  r_l = range.min();
  r_r = range.max();
  return true;
}

double NormalDPRNG::ru() const
{
  return r_u;
}

void NormalDPRNG::setRU(double ru)
{
  r_u = ru;
  if ( r_u < 0 )
    r_u = 0;
  if ( r_u > 1)
    r_u = 1;
  recalculate_interval();
}

double NormalDPRNG::nextOO()
{
  while( true )
  {
    if ( counter == 2 )
    {
      generate();
      counter = 0;
    }
    double result = p_u + r[counter++] * p_o;

    if ( result > i_r_l && result < i_r_r )
      return transform(result);
  }
}

double NormalDPRNG::nextOC()
{
  while( true )
  {
    if ( counter == 2 )
    {
      generate();
      counter = 0;
    }
    double result = p_u + r[counter++] * p_o;

    if ( result > i_r_l && result <= i_r_r )
      return transform(result);
  }
}

double NormalDPRNG::nextCO()
{
  while( true )
  {
    if ( counter == 2 )
    {
      generate();
      counter = 0;
    }
    double result = p_u + r[counter++] * p_o;

    if ( result >= i_r_l && result < i_r_r )
      return transform(result);
  }
}

double NormalDPRNG::nextCC()
{
  while( true )
  {
    if ( counter == 2 )
    {
      generate();
      counter = 0;
    }
    double result = p_u + r[counter++] * p_o;

    if ( result >= i_r_l && result <= i_r_r )
      return transform(result);
  }
}

double NormalDPRNG::o() const
{
  return p_o;
}

void NormalDPRNG::setO( double _o )
{
  p_o = _o;
  recalculate_interval();
}

double NormalDPRNG::ic() const
{
  return c_i;
}

void NormalDPRNG::setIC(double _ic)
{
  c_i = _ic;
  recalculate_interval();
}

double NormalDPRNG::transform( double val ) const
{
  return (r_r-r_l)/(i_r_r - i_r_l)*(val - i_r_l)+r_l;
}

void NormalDPRNG::recalculate_interval()
{
  i_r_l = -p_o*c_i;
  i_r_r = -i_r_l;

  p_u = (i_r_r - i_r_l)*r_u+i_r_l;
}

void NormalDPRNG::generate()
{
  do
  {
    n[0] = 2.0*p->nextCC()-1;
    n[1] = 2.0*p->nextCC()-1;
    w = n[0]*n[0] + n[1]*n[1];
  }while( w >= 1.0 );

  y = std::sqrt((-2*std::log(w))/w);
  r[0] = n[0]*y;
  r[1] = n[1]*y;
}

} // namespace Implementation
} // namespace SRPS
