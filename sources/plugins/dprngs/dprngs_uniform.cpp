/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "dprngs_uniform.h"

#include <SRPS/PRNG>

namespace SRPS {
namespace Implementation {

UniformDPRNG::UniformDPRNG(const QVariantHash & p)
  : p(0),
    min(0),
    max(1),
    diff(1)
{
  Range r;

  bool ok;
  double val = p.value(DMaxName).toDouble(&ok);
  if ( ok ) r.setMax(val);

  val = p.value(DMinName).toDouble(&ok);
  if ( ok ) r.setMin(val);

  setRange(r);
}

UniformDPRNG::~UniformDPRNG()
{
}

const QString UniformDPRNG::StaticClassName
    = QLatin1String("srps.dprng.uniform");

const QString UniformDPRNG::DMinName = QLatin1String("srps.dprng.uniform.min");
const QString UniformDPRNG::DMaxName = QLatin1String("srps.dprng.uniform.max");
const double UniformDPRNG::DMinVal = 0;
const double UniformDPRNG::DMaxVal = 1;

QString UniformDPRNG::componentClassName() const
{
  return StaticClassName;
}

double UniformDPRNG::nextOO()
{
  return min+p->nextOO()*diff;
}

double UniformDPRNG::nextOC()
{
  return min+p->nextOC()*diff;
}

double UniformDPRNG::nextCO()
{
  return min+p->nextCO()*diff;
}

double UniformDPRNG::nextCC()
{
  return min+p->nextCC()*diff;
}

SRPS::PRNG * UniformDPRNG::prng() const
{
  return p;
}

bool UniformDPRNG::setPRNG( SRPS::PRNG * pr )
{
  p = pr;
  return true;
}

UniformDPRNG::Range UniformDPRNG::range() const
{
  return Range(min,max);
}

bool UniformDPRNG::setRange( const Range & range )
{
  min = range.min();
  max = range.max();
  diff = range.length();
  return true;
}

} // namespace Implementation
} // namespace SRPS
