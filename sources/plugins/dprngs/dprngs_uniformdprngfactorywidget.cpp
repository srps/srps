#include "dprngs_uniformdprngfactorywidget.h"
#include "ui_dprngs_uniformdprngfactorywidget.h"

#include "dprngs_uniform.h"

namespace SRPS {
namespace Implementation {

UniformDPRNGFactoryWidget::UniformDPRNGFactoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UniformDPRNGFactoryWidget)
{
    ui->setupUi(this);
}

UniformDPRNGFactoryWidget::~UniformDPRNGFactoryWidget()
{
    delete ui;
}

QVariantHash UniformDPRNGFactoryWidget::componentFactoryWidgetParameters() const
{
  QVariantHash p;
  p.insert(UniformDPRNG::DMaxName,ui->maxDoubleSpinBox->value());
  p.insert(UniformDPRNG::DMinName,ui->minDoubleSpinBox->value());
  return p;
}

bool UniformDPRNGFactoryWidget::componentFactoryWidgetSetParameters(
    const QVariantHash & p)
{
  if( !p.contains(UniformDPRNG::DMaxName) ||
      !p.contains(UniformDPRNG::DMaxName) )
    return false;

  ui->maxDoubleSpinBox->setValue(
      p.value(UniformDPRNG::DMaxName,UniformDPRNG::DMaxVal).toDouble());

  ui->minDoubleSpinBox->setValue(
      p.value(UniformDPRNG::DMinName,UniformDPRNG::DMinVal).toDouble());

  return true;
}

void UniformDPRNGFactoryWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS
