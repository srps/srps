include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}DPRNGs
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += dprngs_plugin.cpp \
    dprngs_uniform.cpp \
    dprngs_normal.cpp \
    dprngs_uniformdprngfactorywidget.cpp \
    dprngs_normaldprngfactorywidget.cpp
HEADERS += dprngs_plugin.h \
    dprngs_uniform.h \
    dprngs_normal.h \
    dprngs_uniformdprngfactorywidget.h \
    dprngs_normaldprngfactorywidget.h

FORMS += \
    dprngs_uniformdprngfactorywidget.ui \
    dprngs_normaldprngfactorywidget.ui

INCLUDEPATH *= ../../../sources/extra/qwt/src

LIBS *= -L../../../

unix:LIBS += -lqwt

win32 {
  CONFIG(debug,debug|release){
    LIBS    *= -lqwtd5
  }else {
    LIBS    *= -lqwt5
  }
}

RESOURCES += \
    resources.qrc
