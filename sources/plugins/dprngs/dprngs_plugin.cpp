/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "dprngs_uniform.h"
#include "dprngs_normal.h"
#include "dprngs_uniformdprngfactorywidget.h"
#include "dprngs_normaldprngfactorywidget.h"

#include "dprngs_plugin.h"

#include <SRPS/SettingsManager>
#include <QtCore/QStringList>

namespace SRPS {
namespace Implementation {


class DPRNGFactory::Private
{
public:
  Private():initialized(false),finalized(false){}
  bool initialized;
  bool finalized;
  QString lastError;
  QVariantHash uniformP;
  QVariantHash normalP;
};

DPRNGFactory::DPRNGFactory( QObject * parent )
  : QObject( parent )
{
  p = new Private;

  p->uniformP.insert(UniformDPRNG::DMaxName,UniformDPRNG::DMaxVal);
  p->uniformP.insert(UniformDPRNG::DMinName,UniformDPRNG::DMinVal);

  p->normalP.insert(NormalDPRNG::RUName,NormalDPRNG::RUVal);
  p->normalP.insert(NormalDPRNG::DOName,NormalDPRNG::DOVal);
}

DPRNGFactory::~DPRNGFactory()
{
  delete p;
}

// Plugin
QString DPRNGFactory::errorString() const
{
  return p->lastError;
}

bool DPRNGFactory::isInitialized() const
{
  return p->initialized;
}

bool DPRNGFactory::isFinalized() const
{
  return p->finalized;
}

bool DPRNGFactory::pluginReadSettings( const SRPS::SettingsManager * sm )
{
  if( !sm->isValid() )
    return false;

  QVariantHash ps;

  ps[UniformDPRNG::DMaxName]=
      sm->value(UniformDPRNG::DMaxName,UniformDPRNG::DMaxVal);
  ps[UniformDPRNG::DMinName]=
      sm->value(UniformDPRNG::DMinName,UniformDPRNG::DMinVal);

  QVariantHash ps2;

  ps2[NormalDPRNG::RUName]=
      sm->value(NormalDPRNG::RUName,NormalDPRNG::RUVal);
  ps2[NormalDPRNG::DOName]=
      sm->value(NormalDPRNG::DOName,NormalDPRNG::DOVal);

  return
      componentFactorySetDefaultParametersFor(UniformDPRNG::StaticClassName,ps)
      &&
      componentFactorySetDefaultParametersFor(NormalDPRNG::StaticClassName,ps2);
}

bool DPRNGFactory::pluginInitialize()
{
  return true;
}

bool DPRNGFactory::pluginFinalize()
{
  return true;
}

bool DPRNGFactory::pluginWriteSettings( SRPS::SettingsManager * sm )
{
  if ( !sm->isValid() )
    return false;

  sm->setValue(UniformDPRNG::DMaxName,
               p->uniformP.value(UniformDPRNG::DMaxName).toDouble());

  sm->setValue(UniformDPRNG::DMinName,
               p->uniformP.value(UniformDPRNG::DMinName).toDouble());

  sm->setValue(NormalDPRNG::RUName,
               p->normalP.value(NormalDPRNG::RUName).toDouble());

  sm->setValue(NormalDPRNG::DOName,
               p->normalP.value(NormalDPRNG::DOName).toDouble());

  return true;
}

// ComponentFactory
QWidget * DPRNGFactory::componentFactoryWidgetFor(const QString &key) const
{
//  if ( key == UniformDPRNG::StaticClassName )
//  {
//      UniformDPRNGFactoryWidget * w = new UniformDPRNGFactoryWidget;
//      w->componentFactoryWidgetSetParameters(p->uniformP);
//      return w;
//  }
  if ( key == NormalDPRNG::StaticClassName )
  {
      NormalDPRNGFactoryWidget * w = new NormalDPRNGFactoryWidget;
      w->componentFactoryWidgetSetParameters(p->normalP);
      return w;
  }
  return ComponentFactory::componentFactoryWidgetFor(key);
}

QVariantHash DPRNGFactory::componentFactoryDefaultParametersFor(const QString &key) const
{
  if ( key == UniformDPRNG::StaticClassName )
      return p->uniformP;
  if ( key == NormalDPRNG::StaticClassName )
      return p->normalP;
  return QVariantHash();
}

bool DPRNGFactory::componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters)
{
  if ( key == UniformDPRNG::StaticClassName )
  {
    bool ok = false;

    double min = parameters.value(UniformDPRNG::DMinName,"na").toDouble(&ok);
    if ( !ok ) return false;

    double max = parameters.value(UniformDPRNG::DMaxName,"na").toDouble(&ok);
    if ( !ok ) return false;

    p->uniformP.insert(UniformDPRNG::DMaxName,max);
    p->uniformP.insert(UniformDPRNG::DMinName,min);
    return true;
  }

  if ( key == NormalDPRNG::StaticClassName )
  {
    bool ok = false;

    double mean = parameters.value(NormalDPRNG::RUName,"na").toDouble(&ok);
    if ( !ok ) return false;

    double variance = parameters.value(NormalDPRNG::DOName,"na").toDouble(&ok);
    if ( !ok ) return false;

    p->normalP.insert(NormalDPRNG::RUName,mean);
    p->uniformP.insert(NormalDPRNG::DOName,variance);
    return true;
  }

  return false;
}

QStringList DPRNGFactory::dprngFactoryKeys() const
{
    return QStringList(UniformDPRNG::StaticClassName) << NormalDPRNG::StaticClassName;
}

DPRNG * DPRNGFactory::dprngFactoryCreate(const QString &dprng,
                                         const QVariantHash &parameters)
{
    if ( dprng == UniformDPRNG::StaticClassName )
        return new UniformDPRNG(parameters);
    if ( dprng == NormalDPRNG::StaticClassName )
        return new NormalDPRNG(parameters);
    return 0;
}

} // namspace Implementation
} // namspace SRPS

Q_EXPORT_PLUGIN2( SRPSDPRNGs, SRPS::Implementation::DPRNGFactory );
