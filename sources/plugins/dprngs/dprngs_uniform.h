/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_IMPLEMENTATION_UNIFORM_H
#define SRPS_IMPLEMENTATION_UNIFORM_H

#include <SRPS/DPRNG>

namespace SRPS {
namespace Implementation {

class UniformDPRNG : public SRPS::DPRNG
{
    Q_DISABLE_COPY(UniformDPRNG)

    SRPS::PRNG * p;
    double min;
    double max;
    double diff;

public:

    static const QString DMinName;
    static const QString DMaxName;
    static const double DMinVal;
    static const double DMaxVal;

    UniformDPRNG( const QVariantHash & p = QVariantHash() );
    virtual ~UniformDPRNG();

    static const QString StaticClassName;

    QString componentClassName() const;

    double nextOO();
    double nextOC();
    double nextCO();
    double nextCC();

    PRNG * prng() const;
    bool setPRNG( PRNG * );

    Range range() const;
    bool setRange( const Range & range );
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_IMPLEMENTATION_UNIFORM_H
