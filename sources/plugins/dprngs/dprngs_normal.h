/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_IMPLEMENTATION_NORMAL_H
#define SRPS_IMPLEMENTATION_NORMAL_H

#include <SRPS/DPRNG>

namespace SRPS {
namespace Implementation {

class NormalDPRNG : public SRPS::DPRNG
{
  SRPS_DISABLE_COPY(NormalDPRNG)
public:

  static const QString RUName;
  static const QString DOName;
  static const QString ICName;

  static const double RUVal;
  static const double DOVal;
  static const double ICVal;

    NormalDPRNG( const QVariantHash & p = QVariantHash() );
    virtual ~NormalDPRNG();

    static const QString StaticClassName;

    QString componentClassName() const;

    bool componentHasProperty( const QString & name ) const;

    int componentPropertyCount() const;

    QStringList componentPropertyNames() const;

    QVariant componentProperty( const QString & name,
                                const QVariant & defaultValue =
                                          QVariant() ) const;

    int componentSetProperty( const QString & name,
                               const QVariant & value );

    double nextOO();
    double nextOC();
    double nextCO();
    double nextCC();

    PRNG * prng() const;
    bool setPRNG( PRNG * );

    Range range() const;
    bool setRange( const Range & range );

    double ru() const;
    void setRU(double ru);
    double o() const;
    void setO( double _o );

    double ic() const;
    void setIC(double _ic);

    double transform( double val ) const;

    void recalculate_interval();

private:

    void generate();

    SRPS::PRNG * p;

    double r_l; // target range
    double r_r;

    double r_u; // realtive mean

    double i_r_l; // internal limits defined by realtive mean, sigma and confidence interval
    double i_r_r;
    double c_i;

    // non-standard normal.
    double p_u;
    double p_o;

    // box-muller

    double n [ 2 ];
    double r [ 2 ];
    double y;
    double w;

    int counter;
};

} // namespace Implementation
} // namespace SRPS

#endif // IMPLEMENTATION_PRNG_H
