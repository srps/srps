/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spaceeditors_editor.h"
#include "ui_spaceeditors_editor.h"
#include "spaceeditors_model.h"
#include "spaceeditors_spacepropertydialog.h"

#include <QtCore/QVector>
#include <QtGui/QInputDialog>
#include <QtGui/QShowEvent>

namespace SRPS {
namespace Implementation {

class SpaceEditor::Private
{
  public:
  Private():space(0),model(0){}
  SRPS::Space * space;
  SpaceTableModel * model;

  Space::Species currentSpecies;
  Space::Cell currentCell;
  QPointer<QLabel> sl;
};

SpaceEditor::SpaceEditor( QWidget * parent )
  : QMainWindow(parent),
    SRPS::SpaceEditor(),
    p( new Private ),
    ui( new Ui::SpaceEditor )
{
  p->sl= new QLabel(this);
  ui->setupUi(this);

  p->model = new SpaceTableModel(this);
  ui->tableEditor->setModel( p->model );

  connect(p->model,SIGNAL(dataChanged(QModelIndex,QModelIndex)),
          this,SLOT(SpeciesAndCellChanged(QModelIndex,QModelIndex)));
  connect(p->model,SIGNAL(dataChanged(QModelIndex,QModelIndex)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(columnsInserted(QModelIndex,int,int)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(rowsRemoved(QModelIndex,int,int)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(columnsRemoved(QModelIndex,int,int)),this,SLOT(spaceChanged()));

  connect(ui->tableEditor->horizontalHeader(),SIGNAL(sectionDoubleClicked(int)),
          this,SLOT(cellDoubleClicked(int)));
  connect(ui->tableEditor->verticalHeader(),SIGNAL(sectionDoubleClicked(int)),
          this,SLOT(speciesDoubleClicked(int)));
  connect(p->model,SIGNAL(headerDataChanged(Qt::Orientation,int,int)),
          this,SLOT(headerDataChanged(Qt::Orientation,int)));

  setCurrentCell(p->currentCell);
  setCurrentSpecies(p->currentSpecies);

  ui->statusbar->addPermanentWidget(p->sl);
  p->sl->show();
}

SpaceEditor::~SpaceEditor()
{
  delete ui;
  delete p;
}

const QString SpaceEditor::StaticClassName =
    QLatin1String("srps.space.editor");

QString SpaceEditor::componentClassName() const
{
  return StaticClassName;
}

QObject * SpaceEditor::componentAsQObject()
{
  return this;
}

const QObject * SpaceEditor::componentAsConstQObject() const
{
  return this;
}

SRPS::Space * SpaceEditor::spaceEdited() const
{
  return p->space;
}

bool SpaceEditor::spaceEdit( SRPS::Space * s )
{
  if ( p->space == s )
    return false;
  p->space = s;
  p->model->setSpace( s );
  spaceChanged();
  updateSpaceDock();
  return true;
}

void SpaceEditor::showEvent(QShowEvent * e)
{
  ui->spaceDock->show();
  ui->speciesDock->show();
  ui->cellDock->show();
  QMainWindow::showEvent(e);
}

void SpaceEditor::on_actionNewSpecies_triggered()
{
  p->model->newSpecies();
}

void SpaceEditor::on_actionDeleteSpecies_triggered()
{
  QSet<int> rows;
  int min = p->model->rowCount()+1;
  QItemSelectionModel * selection = ui->tableEditor->selectionModel();
  foreach( QModelIndex index, selection->selectedIndexes() )
  {
    rows.insert( index.row() );
    if ( index.row() < min )
      min = index.row();
  }
  p->model->removeRows(min,rows.count());
}

void SpaceEditor::cellDoubleClicked( int li )
{
  QString old = p->model->headerData(li,Qt::Horizontal,Qt::DisplayRole).toString();
  QString name = QInputDialog::getText(this,trUtf8("Species name"),trUtf8("Change species name:"),QLineEdit::Normal,old);
  p->model->setHeaderData(li,Qt::Horizontal,name,Qt::EditRole);
}

void SpaceEditor::speciesDoubleClicked( int li )
{
  QString old = p->model->headerData(li,Qt::Vertical,Qt::DisplayRole).toString();
  QString name = QInputDialog::getText(this,trUtf8("Species name"),trUtf8("Change species name:"),QLineEdit::Normal,old);
  p->model->setHeaderData(li,Qt::Vertical,name,Qt::EditRole);
}

void SpaceEditor::headerDataChanged( Qt::Orientation orientation, int first )
{
  if ( orientation == Qt::Horizontal )
    ui->tableEditor->resizeColumnToContents(first);
  else
    ui->tableEditor->resizeRowToContents(first);
}

void SpaceEditor::spaceChanged()
{
  if (!p->space)
  {
   p->sl->setText(
        trUtf8("%n Individuo(s). ","",0)+
        trUtf8("%n Especie(s). ","",0)+
        trUtf8("%n Celdas(s).","",0));
    return;
  }

  int ic = p->space->individualCount();
  int sc = p->space->speciesCount();
  int cc = p->space->cellCount();

  p->sl->setText(
      trUtf8("%n Individuos(s). ","",ic)+
      trUtf8("%n Especie(s). ","",sc)+
      trUtf8("%n Cell(s).","",cc));
  ui->tableEditor->resizeColumnsToContents();
  ui->tableEditor->resizeRowsToContents();
}

void SpaceEditor::updateSpaceDock()
{
  while( ui->spacePropertyTable->rowCount() )
    ui->spacePropertyTable->removeRow(0);

  if( p->space )
  {
    ui->spaceNameEdit->setEnabled(true);
    ui->spaceNameEdit->setText(p->space->componentProperty("srps.name").toString());

    ui->spaceDescriptionEdit->setEnabled(true);
    ui->spaceDescriptionEdit->setText(p->space->componentProperty("srps.description").toString());

    ui->spacePropertyTable->setEnabled(true);
    foreach( QString pname, p->space->componentPropertyNames() )
    {
      if ( pname == QLatin1String("srps.name") ||
           pname == QLatin1String("srps.description") )
        continue;

      QTableWidgetItem * pitem = new QTableWidgetItem;
      pitem->setText(pname);
      pitem->setFlags( Qt::ItemFlags(pitem->flags() & ~Qt::ItemIsEditable) );

      QTableWidgetItem * vitem = new QTableWidgetItem;
      vitem->setText(p->space->componentProperty(pname).toString());
      vitem->setFlags( Qt::ItemFlags(vitem->flags() & ~Qt::ItemIsEditable) );

      ui->spacePropertyTable->insertRow(0);
      ui->spacePropertyTable->setItem(0,0,pitem);
      ui->spacePropertyTable->setItem(0,1,vitem);
    }
  }
  else
  {
    ui->spaceNameEdit->setEnabled(false);
    ui->spaceNameEdit->setText(trUtf8("Sin nombre"));

    ui->spaceDescriptionEdit->setEnabled(false);
    ui->spaceDescriptionEdit->setText(trUtf8("Sin descrición"));

    ui->spacePropertyTable->setEnabled(false);
  }
}

void SpaceEditor::setCurrentCell( const SRPS::Space::Cell & t )
{
  p->currentCell = t;

  while( ui->cellPropertyTable->rowCount() )
    ui->cellPropertyTable->removeRow(0);

  if ( p->currentCell.isValid() )
  {
    ui->cellNameEdit->setEnabled(true);
    ui->cellNameEdit->setText(p->currentCell.property("srps.name").toString());

    ui->cellDescriptionEdit->setEnabled(true);
    ui->cellDescriptionEdit->setText(p->currentCell.property("srps.description").toString());

    ui->cellPropertyTable->setEnabled(true);
    foreach( QString pname, p->currentCell.propertyNames() )
    {
      if ( pname == QLatin1String("srps.name") ||
           pname == QLatin1String("srps.description") )
        continue;

      QTableWidgetItem * pitem = new QTableWidgetItem;
      pitem->setText(pname);
      pitem->setFlags( Qt::ItemFlags(pitem->flags() & ~Qt::ItemIsEditable) );

      QTableWidgetItem * vitem = new QTableWidgetItem;
      vitem->setText(p->currentCell.property(pname).toString());
      vitem->setFlags( Qt::ItemFlags(vitem->flags() & ~Qt::ItemIsEditable) );

      ui->cellPropertyTable->insertRow(0);
      ui->cellPropertyTable->setItem(0,0,pitem);
      ui->cellPropertyTable->setItem(0,1,vitem);
    }
  }
  else
  {
    ui->cellNameEdit->setEnabled(false);
    ui->cellNameEdit->setText(trUtf8("Sin nombre"));

    ui->cellDescriptionEdit->setEnabled(false);
    ui->cellDescriptionEdit->setText(trUtf8("Sin descripción"));

    ui->cellPropertyTable->setEnabled(false);
  }
}

void SpaceEditor::setCurrentSpecies( const SRPS::Space::Species & v )
{
  p->currentSpecies = v;

  while( ui->speciesPropertyTable->rowCount() )
    ui->speciesPropertyTable->removeRow(0);

  if ( p->currentSpecies.isValid() )
  {
    ui->speciesNameEdit->setEnabled(true);
    ui->speciesNameEdit->setText(p->currentSpecies.property("srps.name").toString());

    ui->speciesDescriptionEdit->setEnabled(true);
    ui->speciesDescriptionEdit->setText(p->currentSpecies.property("srps.description").toString());

    ui->speciesLowerLimitEdit->setEnabled(true);
    ui->speciesLowerLimitEdit->setText(p->currentSpecies.property("srps.limit.lower","undefined").toString());

    ui->speciesUpperLimitEdit->setEnabled(true);
    ui->speciesUpperLimitEdit->setText(p->currentSpecies.property("srps.limit.upper","undefined").toString());

    ui->speciesRangeSizeEdit->setEnabled(true);
    ui->speciesRangeSizeEdit->setText(p->currentSpecies.property("srps.range.size","undefined").toString());

    ui->speciesPropertyTable->setEnabled(true);
    foreach( QString pname, p->currentSpecies.propertyNames() )
    {
      if ( pname == QLatin1String("srps.name") ||
           pname == QLatin1String("srps.description") ||
           pname == QLatin1String("srps.limit.lower") ||
           pname == QLatin1String("srps.limit.upper") ||
           pname == QLatin1String("srps.range.size") )
        continue;

      QTableWidgetItem * pitem = new QTableWidgetItem;
      pitem->setText(pname);
      pitem->setFlags( Qt::ItemFlags(pitem->flags() & ~Qt::ItemIsEditable) );

      QTableWidgetItem * vitem = new QTableWidgetItem;
      vitem->setText(p->currentSpecies.property(pname).toString());
      vitem->setFlags( Qt::ItemFlags(vitem->flags() & ~Qt::ItemIsEditable) );

      ui->speciesPropertyTable->insertRow(0);
      ui->speciesPropertyTable->setItem(0,0,pitem);
      ui->speciesPropertyTable->setItem(0,1,vitem);
    }
  }
  else
  {
    ui->speciesNameEdit->setEnabled(false);
    ui->speciesNameEdit->setText(trUtf8("Sin nombre"));

    ui->speciesDescriptionEdit->setEnabled(false);
    ui->speciesDescriptionEdit->setText(trUtf8("Sin descripción"));

    ui->speciesLowerLimitEdit->setEnabled(false);
    ui->speciesLowerLimitEdit->setText("indefinido");

    ui->speciesUpperLimitEdit->setEnabled(false);
    ui->speciesUpperLimitEdit->setText("indefinido");

    ui->speciesRangeSizeEdit->setEnabled(false);
    ui->speciesRangeSizeEdit->setText("indefinido");

    ui->speciesPropertyTable->setEnabled(false);
  }
}

} // namespace Implementation
} // namespace SRPS

void SRPS::Implementation::SpaceEditor::on_tableEditor_clicked(QModelIndex index)
{
  setCurrentCell( p->model->indexToCell(index) );
  setCurrentSpecies( p->model->indexToSpecies(index) );
}

//

void SRPS::Implementation::SpaceEditor::on_spaceNameEdit_textChanged(QString t )
{
  if ( !p->space ) return;

    p->space->componentSetProperty("srps.name", t);
}

void SRPS::Implementation::SpaceEditor::on_spaceDescriptionEdit_textChanged(QString t)
{
  if ( !p->space ) return;

  p->space->componentSetProperty("srps.description", t);
}

void SRPS::Implementation::SpaceEditor::on_actionSpaceAddProperty_triggered()
{
  if ( !p->space ) return;

  SRPS::Implementation::SpacePropertyDialog dlg;
  dlg.setNewMode();
  if ( dlg.exec() == dlg.Accepted )
  {
    QString name = dlg.name();
    QString value = dlg.value();

    if ( name == "srps.name" )
    {
      ui->spaceNameEdit->setText(value);
    }
    else if ( name == "srps.description" )
    {
      ui->spaceDescriptionEdit->setText(value);
    }
    else
    {
      p->space->componentSetProperty( name, value );
      updateSpaceDock();
    }
  }
}

void SRPS::Implementation::SpaceEditor::on_actionSpaceRemoveProperties_triggered()
{
  if ( !p->space ) return;

  foreach( QString name, p->space->componentPropertyNames() )
  {
    if ( name == "srps.name" || name == "srps.description" ) continue;

    p->space->componentSetProperty(name, QVariant());
  }
  updateSpaceDock();
}

void SRPS::Implementation::SpaceEditor::on_actionSpaceRemoveProperty_triggered()
{
  if ( !p->space ) return;

  int cr = ui->spacePropertyTable->currentRow();
  QTableWidgetItem * item = ui->spacePropertyTable->item(cr,0);
  if ( !item ) return;

  if ( !item->isSelected() ) return;

  QString name = item->text();
  p->space->componentSetProperty(name,QVariant());
  updateSpaceDock();
}

//

void SRPS::Implementation::SpaceEditor::on_cellNameEdit_textChanged(QString t )
{
  if ( !p->currentCell.isValid() ) return;

    p->currentCell.setProperty("srps.name", t);
    p->model->updateCellHeader(p->currentCell);
}

void SRPS::Implementation::SpaceEditor::on_cellDescriptionEdit_textChanged(QString t)
{
  if ( !p->currentCell.isValid() ) return;

  p->currentCell.setProperty("srps.description", t);
  p->model->updateCellHeader(p->currentCell);
}

void SRPS::Implementation::SpaceEditor::on_actionCellAddProperty_triggered()
{
  if ( !p->currentCell.isValid() ) return;

  SRPS::Implementation::SpacePropertyDialog dlg;
  dlg.setNewMode();
  if ( dlg.exec() == dlg.Accepted )
  {
    QString name = dlg.name();
    QString value = dlg.value();

    if ( name == "srps.name" )
    {
      ui->cellNameEdit->setText(value);
    }
    else if ( name == "srps.description" )
    {
      ui->cellDescriptionEdit->setText(value);
    }
    else
    {
      p->currentCell.setProperty( name, value );
      setCurrentCell(p->currentCell);
    }
  }
}

void SRPS::Implementation::SpaceEditor::on_actionCellRemoveProperties_triggered()
{
  if (!p->currentCell.isValid()) return;

  foreach( QString name, p->currentCell.propertyNames() )
  {
    if ( name == "srps.name" || name == "srps.description" ) continue;

    p->currentCell.setProperty(name, QVariant());
  }
  setCurrentCell(p->currentCell);
}

void SRPS::Implementation::SpaceEditor::on_actionCellRemoveProperty_triggered()
{
  int cr = ui->cellPropertyTable->currentRow();
  QTableWidgetItem * item = ui->cellPropertyTable->item(cr,0);
  if ( !item ) return;

  if ( !item->isSelected() ) return;

  QString name = item->text();
  p->currentCell.setProperty(name,QVariant());
  setCurrentCell(p->currentCell);
}

//

void SRPS::Implementation::SpaceEditor::on_speciesNameEdit_textChanged(QString t )
{
  if ( !p->currentSpecies.isValid() ) return;

    p->currentSpecies.setProperty("srps.name", t);
    p->model->updateSpeciesHeader(p->currentSpecies);
}

void SRPS::Implementation::SpaceEditor::on_speciesDescriptionEdit_textChanged(QString t)
{
  if ( !p->currentSpecies.isValid() ) return;

  p->currentSpecies.setProperty("srps.description", t);
  p->model->updateSpeciesHeader(p->currentSpecies);
}

void SRPS::Implementation::SpaceEditor::on_actionSpeciesAddProperty_triggered()
{
  if ( !p->currentSpecies.isValid() ) return;

  SRPS::Implementation::SpacePropertyDialog dlg;
  dlg.setNewMode();
  if ( dlg.exec() == dlg.Accepted )
  {
    QString name = dlg.name();
    QString value = dlg.value();

    if ( name == "srps.name" )
    {
      ui->speciesNameEdit->setText(value);
    }
    else if ( name == "srps.description" )
    {
      ui->speciesDescriptionEdit->setText(value);
    }
    else
    {
      p->currentSpecies.setProperty( name, value );
      setCurrentSpecies(p->currentSpecies);
    }
  }
}

void SRPS::Implementation::SpaceEditor::on_actionSpeciesRemoveProperties_triggered()
{
  if ( !p->currentSpecies.isValid() ) return;

  foreach( QString name, p->currentSpecies.propertyNames() )
  {
    if ( name == "srps.name" || name == "srps.description" ||
         name == "srps.limit.lower" || name == "srps.limit.upper" ||
         name == "srps.range.size" ) continue;

    p->currentSpecies.setProperty(name, QVariant());
  }
  setCurrentSpecies(p->currentSpecies);
}

void SRPS::Implementation::SpaceEditor::on_actionSpeciesRemoveProperty_triggered()
{
  int cr = ui->speciesPropertyTable->currentRow();
  QTableWidgetItem * item = ui->speciesPropertyTable->item(cr,0);
  if ( !item ) return;

  if ( !item->isSelected() ) return;

  QString name = item->text();
  p->currentSpecies.setProperty(name,QVariant());
  setCurrentSpecies(p->currentSpecies);
}


void SRPS::Implementation::SpaceEditor::SpeciesAndCellChanged(
    QModelIndex i,
    QModelIndex /*f*/)
{
  if  ( !i.isValid() ) return;

  setCurrentCell(p->model->indexToCell(i));
  setCurrentSpecies(p->model->indexToSpecies(i));
}

void SRPS::Implementation::SpaceEditor::on_tableEditor_activated(QModelIndex index)
{
  setCurrentCell( p->model->indexToCell(index) );
  setCurrentSpecies( p->model->indexToSpecies(index) );
}

void SRPS::Implementation::SpaceEditor::on_spacePropertyTable_doubleClicked(QModelIndex index)
{

}

void SRPS::Implementation::SpaceEditor::on_spacePropertyTable_cellDoubleClicked(int row, int column)
{

}

void SRPS::Implementation::SpaceEditor::on_spacePropertyTable_itemDoubleClicked(QTableWidgetItem* item)
{
  if (!item ) return;

  int r = item->row();

  SRPS::Implementation::SpacePropertyDialog dlg;
  dlg.setEditMode();

  dlg.setName(ui->spacePropertyTable->item(r,0)->text());
  dlg.setValue(ui->spacePropertyTable->item(r,1)->text());

  if ( dlg.exec() )
  {
    p->space->componentSetProperty(dlg.name(),dlg.value());
    updateSpaceDock();
  }
}

void SRPS::Implementation::SpaceEditor::on_cellPropertyTable_itemDoubleClicked(QTableWidgetItem* item)
{
  if (!item ) return;

  if (!p->currentCell.isValid()) return;

  int r = item->row();

  SRPS::Implementation::SpacePropertyDialog dlg;
  dlg.setEditMode();

  dlg.setName(ui->cellPropertyTable->item(r,0)->text());
  dlg.setValue(ui->cellPropertyTable->item(r,1)->text());

  if ( dlg.exec() )
  {
    p->currentCell.setProperty(dlg.name(),dlg.value());
    setCurrentCell(p->currentCell);
  }
}

void SRPS::Implementation::SpaceEditor::on_speciesPropertyTable_itemDoubleClicked(QTableWidgetItem* item)
{
  if (!item ) return;

  if (!p->currentSpecies.isValid()) return;

  int r = item->row();

  SRPS::Implementation::SpacePropertyDialog dlg;
  dlg.setEditMode();

  dlg.setName(ui->speciesPropertyTable->item(r,0)->text());
  dlg.setValue(ui->speciesPropertyTable->item(r,1)->text());

  if ( dlg.exec() )
  {
    p->currentSpecies.setProperty(dlg.name(),dlg.value());
    setCurrentSpecies(p->currentSpecies);
  }
}
