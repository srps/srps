include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}SpaceEditors
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += spaceeditors_model.cpp \
    spaceeditors_plugin.cpp \
    spaceeditors_editor.cpp \
    spaceeditors_spacepropertydialog.cpp
HEADERS += spaceeditors_model.h \
    spaceeditors_plugin.h \
    spaceeditors_editor.h \
    spaceeditors_spacepropertydialog.h
FORMS += spaceeditors_editor.ui \
    spaceeditors_spacepropertydialog.ui

RESOURCES += \
    resources.qrc
