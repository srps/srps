/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_SPACEEDITOR_H
#define SRPS_PLUGIN_SPACEEDITOR_H

#include <SRPS/QObjectFactory>
#include <SRPS/SpaceEditor>

#include <QtCore/QCoreApplication>

namespace SRPS {
namespace Plugin {

namespace Ui {
  class SpaceEditor;
}

class SpaceEditor : public SRPS::SpaceEditor
{
  Q_OBJECT
  Q_DISABLE_COPY(SpaceEditor)
  private:
    class Private;
    Private * p;
    Ui::SpaceEditor * ui;
  public:
    SpaceEditor( QWidget * parent = 0 );
    virtual ~SpaceEditor();

    static const QString ClassName;
    static QString Description();

    QString className() const;

    SRPS::Space * space() const;
    void edit( SRPS::Space * );

  private slots:

    void on_actionDeleteSpecies_triggered();
    void on_actionNewSpecies_triggered();

    void cellDoubleClicked( int );
    void speciesDoubleClicked( int );

    void headerDataChanged( Qt::Orientation orientation, int first );

    void spaceChanged();
};

class SpaceEditors : public QObjectFactory
{
  Q_OBJECT
  Q_DISABLE_COPY(SpaceEditors)
  Q_INTERFACES(SRPS::QObjectFactory)
  public:

    explicit SpaceEditors( QObject * parent = 0 );
    virtual ~SpaceEditors();

    QString className() const;

    QStringList qobjects() const;
    const QMetaObject * qmetaobject( const QString & ) const;
    QObject * create( const QString & );
    QStringList filter( const QString &, int level = 1 ) const;
    QWidget * gui( const QString & );
    QString description(const QString &) const;
};

} // namespace Plugin
} // namespace SRPS

#endif // SRPS_PLUGIN_PLUGIN_SPACEEDITOR_H
