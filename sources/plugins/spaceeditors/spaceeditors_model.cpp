/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spaceeditors_model.h"

namespace SRPS {
namespace Implementation {

// in sprs.model.colwell
static const QString srps_species_lower_limit_property("srps.limit.lower");
static const QString srps_species_upper_limit_property("srps.limit.upper");
static const QString srps_species_range_size_property("srps.range.size");

class SpaceTableModel::Private
{
  public:
  Private():space(0){}
  Space * space;
  Space::MIDList cells;
  Space::MIDList species;
};

SpaceTableModel::SpaceTableModel( QObject * parent )
  : QAbstractTableModel(parent),
    p( new Private )
{
}

SpaceTableModel::~SpaceTableModel()
{
  delete p;
}

Space * SpaceTableModel::space() const
{
  return p->space;
}

void SpaceTableModel::setSpace( Space * s )
{
  if ( p->space == s )
    return;

  if ( p->space )
    disconnect( p->space->componentAsQObject(),SIGNAL(destroyed()),this,SLOT(spaceDestroyed()));

  p->space = s;
  p->cells.clear();
  p->species.clear();

  if ( p->space )
  {
    p->cells = p->space->cellMIDs(Space::CreationOrder);
    p->species = p->space->speciesMIDs(Space::CreationOrder);
    connect(p->space->componentAsQObject(),SIGNAL(destroyed()),this,SLOT(spaceDestroyed()));
    foreach( Space::MemoryIdentifier sp, p->species )
      actualizarRango(sp);
  }
  reset();
}

int SpaceTableModel::rowCount(const QModelIndex &/*parent*/) const
{
  if ( !p->space )
    return 0;
  return p->species.count();
}

int SpaceTableModel::columnCount(const QModelIndex &/*parent*/) const
{
  if ( !p->space )
    return 0;
  return p->cells.count();
}

QVariant SpaceTableModel::data(const QModelIndex &index, int role) const
{
  if( role != Qt::DisplayRole )
    return QVariant();

  if( index.row() >= rowCount() || index.column() >= columnCount() )
    return QVariant();

//  if ( index.column() == 0 )
//    return p->space->cellProperty(p->cells.at(index.row()),srps_species_lower_limit_property);

//  if ( index.column() == 1 )
//    return p->space->cellProperty(p->cells.at(index.row()),srps_species_upper_limit_property);

//  if ( index.column() == 2 )
//    return p->space->cellProperty(p->cells.at(index.row()),srps_species_range_size_property);

//  if ( index.column() == 3 )
//    return p->space->speciesIndividualCount(p->species.at(index.row()));

  return p->space->cellSpeciesIndividualCount(
      p->cells.at(index.column()),
      p->species.at(index.row()) );
}

QVariant SpaceTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if( !p->space )
    return QVariant();
  if ( role != Qt::DisplayRole )
    return QVariant();
  if ( orientation == Qt::Horizontal )
  {
//    if ( section == 0 )
//      return srps_species_lower_limit_property;
//    if ( section == 1 )
//      return srps_species_upper_limit_property;
//    if ( section == 2 )
//      return srps_species_range_size_property;
//    if ( section == 3 )
//      return tr("individualCount");
    QString name = p->space->cellProperty(p->cells.at(section/*-4*/),"srps.name").toString();
    return QString("%1: %2").arg(section+1).arg(name);
  }
  else
  {
    Space::MemoryIdentifier key =p->species.at(section);
    QString name = p->space->speciesProperty(key,"srps.name").toString();
    if ( name.isEmpty() )
      return QString("Species %1").arg(section+1);
    else
      return QString("%1: %2").arg(section+1).arg(name);
  }
}

Qt::ItemFlags SpaceTableModel::flags( const QModelIndex & index ) const
{
//  if ( index.column() > 3 )
    return QAbstractTableModel::flags(index)|Qt::ItemIsEditable;
//  return QAbstractTableModel::flags(index);
}

bool SpaceTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!p->space)
    return false;
  if ( !index.isValid())
    return false;
  if ( role != Qt::EditRole )
    return false;
  if ( index.row() >= rowCount() || index.column() >= columnCount())
    return false;
//  if( index.column() < 4 )
//    return false;
  bool ok = false;
  int newCount = value.toInt(&ok);
  if (!ok)
    return false;
  Space::MemoryIdentifier cell = p->cells.at(index.column()/*-4*/);
  Space::MemoryIdentifier  species = p->species.at(index.row());
  Space::MIDList is = p->space->cellSpeciesIndividualMIDs(cell,species);
  int diff = newCount - is.count();
  if ( diff > 0 ) // agrego
  {
    while( diff -- )
    {
      Space::MemoryIdentifier ni = p->space->speciesCreateIndividualMID(species);
      p->space->cellInsertIndividual(cell,species,ni);
    }
  }
  else if ( diff < 0 )// quito
  {
    while( diff ++ )
      p->space->speciesDeleteIndividual(species,is.takeFirst());
  }
  else
    return false;
  actualizarRango(species);
//  emit dataChanged( this->index(index.row(),0,QModelIndex()),
//                    this->index(index.row(),3,QModelIndex()));
  emit dataChanged( index,index );
  return true;
}

bool SpaceTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
  if( !p->space)
    return false;
  int final = row+count;
  if( row < 0 || row >= rowCount() || final > rowCount() )
    return false;
  beginRemoveRows(parent,row,final-1);
  while( count-- )
    p->space->deleteSpecies( p->species.takeAt(row));
  endRemoveRows();
  return true;
}

bool SpaceTableModel::removeColumns(int column, int count, const QModelIndex &parent)
{
  if( !p->space)
    return false;
  int final = column+count;
  if( column < 0 || column>= columnCount() || final > columnCount() )
    return false;
  beginRemoveColumns(parent,column,final-1);
  while( count-- )
    p->space->deleteCell( p->cells.takeAt( column ) );
  endRemoveColumns();
  return true;
}

bool SpaceTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
  if ( role != Qt::EditRole )
    return false;
  bool result = false;
  if ( orientation == Qt::Horizontal )
  {
//    if ( section < 4 )
//      return false;
    result = p->space->cellSetProperty(p->cells.at(section/*-4*/),"srps.name",value.toString());
  }
  else
  {
    result = p->space->speciesSetProperty(p->species.at(section),"srps.name",value.toString());
  }
  if ( result ) emit headerDataChanged(orientation,section,section);
  return result;
}

void SpaceTableModel::newSpecies()
{
  if ( !p->space )
    return;
  Space::MemoryIdentifier ns = p->space->createSpeciesMID();
//  p->space->speciesSetProperty(ns,"srps.name",tr("Species %1").arg(ns));
  beginInsertRows(QModelIndex(),rowCount(),rowCount());
  p->species << ns;
  endInsertRows();
}

void SpaceTableModel::spaceDestroyed()
{
  disconnect( p->space->componentAsQObject(), SIGNAL(destroyed()),this,SLOT(spaceDestroyed()));
  p->space = 0;
  p->cells.clear();
  p->species.clear();
  reset();
}

void SpaceTableModel::actualizarRango( const Space::MemoryIdentifier & species )
{
  // update ranges
  bool found = false;
  for( int j = 0; j < p->cells.count(); ++j ) // left
  {
    if( p->space->cellSpeciesIndividualCount(p->cells.at(j),species) )
    {
      found = true;
      p->space->speciesSetProperty(species,srps_species_lower_limit_property,j+1);
      break;
    }
  }
  found = false;
  for( int j = p->cells.count()-1; j >= 0; --j ) // right
  {
    if( p->space->cellSpeciesIndividualCount(p->cells.at(j),species) )
    {
      found = true;
      p->space->speciesSetProperty(species,srps_species_upper_limit_property,j+1);
      break;
    }
  }
  if ( !found )
  {
    p->space->speciesSetProperty(species,srps_species_lower_limit_property,QVariant());
    p->space->speciesSetProperty(species,srps_species_upper_limit_property,QVariant());
    p->space->speciesSetProperty(species,srps_species_range_size_property,QVariant());
  }
  else
    p->space->speciesSetProperty(
        species,
        srps_species_range_size_property,
        p->space->speciesProperty(species,srps_species_upper_limit_property).toInt()
        - p->space->speciesProperty(species,srps_species_lower_limit_property).toInt()
        +1);
}

SRPS::Space::Species SpaceTableModel::indexToSpecies( const QModelIndex & index ) const
{
  if ( !p->space ) return Space::Species();

  return p->space->toSpecies( p->species.at( index.row() ));
}

SRPS::Space::Cell SpaceTableModel::indexToCell( const QModelIndex & index ) const
{
  if ( !p->space ) return Space::Cell();

  return p->space->toCell( p->cells.at( index.column() ));
}

void SpaceTableModel::updateSpeciesHeader( const SRPS::Space::Species & species )
{
  if ( !species.isValid() ) return;

  int index = p->species.indexOf(species.mid());
  emit headerDataChanged(Qt::Vertical,index,index);
}

void SpaceTableModel::updateCellHeader( const SRPS::Space::Cell & cell )
{
  if ( !cell.isValid() ) return;

  int index = p->cells.indexOf(cell.mid());
  emit headerDataChanged(Qt::Horizontal,index,index);
}

} // namespace Implementation
} // namespace SRPS
