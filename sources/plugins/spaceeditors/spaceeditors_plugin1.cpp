/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spaceeditorsplugin.h"
#include "ui_spaceeditor.h"
#include "spacetablemodel.h"

#include <QtCore/QVector>
#include <QtGui/QInputDialog>



namespace SRPS {
namespace Plugin {

class SpaceEditor::Private
{
  public:
  Private():space(0),model(0){}
  SRPS::Space * space;
  SpaceTableModel * model;
};

SpaceEditor::SpaceEditor( QWidget * parent )
  : SRPS::SpaceEditor( parent ),
    p( new Private ),
    ui( new Ui::SpaceEditor )
{
  ui->setupUi(this);

  p->model = new SpaceTableModel(this);
  ui->tableEditor->setModel( p->model );

  connect(p->model,SIGNAL(dataChanged(QModelIndex,QModelIndex)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(columnsInserted(QModelIndex,int,int)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(rowsRemoved(QModelIndex,int,int)),this,SLOT(spaceChanged()));
  connect(p->model,SIGNAL(columnsRemoved(QModelIndex,int,int)),this,SLOT(spaceChanged()));

  connect(ui->tableEditor->horizontalHeader(),SIGNAL(sectionDoubleClicked(int)),
          this,SLOT(cellDoubleClicked(int)));
  connect(ui->tableEditor->verticalHeader(),SIGNAL(sectionDoubleClicked(int)),
          this,SLOT(speciesDoubleClicked(int)));
  connect(p->model,SIGNAL(headerDataChanged(Qt::Orientation,int,int)),
          this,SLOT(headerDataChanged(Qt::Orientation,int)));
}

SpaceEditor::~SpaceEditor()
{
  delete ui;
  delete p;
}

const QString SpaceEditor::ClassName = QLatin1String("srps.space.editor");

QString SpaceEditor::Description()
{
  return tr("srps.space.editor.description");
}

QString SpaceEditor::className() const
{
  return ClassName;
}

SRPS::Space * SpaceEditor::space() const
{
  return p->space;
}

void SpaceEditor::edit( SRPS::Space * s )
{
  if ( p->space == s )
    return ;
  p->space = s;
  p->model->setSpace( s );
  spaceChanged();
}

void SpaceEditor::on_actionNewSpecies_triggered()
{
  p->model->newSpecies();
}

void SpaceEditor::on_actionDeleteSpecies_triggered()
{
  QSet<int> rows;
  int min = p->model->rowCount()+1;
  QItemSelectionModel * selection = ui->tableEditor->selectionModel();
  foreach( QModelIndex index, selection->selectedIndexes() )
  {
    rows.insert( index.row() );
    if ( index.row() < min )
      min = index.row();
  }
  p->model->removeRows(min,rows.count());
}

void SpaceEditor::cellDoubleClicked( int li )
{
  QString old = p->model->headerData(li,Qt::Horizontal,Qt::DisplayRole).toString();
  QString name = QInputDialog::getText(this,tr("Variable name"),tr("Change variable name:"),QLineEdit::Normal,old);
  p->model->setHeaderData(li,Qt::Horizontal,name,Qt::EditRole);
}

void SpaceEditor::speciesDoubleClicked( int li )
{
  QString old = p->model->headerData(li,Qt::Vertical,Qt::DisplayRole).toString();
  QString name = QInputDialog::getText(this,tr("Variable name"),tr("Change variable name:"),QLineEdit::Normal,old);
  p->model->setHeaderData(li,Qt::Vertical,name,Qt::EditRole);
}

void SpaceEditor::headerDataChanged( Qt::Orientation orientation, int first )
{
  if ( orientation == Qt::Horizontal )
    ui->tableEditor->resizeColumnToContents(first);
  else
    ui->tableEditor->resizeRowToContents(first);
}

void SpaceEditor::spaceChanged()
{
  if (!p->space)
  {
    statusBar()->showMessage(
        tr("%n Individual(s). ","",0)+
        tr("%n Species(). ","",0)+
        tr("%n Cell(s).","",0));
    return;
  }

  int ic = p->space->individualCount();
  int sc = p->space->speciesCount();
  int cc = p->space->cellCount();

  statusBar()->showMessage(
      tr("%n Individual(s). ","",ic)+
      tr("%n Species(). ","",sc)+
      tr("%n Cell(s).","",cc));
  ui->tableEditor->resizeColumnsToContents();
  ui->tableEditor->resizeRowsToContents();
}

////////////////////////////////

SpaceEditors::SpaceEditors( QObject * parent )
  : QObjectFactory( parent )
{
}

SpaceEditors::~SpaceEditors()
{
}

QString SpaceEditors::className() const
{
  return Plugin::SpaceEditor::ClassName;
}

QStringList SpaceEditors::qobjects() const
{
  return QStringList( Plugin::SpaceEditor::ClassName );
}

const QMetaObject * SpaceEditors::qmetaobject( const QString & n ) const
{
  if ( n == Plugin::SpaceEditor::ClassName )
    return &Plugin::SpaceEditor::staticMetaObject;
  return 0;
}

QObject * SpaceEditors::create( const QString & n )
{
  if ( n == Plugin::SpaceEditor::ClassName )
    return new Plugin::SpaceEditor;
  return 0;
}

QStringList SpaceEditors::filter( const QString & , int /*level*/ ) const
{
  return QStringList();
}

QWidget * SpaceEditors::gui( const QString & /*c*/)
{
  return 0;
}

QString SpaceEditors::description(const QString & c) const
{
  if ( c == Plugin::SpaceEditor::ClassName )
    return Plugin::SpaceEditor::Description();
  return QString();
}

} // namespace Plugin
} // namespace SRPS

Q_EXPORT_PLUGIN2( SRPSSpaceEditors, SRPS::Plugin::SpaceEditors)
