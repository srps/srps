/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spaceeditors_spacepropertydialog.h"
#include "ui_spaceeditors_spacepropertydialog.h"

#include <QtGui/QPushButton>

namespace SRPS {
namespace Implementation {

SpacePropertyDialog::SpacePropertyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SpacePropertyDialog)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

SpacePropertyDialog::~SpacePropertyDialog()
{
    delete ui;
}

QString SpacePropertyDialog::name() const
{
  return ui->nameEdit->text();
}

void SpacePropertyDialog::setName( const QString & name )
{
  ui->nameEdit->setText(name);
}

QString SpacePropertyDialog::value() const
{
  return ui->valueText->toPlainText();
}

void SpacePropertyDialog::setValue( const QString & value )
{
  ui->valueText->setPlainText(value);
}

void SpacePropertyDialog::setNewMode()
{
  ui->nameEdit->setReadOnly(false);
  ui->valueText->setReadOnly(false);
}

void SpacePropertyDialog::setEditMode()
{
  ui->nameEdit->setReadOnly(true);
  ui->valueText->setReadOnly(false);
}

void SpacePropertyDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS

void SRPS::Implementation::SpacePropertyDialog::on_nameEdit_textChanged(QString t)
{
  ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!t.isEmpty());
}
