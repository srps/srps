/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_IMPLEMENTATION_SPACEEDITOR_H
#define SRPS_IMPLEMENTATION_SPACEEDITOR_H

#include <SRPS/Space>
#include <SRPS/SpaceEditor>
#include <QtCore/QModelIndex>

#include <QtGui/QMainWindow>

class QTableWidgetItem;

namespace SRPS {
namespace Implementation {

namespace Ui {
  class SpaceEditor;
}

class SpaceEditor : public QMainWindow, public SRPS::SpaceEditor
{
  Q_OBJECT
  Q_DISABLE_COPY(SpaceEditor)
  Q_INTERFACES(SRPS::SpaceEditor SRPS::Component)

  class Private;
  Private * p;
  Ui::SpaceEditor * ui;

public:

  explicit SpaceEditor( QWidget * parent = 0 );
  virtual ~SpaceEditor();

  static const QString StaticClassName;

  QString componentClassName() const;
  QObject * componentAsQObject();
  const QObject * componentAsConstQObject() const;

  SRPS::Space * spaceEdited() const;

  bool spaceEdit(SRPS::Space *space);

protected:

  void showEvent(QShowEvent *);

private slots:

  void on_speciesPropertyTable_itemDoubleClicked(QTableWidgetItem* item);
  void on_cellPropertyTable_itemDoubleClicked(QTableWidgetItem* item);
  void on_spacePropertyTable_itemDoubleClicked(QTableWidgetItem* item);
  void on_spacePropertyTable_cellDoubleClicked(int row, int column);
  void on_spacePropertyTable_doubleClicked(QModelIndex index);
  void on_tableEditor_activated(QModelIndex index);
  void on_actionSpaceRemoveProperty_triggered();
  void on_actionSpaceRemoveProperties_triggered();
  void on_actionSpaceAddProperty_triggered();
  void on_spaceDescriptionEdit_textChanged(QString );
  void on_spaceNameEdit_textChanged(QString );

  void on_actionCellRemoveProperty_triggered();
  void on_actionCellRemoveProperties_triggered();
  void on_actionCellAddProperty_triggered();
  void on_cellDescriptionEdit_textChanged(QString );
  void on_cellNameEdit_textChanged(QString );

  void on_actionSpeciesRemoveProperty_triggered();
  void on_actionSpeciesRemoveProperties_triggered();
  void on_actionSpeciesAddProperty_triggered();
  void on_speciesDescriptionEdit_textChanged(QString );
  void on_speciesNameEdit_textChanged(QString );
  void on_tableEditor_clicked(QModelIndex index);
  void on_actionDeleteSpecies_triggered();
  void on_actionNewSpecies_triggered();
//  void on_actionDeleteCells_triggered();
//  void on_actionNewCell_triggered();

  void updateSpaceDock();

  void setCurrentCell( const SRPS::Space::Cell & );
  void setCurrentSpecies( const SRPS::Space::Species & );

  void cellDoubleClicked( int );
  void speciesDoubleClicked( int );

  void headerDataChanged( Qt::Orientation orientation, int first );

  void spaceChanged();

  void SpeciesAndCellChanged(QModelIndex,QModelIndex);
};

} // namespace IMPLEMENTATION
} // namespace SRPS

#endif // SRPS_IMPLEMENTATION_IMPLEMENTATION_SPACEEDITOR_H
