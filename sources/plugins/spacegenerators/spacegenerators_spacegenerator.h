/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACEGENERATORS_SPACEGENERATOR_H
#define SRPS_SPACEGENERATORS_SPACEGENERATOR_H

#include <SRPS/SpaceGenerator>

namespace SRPS {
namespace Implementation {

class SpaceGenerator : public SRPS::SpaceGenerator
{
    class Private;
    Private * p;
public:
    SpaceGenerator();
    virtual ~SpaceGenerator();

    static QVariantHash defaultParameters();

    void setup( const QVariantHash & parameters );

    // Component

    static const QString StaticClassName;

    QString componentClassName() const;

    int prepareGenerate( SRPS::Space * space );

    int generate();
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_SPACEGENERATORS_SPACEGENERATOR_H
