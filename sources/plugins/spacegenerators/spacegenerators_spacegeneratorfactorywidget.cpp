/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spacegenerators_spacegeneratorfactorywidget.h"
#include "ui_spacegenerators_spacegeneratorfactorywidget.h"

#include "spacegenerators_spacegenerator.h"
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

SpaceGeneratorFactoryWidget::SpaceGeneratorFactoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpaceGeneratorFactoryWidget)
{
    ui->setupUi(this);
    componentFactoryWidgetSetParameters(Implementation::SpaceGenerator::defaultParameters());
}

SpaceGeneratorFactoryWidget::~SpaceGeneratorFactoryWidget()
{
    delete ui;
}

QVariantHash SpaceGeneratorFactoryWidget::componentFactoryWidgetParameters() const
{
  QVariantHash r;

  r.insert("srps.space.generator.script.cell",ui->cellCountEdit->text());
  r.insert("srps.space.generator.script.species",ui->speciesCountEdit->text());
  r.insert("srps.space.generator.script.individual",ui->speciesIndividualCountEdit->text());
  r.insert("srps.space.generator.script.species.range.lower",ui->speciesLowerRangeEdit->text());
  r.insert("srps.space.generator.script.species.range.upper",ui->speciesUpperRangeEdit->text());

  for( int  i = 0; i < ui->tableWidget->rowCount(); ++i )
  {
    r.insert(
        QString("srps.space.generator.variable.%1").arg(
            ui->tableWidget->item(i,0)->text()
            ),
        ui->tableWidget->item(i,1)->text()
        );
  }

  return r;
}

bool SpaceGeneratorFactoryWidget::componentFactoryWidgetSetParameters( const QVariantHash & parameters )
{
  while( ui->tableWidget->rowCount() )
    ui->tableWidget->removeRow(0);

  qDebug() << Q_FUNC_INFO <<parameters;

  // evaluate variables
  foreach( QString key, parameters.keys() )
  {
    if ( key.startsWith("srps.space.generator.variable.") )
    {
      QString variable = key;
      variable.remove("srps.space.generator.variable.");
      ui->tableWidget->insertRow(0);
      ui->tableWidget->setItem(0,0,new QTableWidgetItem(variable));
      qDebug() << Q_FUNC_INFO <<parameters.value(variable);
      int val = parameters.value(key).toInt();
      ui->tableWidget->setItem(0,1,new QTableWidgetItem(QString::number(val)));
    }
    else if ( key == "srps.space.generator.script.cell" )
    {
      ui->cellCountEdit->setText(parameters.value(key).toString());
    }
    else if ( key == "srps.space.generator.script.species" )
    {
      ui->speciesCountEdit->setText(parameters.value(key).toString());
    }
    else if ( key == "srps.space.generator.script.individual" )
    {
      ui->speciesIndividualCountEdit->setText(parameters.value(key).toString());
    }
    else if ( key == "srps.space.generator.script.species.range.lower" )
    {
      ui->speciesLowerRangeEdit->setText(parameters.value(key).toString());
    }
    else if ( key == "srps.space.generator.script.species.range.upper" )
    {
      ui->speciesUpperRangeEdit->setText(parameters.value(key).toString());
    }
  }

  return true;
}

void SpaceGeneratorFactoryWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS

void SRPS::Implementation::SpaceGeneratorFactoryWidget::on_addVariableTool_clicked()
{
    ui->tableWidget->insertRow(0);
}

namespace SRPS{
namespace Implementation {

void SRPS::Implementation::SpaceGeneratorFactoryWidget::on_removeVariableTool_clicked()
{
  QList<QTableWidgetItem*> items = ui->tableWidget->selectedItems();

  if ( !items.count( )) return;


    int r = items.takeFirst()->row();
    ui->tableWidget->removeRow(r);
}

}}

void SRPS::Implementation::SpaceGeneratorFactoryWidget::on_clearVariablesTool_clicked()
{
    while( ui->tableWidget->rowCount() )
      ui->tableWidget->removeRow(0);
}
