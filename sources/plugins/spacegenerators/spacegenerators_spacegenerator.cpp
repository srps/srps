/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <SRPS/Space>

#include "spacegenerators_spacegenerator.h"

#include <QtCore/QHash>
#include <QtCore/QVariantHash>
#include <QtScript>

namespace SRPS {
namespace Implementation {

class Implementation::SpaceGenerator::Private
{
  public:
  Private():space(0),engine(0){}
  Space * space;
  QScriptEngine * engine;
  QString cell_script;
  QString species_script;
  QString individual_script;
  QString species_range_lower_script;
  QString species_range_upper_script;

};

const QString SpaceGenerator::StaticClassName = QLatin1String("srps.space.generator");

SpaceGenerator::SpaceGenerator( )
    : SRPS::SpaceGenerator(),
    p( new Private )
{
  setup(defaultParameters());
}

SpaceGenerator::~SpaceGenerator()
{
    delete p;
}

QVariantHash SpaceGenerator::defaultParameters()
{
  QVariantHash p;

  p.insert("srps.space.generator.variable.cell_min",10);
  p.insert("srps.space.generator.variable.cell_max",100);
  p.insert("srps.space.generator.variable.species_min",10);
  p.insert("srps.space.generator.variable.species_max",100);
  p.insert("srps.space.generator.variable.individual_min",10);
  p.insert("srps.space.generator.variable.individual_max",100);

  p.insert("srps.space.generator.script.cell",
           "Math.floor(Math.random() * (cell_max - cell_min + 1) + cell_min)");
  p.insert("srps.space.generator.script.species",
           "Math.floor(Math.random() * (species_max - species_min + 1) + species_min)");
  p.insert("srps.space.generator.script.individual",
           "Math.floor(Math.random() * (individual_max - individual_min + 1) + individual_min)/5");
  p.insert("srps.space.generator.script.species.range.lower",
           "Math.floor(Math.random() * (cell_count + 1) )");
  p.insert("srps.space.generator.script.species.range.upper",
           "Math.floor(Math.random() * (cell_count + 1) )");


//  qDebug() << Q_FUNC_INFO << p;
  return p;
}

static void evaluateVariable( QScriptEngine * engine, const QString & script )
{
//  qDebug()<< Q_FUNC_INFO << "eval:" << script;
  QScriptValue result = engine->evaluate(script);
  if ( result.isError() )
  {
    qDebug() << Q_FUNC_INFO << "error:" << result.toString();
    qDebug() << Q_FUNC_INFO << "at:" << engine->uncaughtExceptionLineNumber();
  }
}

void SpaceGenerator::setup( const QVariantHash & parameters )
{
  if ( p->engine )
  {
    delete p->engine;
  }

  p->engine = new QScriptEngine;


  qDebug() << Q_FUNC_INFO;
  // evaluate variables
  foreach( QString key, parameters.keys() )
  {
    if ( key.startsWith("srps.space.generator.variable.") )
    {
      QString variable = key;
      variable.remove("srps.space.generator.variable.");
      if ( !variable.isEmpty() )
        evaluateVariable(p->engine,QString("var %1 = %2;")
                          .arg(variable)
                          .arg(parameters.value(key).toInt()));
    }
    else if ( key == "srps.space.generator.script.cell" )
    {
      p->cell_script = parameters.value(key).toString();
    }
    else if ( key == "srps.space.generator.script.species" )
    {
      p->species_script = parameters.value(key).toString();
    }
    else if ( key == "srps.space.generator.script.individual" )
    {
      p->individual_script = parameters.value(key).toString();
    }
    else if ( key == "srps.space.generator.script.species.range.lower" )
    {
      p->species_range_lower_script = parameters.value(key).toString();
    }
    else if ( key == "srps.space.generator.script.species.range.upper" )
    {
      p->species_range_upper_script = parameters.value(key).toString();
    }
  }
}


// Component
QString SpaceGenerator::componentClassName() const
{
    return StaticClassName;
}

static int evaluateInt( QScriptEngine * engine, const QString script )
{
//  qDebug()<< Q_FUNC_INFO << "eval:" << script;
  QScriptValue result = engine->evaluate(script);
  if ( result.isError() )
  {
    qDebug() << Q_FUNC_INFO << "error:" << result.toString();
    qDebug() << Q_FUNC_INFO << "at:" << engine->uncaughtExceptionLineNumber();
  }
//  qDebug() << Q_FUNC_INFO << "result int:" << result.toInt32();
  return result.toInt32();
}

int SpaceGenerator::prepareGenerate( SRPS::Space * space )
{
  if ( !space ) return -1;

  p->space = space;
  return 0;
}



int SpaceGenerator::generate()
{
  if ( !p->space ) return -1;

  p->space->componentSetProperty("cell_script",p->cell_script);
  int cell_count = evaluateInt(p->engine,p->cell_script);
  if ( cell_count < 0 ) cell_count = 0;
  evaluateVariable(p->engine,QString("var cell_count = %1;").arg(cell_count));

  SRPS::Space::MIDList cells;
  while( cell_count-- )
    cells << p->space->createCellMID();

  p->space->componentSetProperty("species_script",p->species_script);
  int species_count = evaluateInt(p->engine,p->species_script);
  if ( species_count < 0 ) species_count = 0;
  evaluateVariable(p->engine,QString("var species_count = %1;").arg(species_count));

  SRPS::Space::MIDList speciess;
  int tmp = species_count;
  while( tmp-- )
    speciess << p->space->createSpeciesMID();

  for( int i = 0; i < species_count; ++i )
  {
    p->engine->evaluate(QString("var species = %1").arg(i+1));

    int species_range_lower = evaluateInt(p->engine,p->species_range_lower_script);
    if ( species_range_lower < 0 ) species_range_lower = 0;
    if ( species_range_lower >= cells.count() ) species_range_lower = cells.count()-1;

    int species_range_upper = evaluateInt(p->engine,p->species_range_upper_script);
    if ( species_range_upper < 0 ) species_range_upper = 0;
    if ( species_range_upper >= cells.count() ) species_range_upper = cells.count()-1;

    if ( species_range_lower > species_range_upper )
      qSwap(species_range_lower,species_range_upper);

    int range = species_range_upper-species_range_lower+1;

    int offset = qrand() % (cells.count()-range+1);


    int individual_count = evaluateInt(p->engine,p->individual_script);
    if ( individual_count <= 0 ) individual_count = 1;

    while( individual_count-- )
    {
      Space::MemoryIdentifier imid = p->space->speciesCreateIndividualMID(speciess.at(i));
      if ( cells.count() )
      {
        int pos = offset + qrand()%range;
        p->space->cellInsertIndividual(cells.at(pos),speciess.at(i),imid);
      }
    }
  }

  p->space = 0;
  return 0;
}

} // namespace Implementation
} // namespace SRPS
