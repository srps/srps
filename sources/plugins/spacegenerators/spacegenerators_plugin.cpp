/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <SRPS/SettingsManager>
#include "spacegenerators_plugin.h"
#include "spacegenerators_spacegenerator.h"
#include "spacegenerators_spacegeneratorfactorywidget.h"

namespace SRPS {
  namespace Implementation {


class SRPS::Implementation::SpaceGeneratorFactory::Private
{
public:
  Private():initialized(false),finalized(false){}
  bool initialized;
  bool finalized;
  QString lastError;
  QVariantHash defaultProperties;
};

SpaceGeneratorFactory::SpaceGeneratorFactory(QObject *parent) :
  QObject(parent),
  SRPS::SpaceGeneratorFactory(),
  p( new Private )
{
  p->defaultProperties = Implementation::SpaceGenerator::defaultParameters();
}

SpaceGeneratorFactory::~SpaceGeneratorFactory()
{
  delete p;
}

// Plugin
QString SpaceGeneratorFactory::errorString() const
{
  return p->lastError;
}

bool SpaceGeneratorFactory::isInitialized() const
{
  return p->initialized;
}

bool SpaceGeneratorFactory::isFinalized() const
{
  return p->finalized;
}

bool SpaceGeneratorFactory::pluginReadSettings( const SRPS::SettingsManager * sm )
{
//  if( !sm->isValid() )
    return false;
}

bool SpaceGeneratorFactory::pluginInitialize()
{
  return true;
}

bool SpaceGeneratorFactory::pluginFinalize()
{
  return true;
}

bool SpaceGeneratorFactory::pluginWriteSettings( SRPS::SettingsManager * sm )
{
//  if ( !sm->isValid() )
//    return false;

//  sm->setValue(Implementation::SpaceGenerator::StaticDefaultVariableCountParameterName,
//                p->defaultProperties.value(
//                    Implementation::SpaceGenerator::StaticDefaultVariableCountParameterName).toInt());
//  sm->setValue(Implementation::SpaceGenerator::StaticDefaultTupleCountParameterName,
//                p->defaultProperties.value(
//                    Implementation::SpaceGenerator::StaticDefaultTupleCountParameterName).toInt());
  return true;
}

// ComponentFactory
QWidget * SpaceGeneratorFactory::componentFactoryWidgetFor(const QString &key) const
{
  if ( key == Implementation::SpaceGenerator::StaticClassName )
  {
      SpaceGeneratorFactoryWidget * w = new SpaceGeneratorFactoryWidget;
      w->componentFactoryWidgetSetParameters(p->defaultProperties);
      return w;
  }
  return 0;
}

QVariantHash SpaceGeneratorFactory::componentFactoryDefaultParametersFor(const QString &key) const
{
  if ( key == Implementation::SpaceGenerator::StaticClassName )
      return p->defaultProperties;
  return QVariantHash();
}

bool SpaceGeneratorFactory::componentFactorySetDefaultParametersFor(const QString &key, const QVariantHash &parameters)
{
  if ( key != Implementation::SpaceGenerator::StaticClassName )
    return false;

  p->defaultProperties = parameters;

//  int t = parameters.value(Implementation::SpaceGenerator::StaticDefaultTupleCountParameterName,-1).toInt();
//  if ( t < 0 )
//    return false;

//  int v = parameters.value(Implementation::SpaceGenerator::StaticDefaultVariableCountParameterName,-1).toInt();
//  if ( v < 0 )
//    return false;

//  p->defaultProperties[Implementation::SpaceGenerator::StaticDefaultTupleCountParameterName] = t;
//  p->defaultProperties[Implementation::SpaceGenerator::StaticDefaultVariableCountParameterName] = v;
  return true;
}

// SpaceGeneratorFactory
QStringList SpaceGeneratorFactory::spaceGeneratorFactoryKeys() const
{
  return QStringList(SRPS::Implementation::SpaceGenerator::StaticClassName);
}

SRPS::SpaceGenerator * SpaceGeneratorFactory::spaceGeneratorFactoryCreate (const QString &spaceGenerator,
                                                const QVariantHash &parameters)
{
  if( spaceGenerator == SRPS::Implementation::SpaceGenerator::StaticClassName )
  {

    SpaceGenerator * g =
        new SRPS::Implementation::SpaceGenerator;
    g->setup(parameters);
    return g;
  }
  return 0;
}

}
}

Q_EXPORT_PLUGIN2(SRPSSpaceGenerators,SRPS::Implementation::SpaceGeneratorFactory)
