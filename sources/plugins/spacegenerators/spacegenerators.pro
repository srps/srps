include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}SpaceGenerators
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += \
    spacegenerators_plugin.cpp \
    spacegenerators_spacegenerator.cpp \
    spacegenerators_spacegeneratorfactorywidget.cpp
HEADERS += \
    spacegenerators_plugin.h \
    spacegenerators_spacegenerator.h \
    spacegenerators_spacegeneratorfactorywidget.h

FORMS += \
    spacegenerators_spacegeneratorfactorywidget.ui

QT += script
