/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SPACEGENERATORS_SPACEGENERATORFACTORYWIDGET_H
#define SPACEGENERATORS_SPACEGENERATORFACTORYWIDGET_H

#include <SRPS/ComponentFactoryWidget>
#include <QtGui/QWidget>

namespace SRPS {
namespace Implementation {

namespace Ui {
    class SpaceGeneratorFactoryWidget;
}

class SpaceGeneratorFactoryWidget : public QWidget, public SRPS::ComponentFactoryWidget
{
    Q_OBJECT
    Q_INTERFACES(SRPS::ComponentFactoryWidget)

public:
    explicit SpaceGeneratorFactoryWidget(QWidget *parent = 0);
    virtual ~SpaceGeneratorFactoryWidget();

    QVariantHash componentFactoryWidgetParameters() const;
    bool componentFactoryWidgetSetParameters(const QVariantHash & parameters );

protected:
    void changeEvent(QEvent *e);

private:
    Ui::SpaceGeneratorFactoryWidget *ui;

private slots:
    void on_clearVariablesTool_clicked();
    void on_removeVariableTool_clicked();
    void on_addVariableTool_clicked();
};


} // namespace Implementation
} // namespace SRPS
#endif // SPACEGENERATORS_SPACEGENERATORFACTORYWIDGET_H
