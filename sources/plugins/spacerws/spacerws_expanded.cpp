/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spacerws_expanded.h"

#include <QtCore/QBuffer>
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

SpaceExpandedRW::SpaceExpandedRW()
  : SpaceRW()
{
  read_device = 0;
  write_device = 0;
  read_space = 0;
  write_space = 0;
}

SpaceExpandedRW::~SpaceExpandedRW()
{
}

const QString SpaceExpandedRW::StaticClassName =  QLatin1String("srps.space.rw.txt.expanded");

QString SpaceExpandedRW::componentClassName() const
{
    return StaticClassName;
}

int SpaceExpandedRW::prepareSpaceRead( QIODevice * device, SRPS::Space * space )
{
  if ( !space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  read_device = device;
  read_space = space;

  return 0;
}

int SpaceExpandedRW::readSpace()
{
  if ( !read_space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! read_device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( read_device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");


  bool header_readed = false;
  bool species_name_left = false;
  bool species_name_rigth = false;
  bool create_cell = true;

  Space::MIDList cells;

    static const char SEPARATOR_SINGLE = '|';
  static const QByteArray SPECIES_NAME = "species_name";

  while( !read_device->atEnd() )
  {
    QByteArray dataline = read_device->readLine().simplified();

    if ( !dataline.length() ) continue; // skip empty lines.

    QList<QByteArray> tokens = dataline.split(SEPARATOR_SINGLE);
    QList<QByteArray>::iterator iterator = tokens.begin();
    QList<QByteArray>::iterator end      = tokens.end();
    for( ; iterator != end; ++iterator )
      *iterator = iterator->simplified();

    // try to read header
    if ( !header_readed )
    {
      int size = tokens.size();
      if ( tokens.first() == SPECIES_NAME ) // header!!!
      {
        species_name_left = true;
        --size;
      }
      if ( tokens.last() == SPECIES_NAME ) // header!!!
      {
        species_name_rigth = true;
        --size;
      }
      header_readed = species_name_left || species_name_rigth;

      // for parsing header like: |species_name
      // for parsing header like: species_name|
      if( tokens.size() == 2 && header_readed ) --size;
      if ( header_readed )
      {
        if( size > 0)
        {
          create_cell = false;
          for( int c = species_name_left?1:0;
               species_name_left?c<=size:c<size;
               ++c )
          {
            Space::MemoryIdentifier cmid = read_space->createCellMID();
            read_space->cellSetProperty(cmid,"srps.name",QString(tokens.at(c)));
            cells << cmid;
          }
        }
//        else, incomplete header, make cell creation dynamic.
        continue;
      }
    }

    header_readed = true;
    int size = tokens.size();
    // read and create if necessary
    QByteArray name;
    bool species_name_left_ok;
    bool species_name_rigth_ok;
    tokens.first().toInt(&species_name_left_ok);
    if( species_name_left && !species_name_left_ok )
    {
      name = tokens.first();
      --size;
    }
    tokens.last().toInt(&species_name_rigth_ok);
    if ( species_name_rigth && !species_name_rigth_ok )
    {
      --size;
      if ( tokens.last() != name )
      {
        if ( name.size() )
          name.append(' ').append(tokens.last());
        else
          name = tokens.last();
      }
    }

    if ( size <= 0 ) // create especies but not distribute it.
    {
      Space::MemoryIdentifier smid = read_space->createSpeciesMID();
      if ( read_space->speciesIsValid(smid) && name.count() )
      {
        read_space->speciesSetProperty(smid,"srps.name",QString(name));
      }
    }
    else
    {
      if ( species_name_left && !species_name_left_ok )
        tokens.pop_front();

      int real_count = size;
      if ( size > cells.count() && create_cell )
      {
        int val = size - cells.count();
        while( val -- )
         cells << read_space->createCellMID();
      }

      Space::MemoryIdentifier smid = read_space->createSpeciesMID();
      if ( ( species_name_left && !species_name_left_ok ) ||
           ( species_name_rigth && !species_name_rigth_ok ) )
        read_space->speciesSetProperty(smid,"srps.name",QString(name));

      for( int c = 0; c < real_count; ++c )
      {
        int ic = tokens.at(c).toInt();
        while( ic -- )
          read_space->cellInsertIndividual(cells.at(c),
                                      smid,
                                      read_space->speciesCreateIndividualMID(smid)
                                      );
      }
    }
  }
  return 0;
}

int SpaceExpandedRW::prepareSpaceWrite( const SRPS::Space * space , QIODevice * device )
{
  if ( !space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::WriteOnly ) )
    return 201; //tr("QIODevice can't read.");

  write_device = device;
  write_space = space;

  return 0;
}

int SpaceExpandedRW::writeSpace()
{
  if ( !write_space )
    return 100;//tr("SRPS::Space is null.");

  if ( ! write_device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( write_device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  QBuffer buffer;
  if ( ! buffer.open( QIODevice::WriteOnly ) )
    return 400;//tr("Interal QBuffer cant write.");
  {
    QTextStream stream(&buffer);

    Space::MIDList cellMIDs = write_space->cellMIDs(Space::CreationOrder);
    Space::MIDList speciesMIDs = write_space->speciesMIDs(Space::CreationOrder);

    // calculate paddings
    int lastCol =  cellMIDs.count();
    QVector<int> pads(lastCol+1); // columns based, first column

    static const QByteArray SPECIES_NAME = "species_name";
    pads[lastCol] = SPECIES_NAME.length();

    for( int c = 0; c < cellMIDs.count(); ++c )
    {
      QByteArray sname = write_space->cellProperty(
          cellMIDs.at(c),
          "srps.name",c+1).toString().simplified().toAscii();
      if ( !write_space->cellHasProperty(cellMIDs.at(c),"srps.name") || !sname.count() )
        sname = QByteArray::number(c+1);

      pads[c] = sname.count();

      for( int r = 0; r < speciesMIDs.count(); ++r )
      {
        QByteArray text = QByteArray::number(
            write_space->cellSpeciesIndividualCount(cellMIDs.at(c),
                                              speciesMIDs.at(r))
        );
        int length = text.length();

        if ( pads.at(c) < length )
          pads[c] = length;

        if ( c == 0 )
        {
          text = write_space->speciesProperty(speciesMIDs.at(r),
                                        "srps.name").toString().simplified().toAscii();
          length = text.length();
          if ( pads.at(lastCol) < length )
            pads[lastCol] = length;
        }
      }
    }

    stream.setCodec("UTF-8");
    static const QByteArray SEPARATOR = " | ";
    static const QByteArray CRLF = "\r\n";

    // With pads calculated, printed data is more readable
    // write header

    stream.setFieldWidth(pads.at(lastCol));
    stream.setFieldAlignment(stream.AlignLeft);
    stream << SPECIES_NAME;
    stream.setFieldAlignment(stream.AlignRight);
    stream.setFieldWidth(0);
    stream << SEPARATOR;

    for( int i = 0; i < cellMIDs.count(); ++i )
    {
      stream.setFieldWidth(pads.at(i));
      QByteArray sname = write_space->cellProperty(
          cellMIDs.at(i),
          "srps.name",i+1).toString().simplified().toAscii();
      if ( !write_space->cellHasProperty(cellMIDs.at(i),"srps.name") || !sname.count() )
        sname = QByteArray::number(i+1);

      stream << sname;
      stream.setFieldWidth(0);
      stream << SEPARATOR;
    }

    stream.setFieldWidth(pads.at(lastCol));
    stream.setFieldAlignment(stream.AlignLeft);
    stream << SPECIES_NAME;
    stream.setFieldAlignment(stream.AlignRight);
    stream.setFieldWidth(0);
    stream << CRLF;

    // write data, at end, species name,
    for( int r = 0; r < speciesMIDs.count(); ++r )
    {
      QByteArray sname = write_space->speciesProperty(speciesMIDs.at(r),
                                                "srps.name").toString().simplified().toAscii();

      stream.setFieldWidth(pads.at(lastCol));
      stream.setFieldAlignment(stream.AlignLeft);
      stream << sname;
      stream.setFieldWidth(0);
      stream << SEPARATOR;
      stream.setFieldAlignment(stream.AlignRight);

      for( int c = 0; c < cellMIDs.count(); ++c )
      {
        stream.setFieldWidth(pads.at(c));
        stream << write_space->cellSpeciesIndividualCount(cellMIDs.at(c),
                                                    speciesMIDs.at(r));
        stream.setFieldWidth(0);
        stream << SEPARATOR;
      }

      stream.setFieldWidth(pads.at(lastCol));
      stream.setFieldAlignment(stream.AlignLeft);
      stream << sname;
      stream.setFieldAlignment(stream.AlignRight);
      stream.setFieldWidth(0);
      stream << CRLF;
    }
  }

  if ( write_device->write( buffer.data() ) != buffer.data().length() )
    return 401;//tr("Cant write buffer to write_device");

  write_device = 0;
  write_space = 0;

  return 0;//;QString();
}

} // namespace Implementation
} // namespace SRPS
