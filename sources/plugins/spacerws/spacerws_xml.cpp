/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spacerws_xml.h"

#include <QtCore/QBuffer>
#include <QtXml/QDomDocument>
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

static const QString NAMESPACE = QString("srps");
static const QString NAMESPACEURI = QString(":/srps/space/rw/xml");
static const QString NAMESPACEPREFIX = QString("srps");
static const QString ROOT = QString("space");
static const QString CELL = QString("cell");
static const QString SPECIES = QString("species");
static const QString INDIVIDUAL = QString("individual");
static const QString NAME = QString("name");
static const QString DESCRIPTION = QString("description");
static const QString PROPERTY = QString("property");

SpaceXmlRW::SpaceXmlRW()
  : SpaceRW()
{
  read_device = 0;
  write_device = 0;
  read_space = 0;
  write_space = 0;
}

SpaceXmlRW::~SpaceXmlRW()
{
}

const QString SpaceXmlRW::StaticClassName =  QLatin1String("srps.space.rw.xml");

QString SpaceXmlRW::componentClassName() const
{
    return StaticClassName;
}

int SpaceXmlRW::prepareSpaceRead( QIODevice * device, SRPS::Space * space )
{
  if ( !space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  read_device = device;
  read_space = space;

  return 0;
}


int SpaceXmlRW::readSpace()
{
  if ( !read_space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! read_device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( read_device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  QDomDocument document;
  xmlError.clear();
  if ( !document.setContent(read_device,&xmlError) )
    return 300;

  QDomElement root = document.documentElement();
  if ( root.tagName() != ROOT )
    return 301;

  QDomElement element = root.firstChildElement();

  Space::MIDList cells;

  while(!element.isNull())
  {
    if ( element.tagName() == PROPERTY )
    {
      QString name = element.attribute("name");
      if ( !name.isEmpty() )
        read_space->componentSetProperty(name,element.text());
    }
    else if ( element.tagName() == CELL )
    {
      QDomElement cellElement = element.firstChildElement();
      Space::MemoryIdentifier cell = read_space->createCellMID();
      cells << cell;
      while( !cellElement.isNull() )
      {
        if ( cellElement.tagName() == PROPERTY )
        {
          QString name = cellElement.attribute("name");
          if ( !name.isEmpty() )
            read_space->cellSetProperty(cell,name,cellElement.text());
        }
        cellElement = cellElement.nextSiblingElement();
      }
    }
    else if ( element.tagName() == SPECIES )
    {
      QDomElement speciesElement = element.firstChildElement();
      Space::MemoryIdentifier species = read_space->createSpeciesMID();
      while( !speciesElement.isNull() )
      {
        if ( speciesElement.tagName() == PROPERTY )
        {
          QString name = speciesElement.attribute("name");
          if ( !name.isEmpty() )
            read_space->speciesSetProperty(species,name,speciesElement.text());
        }
        else if ( speciesElement.tagName() == INDIVIDUAL )
        {
          QDomElement individualElement = speciesElement.firstChildElement();
          Space::MemoryIdentifier individual = read_space->speciesCreateIndividualMID(species);

          while( !individualElement.isNull() )
          {
            if ( individualElement.tagName() == PROPERTY )
            {
              QString name = individualElement.attribute("name");
              if ( !name.isEmpty() )
                read_space->individualSetProperty(individual,name,individualElement.text());
            }
            individualElement = individualElement.nextSiblingElement();
          }

          // in cel????

          bool ok = false;
          int index = speciesElement.attribute(CELL).toInt(&ok);

          if ( index >= 0 && index < cells.count() && ok )
            read_space->cellInsertIndividual(cells.at(index),species,individual);
        }
        speciesElement = speciesElement.nextSiblingElement();
      }
    }

    element = element.nextSiblingElement();
  }

  read_device = 0;
  read_space = 0;

  return 0;
}

int SpaceXmlRW::prepareSpaceWrite( const SRPS::Space * space, QIODevice * device )
{
  if ( !space )
    return 100;//tr("SRPS::Space is null.");

  if ( ! device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  write_device =  device;
  write_space = space;

  return 0;
}

int SpaceXmlRW::writeSpace()
{
  if ( !write_space )
    return 100;//tr("SRPS::Space is null.");

  if ( ! write_device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( write_device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  QBuffer buffer;
  if ( ! buffer.open( QIODevice::WriteOnly ) )
    return 400;//tr("Interal QBuffer cant write.");
  {
    QDomDocument document;
    QDomElement root = document.createElement(ROOT);
    document.appendChild(root);

    foreach( QString pname, write_space->componentPropertyNames() )
    {
      QDomElement property = document.createElement(PROPERTY);
      property.setAttribute("name",pname);
      QDomText text = document.createTextNode(write_space->componentProperty(pname).toString());
      property.appendChild(text);
      root.appendChild(property);
    }

    Space::MIDList cells = write_space->cellMIDs(Space::CreationOrder); // lista con orden
    QHash<Space::MemoryIdentifier,int> cellIndex;

    {
      int counter = -1;
      Space::MIDList::const_iterator iterator = cells.constBegin();
      Space::MIDList::const_iterator end = cells.constEnd();
      for( ; iterator != end; ++iterator )
        cellIndex.insert(*iterator,++counter); // starts in ONE, rest 1 in reading.
    }

    foreach( Space::MemoryIdentifier cell, cells )
    {
      QDomElement cellElement = document.createElement(CELL);
      foreach( QString pname, write_space->cellPropertyNames(cell) )
      {
        QDomText text = document.createTextNode(write_space->cellProperty(cell,
                                                                    pname).toString());
        QDomElement property = document.createElement(PROPERTY);
        property.setAttribute("name",pname);
        property.appendChild(text);
        cellElement.appendChild(property);
      }
      root.appendChild(cellElement);
    }

    Space::MIDList species = write_space->speciesMIDs(Space::CreationOrder); // tuplas con orden

    foreach( Space::MemoryIdentifier specie, species )
    {
      QDomElement specieElement = document.createElement(SPECIES);
      foreach( QString pname, write_space->speciesPropertyNames(specie) )
      {
        QDomText text = document.createTextNode(write_space->speciesProperty(specie,
                                                                       pname).toString());
        QDomElement property = document.createElement(PROPERTY);
        property.setAttribute("name",pname);
        property.appendChild(text);
        specieElement.appendChild(property);
      }
      foreach( Space::MemoryIdentifier individual, write_space->speciesIndividualMIDs(specie,Space::CreationOrder) )
      {
        QDomElement individualElement = document.createElement(INDIVIDUAL);

        Space::MemoryIdentifier cmid = write_space->individualCellMID(individual);
        if ( write_space->cellIsValid(cmid) )
        {
          individualElement.setAttribute(CELL,cellIndex.value(cmid));
        }

        foreach( QString pname, write_space->individualPropertyNames(individual) )
        {
          QDomText text = document.createTextNode(
              write_space->individualProperty(individual,
                                        pname).toString());
          QDomElement property = document.createElement(PROPERTY);
          property.setAttribute("name",pname);
          property.appendChild(text);
          specieElement.appendChild(property);
        }
        specieElement.appendChild(individualElement);
      }
      root.appendChild(specieElement);
    }

    QTextStream stream(&buffer);
    stream << document.toString();
  }

  if ( write_device->write( buffer.data() ) != buffer.data().length() )
    return 401;//tr("Cant write buffer to device");

  write_device = 0;
  write_space = 0;

  return 0;//;QString();
}

} // namespace Implementation
} // namespace SRPS
