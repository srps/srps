/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SPACEXMLRW_H
#define SPACEXMLRW_H

#include <SRPS/Space>
#include <SRPS/SpaceRW>

namespace SRPS {
namespace Implementation {

class SpaceXmlRW : public SRPS::SpaceRW
{
  Q_DISABLE_COPY(SpaceXmlRW)
  QString xmlError;

  SRPS::Space * read_space;
  QIODevice * read_device;
  const SRPS::Space * write_space;
  QIODevice * write_device;

  public:
    explicit SpaceXmlRW();
    virtual ~SpaceXmlRW();

    static const QString StaticClassName;

    QString componentClassName() const;

    QString resultString( int code ) const;

    int prepareSpaceRead( QIODevice * device, SRPS::Space * space );

    int readSpace();

    int prepareSpaceWrite( const SRPS::Space * space, QIODevice * device );

    int writeSpace();
};

} // namespace Implementation
} // namespace SRPS

#endif // SPACEXMLRW_H
