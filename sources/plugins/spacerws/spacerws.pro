include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}SpaceRWs
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += spacerws_plugin.cpp \
    spacerws_xml.cpp \
    spacerws_expanded.cpp \
    spacerws_simplified.cpp
HEADERS += spacerws_plugin.h \
    spacerws_xml.h \
    spacerws_expanded.h \
    spacerws_simplified.h
QT += xml

RESOURCES += \
    resources.qrc
