/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spacerws_simplified.h"

#include <QtCore/QBuffer>
#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

SpaceSimplifiedRW::SpaceSimplifiedRW()
  : SpaceRW()
{
  read_device = 0;
  write_device = 0;
  read_space = 0;
  write_space = 0;
}

SpaceSimplifiedRW::~SpaceSimplifiedRW()
{
}

const QString SpaceSimplifiedRW::StaticClassName =  QLatin1String("srps.space.rw.txt.simplified");

QString SpaceSimplifiedRW::componentClassName() const
{
    return StaticClassName;
}

int SpaceSimplifiedRW::prepareSpaceRead( QIODevice * device, SRPS::Space * space )
{
  if ( !space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");

  read_device = device;
  read_space = space;

  return 0;
}

int SpaceSimplifiedRW::readSpace()
{
  if ( !read_space )
    return 100; //tr("SRPS::Space is null.");

  if ( ! read_device )
    return 200; //tr("QIODevice is null.");
  if ( ! ( read_device->openMode() & QIODevice::ReadOnly ) )
    return 201; //tr("QIODevice can't read.");


  bool cell_count_readed = false;
  bool can_create_cell = true;

  Space::MIDList cells;

  while( !read_device->atEnd() )
  {
    QByteArray line = read_device->readLine().simplified();
    if ( line.isEmpty() ) continue;

    QList<QByteArray> tokens = line.split(' ');
    if( tokens.size() == 1 && !cell_count_readed )
    {
      bool ok;
      int cell_count = tokens.at(0).toInt(&ok);

      if ( !ok || cell_count <= 0 ) // WTF? First not empty line and text?
                                    // Negative cell count, BAD! ignore line.
                                    // but mark header has readed and enable dynamic
                                    // cell creation.
      {
        cell_count_readed = true;
        continue;
      }

      while( cell_count -- )
        cells << read_space->createCellMID();

      cell_count_readed = true;
      can_create_cell = false;
    }
    else
    {
      cell_count_readed = true;
      bool ok = true;
      int ll = 0;
      int ul = 0;
      QByteArray name;

      switch( tokens.size() ) // species with range 1 at pos
      {
      case 0:
        ok = false; // WTF?
        break;
      case 1:
        ll = tokens.at(0).toInt(&ok) - 1;
        ul = ll;
        if ( !ok ) continue; // can convert text, skip line.
        break;
      case 2:
        ll = tokens.at(0).toInt(&ok) - 1;
        if ( !ok ) continue; // skip line

        ul = tokens.at(1).toInt( &ok ) - 1;
        if (!ok) // range 1 size with name
        {
          ul = ll;
          name = tokens.at(1);
        }
        break;
      default:
        ll = tokens.takeFirst().toInt() - 1;
        ul = tokens.takeFirst().toInt() - 1;
        foreach( QByteArray data, tokens )
          name.append(' ').append(data);
      }
      if ( ll < 0 ) ll = 0;
      if ( ul < 0 ) ul = 0;
      if ( ll > ul )
        qSwap(ll,ul);
      if ( ul >= cells.count() )
      {
        if ( can_create_cell )
        {
          int cell_count = 1 + ul - cells.count();
          while( cell_count -- )
            cells << read_space->createCellMID();
        }
        else
        {
          ul = cells.count() - 1;
          if ( ll > ul )
            ll = ul;
        }
      }

      Space::MemoryIdentifier smid = read_space->createSpeciesMID();
      if ( name.size() )
        read_space->speciesSetProperty(smid,"srps.name",QString(name.simplified()));
      for( int i = ll; i <= ul; ++i )
      {
        Space::MemoryIdentifier imid = read_space->speciesCreateIndividualMID(smid);
        read_space->cellInsertIndividual(cells.at(i),smid,imid);
      }
    }
  }

  read_device = 0;
  read_space = 0;
  return 0;
}

struct SpeciesSimplifiedData
{
  SpeciesSimplifiedData()
  {
    lower_limit = 0;
    upper_limit = 0;
  }

  int lower_limit;
  int upper_limit;
  QByteArray name;
};

int SpaceSimplifiedRW::prepareSpaceWrite( const SRPS::Space * space, QIODevice * device )
{
  if ( !space )
    return 100;//tr("SRPS::Space is null.");

  if ( ! device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  write_device = device;
  write_space = space;

  return 0;
}

int SpaceSimplifiedRW::writeSpace()
{
  if ( !write_space )
    return 100;//tr("SRPS::Space is null.");

  if ( ! write_device )
    return 200;//tr("QIODevice is null.");
  if ( ! ( write_device->openMode() & QIODevice::WriteOnly ) )
    return 202;//tr("QIODevice can't write.");

  QBuffer buffer;
  if ( ! buffer.open( QIODevice::WriteOnly ) )
    return 400;//tr("Interal QBuffer cant write.");
  {
    Space::MIDList cellMIDs = write_space->cellMIDs(Space::CreationOrder);
    Space::MIDList speciesMIDs = write_space->speciesMIDs(Space::CreationOrder);

    int lower_limit_pad = 0;
    int upper_limit_pad = 0;
    QVector<SpeciesSimplifiedData> speciesData;

    for( int s = 0; s < speciesMIDs.count(); ++s )
    {
      // save name
      SpeciesSimplifiedData sd;
      sd.name = write_space->speciesProperty(speciesMIDs.at(s),"srps.name").toString().simplified().toAscii();

      int fieldSize = 0;

      // find lower limit
      for( int c = 0; c < cellMIDs.count(); ++c )
      {
        if ( write_space->cellHasSpecies(cellMIDs.at(c),speciesMIDs.at(s)) )
        {
          sd.lower_limit = c+1;
          fieldSize = QByteArray::number(sd.lower_limit).size();
          if ( lower_limit_pad < fieldSize )
            lower_limit_pad = fieldSize;
          break;
        }
      }

      // find upper limit
      for( int c = cellMIDs.count()-1; c >= 0; --c )
      {
        if ( write_space->cellHasSpecies(cellMIDs.at(c),speciesMIDs.at(s)) )
        {
          sd.upper_limit = c+1;
          fieldSize = QByteArray::number(sd.upper_limit).size();
          if ( upper_limit_pad < fieldSize )
            upper_limit_pad = fieldSize;
          break;
        }
      }

      // save
      speciesData.append(sd);
    }

    QTextStream stream(&buffer);
    stream.setCodec("UTF-8");

    static const QByteArray CRLF = "\r\n";
    static const QByteArray SEPARATOR = " ";

    stream << cellMIDs.count() << CRLF;

    foreach( SpeciesSimplifiedData sd, speciesData )
    {
      stream.setFieldAlignment(stream.AlignRight);
      stream.setFieldWidth(lower_limit_pad);
      stream << sd.lower_limit;
      stream.setFieldWidth(0);
      stream << SEPARATOR;
      stream.setFieldWidth(upper_limit_pad);
      stream << sd.upper_limit;
      if ( sd.name.count() )
      {
        stream.setFieldWidth(0);
        stream << SEPARATOR;
        stream.setFieldAlignment(stream.AlignLeft);
        stream.setFieldWidth(0);
        stream << sd.name;
      }
      stream << CRLF;
    }
  }

  if ( write_device->write( buffer.data() ) != buffer.data().length() )
    return 401;//tr("Cant write buffer to device");

  write_device = 0;
  write_space = 0;

  return 0;//;QString();
}

} // namespace Implementation
} // namespace SRPS
