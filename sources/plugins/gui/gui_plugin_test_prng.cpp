/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"

#include "gui_wrapperdialog.h"
#include "gui_multiplenewcomponentwidget.h"

#include <SRPS/PRNG>
#include <SRPS/DPRNG>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include <QtCore/QFutureWatcher>
#include <QtCore/QTime>
#include <QtGui/QProgressDialog>
#include <QtGui/QTabWidget>
#include <QtGui/QMdiSubWindow>
#include <QtGui/QTableWidget>

#include <QtConcurrentMap>
#include <QtConcurrentRun>

struct TestData
{
  TestData():
      prngName(QLatin1String("Invalid name")),
      miliseconds(0),
      values()
  {}

  QString prngName;
  int miliseconds;
  QwtArray<double> values;
};

static const int COUNT = 1000*100*1;

static TestData testprng( SRPS::DPRNG * prng )
{
//  SRPS::PRNG * prng = const_cast<SRPS::PRNG*>(engine);
  TestData data;
  if ( !prng ) return data;

  data.prngName = prng->componentClassName();
  QTime time;
  data.values.resize(COUNT);
  time.start();

  QwtArray<double>::iterator iterator = data.values.begin();
  QwtArray<double>::iterator end = data.values.end();

  for ( ; iterator != end; ++iterator )
    *iterator = prng->nextOO();

  data.miliseconds = time.elapsed();

  return data;
}

static TestData qRandTest()
{
  TestData data;

  data.values.resize(COUNT);

  data.prngName = QObject::tr("Qt qrand()");

  QTime time;
  time.start();

  int tmp = COUNT;
  while( --tmp + 1 ) data.values[tmp] = double(qrand()) / RAND_MAX ;

  data.miliseconds = time.elapsed();

  return data;
}


namespace SRPS {
namespace Implementation {

void MainWindow::on_actionTestPRNG_triggered(){

  WrapperDialog wdlg;
  MultipleNewComponetWidget * mncw = new MultipleNewComponetWidget;
  mncw->setFactory(p->SRPS_Factory);
  mncw->setComponents(p->SRPS_Factory->prngFactoryKeys());
  mncw->setup();

  wdlg.setWidget(mncw);

  if ( wdlg.exec() != wdlg.Accepted ) return;

  QList<SRPS::DPRNG*> prngs;

  QList<QPair<QString,QVariantHash> > confs = mncw->selectedComponentConfigurations();
  while( !confs.isEmpty() )
  {
    QPair<QString,QVariantHash> conf = confs.takeFirst();
    SRPS::PRNG * prng = p->SRPS_Factory->prngFactoryCreate(conf.first,conf.second);
    if ( !prng ) continue;
    SRPS::DPRNG * dprng = p->SRPS_Factory->dprngFactoryCreate("srps.dprng.normal");
    dprng->setPRNG(prng);
//    dprng->setRange(DPRNG::Range(0,1));
    prngs << dprng;
  }

  //  From Qt Exmaple :D
  // Create a progress dialog.
  QProgressDialog dialog;
  dialog.setLabelText(QString("Testing PRNGs using %1 thread(s)...").arg(QThread::idealThreadCount()));

  // Create a QFutureWatcher and conncect signals and slots.
  QFutureWatcher<TestData> futureWatcher;
  QObject::connect(&futureWatcher, SIGNAL(finished()), &dialog, SLOT(reset()));
  QObject::connect(&dialog, SIGNAL(canceled()), &futureWatcher, SLOT(cancel()));
  QObject::connect(&futureWatcher, SIGNAL(progressRangeChanged(int,int)), &dialog, SLOT(setRange(int,int)));
  QObject::connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &dialog, SLOT(setValue(int)));

  // Start the computation.
  futureWatcher.setFuture(QtConcurrent::mapped(prngs,testprng));

  // Display the dialog and start the event loop.
  dialog.exec();

  QFuture<TestData> qrandData = QtConcurrent::run(qRandTest);

  futureWatcher.waitForFinished();

  qDeleteAll(prngs);

  int PRNG_TEXT_COUNTER = 0;

  if ( !futureWatcher.isCanceled() )
  {
    QTabWidget * tabWidget = new QTabWidget;

    QwtArray<double> xData;
    xData.resize(COUNT);
    {
      int temp = COUNT;
      while(--temp + 1) xData[temp] = temp;
    }

    QTableWidget * resultTable = new QTableWidget;
    tabWidget->addTab(resultTable,tr("Resume"));

    resultTable->setSortingEnabled(true);

    resultTable->setColumnCount(4);
    resultTable->setEditTriggers(QTableWidget::NoEditTriggers);
    resultTable->verticalHeader()->hide();
    resultTable->setHorizontalHeaderLabels(
        QStringList() << tr("ID") << tr("PRNG name") << tr("Iterations") << tr("Elapsed time")
        );

    QPen pen;
    pen.setWidthF(0.25);
    // qrand
    {
      TestData qdata = qrandData.result();
      QwtPlot * plot = new QwtPlot;
      plot->setAxisTitle(QwtPlot::yLeft,tr("Range"));
      plot->setAxisTitle(QwtPlot::xBottom,tr("Iteration"));
      plot->setTitle(
          tr("Scatter plot for: %1 in %2 miliseconds")
            .arg(qdata.prngName)
            .arg(qdata.miliseconds));
      QwtPlotCurve * curve = new QwtPlotCurve;
      curve->setData(xData,qdata.values);
      curve->setStyle(QwtPlotCurve::Dots);
      curve->setPen(pen);
      curve->attach(plot);
      tabWidget->addTab(plot,QString("%1: %2").arg(++PRNG_TEXT_COUNTER).arg(qdata.prngName));
      plot->replot();

      int r = resultTable->rowCount();
      resultTable->insertRow(r);

      resultTable->setItem(r,0,new QTableWidgetItem(QString::number(PRNG_TEXT_COUNTER)));
      resultTable->setItem(r,1,new QTableWidgetItem(qdata.prngName));
      resultTable->setItem(r,
                           2,
                           new QTableWidgetItem(QString::number(COUNT)));
      resultTable->setItem(r,
                           3,
                           new QTableWidgetItem(QString::number(qdata.miliseconds)));
    }

    {
      QFuture<TestData> future = futureWatcher.future();
      QFuture<TestData>::const_iterator iterator = future.constBegin();
      QFuture<TestData>::const_iterator end = future.constEnd();
      for( ;  iterator != end; ++iterator )
      {
        QwtPlot * plot = new QwtPlot;
        plot->setAxisTitle(QwtPlot::yLeft,tr("Range"));
        plot->setAxisTitle(QwtPlot::xBottom,tr("Iteration"));
        plot->setTitle(
            tr("Scatter plot for: %1 in %2 miliseconds")
              .arg(iterator->prngName)
              .arg(iterator->miliseconds));
        QwtPlotCurve * curve = new QwtPlotCurve;
        curve->setData(xData,iterator->values);
        curve->setStyle(QwtPlotCurve::Dots);
        curve->setPen(pen);
        curve->attach(plot);
        tabWidget->addTab(plot,QString("%1: %2")
                                .arg(++PRNG_TEXT_COUNTER)
                                .arg(iterator->prngName));
        plot->replot();

        int r = resultTable->rowCount();
        resultTable->insertRow(r);

        resultTable->setItem(r,0,new QTableWidgetItem(QString::number(PRNG_TEXT_COUNTER)));
        resultTable->setItem(r,1,new QTableWidgetItem(iterator->prngName));
        resultTable->setItem(r,2,new QTableWidgetItem(QString::number(COUNT)));
        resultTable->setItem(r,3,new QTableWidgetItem(QString::number(iterator->miliseconds)));
      }
    }

    QMdiSubWindow * subwindow = ui->mdiArea->addSubWindow(tabWidget);
    subwindow->setWindowTitle(tr("PRNGs Test Result"));
    subwindow->setAttribute(Qt::WA_DeleteOnClose,true);
    subwindow->showNormal();
  }
}

}
}
