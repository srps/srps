/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_SRPSFACTORY_H
#define GUI_SRPSFACTORY_H

#include <SRPS/ComponentFactory>
#include <SRPS/CurveFactory>
#include <SRPS/CurveEditorFactory>
#include <SRPS/CurveRWFactory>
#include <SRPS/CurveVisualizerFactory>
#include <SRPS/SpaceFactory>
#include <SRPS/SpaceEditorFactory>
#include <SRPS/SpaceRWFactory>
#include <SRPS/PRNGFactory>
#include <SRPS/DPRNGFactory>
#include <SRPS/ModelFactory>
#include <SRPS/AccumulatorFactory>
#include <SRPS/SpaceGeneratorFactory>
#include <SRPS/SimulatorFactory>
#include <SRPS/SettingsManager>

#include <QtCore/QObject>

namespace SRPS {
namespace Implementation {

class SRPSFactory :
    public QObject,
    public SRPS::ComponentFactory,
    public SRPS::CurveFactory,
    public SRPS::CurveEditorFactory,
    public SRPS::CurveRWFactory,
    public SRPS::CurveVisualizerFactory,
    public SRPS::SpaceFactory,
    public SRPS::SpaceEditorFactory,
    public SRPS::SpaceRWFactory,
    public SRPS::PRNGFactory,
    public SRPS::DPRNGFactory,
    public SRPS::ModelFactory,
    public SRPS::AccumulatorFactory,
    public SRPS::SpaceGeneratorFactory,
    public SRPS::SimulatorFactory
{
  Q_OBJECT
  Q_INTERFACES(SRPS::ComponentFactory
               SRPS::CurveFactory
               SRPS::CurveEditorFactory
               SRPS::CurveRWFactory
               SRPS::CurveVisualizerFactory
               SRPS::SpaceFactory
               SRPS::SpaceEditorFactory
               SRPS::SpaceRWFactory
               SRPS::PRNGFactory
               SRPS::DPRNGFactory
               SRPS::ModelFactory
               SRPS::AccumulatorFactory
               SRPS::SpaceGeneratorFactory
               SRPS::SimulatorFactory)

  class Private;
  Private * p;

public:

  static const QString DefaultCurve;
  static const QString DefaultCurveEditor;
  static const QString DefaultCurveRW;
  static const QString DefaultCurveVisualizer;
  static const QString DefaultSpace;
  static const QString DefaultSpaceEditor;
  static const QString DefaultSpaceRW;
  static const QString DefaultPRNG;
  static const QString DefaultDPRNG;

  explicit SRPSFactory(QObject *parent = 0);
  virtual ~SRPSFactory();


  QWidget * componentFactoryWidgetFor(const QString & key) const;

  QVariantHash componentFactoryDefaultParametersFor(const QString & key) const;

  bool componentFactorySetDefaultParametersFor(const QString & key,
                                      const QVariantHash & parameters);

  // Curve

  QString curveFactoryDefault() const;
  void setCurveFactoryDefault( const QString & );

  QStringList curveFactoryKeys() const;

  SRPS::Curve * curveFactoryCreate( const QString & curve,
                                    const QVariantHash & parameters
                                                      = QVariantHash() );

  // Curve EDItors

  QString curveEditorFactoryDefault() const;
  void setCurveEditorFactoryDefault( const QString & );

  QStringList curveEditorFactoryKeys() const;

  SRPS::CurveEditor *curveEditorFactoryCreate( const QString & curveEditor,
                                               const QVariantHash & parameters
                                                  = QVariantHash() );

  // Curve RWs

  QString curveRWFactoryDefault() const;
  void setCurveRWFactoryDefault( const QString & );

  QStringList curveRWFactoryKeys() const;

  SRPS::CurveRW *curveRWFactoryCreate( const QString & curveRW,
                                               const QVariantHash & parameters
                                                  = QVariantHash() );

  // Curve Visualizers

  QString curveVisualizerFactoryDefault() const;
  void setCurveVisualizerFactoryDefault( const QString & );

  QStringList curveVisualizerFactoryKeys() const;

  SRPS::CurveVisualizer *curveVisualizerFactoryCreate( const QString & curveVisualizer,
                                               const QVariantHash & parameters
                                                  = QVariantHash() );

  // Space

  QString spaceFactoryDefault() const;
  void setSpaceFactoryDefault( const QString & );

  QStringList spaceFactoryKeys() const;

  SRPS::Space *spaceFactoryCreate( const QString & space,
                                               const QVariantHash & parameters
                                                  = QVariantHash() );

  // SpaceEditor

  QString spaceEditorFactoryDefault() const;
  void setSpaceEditorFactoryDefault( const QString & );

  QStringList spaceEditorFactoryKeys() const;

  SRPS::SpaceEditor *spaceEditorFactoryCreate( const QString & spaceEditor,
                                               const QVariantHash & parameters
                                                  = QVariantHash() );

  // SpaceRW

  QString spaceRWFactoryDefault() const;
  void setSpaceRWFactoryDefault( const QString & );

  QStringList spaceRWFactoryKeys() const;

  SRPS::SpaceRW *spaceRWFactoryCreate( const QString & spaceRW,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  // PRNG

  QString prngFactoryDefault() const;
  void setPRNGFactoryDefault( const QString & );

  QStringList prngFactoryKeys() const;

  SRPS::PRNG *prngFactoryCreate( const QString & prng,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  // DPRNG

  QString dprngFactoryDefault() const;
  void setDPRNGFactoryDefault( const QString & );

  QStringList dprngFactoryKeys() const;

  SRPS::DPRNG *dprngFactoryCreate( const QString & dprng,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  // Model

  QStringList modelFactoryKeys() const;

  SRPS::Model *modelFactoryCreate( const QString & model,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  // Accumulator

  QStringList accumulatorFactoryKeys() const;

  SRPS::Accumulator *accumulatorFactoryCreate( const QString & accumulator,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  // Generators

  QStringList spaceGeneratorFactoryKeys() const;

  SRPS::SpaceGenerator *spaceGeneratorFactoryCreate( const QString & spaceGenerator,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  QStringList simulatorFactoryKeys() const;

  SRPS::Simulator *simulatorFactoryCreate( const QString & simulator,
                                       const QVariantHash & parameters
                                                  = QVariantHash() );

  // others

  void loadSettings(SRPS::SettingsManager *);
  void saveSettings(SRPS::SettingsManager *);

  void addComponentFactory( QObject * object );

};

} // namespace Implementation
} // namespace SRPS

#endif // GUI_SRPSFACTORY_H
