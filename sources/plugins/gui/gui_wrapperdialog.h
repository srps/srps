/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_WRAPPERDIALOG_H
#define GUI_WRAPPERDIALOG_H

#include <QtGui/QDialog>

namespace SRPS {
namespace Implementation {

namespace Ui {
    class WrapperDialog;
}

class WrapperDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WrapperDialog(QWidget *parent = 0);
    virtual ~WrapperDialog();

    void setWidget( QWidget * widget );

protected:
    void changeEvent(QEvent *e);

private:
    Ui::WrapperDialog *ui;
};


} // namespace Implementation
} // namespace SRPS
#endif // GUI_WRAPPERDIALOG_H
