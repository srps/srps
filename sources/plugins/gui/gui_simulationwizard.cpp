/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_simulationwizard.h"
#include <QtGui/QGridLayout>
#include <QtGui/QFormLayout>
#include <QtGui/QLabel>

namespace SRPS {
namespace Implementation {

SimulationWizardSinglePage::SimulationWizardSinglePage(QWidget * parent ):
        QWizardPage(parent),
        widget(new SingleNewComponentWidget )
{
    QGridLayout * layout = new QGridLayout;
    layout->addWidget(widget);
    setLayout(layout);

    connect(widget,SIGNAL(selectedComponentChanged(QString)),
            this,SIGNAL(completeChanged()));
}

SimulationWizardSinglePage::~SimulationWizardSinglePage()
{
}

bool SimulationWizardSinglePage::isComplete()
{
    return !selectedComponent().isEmpty();
}

void SimulationWizardSinglePage::setFactory( SRPSFactory * factory )
{
    widget->setFactory(factory);
}

void SimulationWizardSinglePage::setComponents( const QStringList & components )
{
    widget->setComponents(components);
}

void SimulationWizardSinglePage::setup()
{
    widget->setup();
    emit completeChanged();
}

QString SimulationWizardSinglePage::selectedComponent() const
{
    return widget->selectedComponent();
}

QVariantHash SimulationWizardSinglePage::selectedComponentConfiguration() const
{
    return widget->selectedComponentConfiguration();
}

//

SimulationWizardMultiplePage::SimulationWizardMultiplePage(QWidget * parent ):
        QWizardPage(parent),
        widget(new MultipleNewComponetWidget )
{
    QGridLayout * layout = new QGridLayout;
    layout->addWidget(widget);
    setLayout(layout);

    connect(widget,SIGNAL(selectedComponentConfigurationCountChanged(int)),
            this,SIGNAL(completeChanged()));
}

SimulationWizardMultiplePage::~SimulationWizardMultiplePage()
{
}

bool SimulationWizardMultiplePage::isComplete()
{
    return !selectedComponentConfigurations().isEmpty();
}

void SimulationWizardMultiplePage::setFactory( SRPSFactory * factory )
{
    widget->setFactory(factory);
}

void SimulationWizardMultiplePage::setComponents( const QStringList & components )
{
    widget->setComponents(components);
}

void SimulationWizardMultiplePage::setup()
{
    widget->setup();
    emit completeChanged();
}

bool SimulationWizardMultiplePage::validatePage()
{
  return !selectedComponentConfigurations().isEmpty();
}

QList<QPair<QString,QVariantHash> > SimulationWizardMultiplePage::selectedComponentConfigurations() const
{
    return widget->selectedComponentConfigurations();
}

//

SimulationWizardFinalPage::SimulationWizardFinalPage(QWidget* parent):
    QWizardPage(parent)
{
  spin = new QSpinBox;
  spin->setRange(0,1000000);
  spin->setValue(100);

  check = new QCheckBox;
  check->setChecked(false);
  check->setText(trUtf8("¿Acumular espacio de referencia?"));

  QLabel *label = new QLabel(trUtf8("Cantidad de iteraciones por modelo"));

  QFormLayout * layout = new QFormLayout;
  layout->addRow(label,spin);
  layout->addRow(0,check);

  setLayout(layout);
}

SimulationWizardFinalPage::~SimulationWizardFinalPage()
{

}

int SimulationWizardFinalPage::iterations() const
{
  return spin->value();
}

bool SimulationWizardFinalPage::accumulate() const
{
  return check->isChecked();
}

//

SimulationWizard::SimulationWizard(QWidget * parent ):
        QWizard(parent),
        modelPage( new SimulationWizardMultiplePage ),
        accumulatorPage( new SimulationWizardMultiplePage ),
        prngPage( new SimulationWizardSinglePage ),
        dprngPage( new SimulationWizardSinglePage ),
        simulatorPage( new SimulationWizardSinglePage ),
        finalPage(new SimulationWizardFinalPage )
{

  modelPage->setTitle(tr("Modelos"));
  modelPage->setSubTitle(tr("Seleccione los modelos que desea simular"));

  accumulatorPage->setTitle(tr("Acumuladores"));
  accumulatorPage->setSubTitle(tr("Seleccione los acumuldores"));

  prngPage->setTitle(tr("Generador Aleatorio"));
  prngPage->setSubTitle(tr("Seleccione el generador aelatorio"));

  dprngPage->setTitle(trUtf8("Distribución númerica"));
  dprngPage->setSubTitle(trUtf8("Seleccione la distribución a utilizar"));

  simulatorPage->setTitle(tr("Simulador"));
  simulatorPage->setSubTitle(trUtf8("Seleccione el componete que realizará las simulaciones"));

  finalPage->setTitle(tr("Ajustes finales"));
  finalPage->setSubTitle(tr("Ajustes finales"));

    addPage(modelPage);
    addPage(accumulatorPage);
    addPage(prngPage);
    addPage(dprngPage);
    addPage(simulatorPage);
    addPage(finalPage);
}

SimulationWizard::~SimulationWizard()
{
}

QList<QPair<QString,QVariantHash> > SimulationWizard::modelConfigurations() const
{
    return modelPage->selectedComponentConfigurations();
}

QList<QPair<QString,QVariantHash> > SimulationWizard::accumulatorConfigurations() const
{
    return accumulatorPage->selectedComponentConfigurations();
}

QPair<QString,QVariantHash> SimulationWizard::prngConfiguration() const
{
    return QPair<QString,QVariantHash>(
            prngPage->selectedComponent(),
            prngPage->selectedComponentConfiguration()
            );
}

QPair<QString,QVariantHash> SimulationWizard::dprngConfiguration() const
{
    return QPair<QString,QVariantHash>(
        dprngPage->selectedComponent(),
        dprngPage->selectedComponentConfiguration()
        );
}

QPair<QString,QVariantHash> SimulationWizard::simulatorConfiguration() const
{
  return QPair<QString,QVariantHash>(
      simulatorPage->selectedComponent(),
      simulatorPage->selectedComponentConfiguration()
      );
}

void SimulationWizard::setFactory( SRPSFactory * factory )
{
    if ( factory )
    {
        modelPage->setFactory(factory);
        modelPage->setComponents(factory->modelFactoryKeys());
        modelPage->setup();

        accumulatorPage->setFactory(factory);
        accumulatorPage->setComponents(factory->accumulatorFactoryKeys());
        accumulatorPage->setup();

        prngPage->setFactory(factory);
        prngPage->setComponents(factory->prngFactoryKeys());
        prngPage->setup();

        dprngPage->setFactory(factory);
        dprngPage->setComponents(factory->dprngFactoryKeys());
        dprngPage->setup();

        simulatorPage->setFactory(factory);
        simulatorPage->setComponents(factory->simulatorFactoryKeys());
        simulatorPage->setup();
    }
}

int SimulationWizard::iterations() const
{
  return finalPage->iterations();
}

bool SimulationWizard::accumulateBaseSpace() const
{
  return finalPage->accumulate();
}

} // namespace Implementation
} // namespace SRPS
