/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"

#include "gui_wrapperdialog.h"
#include "gui_singlenewcomponentwidget.h"

#include <QtCore/QLibraryInfo>

#include <QtGui/QMessageBox>
#include <QtGui/QFileDialog>

namespace SRPS {
namespace Implementation {

// connections

void MainWindow::on_FileRW_projectFound( const QString & f )
{
  on_actionProjectNew_triggered();

  QTreeWidgetItem * item =currentProjectItem();

  if ( item )
  {
    item->setText(0,QFileInfo(f).baseName());
    item->setToolTip(0,f);
  }

}

void MainWindow::on_FileRW_fileSaved( const QString & f )
{
  QTreeWidgetItem * item = currentProjectItem();
  if ( item )
  {
    item->setText(0,QFileInfo(f).baseName());
    item->setToolTip(0,f);
  }

  QMessageBox::information(this,
                           trUtf8("Guardar"),
                           trUtf8("Projecto guardado en: %1").arg(f));
}

void MainWindow::on_FileRW_error( const QString & e )
{
  QMessageBox::critical(this,trUtf8("Error"),e);
}

void MainWindow::on_FileRW_exportedToFile( const QString & filepath,
                                           const SRPS::Curve * /*curve*/ )
{
  QMessageBox::information(this,trUtf8("SRPS"),trUtf8("Curva exportada a: %1").arg(filepath));
}

void MainWindow::on_FileRW_exportedToFile( const QString & filepath,
                                           const SRPS::Space * /*space*/ )
{
  QMessageBox::information(this,trUtf8("SRPS"),trUtf8("Espacio exportado a: %1").arg(filepath));
}

// actions

void MainWindow::on_actionExportToFile_triggered()
{
  QTreeWidgetItem * item = ui->projectsTree->currentItem();
  if (!item) return;

  if ( !p->curveHash.contains(item) && !p->spaceHash.contains(item) )
    return;

  QString filepath = QFileDialog::getSaveFileName(this,
                                                  trUtf8("Exportar"),
                                                  QLibraryInfo::location(QLibraryInfo::DataPath),
                                                  trUtf8("Archivo SRPS (*.srps)"));

  if (filepath.isEmpty()) return;

  WrapperDialog dlg;
  SingleNewComponentWidget * w = new SingleNewComponentWidget;
  dlg.setWidget(w);
  w->setFactory(p->SRPS_Factory);

  if( p->spaceHash.contains(item) )
  {
    w->setComponents(p->SRPS_Factory->spaceRWFactoryKeys());
    w->setup();

    if ( dlg.exec() == QDialog::Rejected ) return;

    p->File_RW->setSpaceRWKey(w->selectedComponent(),
                              w->selectedComponentConfiguration());

    p->File_RW->exportToFile(filepath,p->spaceHash.value(item)->getSpace());
  }
  else if ( p->curveHash.contains(item) )
  {
    w->setComponents(p->SRPS_Factory->curveRWFactoryKeys());
    w->setup();

    if ( dlg.exec() == QDialog::Rejected ) return;

    p->File_RW->setCurveRWKey(w->selectedComponent(),
                              w->selectedComponentConfiguration());

    p->File_RW->exportToFile(filepath,p->curveHash.value(item)->getCurve());
  }
}

void MainWindow::on_FileRW_curveReaded( const QString & filepath, SRPS::Curve * curve )
{
  if (!curve) return;

  if ( !curve->componentHasProperty("srps.name") )
    curve->componentSetProperty("srps.name",QFileInfo(filepath).baseName());

  QTreeWidgetItem * item = currentProjectItem();
  if ( !item )
    on_actionProjectNew_triggered();
  on_actionCurveNew_triggered(curve);
}

void MainWindow::on_FileRW_spaceReaded( const QString & filepath, SRPS::Space * space )
{
  if (!space) return;

  if ( !space->componentHasProperty("srps.name") )
    space->componentSetProperty("srps.name",QFileInfo(filepath).baseName());

  QTreeWidgetItem * item = currentProjectItem();
  if ( !item )
    on_actionProjectNew_triggered();
  on_actionSpaceNew_triggered(space);
}

}
}

