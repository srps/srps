/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_SETTINGSDIALOG_H
#define GUI_SETTINGSDIALOG_H

#include <QtGui/QDialog>

class QTreeWidgetItem;

namespace SRPS {

class SettingsManager;

namespace Implementation {

namespace Ui {
    class SettingsDialog;
}

/*!
  \brief Settings Dialog For SRPS
 */
class SettingsDialog : public QDialog
{
    Q_OBJECT

    class Private;
    Private * p;

public:
    explicit SettingsDialog(QWidget *parent = 0);
    virtual ~SettingsDialog();

    // target objects for settings.
    void setSRPSFactory( QObject * object );

    void reject();
    void accept();

protected:

    void changeEvent(QEvent *e);

private slots:

    void changeSettingsPage( QTreeWidgetItem * item );

    void loadSettings();
    void saveSettings();

    void configureDefaultComponent( const QString & key );

    void configureComponentSwitcher();

private:
    Ui::SettingsDialog *ui;
};


} // namespace Implementation
} // namespace SRPS
#endif // GUI_SETTINGSDIALOG_H
