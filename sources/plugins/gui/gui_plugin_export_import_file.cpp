#include <SRPS/SpaceRW>

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"

#include "gui_curveitem.h"
#include "gui_spaceitem.h"

#include <QtCore/QBuffer>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

namespace SRPS
{
namespace Implementation
{

void MainWindow::on_actionExportToFile_triggered()
{
  QTreeWidgetItem * item = ui->projectsTree->currentItem();
  if ( !item ) return;

  CurveItem * citem = dynamic_cast<CurveItem*>(item);
  SpaceItem * sitem = dynamic_cast<SpaceItem*>(item);

  if ( !citem && !sitem ) return;

  QString filepath = QFileDialog::getSaveFileName(this,
                                                  tr("Export to file"),
                                                  "",
                                                  "SRPS Export File (*.srps)");

  if ( filepath.isEmpty() ) return;

  // first try to export data to memory.

  QString component;
  QByteArray exported_data;

  QBuffer buffer(&exported_data);
  buffer.open(QBuffer::WriteOnly);

  QByteArray second_header = "_type_ _component_ _rw_";

  int export_result = 0;

  if ( citem )
  {
    QMessageBox::information(this,tr("Export Curve to File"),
                             tr("Not implemented yet."));
    return;
  }
  else if ( sitem )
  {
    component = p->SRPS_Factory->spaceRWFactoryDefault();
    SRPS::SpaceRW * rw = p->SRPS_Factory->spaceRWFactoryCreate(component);

    if ( !rw )
    {
      QMessageBox::critical(this,tr("Export Space to File"),
                            tr("Can't create SpaceRW component: %1").arg(component));
      return;
    }

    if ( export_result = rw->write(sitem->space(),&buffer) < 0 ) // error
    {
      QMessageBox::critical(this,tr("Export Space to File"),
                            tr("Space can't be exported '%1' [%2]: %3")
                              .arg(component)
                              .arg(export_result)
                              .arg(rw->resultString(export_result)));
      delete rw;
      return;
    }
    delete rw;
    second_header = second_header.replace("_type_","space");
    second_header = second_header.replace("_component_",sitem->space()->componentClassName().toAscii().simplified());
    second_header = second_header.replace("_rw_",component.toAscii().simplified());
  }

  if ( !export_result ) // all good.
  {
    QFile exportedFile(filepath);
    if ( !exportedFile.open(QFile::WriteOnly) )
    {
      QMessageBox::critical(this,tr("Export Space to File"),
                            tr("File '%1' can't be opened for writing: %2")
                              .arg(filepath)
                              .arg(exportedFile.errorString()));
      return;
    }

    buffer.close();

    static const QByteArray SRPS_HEADER = "SRPS 1";
    static const QByteArray CRLF = "\r\n";

    exportedFile.write(SRPS_HEADER);
    exportedFile.write(CRLF);
    exportedFile.write(second_header);
    exportedFile.write(CRLF);
    exportedFile.write(buffer.data());

  }
}

void MainWindow::on_actionImportFromFile_triggered()
{
  if ( ui->projectsTree->topLevelItemCount() == 0 ) return; // no projects

  QString filepath = QFileDialog::getOpenFileName(this,
                                                  tr("Export from File"),
                                                  "",
                                                  "SRPS Import File (*.srps)");

  if ( filepath.isEmpty() ) return;

  QFile importedFile(filepath);
  if ( !importedFile.open(QFile::ReadOnly) )
  {
    QMessageBox::critical(this,tr("Import from File"),
                          tr("File '%1' can't be opened for reading: %2")
                            .arg(filepath)
                            .arg(importedFile.errorString()));
  }

  QByteArray readed_header = importedFile.readLine().simplified();

  static const QByteArray SRPS_HEADER = "SRPS 1";
  if ( readed_header != SRPS_HEADER )
  {
    QMessageBox::critical(this,tr("Import from File"),
                          tr("Invalid header format.")
                            .arg(filepath)
                            .arg(importedFile.errorString()));
    return;
  }

  readed_header = importedFile.readLine().simplified();
  QList<QByteArray> tokens = readed_header.split(' ');

  QString type;
  QString component = "default";
  QString component_rw;

  if ( ! tokens.size() )
  {
    QMessageBox::critical(this,tr("Import from File"),
                          tr("Invalid second header format.")
                            .arg(filepath)
                            .arg(importedFile.errorString()));
    return;
  }

  type = tokens.at(0);

  if ( type != "space" && type != "curve" )
  {
    QMessageBox::critical(this,tr("Import from File"),
                          tr("Unknow saved datatype.")
                            .arg(filepath)
                            .arg(importedFile.errorString()));
    return;
  }

  if ( tokens.size() == 2 )
  {
    component_rw = tokens.at(1);
  }
  else if ( tokens.size() > 2 )
  {
    component = tokens.at(1);
    component_rw = tokens.at(2);
  }

  if ( type == "space" )
  {
    if ( component == "default" )
      component = p->SRPS_Factory->spaceFactoryDefault();

    SRPS::Space * space = p->SRPS_Factory->spaceFactoryCreate(component);
    if ( !space )
    {
      QMessageBox::critical(this,tr("Import from File"),
                            tr("Can't create space component: %1")
                              .arg(component));
      return;
    }

    SRPS::SpaceRW * rw = p->SRPS_Factory->spaceRWFactoryCreate(component_rw);
    if ( !rw )
    {
      QMessageBox::critical(this,tr("Import from File"),
                            tr("Can't create space rw component: %1, the space can't be imported.")
                              .arg(component_rw));
      return;
    }

    int result = rw->read(&importedFile,space);
    if ( result < 0 )
    {
      QMessageBox::critical(this,tr("Import from File"),
                            tr("Space can't be imported '%1' [%2]: %3")
                              .arg(component)
                              .arg(result)
                              .arg(rw->resultString(result)));
      delete rw;
      delete space;
      return;
    }
    delete rw;

    if ( ui->projectsTree->topLevelItemCount() == 1 )
    {
      ProjectItem * pitem = dynamic_cast<ProjectItem*>(ui->projectsTree->topLevelItem(0));
      if ( pitem )
      {
        pitem->addSpace(space);
      }
      else
        delete space;
    }
    else
    {
      delete space;
    }
  }

  if ( type == "curve")
  {
    QMessageBox::information(this,tr("Export Curve to File"),
                             tr("Not implemented yet."));
  }
}

}
}
