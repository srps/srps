/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_IMPLEMENTATION_ABOUTSRPSDIALOG_H
#define SRPS_IMPLEMENTATION_ABOUTSRPSDIALOG_H

#include <QtGui/QDialog>

namespace SRPS {
namespace Implementation {

namespace Ui {
    class AboutDialog;
}

class AboutDialog : public QDialog
{
  Q_OBJECT
  public:
    explicit AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

  Q_SLOT void render(QObject *object);

  protected:
    void changeEvent(QEvent *e);

  private:
    Ui::AboutDialog *ui;
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_PROGRAMABOUTSRPSDIALOG_H
