/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_SINGLENEWCOMPONETWIDGET_H
#define GUI_SINGLENEWCOMPONETWIDGET_H

#include <QtCore/QVariantHash>
#include <QtGui/QWidget>

class QListWidgetItem;

namespace SRPS {
namespace Implementation {

class SRPSFactory;

namespace Ui {
    class SingleNewComponentWidget;
}

class SingleNewComponentWidget : public QWidget
{
    Q_OBJECT

  class Private;
  Private *p;

public:
    explicit SingleNewComponentWidget(QWidget *parent = 0);
    virtual ~SingleNewComponentWidget();

    void setFactory( SRPSFactory * factory );
    void setComponents( const QStringList & components );
    void setup();

    QString selectedComponent() const;

    QVariantHash selectedComponentConfiguration() const;

    signals:

    void selectedComponentChanged( const QString key );

protected:
    void changeEvent(QEvent *e);

private:
    Ui::SingleNewComponentWidget *ui;

private slots:
    void on_componentListWidget_itemClicked(QListWidgetItem* item);
};


} // namespace Implementation
} // namespace SRPS
#endif // GUI_SINGLENEWCOMPONETWIDGET_H
