/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_srpsfactory.h"

namespace SRPS {
namespace Implementation {

class SRPSFactory::Private
{
public:
  QHash<QString, QString> defaultTable;
  QHash<QString, QVariantHash> defaultParameters;
  QHash<QString, SRPS::ComponentFactory*> componentTable;
  QHash<QString, SRPS::CurveFactory*> curveTable;
  QHash<QString, SRPS::CurveEditorFactory*> curveEditorTable;
  QHash<QString, SRPS::CurveRWFactory*> curveRWTable;
  QHash<QString, SRPS::CurveVisualizerFactory*> curveVisualizerTable;
  QHash<QString, SRPS::SpaceFactory*> spaceTable;
  QHash<QString, SRPS::SpaceEditorFactory*> spaceEditorTable;
  QHash<QString, SRPS::SpaceRWFactory*> spaceRWTable;
  QHash<QString, SRPS::PRNGFactory*> prngTable;
  QHash<QString, SRPS::DPRNGFactory*> dprngTable;
  QHash<QString, SRPS::ModelFactory*> modelTable;
  QHash<QString, SRPS::AccumulatorFactory*> accumulatorTable;
  QHash<QString, SRPS::SpaceGeneratorFactory*> spaceGeneratorTable;
  QHash<QString, SRPS::SimulatorFactory*> simulatorTable;
};

const QString SRPSFactory::DefaultCurve
    = QLatin1String("srps.curve.default");

const QString SRPSFactory::DefaultCurveEditor
    = QLatin1String("srps.curve.editor.default");

const QString SRPSFactory::DefaultCurveRW
    = QLatin1String("srps.curve.rw.default");

const QString SRPSFactory::DefaultCurveVisualizer
    = QLatin1String("srps.curve.visualizer.default");

const QString SRPSFactory::DefaultSpace
    = QLatin1String("srps.space.default");

const QString SRPSFactory::DefaultSpaceEditor
    = QLatin1String("srps.space.editor.default");

const QString SRPSFactory::DefaultSpaceRW
    = QLatin1String("srps.space.rw.default");

const QString SRPSFactory::DefaultPRNG
    = QLatin1String("srps.prng.default");

const QString SRPSFactory::DefaultDPRNG
    = QLatin1String("srps.dprng.default");

SRPSFactory::SRPSFactory(QObject *parent) :
    QObject(parent),
    SRPS::ComponentFactory(),
    SRPS::CurveFactory(),
    SRPS::CurveEditorFactory(),
    SRPS::SpaceFactory(),
    SRPS::SpaceEditorFactory(),
    SRPS::SpaceRWFactory(),
    SRPS::PRNGFactory(),
    SRPS::DPRNGFactory(),
    SRPS::SimulatorFactory(),
    p( new Private )
{
  p->defaultTable.insert(DefaultCurve,QString());
  p->defaultTable.insert(DefaultCurveEditor,QString());
  p->defaultTable.insert(DefaultCurveRW,QString());
  p->defaultTable.insert(DefaultCurveVisualizer,QString());
  p->defaultTable.insert(DefaultSpace,QString());
  p->defaultTable.insert(DefaultSpaceEditor,QString());
  p->defaultTable.insert(DefaultSpaceRW,QString());
  p->defaultTable.insert(DefaultPRNG,QString());
  p->defaultTable.insert(DefaultDPRNG,QString());

  p->defaultParameters.insert(DefaultCurve,QVariantHash());
  p->defaultParameters.insert(DefaultCurveEditor,QVariantHash());
  p->defaultParameters.insert(DefaultCurveRW,QVariantHash());
  p->defaultParameters.insert(DefaultCurveVisualizer,QVariantHash());
  p->defaultParameters.insert(DefaultSpace,QVariantHash());
  p->defaultParameters.insert(DefaultSpaceEditor,QVariantHash());
  p->defaultParameters.insert(DefaultSpaceRW,QVariantHash());
  p->defaultParameters.insert(DefaultPRNG,QVariantHash());
  p->defaultParameters.insert(DefaultDPRNG,QVariantHash());
}

SRPSFactory::~SRPSFactory()
{
  delete p;
}

QWidget * SRPSFactory::componentFactoryWidgetFor(const QString & key) const
{
  QString component = p->defaultTable.value(key,key);

  SRPS::ComponentFactory * cf = p->componentTable.value(component);

  if ( !cf )
    return ComponentFactory::componentFactoryWidgetFor(key);

  return cf->componentFactoryWidgetFor(component);
}

QVariantHash SRPSFactory::componentFactoryDefaultParametersFor(
                                         const QString & key) const
{
  if ( p->defaultParameters.contains(key) )
  {
    QVariantHash r = p->defaultParameters.value(key);
    if ( r.isEmpty() )
      r = componentFactoryDefaultParametersFor(p->defaultTable.value(key));
    return r;
  }

  SRPS::ComponentFactory * cf = p->componentTable.value(key);

  if ( !cf )
    return QVariantHash();

  return cf->componentFactoryDefaultParametersFor(key);
}

bool SRPSFactory::componentFactorySetDefaultParametersFor(
                                         const QString & key,
                                         const QVariantHash & parameters)
{
  if ( p->defaultParameters.contains(key) )
  {
    p->defaultParameters.insert(key,parameters);
    return true;
  }

  SRPS::ComponentFactory * cf = p->componentTable.value(key);

  if ( !cf )
    return false;

  return cf->componentFactorySetDefaultParametersFor(key,parameters);
}

QString SRPSFactory::curveFactoryDefault() const
{
  return p->defaultTable.value(DefaultCurve);
}

void SRPSFactory::setCurveFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultCurve,c);
}

QStringList SRPSFactory::curveFactoryKeys() const
{
  return p->curveTable.keys();
}

SRPS::Curve * SRPSFactory::curveFactoryCreate( const QString & curve,
                                  const QVariantHash & parameters )
{
  QString component = curve;
  QVariantHash configuration = parameters;

  if ( component.isEmpty() )
  {
    component = DefaultCurve;
//    if ( configuration.isEmpty() )
//      configuration = componentFactoryDefaultParametersFor(DefaultCurve);
  }

  component = p->defaultTable.value(component,component);

  SRPS::CurveFactory * factory = p->curveTable.value(component);

  if ( !factory ) return 0;

  return factory->curveFactoryCreate(component,configuration);
}

QString SRPSFactory::curveEditorFactoryDefault() const
{
  return p->defaultTable.value(DefaultCurveEditor);
}

void SRPSFactory::setCurveEditorFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultCurveEditor,c);
}

QStringList SRPSFactory::curveEditorFactoryKeys() const
{
  return p->curveEditorTable.keys();
}

SRPS::CurveEditor * SRPSFactory::curveEditorFactoryCreate(
                      const QString & curveEditor,
                      const QVariantHash & parameters )
{
  QString component = curveEditor;

  if ( component.isEmpty() ) component = DefaultCurveEditor;

  component = p->defaultTable.value(component,component);

  SRPS::CurveEditorFactory * factory = p->curveEditorTable.value(component);

  if ( !factory ) return 0;

  return factory->curveEditorFactoryCreate(component,parameters);
}

QString SRPSFactory::curveRWFactoryDefault() const
{
  return p->defaultTable.value(DefaultCurveRW);
}

void SRPSFactory::setCurveRWFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultCurveRW,c);
}

QStringList SRPSFactory::curveRWFactoryKeys() const
{
  return p->curveRWTable.keys();
}

SRPS::CurveRW * SRPSFactory::curveRWFactoryCreate(
                      const QString & curveRW,
                      const QVariantHash & parameters )
{
  QString component = curveRW;

  if ( component.isEmpty() ) component = DefaultCurveRW;

  component = p->defaultTable.value(component,component);

  SRPS::CurveRWFactory * factory = p->curveRWTable.value(component);

  if ( !factory ) return 0;

  return factory->curveRWFactoryCreate(component,parameters);
}

QString SRPSFactory::curveVisualizerFactoryDefault() const
{
  return p->defaultTable.value(DefaultCurveVisualizer);
}

void SRPSFactory::setCurveVisualizerFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultCurveVisualizer,c);
}

QStringList SRPSFactory::curveVisualizerFactoryKeys() const
{
  return p->curveVisualizerTable.keys();
}

SRPS::CurveVisualizer * SRPSFactory::curveVisualizerFactoryCreate(
                      const QString & curveVisualizer,
                      const QVariantHash & parameters )
{
  QString component = curveVisualizer;

  if ( component.isEmpty() ) component = DefaultCurveVisualizer;

  component = p->defaultTable.value(component,component);

  SRPS::CurveVisualizerFactory * factory = p->curveVisualizerTable.value(component);

  if ( !factory ) return 0;

  return factory->curveVisualizerFactoryCreate(component,parameters);
}

QString SRPSFactory::spaceFactoryDefault() const
{
  return p->defaultTable.value(DefaultSpace);
}

void SRPSFactory::setSpaceFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultSpace,c);
}

QStringList SRPSFactory::spaceFactoryKeys() const
{
  return p->spaceTable.keys();
}

SRPS::Space * SRPSFactory::spaceFactoryCreate(
                      const QString & space,
                      const QVariantHash & parameters )
{
  QString component = space;

  if ( component.isEmpty() ) component = DefaultSpace;

  component = p->defaultTable.value(component,component);

  SRPS::SpaceFactory * factory = p->spaceTable.value(component);

  if ( !factory ) return 0;

  return factory->spaceFactoryCreate(component,parameters);
}

//

QString SRPSFactory::spaceEditorFactoryDefault() const
{
  return p->defaultTable.value(DefaultSpaceEditor);
}

void SRPSFactory::setSpaceEditorFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultSpaceEditor,c);
}

QStringList SRPSFactory::spaceEditorFactoryKeys() const
{
  return p->spaceEditorTable.keys();
}

SRPS::SpaceEditor * SRPSFactory::spaceEditorFactoryCreate(
                      const QString & spaceEditor,
                      const QVariantHash & parameters )
{
  QString component = spaceEditor;

  if ( component.isEmpty() ) component = DefaultSpaceEditor;

  component = p->defaultTable.value(component,component);

  SRPS::SpaceEditorFactory * factory = p->spaceEditorTable.value(component);

  if ( !factory ) return 0;

  return factory->spaceEditorFactoryCreate(component,parameters);
}

//

QString SRPSFactory::spaceRWFactoryDefault() const
{
  return p->defaultTable.value(DefaultSpaceRW);
}

void SRPSFactory::setSpaceRWFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultSpaceRW,c);
}

QStringList SRPSFactory::spaceRWFactoryKeys() const
{
  return p->spaceRWTable.keys();
}

SRPS::SpaceRW * SRPSFactory::spaceRWFactoryCreate(
                      const QString & spaceRW,
                      const QVariantHash & parameters )
{
  QString component = spaceRW;

  if ( component.isEmpty() ) component = DefaultSpaceRW;

  component = p->defaultTable.value(component,component);

  SRPS::SpaceRWFactory * factory = p->spaceRWTable.value(component);

  if ( !factory ) return 0;

  return factory->spaceRWFactoryCreate(component,parameters);
}

//

QString SRPSFactory::prngFactoryDefault() const
{
  return p->defaultTable.value(DefaultPRNG);
}

void SRPSFactory::setPRNGFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultPRNG,c);
}

QStringList SRPSFactory::prngFactoryKeys() const
{
  return p->prngTable.keys();
}

SRPS::PRNG * SRPSFactory::prngFactoryCreate(
                      const QString & prng,
                      const QVariantHash & parameters )
{
  QString component = prng;

  if ( component.isEmpty() ) component = DefaultPRNG;

  component = p->defaultTable.value(component,component);

  SRPS::PRNGFactory * factory = p->prngTable.value(component);

  if ( !factory ) return 0;

  return factory->prngFactoryCreate(component,parameters);
}

//

QString SRPSFactory::dprngFactoryDefault() const
{
  return p->defaultTable.value(DefaultDPRNG);
}

void SRPSFactory::setDPRNGFactoryDefault( const QString & c )
{
  p->defaultTable.insert(DefaultDPRNG,c);
}

QStringList SRPSFactory::dprngFactoryKeys() const
{
  return p->dprngTable.keys();
}

SRPS::DPRNG * SRPSFactory::dprngFactoryCreate(
                      const QString & dprng,
                      const QVariantHash & parameters )
{
  QString component = dprng;

  if ( component.isEmpty() ) component = DefaultDPRNG;

  component = p->defaultTable.value(component,component);

  SRPS::DPRNGFactory * factory = p->dprngTable.value(component);

  if ( !factory ) return 0;

  return factory->dprngFactoryCreate(component,parameters);
}

QStringList SRPSFactory::modelFactoryKeys() const
{
  return p->modelTable.keys();
}

SRPS::Model * SRPSFactory::modelFactoryCreate(
                      const QString & model,
                      const QVariantHash & parameters )
{
  QString component = model;

//  if ( component.isEmpty() ) component = DefaultDPRNG;

//  component = p->defaultTable.value(component,component);

  SRPS::ModelFactory * factory = p->modelTable.value(component);

  if ( !factory ) return 0;

  return factory->modelFactoryCreate(component,parameters);
}

QStringList SRPSFactory::accumulatorFactoryKeys() const
{
  return p->accumulatorTable.keys();
}

SRPS::Accumulator * SRPSFactory::accumulatorFactoryCreate(
                      const QString & accumulator,
                      const QVariantHash & parameters )
{
  QString component = accumulator;

//  if ( component.isEmpty() ) component = DefaultDPRNG;

//  component = p->defaultTable.value(component,component);

  SRPS::AccumulatorFactory* factory = p->accumulatorTable.value(component);

  if ( !factory ) return 0;

  return factory->accumulatorFactoryCreate(component,parameters);
}

QStringList SRPSFactory::spaceGeneratorFactoryKeys() const
{
  return p->spaceGeneratorTable.keys();
}

SRPS::SpaceGenerator * SRPSFactory::spaceGeneratorFactoryCreate(
                      const QString & spaceGenerator,
                      const QVariantHash & parameters )
{
  QString component = spaceGenerator;

//  if ( component.isEmpty() ) component = DefaultDPRNG;

//  component = p->defaultTable.value(component,component);

  SRPS::SpaceGeneratorFactory* factory = p->spaceGeneratorTable.value(component);

  if ( !factory ) return 0;

  return factory->spaceGeneratorFactoryCreate(component,parameters);
}

QStringList SRPSFactory::simulatorFactoryKeys() const
{
  return p->simulatorTable.keys();
}

SRPS::Simulator * SRPSFactory::simulatorFactoryCreate(
                      const QString & simulator,
                      const QVariantHash & parameters )
{
  QString component = simulator;

  SRPS::SimulatorFactory* factory = p->simulatorTable.value(component);

  if ( !factory ) return 0;

  return factory->simulatorFactoryCreate(component,parameters);
}

//

static QVariantHash loadParameters( SRPS::SettingsManager * sm,
                                    const QString & prefix )
{
  static const QString ParameterKeyName("%1.parameter.%2");

  if ( !sm ) return QVariantHash();

  QVariantHash result;

  const QString targetprefix = ParameterKeyName.arg(prefix,QString());
  foreach( QString targetkey, sm->keys() )
  {
    if ( targetkey.startsWith(targetprefix) )
      result.insert( targetkey.replace(targetprefix,""),
                     sm->value(targetkey));
  }

  return result;
}

void SRPSFactory::loadSettings(SRPS::SettingsManager * sm)
{
  if ( !sm ) return;

  // Curve
  {
    QString dc = sm->value(DefaultCurve).toString();
    if ( dc.isEmpty() && !p->curveTable.isEmpty() )
      dc = p->curveTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultCurve);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setCurveFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultCurve,c);
  }

  // CurveEditor
  {
    QString dc = sm->value(DefaultCurveEditor).toString();
    if ( dc.isEmpty() && !p->curveEditorTable.isEmpty() )
      dc = p->curveEditorTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultCurveEditor);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setCurveEditorFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultCurveEditor,c);
  }

  // CurveRW
  {
    QString dc = sm->value(DefaultCurveRW).toString();
    if ( dc.isEmpty() && !p->curveRWTable.isEmpty() )
      dc = p->curveRWTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultCurveRW);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setCurveRWFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultCurveRW,c);
  }

  // CurveVisualizer
  {
    QString dc = sm->value(DefaultCurveVisualizer).toString();
    if ( dc.isEmpty() && !p->curveVisualizerTable.isEmpty() )
      dc = p->curveVisualizerTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultCurveVisualizer);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setCurveVisualizerFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultCurveVisualizer,c);
  }

  // Space
  {
    QString dc = sm->value(DefaultSpace).toString();
    if ( dc.isEmpty() && !p->spaceTable.isEmpty() )
      dc = p->spaceTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultSpace);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setSpaceFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultSpace,c);
  }

  // SpaceEditor
  {
    QString dc = sm->value(DefaultSpaceEditor).toString();
    if ( dc.isEmpty() && !p->spaceEditorTable.isEmpty() )
      dc = p->spaceEditorTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultSpaceEditor);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setSpaceEditorFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultSpaceEditor,c);
  }

  // SpaceRW
  {
    QString dc = sm->value(DefaultSpaceRW).toString();
    if ( dc.isEmpty() && !p->spaceRWTable.isEmpty() )
      dc = p->spaceRWTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultSpaceRW);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setSpaceRWFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultSpaceRW,c);
  }

  // PRNG
  {
    QString dc = sm->value(DefaultPRNG).toString();
    if ( dc.isEmpty() && !p->prngTable.isEmpty() )
      dc = p->prngTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultPRNG);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setPRNGFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultPRNG,c);
  }

  // DPRNG
  {
    QString dc = sm->value(DefaultDPRNG).toString();
    if ( dc.isEmpty() && !p->dprngTable.isEmpty() )
      dc = p->dprngTable.keys().first();

    QVariantHash c = loadParameters(sm,DefaultDPRNG);
    if( c.isEmpty() && p->componentTable.contains( dc ) )
      c = p->componentTable.value(dc)->componentFactoryDefaultParametersFor(dc);

    setDPRNGFactoryDefault(dc);
    componentFactorySetDefaultParametersFor(DefaultDPRNG,c);
  }
}

static void saveParameters( SRPS::SettingsManager * sm,
                            const QString & prefix,
                            const QVariantHash & parameters)
{
  if ( !sm ) return;

  static const QString ParameterKeyName("%1.parameter.%2");

  QHashIterator<QString,QVariant> iterator(parameters);
  while( iterator.hasNext() )
  {
    iterator.next();

    QString key = ParameterKeyName.arg(prefix,iterator.key());
    sm->setValue(key,iterator.value());
  }
}

void SRPSFactory::saveSettings(SRPS::SettingsManager * sm)
{
  if ( !sm ) return;

  // Curve
  {
    sm->setValue(DefaultCurve,curveFactoryDefault());
    saveParameters(sm,
                   DefaultCurve,
                   componentFactoryDefaultParametersFor(DefaultCurve));
  }

  // Curve Editor
  {
    sm->setValue(DefaultCurveEditor,curveEditorFactoryDefault());
    saveParameters(sm,
                   DefaultCurveEditor,
                   componentFactoryDefaultParametersFor(DefaultCurveEditor));
  }

  // Curve RW
  {
    sm->setValue(DefaultCurveRW,curveRWFactoryDefault());
    saveParameters(sm,
                   DefaultCurveRW,
                   componentFactoryDefaultParametersFor(DefaultCurveRW));
  }

    // Curve Visualizer
  {
    sm->setValue(DefaultCurveVisualizer,curveVisualizerFactoryDefault());
    saveParameters(sm,
                   DefaultCurveVisualizer,
                   componentFactoryDefaultParametersFor(DefaultCurveVisualizer));
  }

  // Space
  {
    sm->setValue(DefaultSpace,spaceFactoryDefault());
    saveParameters(sm,
                   DefaultSpace,
                   componentFactoryDefaultParametersFor(DefaultSpace));
  }

  // SpaceEditor
  {
    sm->setValue(DefaultSpaceEditor,spaceEditorFactoryDefault());
    saveParameters(sm,
                   DefaultSpaceEditor,
                   componentFactoryDefaultParametersFor(DefaultSpaceEditor));
  }

  // SpaceRW
  {
    sm->setValue(DefaultSpaceRW,spaceRWFactoryDefault());
    saveParameters(sm,
                   DefaultSpaceRW,
                   componentFactoryDefaultParametersFor(DefaultSpaceRW));
  }

  // PRNG
  {
    sm->setValue(DefaultPRNG,prngFactoryDefault());
    saveParameters(sm,
                   DefaultPRNG,
                   componentFactoryDefaultParametersFor(DefaultPRNG));
  }

  // DPRNG
  {
    sm->setValue(DefaultDPRNG,dprngFactoryDefault());
    saveParameters(sm,
                   DefaultDPRNG,
                   componentFactoryDefaultParametersFor(DefaultDPRNG));
  }

}

void SRPSFactory::addComponentFactory(QObject *object)
{
  SRPS::ComponentFactory * componentFactory =
      qobject_cast<SRPS::ComponentFactory*>(object);

  if ( SRPS::CurveFactory * curveFactory =
       qobject_cast<SRPS::CurveFactory*>(object))
  {
    foreach( QString key, curveFactory->curveFactoryKeys() )
    {
      p->curveTable.insert(key,curveFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::CurveEditorFactory * curveEditorFactory =
       qobject_cast<SRPS::CurveEditorFactory*>(object))
  {
    foreach( QString key, curveEditorFactory->curveEditorFactoryKeys() )
    {
      p->curveEditorTable.insert(key,curveEditorFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::CurveRWFactory * curveRWFactory =
       qobject_cast<SRPS::CurveRWFactory*>(object))
  {
    foreach( QString key, curveRWFactory->curveRWFactoryKeys() )
    {
      p->curveRWTable.insert(key,curveRWFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::CurveVisualizerFactory * curveVisualizerFactory =
       qobject_cast<SRPS::CurveVisualizerFactory*>(object))
  {
    foreach( QString key, curveVisualizerFactory->curveVisualizerFactoryKeys() )
    {
      p->curveVisualizerTable.insert(key,curveVisualizerFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::SpaceFactory * spaceFactory=
       qobject_cast<SRPS::SpaceFactory*>(object))
  {
    foreach( QString key, spaceFactory->spaceFactoryKeys() )
    {
      p->spaceTable.insert(key,spaceFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::SpaceEditorFactory * spaceEditorFactory=
       qobject_cast<SRPS::SpaceEditorFactory*>(object))
  {
    foreach( QString key, spaceEditorFactory->spaceEditorFactoryKeys() )
    {
      p->spaceEditorTable.insert(key,spaceEditorFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::SpaceRWFactory * spaceRWFactory=
       qobject_cast<SRPS::SpaceRWFactory*>(object))
  {
    foreach( QString key, spaceRWFactory->spaceRWFactoryKeys() )
    {
      p->spaceRWTable.insert(key,spaceRWFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::PRNGFactory * prngFactory=
       qobject_cast<SRPS::PRNGFactory*>(object))
  {
    foreach( QString key, prngFactory->prngFactoryKeys() )
    {
      p->prngTable.insert(key,prngFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::DPRNGFactory * dprngFactory=
       qobject_cast<SRPS::DPRNGFactory*>(object))
  {
    foreach( QString key, dprngFactory->dprngFactoryKeys() )
    {
      p->dprngTable.insert(key,dprngFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::ModelFactory * modelFactory=
       qobject_cast<SRPS::ModelFactory*>(object))
  {
    foreach( QString key, modelFactory->modelFactoryKeys() )
    {
      p->modelTable.insert(key,modelFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::AccumulatorFactory * accumulatorFactory=
       qobject_cast<SRPS::AccumulatorFactory*>(object))
  {
    foreach( QString key, accumulatorFactory->accumulatorFactoryKeys() )
    {
      p->accumulatorTable.insert(key,accumulatorFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::SpaceGeneratorFactory * spaceGeneratorFactory=
       qobject_cast<SRPS::SpaceGeneratorFactory*>(object))
  {
    foreach( QString key, spaceGeneratorFactory->spaceGeneratorFactoryKeys() )
    {
      p->spaceGeneratorTable.insert(key,spaceGeneratorFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }

  if ( SRPS::SimulatorFactory * simulatorFactory=
       qobject_cast<SRPS::SimulatorFactory*>(object))
  {
    foreach( QString key, simulatorFactory->simulatorFactoryKeys() )
    {
      p->simulatorTable.insert(key,simulatorFactory);
      if ( componentFactory )
        p->componentTable.insert(key,componentFactory);
    }
  }
}

} // namespace Implementation
} // namespace SRPS
