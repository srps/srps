/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_multiplenewcomponentwidget.h"
#include "ui_gui_multiplenewcomponentwidget.h"

#include <SRPS/ComponentFactoryWidget>

#include "gui_srpsfactory.h"

#include <QtCore/QPointer>
#include <QtCore/QMutableHashIterator>
#include <QtGui/QListWidgetItem>
#include <QtGui/QLabel>

namespace SRPS {
namespace Implementation {

class MultipleNewComponetWidget::Private
{
public:
  QPointer<SRPSFactory> factory;
  QStringList components;

  QHash<QListWidgetItem*,QWidget*> componentTable;
};

MultipleNewComponetWidget::MultipleNewComponetWidget(QWidget *parent) :
    QWidget(parent),
    p( new Private ),
    ui(new Ui::MultipleNewComponetWidget)
{
    ui->setupUi(this);
    connect(ui->componentListWidget,SIGNAL(itemDoubleClicked(QListWidgetItem*)),
            this,SLOT(on_addTool_clicked()));
}

MultipleNewComponetWidget::~MultipleNewComponetWidget()
{
    delete ui;
    delete p;
}

void MultipleNewComponetWidget::setFactory( SRPSFactory * factory )
{
  p->factory = factory;
}

void MultipleNewComponetWidget::setComponents( const QStringList & components )
{
  p->components = components;
}

void MultipleNewComponetWidget::setup()
{
  QMutableHashIterator<QListWidgetItem*,QWidget*> iterator(p->componentTable);
  while( iterator.hasNext() )
  {
    iterator.next();
    delete iterator.key();
    delete iterator.value();
    iterator.remove();
  }

  foreach( QString key, p->components )
  {
    QListWidgetItem * item = new QListWidgetItem(key);
    if ( p->factory )
    {
      QWidget * widget = p->factory->componentFactoryWidgetFor(key);
      if ( widget )
      {

        ComponentFactoryWidget * cfw = qobject_cast<ComponentFactoryWidget*>(widget);
        if ( cfw ) cfw->componentFactoryWidgetSetParameters(
            p->factory->componentFactoryDefaultParametersFor(key)
            );
      }
      else
      {
        widget = new QLabel(tr("Component does not provides graphical configuration."));
      }

      p->componentTable.insert(item,widget);
      ui->componentFactoryWidgetStack->addWidget(widget);
    }
    ui->componentListWidget->addItem(item);
  }

  ui->componentListWidget->setCurrentRow(0);
}

QList<QPair<QString,QVariantHash> > MultipleNewComponetWidget::selectedComponentConfigurations() const
{
  QList<QPair<QString,QVariantHash> > result;

  for( int i = 0; i < ui->selectedComponentListWidget->count(); ++i )
  {
    QListWidgetItem * item = ui->selectedComponentListWidget->item(i);

    QString key = item->text();
    QVariantHash params;

    QWidget * widget = p->componentTable.value(item);
    if ( widget )
    {
      ComponentFactoryWidget * cfw = qobject_cast<ComponentFactoryWidget*>(widget);

      if ( cfw )
        params = cfw->componentFactoryWidgetParameters();
    }

    QPair<QString,QVariantHash> pair;
    pair.first = key;
    pair.second = params;

    result.append( pair );
  }

  return result;
}

void MultipleNewComponetWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MultipleNewComponetWidget::on_componentListWidget_itemClicked(QListWidgetItem* item)
{
  if ( !item ) return;

  ui->selectedComponentListWidget->clearSelection();
  ui->componentFactoryWidgetStack->setCurrentWidget(p->componentTable.value(item));
}

void MultipleNewComponetWidget::on_selectedComponentListWidget_itemClicked(QListWidgetItem* item)
{
  if ( !item ) return;

  ui->componentListWidget->clearSelection();
  ui->componentFactoryWidgetStack->setCurrentWidget(p->componentTable.value(item));
}

void SRPS::Implementation::MultipleNewComponetWidget::on_removeAllTool_clicked()
{
  while( ui->selectedComponentListWidget->count() )
  {
    QListWidgetItem * item = ui->selectedComponentListWidget->takeItem(0);
    delete p->componentTable.value(item);
    delete item;
  }

  emit selectedComponentConfigurationCountChanged(ui->selectedComponentListWidget->count());
}

void SRPS::Implementation::MultipleNewComponetWidget::on_addTool_clicked()
{
  QList<QListWidgetItem*> items = ui->componentListWidget->selectedItems();
  if ( !items.count() ) return;

  QListWidgetItem * item = items.takeFirst();

  QVariantHash param;
  QWidget * srcw = p->componentTable.value(item);
  if ( srcw )
  {
    ComponentFactoryWidget * cfw = qobject_cast<ComponentFactoryWidget*>(srcw);
    if ( cfw )
      param = cfw->componentFactoryWidgetParameters();
  }

  QListWidgetItem *sitem = new QListWidgetItem(item->text());
  QWidget*  dstw = p->factory->componentFactoryWidgetFor(sitem->text());
  if ( dstw )
  {
    ComponentFactoryWidget * cfw = qobject_cast<ComponentFactoryWidget*>(dstw);
    if ( cfw )
      cfw->componentFactoryWidgetSetParameters(param);
  }
  else
  {
    dstw = new QLabel(tr("Component does not provides graphical configuration."));
  }

  p->componentTable.insert(sitem,dstw);
  ui->selectedComponentListWidget->addItem(sitem);

  emit selectedComponentConfigurationCountChanged(ui->selectedComponentListWidget->count());
}

void SRPS::Implementation::MultipleNewComponetWidget::on_removeTool_clicked()
{

    foreach( QListWidgetItem * item, ui->selectedComponentListWidget->selectedItems() )
  {
    delete p->componentTable.value(item);
    delete item;
  }

    emit selectedComponentConfigurationCountChanged(ui->selectedComponentListWidget->count());
}

} // namespace Implementation
} // namespace SRPS

