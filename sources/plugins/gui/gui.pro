include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
include (qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.pri)
TARGET = $${SRPS_NAME}GUI
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
RESOURCES += resources/resources.qrc
INCLUDEPATH *= ../../../sources/extra/qwt/src
LIBS *= -L../../../
unix:LIBS += -lqwt
win32 {
    CONFIG(debug,debug|release):LIBS *= -lqwtd5
    else:LIBS *= -lqwt5
}
HEADERS += gui_wrapperdialog.h \
    gui_srpsfactory.h \
    gui_singlenewcomponentwidget.h \
    gui_simulationwizard.h \
    gui_settingsdialog.h \
    gui_plugin_private.h \
    gui_plugin.h \
    gui_multiplenewcomponentwidget.h \
    gui_aboutdialog.h \
    gui_filerw.h \
    gui_simulatordialog.h
SOURCES += gui_wrapperdialog.cpp \
    gui_srpsfactory.cpp \
    gui_singlenewcomponentwidget.cpp \
    gui_simulationwizard.cpp \
    gui_settingsdialog.cpp \
    gui_plugin.cpp \
    gui_multiplenewcomponentwidget.cpp \
    gui_aboutdialog.cpp \
    gui_plugin_project_actions.cpp \
    gui_plugin_space_actions.cpp \
    gui_plugin_curve_actions.cpp \
    gui_filerw.cpp \
    gui_plugin_filerw_actions.cpp \
    gui_plugin_test_prng.cpp \
    gui_simulatordialog.cpp
FORMS += gui_multiplenewcomponentwidget.ui \
    gui_wrapperdialog.ui \
    gui_singlenewcomponentwidget.ui \
    gui_settingsdialog.ui \
    gui_plugin.ui \
    gui_aboutdialog.ui \
    gui_simulatordialog.ui
