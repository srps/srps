/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_MULTIPLENEWCOMPONETWIDGET_H
#define GUI_MULTIPLENEWCOMPONETWIDGET_H

#include <QtCore/QVariantHash>
#include <QtGui/QWidget>

class QListWidgetItem;

namespace SRPS {
namespace Implementation {

class SRPSFactory;

namespace Ui {
    class MultipleNewComponetWidget;
}

class MultipleNewComponetWidget : public QWidget
{
    Q_OBJECT

  class Private;
  Private *p;

public:
    explicit MultipleNewComponetWidget(QWidget *parent = 0);
    virtual ~MultipleNewComponetWidget();

    void setFactory( SRPSFactory * factory );
    void setComponents( const QStringList & components );
    void setup();

    QList<QPair<QString,QVariantHash> > selectedComponentConfigurations() const;

    signals:

    void selectedComponentConfigurationCountChanged(int c);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MultipleNewComponetWidget *ui;

private slots:
    void on_removeTool_clicked();
    void on_addTool_clicked();
    void on_removeAllTool_clicked();
    void on_componentListWidget_itemClicked(QListWidgetItem* item);
    void on_selectedComponentListWidget_itemClicked(QListWidgetItem* item);
};


} // namespace Implementation
} // namespace SRPS
#endif // GUI_MULTIPLENEWCOMPONETWIDGET_H
