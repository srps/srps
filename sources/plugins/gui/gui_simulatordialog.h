/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_SIMULATORDIALOG_H
#define GUI_SIMULATORDIALOG_H

#include <QtGui/QDialog>

namespace SRPS {

class Simulator;

namespace Implementation {

namespace Ui {
    class SimulatorDialog;
}

class SimulatorDialog : public QDialog
{
    Q_OBJECT
    int tid;
    Simulator * simulator;

public:
    explicit SimulatorDialog(QWidget *parent = 0);
    virtual ~SimulatorDialog();

    int startTracking( SRPS::Simulator * simulator );

  private slots:

    void cancelSimulator();

protected:
    void changeEvent(QEvent *e);
    void timerEvent(QTimerEvent *);

private:
    Ui::SimulatorDialog *ui;
};


} // namespace Implementation
} // namespace SRPS
#endif // GUI_SIMULATORDIALOG_H
