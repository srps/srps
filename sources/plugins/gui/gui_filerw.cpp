/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_filerw.h"

#include <QtCore/QBuffer>

namespace SRPS {
namespace Implementation {

static const QByteArray SRPS_VERSION = "1";
static const QByteArray SRPS_HEADER = "SRPS";
static const QByteArray PROJECT_HEADER = "PROJECT";
static const QByteArray FILE_HEADER = "FILE";

static const QByteArray ENDL = "\n";
static const QByteArray SPACE = " ";

static const QByteArray SPACE_ITEM = "space";
static const QByteArray CURVE_ITEM = "curve";

FileRW::FileRW( QObject * parent ) :
    QObject(parent),
    space_factory(0),
    curve_factory(0),
    space_rw_factory(0),
    curve_rw_factory(0)
{
}

FileRW::~FileRW()
{
}

void FileRW::setSpaceRWKey( const QString & k, const QVariantHash & conf )
{
  space_rw_key = k;
  space_rw_conf = conf;
}

void FileRW::setCurveRWKey( const QString & k, const QVariantHash & conf )
{
  curve_rw_key = k;
  curve_rw_conf = conf;
}

void FileRW::setSpaceKey( const QString & k )
{
  space_key = k;
}

void FileRW::setCurveKey( const QString & k )
{
  curve_key = k;
}

void FileRW::setFactory( SRPS::SpaceRWFactory * rw_factory )
{
  space_rw_factory = rw_factory;
}

void FileRW::setFactory( SRPS::CurveRWFactory * rw_factory )
{
  curve_rw_factory = rw_factory;
}

void FileRW::setFactory( SRPS::SpaceFactory * _space_factory )
{
  space_factory = _space_factory;
}

void FileRW::setFactory( SRPS::CurveFactory * _curve_factory )
{
  curve_factory = _curve_factory;
}

int FileRW::readFile( const QString & filepath )
{
  if ( !generalCheck() ) return -1;

  QFile * file =openFile(filepath,QFile::ReadOnly);
  if ( ! file ) return -1;

  QByteArray header = file->readLine().simplified();
  QList<QByteArray> tokens = header.split(' ');
  if ( tokens.size() != 3 )
  {
    delete file;
    emit error(tr("Unknow file"));
    return -1;
  }

  if ( tokens.at(0) != SRPS_HEADER )
  {
    delete file;
    emit error(tr("Not a SRPS File"));
    return -1;
  }

  if ( tokens.at(1) != SRPS_VERSION ) // read one
  {
    delete file;
    emit error(tr("Diferent file version, only %1 version suported").arg(QString(SRPS_VERSION)));
    return -1;
  }

  if ( tokens.at(2) == FILE_HEADER ) // read one
  {
    readOne(file);
  }
  else if ( tokens.at(2) == PROJECT_HEADER )
  {
    emit projectFound(filepath);
    while( !file->atEnd() )
    {
      readOne(file);
    }
  }
  else
  {
    delete file;
    emit error(tr("Unknow SRPS file format"));
    return -1;
  }


  delete file;
  return 0;
}

int FileRW::saveFile( const QString & filepath,
                      const QList<SRPS::Space*> & spaces,
                      const QList<SRPS::Curve*> & curves )
{
  if(!generalCheck()) return -1;

  QFile * file = openFile(filepath,QFile::WriteOnly);
  if (!file ) return -1;

  file->write(SRPS_HEADER);
  file->write(SPACE);
  file->write(SRPS_VERSION);
  file->write(SPACE);
  file->write(PROJECT_HEADER);
  file->write(ENDL);

  foreach( SRPS::Space * space, spaces )
  {
    SRPS::SpaceRW * rw = space_rw_factory->spaceRWFactoryCreate(space_rw_key);
    if ( !rw )
    {
      delete file;
      emit error(tr("Cant create SpaceRW for file: %1").arg(filepath));
      return -1;
    }

    QByteArray data;
    QBuffer buffer(&data);
    buffer.open(QBuffer::WriteOnly);

    if ( rw->prepareSpaceWrite(space,&buffer) )
    {
      delete file;
      emit error(tr("Cant prepare SpaceRW for file: %1").arg(filepath));
      return -1;
    }

    if ( rw->writeSpace() )
    {
      delete rw;
      delete file;
      emit error(tr("Cant prepare SpaceRW for file: %1").arg(filepath));
      return -1;
    }

    buffer.close();

    file->write(SPACE_ITEM);
    file->write(SPACE);
    file->write(space->componentClassName().toLatin1());
    file->write(SPACE);
    file->write(rw->componentClassName().toLatin1());
    file->write(SPACE);
    file->write(QByteArray::number(data.size()));
    file->write(ENDL);
    file->write(data);

    delete rw;
  }

  foreach ( SRPS::Curve * curve, curves )
  {
    SRPS::CurveRW * rw = curve_rw_factory->curveRWFactoryCreate(curve_rw_key);
    if ( !rw )
    {
      delete file;
      emit error(tr("Cant create CurveRW for file: %1").arg(filepath));
      return -1;
    }

    QByteArray data;
    QBuffer buffer(&data);
    buffer.open(QBuffer::WriteOnly);

    if ( rw->prepareCurveWrite(curve,&buffer) )
    {
      delete file;
      emit error(tr("Cant prepare CurveRW for file: %1").arg(filepath));
      return -1;
    }

    if ( rw->writeCurve() )
    {
      delete rw;
      delete file;
      emit error(tr("Cant prepare CurveRW for file: %1").arg(filepath));
      return -1;
    }

    buffer.close();

    file->write(CURVE_ITEM);
    file->write(SPACE);
    file->write(curve->componentClassName().toLatin1());
    file->write(SPACE);
    file->write(rw->componentClassName().toLatin1());
    file->write(SPACE);
    file->write(QByteArray::number(data.size()));
    file->write(ENDL);
    file->write(data);

    delete rw;
  }

  file->close();
  delete file;
  emit fileSaved(filepath);
  return 0;
}

int FileRW::exportToFile( const QString & filepath, const SRPS::Curve * curve )
{
  if ( !generalCheck() ) return -1;

  if (!curve )
  {
    emit error(tr("Null Curve"));
    return -1;
  }

  SRPS::CurveRW * rw = curve_rw_factory->curveRWFactoryCreate(curve_rw_key,curve_rw_conf);
  if (!rw)
  {
    emit error(tr("Cnat create writer %1").arg(curve_rw_key));
    return -1;
  }

  QByteArray data;
  QBuffer buffer(&data);
  buffer.open(QBuffer::WriteOnly);

  int rw_error = rw->prepareCurveWrite(curve,&buffer);
  if ( rw_error )
  {
    emit error(tr("Cant prepare curve write [%1]").arg(rw_error));
    delete rw;
    return -1;
  }

  rw_error = rw->writeCurve();
  buffer.close();
  if ( rw_error )
  {
    emit error(tr("Cant write curve to buffer [%1]").arg(rw_error));
    delete rw;
    return -1;
  }

  delete rw;

  QFile * file = openFile(filepath,QFile::WriteOnly);
  if ( !file )
    return -1;

  file->write(SRPS_HEADER);
  file->write(SPACE);
  file->write(SRPS_VERSION);
  file->write(SPACE);
  file->write(FILE_HEADER);
  file->write(ENDL);

  file->write(CURVE_ITEM);
  file->write(SPACE);
  file->write(curve->componentClassName().toLatin1());
  file->write(SPACE);
  file->write(curve_rw_key.toLatin1());
  file->write(ENDL);

  file->write(buffer.data());

  file->flush();
  file->close();

  // good
  delete file;

  emit exportedToFile(filepath,curve);

  return 0;
}

int FileRW::exportToFile( const QString & filepath, const SRPS::Space * space )
{
  if ( !generalCheck() ) return -1;

  if (!space )
  {
    emit error(tr("Null Space"));
    return -1;
  }

  SRPS::SpaceRW * rw = space_rw_factory->spaceRWFactoryCreate(space_rw_key,space_rw_conf);
  if (!rw)
  {
    emit error(tr("Cnat create writer %1").arg(space_rw_key));
    return -1;
  }

  QByteArray data;
  QBuffer buffer(&data);
  buffer.open(QBuffer::WriteOnly);

  int rw_error = rw->prepareSpaceWrite(space,&buffer);
  if ( rw_error )
  {
    emit error(tr("Cant prepare space write [%1]").arg(rw_error));
    delete rw;
    return -1;
  }

  rw_error = rw->writeSpace();
  buffer.close();
  if ( rw_error )
  {
    emit error(tr("Cant write space to buffer [%1]").arg(rw_error));
    delete rw;
    return -1;
  }

  delete rw;

  QFile * file = openFile(filepath,QFile::WriteOnly);
  if ( !file )
    return -1;

  file->write(SRPS_HEADER);
  file->write(SPACE);
  file->write(SRPS_VERSION);
  file->write(SPACE);
  file->write(FILE_HEADER);
  file->write(ENDL);

  file->write(SPACE_ITEM);
  file->write(SPACE);
  file->write(space->componentClassName().toLatin1());
  file->write(SPACE);
  file->write(space_rw_key.toLatin1());
  file->write(ENDL);

  file->write(buffer.data());

  file->flush();
  file->close();

  // good
  delete file;

  emit exportedToFile(filepath,space);

  return 0;
}

bool FileRW::generalCheck()
{
  if ( !space_rw_factory )
  {
    emit error(tr("No SpaceRWFactory"));
    return false;
  }

  if ( !curve_rw_factory )
  {
    emit error(tr("No CurveRWFactory"));
    return false;
  }

  return true;
}

void FileRW::readOne( QFile * file )
{
  if ( !file ) return;

  QList<QByteArray> tokens = file->readLine().simplified().split(' ');
  int size = tokens.size();

  if ( size < 2 ) return;

  QString component_type = tokens.at(0);
  QString component_key = space_key;
  QString rw_key;
  int dsize = -1;

  bool toInt;
  if ( size == 2 )
    rw_key = tokens.at(1);
  else if ( size == 3 )
  {
    dsize = tokens.at(2).toInt(&toInt);
    if ( toInt )
      rw_key = tokens.at(1);
    else
    {
      component_key = tokens.at(1);
      rw_key = tokens.at(2);
      dsize = -1;
    }
  }
  else if ( size > 3 )
  {
    dsize = tokens.at(3).toInt(&toInt);
    component_key = tokens.at(1);
    rw_key = tokens.at(2);
    if ( !toInt )
      dsize = -1;
  }
  if ( component_key == "default" )
    component_key = space_key;

  // change device?
  QByteArray rdata;
  QBuffer buffer;
  QIODevice * device = 0;
  if ( dsize < 0 )
    device = file;
  else
  {
    rdata = file->read(dsize);
    Q_ASSERT(rdata.size()==dsize);
    buffer.setBuffer(&rdata);
    buffer.open(QBuffer::ReadOnly);
    device = &buffer;
  }

  if ( component_type == SPACE_ITEM ) // space
  {
    SRPS::Space * space = space_factory->spaceFactoryCreate(component_key);
    if (!space)
    {
      emit error(tr("Cant create Space '%1'").arg(component_key));
      return;
    }
    SRPS::SpaceRW * rw = space_rw_factory->spaceRWFactoryCreate(rw_key);
    if (!rw)
    {
      emit error(tr("Cant create Space '%1'").arg(component_key));
      return;
    }

    if ( rw->prepareSpaceRead(device,space) )
    {
      emit error(tr("Cant prepare read '%1'").arg(rw_key));
      delete rw;
      delete space;
      return;
    }
    if ( rw->readSpace() )
    {
      emit error(tr("Cant read space with: '%1'").arg(rw_key));
      delete rw;
      delete space;
      return;
    }
    delete rw;
    emit spaceReaded(file->fileName(),space); // lose space ownership.
  }
  else if ( component_type == CURVE_ITEM ) // curve
  {
    SRPS::Curve * curve = curve_factory->curveFactoryCreate(curve_key);
    if (!curve)
    {
      emit error(tr("Cant create Curve '%1'").arg(curve_key));
      return;
    }
    SRPS::CurveRW * rw = curve_rw_factory->curveRWFactoryCreate(rw_key);
    if (!rw)
    {
      emit error(tr("Cant create Curve '%1'").arg(curve_key));
      return;
    }
    if ( rw->prepareCurveRead(device,curve) )
    {
      emit error(tr("Cant prepare read '%1'").arg(rw_key));
      delete rw;
      delete curve;
      return;
    }
    if ( rw->readCurve() )
    {
      emit error(tr("Cant read curve with: '%1'").arg(rw_key));
      delete rw;
      delete curve;
      return;
    }
    delete rw;
    emit curveReaded(file->fileName(),curve); // lose curve ownership.
  }
  else
  {
    emit error(tr("Unknow component"));
  }
}

QFile * FileRW::openFile( const QString & filepath, QFile::OpenMode mode )
{
  if ( filepath.isEmpty() )
  {
    emit error(tr("Empty filepath"));
    return 0;
  }

  QFile * file = new QFile(filepath);
  if ( !file->open(mode) )
  {
    emit error(tr("Can't open file '%1': %2").arg(filepath).arg(file->errorString()));
    delete file;
    return 0;
  }

  return file;
}

} // namespace Implemenation
} // namespace SRPS
