/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_aboutdialog.h"
#include "ui_gui_aboutdialog.h"

#include <QtCore/QFile>
#include <QtCore/QTextStream>

/*!
  \class SRPS::Implementation::AboutDialog aboutdialog.h aboutdialog.h
  \brief Generic About Dialog for QObjects

  This Dialog renders QObject's about information from resources. It uses the
  QObject's class name to search files. For example: for rendering this class
  about information the render() method search for the following files in
  resource system with ':/srps/program/aboutdialog/' prefix.

  \li name.txt Contains the name of the component.
  \li description.txt Contains the component's description.
  \li icon.png the Component's icon.
  \li about.html About page.
  \li license.html License page.
 */

/*!
  \fn void SRPS::Implementation::AboutDialog::render(QObject *object);
  \brief Renders QObject about info.
  \param object render's target.
 */

namespace SRPS {
namespace Implementation {

static QString readFile( const QString & f )
{
  QFile file(f);

  if ( !file.open( QFile::ReadOnly) )
    return AboutDialog::tr("cant open %1").arg(f);

  QTextStream stream ( &file );
  return stream.readAll();
}

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::render(QObject * object)
{
  QString path = object->metaObject()->className();
  path = path.replace("::","/").toLower();

  QPixmap icon( QString(":/%1/icon.png").arg( path ) );
  ui->iconLabel->setPixmap(icon);

  ui->aboutText->setHtml( readFile( QString(":/%1/about.html").arg( path ) ) );

  ui->licenseText->setHtml( readFile( QString(":/%1/license.html").arg( path ) ) );

  ui->nameLabel->setText( readFile(QString(":/%1/name.txt").arg( path )) );

  ui->descriptionLabel->setText( readFile(QString(":/%1/description.txt").arg( path )) );
}

void AboutDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS
