/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_singlenewcomponentwidget.h"
#include "ui_gui_singlenewcomponentwidget.h"

#include <SRPS/ComponentFactoryWidget>

#include "gui_srpsfactory.h"

#include <QtCore/QPointer>
#include <QtCore/QMutableHashIterator>
#include <QtGui/QListWidgetItem>
#include <QtGui/QLabel>

namespace SRPS {
namespace Implementation {

class SingleNewComponentWidget::Private
{
public:
  QPointer<SRPSFactory> factory;
  QStringList components;

  QHash<QListWidgetItem*,QWidget*> componentTable;
};

SingleNewComponentWidget::SingleNewComponentWidget(QWidget *parent) :
    QWidget(parent),
    p( new Private ),
    ui(new Ui::SingleNewComponentWidget)
{
    ui->setupUi(this);
}

SingleNewComponentWidget::~SingleNewComponentWidget()
{
    delete ui;
    delete p;
}

void SingleNewComponentWidget::setFactory( SRPSFactory * factory )
{
  p->factory = factory;
}

void SingleNewComponentWidget::setComponents( const QStringList & components )
{
  p->components = components;
}

void SingleNewComponentWidget::setup()
{
  QMutableHashIterator<QListWidgetItem*,QWidget*> iterator(p->componentTable);
  while( iterator.hasNext() )
  {
    iterator.next();
    delete iterator.key();
    delete iterator.value();
    iterator.remove();
  }

  foreach( QString key, p->components )
  {
    QListWidgetItem * item = new QListWidgetItem(key);
    if ( p->factory )
    {
      QWidget * widget = p->factory->componentFactoryWidgetFor(key);
      if ( widget )
      {
        ComponentFactoryWidget * cfw = qobject_cast<ComponentFactoryWidget*>(widget);
        if ( cfw ) cfw->componentFactoryWidgetSetParameters(
            p->factory->componentFactoryDefaultParametersFor(key)
            );
      }
      else
      {
          widget = new QLabel(tr("Component does not provides or need configuration"));
      }
      p->componentTable.insert(item,widget);
      ui->componentFactoryWidgetStack->addWidget(widget);
    }
    ui->componentListWidget->addItem(item);
  }

  ui->componentListWidget->setCurrentRow(0);
}

QString SingleNewComponentWidget::selectedComponent() const
{
  QListWidgetItem * item = ui->componentListWidget->currentItem();
  if ( !item ) return QString();

  return item->text();
}

QVariantHash SingleNewComponentWidget::selectedComponentConfiguration() const
{
  QListWidgetItem * item = ui->componentListWidget->currentItem();
  if ( !item ) return QVariantHash();

  QWidget * widget = p->componentTable.value(item);
  if ( !widget ) return QVariantHash();

  ComponentFactoryWidget * cfw = qobject_cast<ComponentFactoryWidget*>(widget);

  if ( !cfw ) return QVariantHash();

  return cfw->componentFactoryWidgetParameters();
}

void SingleNewComponentWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SingleNewComponentWidget::on_componentListWidget_itemClicked(QListWidgetItem* item)
{
  if ( !item ) return;

  ui->componentFactoryWidgetStack->setCurrentWidget(p->componentTable.value(item));
  emit selectedComponentChanged(item->text());
}


} // namespace Implementation
} // namespace SRPS
