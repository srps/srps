/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"
#include "gui_singlenewcomponentwidget.h"
#include "gui_wrapperdialog.h"

#include "gui_simulationwizard.h"
#include "gui_simulatordialog.h"

#include <SRPS/SpaceEditor>
#include <SRPS/SpaceGenerator>
#include <SRPS/Simulator>

#include <QtGui/QMessageBox>
#include <QtGui/QTabWidget>
#include <QtGui/QMdiSubWindow>
#include <QtGui/QLabel>

#include <QtCore/QDebug>

namespace SRPS {
namespace Implementation {

// space helpers

SRPS::Space * MainWindow::createSpace() const
{
  return p->SRPS_Factory->spaceFactoryCreate(QString());
}

SRPS::SpaceEditor * MainWindow::createSpaceEditor() const
{
  SRPS::SpaceEditor * editor = p->SRPS_Factory->spaceEditorFactoryCreate(QString());
  if ( !editor )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo editor"),
                          trUtf8("Comprobar configuración."));
    return 0;
  }

  QWidget * widget = qobject_cast<QWidget*>(editor->componentAsQObject());
  if ( !widget )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo editor"),
                          trUtf8("Comprobar configuración, no es gráfico"));
    delete editor;
    return 0;
  }

  return editor;
}

SRPS::SpaceGenerator * MainWindow::createSpaceGenerator() const
{
  SingleNewComponentWidget * widget = new SingleNewComponentWidget;
  widget->setFactory(p->SRPS_Factory);
  widget->setComponents(p->SRPS_Factory->spaceGeneratorFactoryKeys());
  widget->setup();

  WrapperDialog dlg;
  dlg.setWidget(widget);
  if( dlg.exec() == QDialog::Rejected )
    return 0;

  SRPS::SpaceGenerator * generator =
      p->SRPS_Factory->spaceGeneratorFactoryCreate(widget->selectedComponent(),
                                                   widget->selectedComponentConfiguration());

  if ( !generator )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo Generador"),
                          trUtf8("Comprobrar configuración"));
    return 0;
  }

  return generator;
}

void MainWindow::spaceitem_doubleclicked( QTreeWidgetItem * item )
{
  if ( !p->spaceWidgetHash.contains(item) ) return;

  QWidget * widget = p->spaceWidgetHash.value(item);
  QMdiSubWindow * sub = qobject_cast<QMdiSubWindow*>(widget);

  if ( !sub )
  {
    delete widget;

    SpaceEditor * editor = createSpaceEditor();
    if ( editor )
      editor->spaceEdit(p->spaceHash.value(item));

    QTabWidget * tab = new QTabWidget;
    if ( editor )
      tab->addTab(qobject_cast<QWidget*>(editor->componentAsQObject()),trUtf8("Editor"));
    else
      tab->addTab(new QLabel(trUtf8("No hay editores disponibles")),trUtf8("Editor"));

    sub = ui->mdiArea->addSubWindow(tab);
    sub->setAttribute(Qt::WA_DeleteOnClose,true);
    sub->setWindowTitle(item->text(0));

    p->spaceWidgetHash.insert(item,sub);
    p->widgetSpaceHash.insert(sub,item);
  }

  if ( sub )
    sub->showMaximized();
}

void MainWindow::on_actionSpaceNew_triggered( SRPS::Space * space )
{
  static int NewSpaceCounter = 0;

  if (!space)
  {
    space = createSpace();

    SpaceGenerator * generator = createSpaceGenerator();
    if(!generator)
    {
      delete space;
      return;
    }

    if(generator->prepareGenerate(space))
    {
      delete generator;
      delete space;
      return;
    }

    if(generator->generate())
    {
      delete generator;
      delete space;
      return;
    }
  }

  if ( !space ) return;

  QSpace * qspace = new QSpace(this);
  qspace->setOwnership(true);
  qspace->setSpace(space);

  QTreeWidgetItem * projectSpaceItem = currentProjectSpaceItem();
  if ( !projectSpaceItem) return;

  QString spaceName = space->componentProperty("srps.name").toString();
  if ( spaceName.isEmpty() )
    spaceName = trUtf8("Espacio sin nombre %1").arg(++NewSpaceCounter);

  QTreeWidgetItem * spaceItem = new QTreeWidgetItem;
  spaceItem->setText(0,spaceName);
//  spaceItem->setFlags(
//      Qt::ItemFlags(spaceItem->flags()&
//                    Qt::ItemIsDragEnabled&
//                    ~Qt::ItemIsDropEnabled));

  projectSpaceItem->addChild(spaceItem);
  projectSpaceItem->setExpanded(true);

  ui->projectsTree->setCurrentItem(spaceItem);
  on_projectsTree_itemDoubleClicked(spaceItem,0);

  p->spaceWidgetHash.insert(spaceItem,0);
  p->spaceHash.insert(spaceItem,qspace);
  connect(qspace,SIGNAL(spacePropertyUpdated(QString,QVariant)),
          this,SLOT(spacePropertyUpdated(QString,QVariant)));
}

void MainWindow::on_actionSpaceDelete_triggered()
{
  QTreeWidgetItem * item = ui->projectsTree->currentItem();

  if ( p->spaceWidgetHash.contains(item) )
  {
    QSpace * space = p->spaceHash.take(item);
    QWidget * w = p->spaceWidgetHash.take(item);
    p->widgetSpaceHash.remove(w);
    delete w;
    delete item;
    delete space;
  }
}

void MainWindow::spacePropertyUpdated( const QString & name, const QVariant & value )
{
  QSpace * qspace = qobject_cast<QSpace*>(sender());
  if ( !qspace ) return;

  QString text = value.toString();
  if ( name != "srps.name" ) return;
  if ( text.isEmpty() )
    text = trUtf8("Espacio sin nombre");

  QList<QTreeWidgetItem*> items = p->spaceHash.keys(qspace);
  foreach( QTreeWidgetItem * item, items )
  {
    item->setText(0,text);
    if ( p->spaceWidgetHash.value(item) )
      p->spaceWidgetHash.value(item)->setWindowTitle(text);
  }
}

void MainWindow::on_actionSpaceSimulate_triggered()
{
  QTreeWidgetItem * item = ui->projectsTree->currentItem();

  if ( !p->spaceHash.contains(item) ) return;

  SimulationWizard wizard;
  wizard.resize(700,400);
  wizard.setFactory(p->SRPS_Factory);
  if ( wizard.exec() != QWizard::Accepted )
    return;

  Simulator::JobList jobs; // for simulator
  Simulator::ComponentConfigurationList accumulatorList;

  int iteratios = wizard.iterations();
  bool accumulateOriginal = wizard.accumulateBaseSpace();

  QPair<QString,QVariantHash> prng = wizard.prngConfiguration();
  QPair<QString,QVariantHash> dprng = wizard.dprngConfiguration();

  QList<QPair<QString,QVariantHash> > accs =wizard.accumulatorConfigurations();

  while(!accs.isEmpty())
  {
    QPair<QString,QVariantHash> mc = accs.takeFirst();
    Simulator::ComponentConfiguration cc;
    cc.component = mc.first;
    cc.configuration = mc.second;
    accumulatorList.append(cc);
  }

  if ( accumulateOriginal ) // make job for accumulate
  {
    Simulator::Job job;
    job.setAccumulateOnly(true);
    job.setAccumulatorConfigurations(accumulatorList);
    jobs.append(job);
  }

  QList<QPair<QString,QVariantHash> > mods = wizard.modelConfigurations();

  while( !mods.isEmpty() )
  {
    QPair<QString,QVariantHash> mc = mods.takeFirst();
    Simulator::Job job;

    job.setAccumulateOnly(false);
    job.setIterations(iteratios);
    job.setAccumulatorConfigurations(accumulatorList);

    Simulator::ComponentConfiguration cc;

    cc.component = mc.first;
    cc.configuration = mc.second;
    job.setModelConfiguration(cc);

    cc.component = prng.first;
    cc.configuration = prng.second;
    job.setPRNGConfiguration(cc);

    cc.component = dprng.first;
    cc.configuration = dprng.second;
    job.setDPRNGConfiguration(cc);

    qDebug() << cc.component << cc.configuration;
    jobs.append(job);
  }

  QPair<QString,QVariantHash> simulatorConf = wizard.simulatorConfiguration();

  SRPS::Simulator * simulator = p->SRPS_Factory->simulatorFactoryCreate(simulatorConf.first,
                                                                      simulatorConf.second);

//  if ( !simulator )
//  {
//    QMessageBox::critical(this,trUtf8("Simulator"),trUtf8("Cant create Simulator"));
//    return;
//  }

  if ( simulator )
  {
    simulator->setFactory(dynamic_cast<SRPS::CurveFactory*>(p->SRPS_Factory));
    simulator->setFactory(dynamic_cast<SRPS::AccumulatorFactory*>(p->SRPS_Factory));
    simulator->setFactory(dynamic_cast<SRPS::ModelFactory*>(p->SRPS_Factory));
    simulator->setFactory(dynamic_cast<SRPS::PRNGFactory*>(p->SRPS_Factory));
    simulator->setFactory(dynamic_cast<SRPS::DPRNGFactory*>(p->SRPS_Factory));

    simulator->setSourceSpace(p->spaceHash.value(item)->getSpace());
    simulator->startJobs(jobs);
  }

  SimulatorDialog dlg;
  dlg.resize(640,480);
  dlg.startTracking(simulator);

  if ( simulator )
  {
    simulator->waitSimulator();
    if ( simulator->isCanceled() )
    {
//      QMessageBox::information(this,trUtf8("Simulator"),trUtf8("Canceled by user"));
      return;
    }
    else
    {
      foreach( SRPS::Curve * c, simulator->getResults() )
      {
        c->componentSetProperty("srps.name",
                                QString("%1(%2)")
                                  .arg(p->spaceHash.value(item)
                                       ->getSpace()
                                       ->componentProperty("srps.name").toString())
                                  .arg(simulatorConf.first));
        on_actionCurveNew_triggered(c);
      }
    }
  }
}

}}
