/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"

#include "gui_aboutdialog.h"
#include "gui_settingsdialog.h"

#include <SRPS/QCurve>
#include <SRPS/CurveEditor>
#include <SRPS/CurveRW>
#include <SRPS/SpaceGenerator>
#include <SRPS/Plugin>
#include <SRPS/PluginLoader>
#include <SRPS/ResourceLoader>
#include <SRPS/SettingsManager>

#include <QtWindowListMenu>

#include <QtGui/QMessageBox>

namespace SRPS {
namespace Implementation {

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    p ( new Private ),
    ui(new Ui::MainWindow)
{
  p->SRPS_Factory = new SRPSFactory(this);

  // FileRW
  p->File_RW = new FileRW(this);
  p->File_RW->setObjectName("FileRW");
  p->File_RW->setFactory(dynamic_cast<SRPS::SpaceRWFactory*>(p->SRPS_Factory));
  p->File_RW->setFactory(dynamic_cast<SRPS::CurveRWFactory*>(p->SRPS_Factory));
  p->File_RW->setFactory(dynamic_cast<SRPS::SpaceFactory*>(p->SRPS_Factory));
  p->File_RW->setFactory(dynamic_cast<SRPS::CurveFactory*>(p->SRPS_Factory));
  // File RW

  ui->setupUi(this);

  QtWindowListMenu * _menu = new QtWindowListMenu;
  _menu->attachToMdiArea(ui->mdiArea);
  menuBar()->insertMenu(ui->menuHelp->menuAction(),_menu);
  _menu->menuAction()->setText(trUtf8("Ventanas"));

  ui->projectsTree->header()->hide();

  // my connection
//  connect(p->File_RW,SIGNAL(error(QString)),
//          this,SLOT(on_FileRW_error(QString)));
//  connect(p->File_RW,SIGNAL(exportedToFile(QString,const SRPS::Curve*)),
//          this,SLOT(on_FileRW_exportedToFile(QString,const SRPS::Curve*)));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete p;
}

void MainWindow::setResourceLoader( QObject * rl )
{
  if ( p->ResourceLoader )
  {
    disconnect(p->ResourceLoader,SIGNAL(destroyed()),
               this,SLOT(deletedResourceLoader()));
    p->ResourceLoader = 0;
  }

  p->ResourceLoader = qobject_cast<SRPS::ResourceLoader*>(rl);

  if ( p->ResourceLoader )
  {
    connect(p->ResourceLoader,SIGNAL(destroyed()),
            this,SLOT(deletedResourceLoader()));
  }
}

void MainWindow::setPluginLoader( QObject * pl )
{
  if ( p->PluginLoader )
  {
    disconnect(p->PluginLoader,SIGNAL(destroyed()),
               this,SLOT(deletedPluginLoader()));
    p->PluginLoader = 0;
  }

  p->PluginLoader = qobject_cast<SRPS::PluginLoader*>(pl);

  if ( p->PluginLoader )
  {
    connect(p->PluginLoader,SIGNAL(destroyed()),
            this,SLOT(deletedPluginLoader()));

    foreach( QObject * object, p->PluginLoader->plugins() )
      p->SRPS_Factory->addComponentFactory(object);

    SRPS::SettingsManager * sm =
        p->PluginLoader->settingsManagerFor(p->SRPS_Factory);

    p->SRPS_Factory->loadSettings(sm);
    delete sm;
  }
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

//SRPS::SpaceEditor * MainWindow::createSpaceEditor() const
//{
//  SRPS::SpaceEditor * editor =
//      p->SRPS_Factory->spaceEditorFactoryCreate(QString());

//  if ( !editor )
//  {
//    QMessageBox::critical(const_cast<MainWindow*>(this),
//                          tr("New Editor"),
//                          tr("Check settings."));
//    return 0;
//  }

//  QWidget * widget = qobject_cast<QWidget*>(editor->componentAsQObject());
//  if ( !widget )
//  {
//    QMessageBox::critical(const_cast<MainWindow*>(this),
//                          tr("New Editor"),
//                          tr("Check settings, not widget"));
//    delete editor;
//    return 0;
//  }

//  return editor;
//}

//SRPS::Space * MainWindow::createSpace() const
//{
//  SRPS::Space * space = p->SRPS_Factory->spaceFactoryCreate(QString());
//  if ( !space )
//  {
//    QMessageBox::critical(const_cast<MainWindow*>(this),
//                          tr("New Space"),
//                          tr("Check settings."));
//    return 0;
//  }

//  return space;
//}

void MainWindow::deletedPluginLoader()
{
  p->PluginLoader = 0;
  setPluginLoader(0);
}

void MainWindow::deletedResourceLoader()
{
  p->ResourceLoader = 0;
  setResourceLoader(0);
}

void SRPS::Implementation::MainWindow::on_actionAboutQt_triggered()
{
    QMessageBox::aboutQt(this);
}

void SRPS::Implementation::MainWindow::on_actionSettings_triggered()
{
  SRPS::Implementation::SettingsDialog dlg;

  dlg.setSRPSFactory(p->SRPS_Factory);

  int r = dlg.exec();
  if (  r == QDialog::Accepted && p->PluginLoader )
  {
    SRPS::SettingsManager * sm =
        p->PluginLoader->settingsManagerFor(p->SRPS_Factory);

    p->SRPS_Factory->saveSettings(sm);
    delete sm;
  }
  else if ( r == QDialog::Rejected && p->PluginLoader )
  {
    SRPS::SettingsManager * sm =
        p->PluginLoader->settingsManagerFor(p->SRPS_Factory);

    p->SRPS_Factory->loadSettings(sm);
    delete sm;
  }
}

void SRPS::Implementation::MainWindow::on_actionQuit_triggered()
{
    close();
}

void SRPS::Implementation::MainWindow::on_actionAboutSRPS_triggered()
{
    SRPS::Implementation::AboutDialog dlg;
    dlg.render(this);
    dlg.exec();
}

void MainWindow::on_projectsTree_itemDoubleClicked(QTreeWidgetItem* item,
                                                   int /*column*/)
{
  if ( p->curveHash.contains(item) )
    curveitem_doubleclicked(item);

  if ( p->spaceHash.contains(item) )
    spaceitem_doubleclicked(item);
}

} // namespace Implementation
} // namespace SRPS

Q_EXPORT_PLUGIN2(SRPSGUI,SRPS::Implementation::MainWindow);
