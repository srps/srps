/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_PLUGIN_PRIVATE_H
#define GUI_PLUGIN_PRIVATE_H

#include <SRPS/PluginLoader>
#include <SRPS/ResourceLoader>
#include <SRPS/QCurve>
#include <SRPS/QSpace>

#include "gui_srpsfactory.h"
#include "gui_filerw.h"

#include "gui_plugin.h"

#include <QtGui/QTreeWidgetItem>

namespace SRPS {
namespace Implementation  {

class MainWindow::Private
{
public:
  Private():
      PluginLoader(0),
      ResourceLoader(0),
      SRPS_Factory(0)
  {}

  SRPS::PluginLoader * PluginLoader;
  SRPS::ResourceLoader * ResourceLoader;
  SRPSFactory * SRPS_Factory;
  FileRW * File_RW;

  QHash<QTreeWidgetItem*,QPointer<QSpace> > spaceHash;
  QHash<QTreeWidgetItem*,QPointer<QWidget> > spaceWidgetHash;
  QHash<QWidget*,QTreeWidgetItem*> widgetSpaceHash;

  QHash<QTreeWidgetItem*,QPointer<QCurve> > curveHash;
  QHash<QTreeWidgetItem*,QPointer<QWidget> > curveWidgetHash;
  QHash<QWidget*,QTreeWidgetItem*> widgetCurveHash;
};

}
}

#endif // GUI_PLUGIN_PRIVATE_H
