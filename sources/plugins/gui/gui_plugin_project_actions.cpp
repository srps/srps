/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"

#include <QtCore/QLibraryInfo>

#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

namespace SRPS {
namespace Implementation {

// project helpers

QTreeWidgetItem * MainWindow::currentProjectItem() const
{
  QTreeWidgetItem * item = ui->projectsTree->currentItem();
  if ( !item ) return 0;

  while( item->parent() )
    item = item->parent();

  return item;
}

QTreeWidgetItem * MainWindow::currentProjectCurveItem() const
{
  QTreeWidgetItem * item = currentProjectItem();
  if ( !item ) return 0;

  return item->child(1);
}

QTreeWidgetItem * MainWindow::currentProjectSpaceItem() const
{
  QTreeWidgetItem * item = currentProjectItem();
  if ( !item ) return 0;

  return item->child(0);
}

QList<SRPS::Curve*> MainWindow::currentProjectCurves() const
{
  QList<SRPS::Curve*> result;
  QTreeWidgetItem * citem = currentProjectCurveItem();

  if (!citem) return result;

  for( int i = 0; i < citem->childCount(); ++i )
  {
    SRPS::QCurve * qcurve = p->curveHash.value(citem->child(i));
    if( qcurve )
      if ( qcurve->getCurve() )
        result << qcurve->getCurve();
  }

  return result;
}

QList<SRPS::Space*> MainWindow::currentProjectSpaces() const
{
  QList<SRPS::Space*> result;
  QTreeWidgetItem * sitem = currentProjectSpaceItem();

  if (!sitem) return result;

  for( int i = 0; i < sitem->childCount(); ++i )
  {
    SRPS::QSpace * qspace = p->spaceHash.value(sitem->child(i));
    if( qspace )
      if ( qspace->getSpace() )
        result << qspace->getSpace();
  }

  return result;
}

void MainWindow::on_actionProjectNew_triggered()
{
  static int NewProjectCounter = 0;
  static int ProjectEnableActions = true;

  QString projectName = trUtf8("Projecto sin nombre %1").arg(++NewProjectCounter);

  QTreeWidgetItem * projectItem = new QTreeWidgetItem;
  projectItem->setText(0,projectName);
//  projectItem->setFlags(
//      Qt::ItemFlags(projectItem->flags()&
//                    ~Qt::ItemIsDragEnabled&
//                    ~Qt::ItemIsDropEnabled));

  QTreeWidgetItem * curveItem = new QTreeWidgetItem;
  curveItem->setFlags(
      Qt::ItemFlags(curveItem->flags()&
                    ~Qt::ItemIsSelectable));
  curveItem->setText(0,trUtf8("Curvas"));

  QTreeWidgetItem * spaceItem = new QTreeWidgetItem;
  spaceItem->setFlags(
      Qt::ItemFlags(spaceItem->flags()&
                    ~Qt::ItemIsSelectable));
  spaceItem->setText(0,trUtf8("Espacios"));

  projectItem->addChild(spaceItem);
  projectItem->addChild(curveItem);

  ui->projectsTree->addTopLevelItem(projectItem);
  projectItem->setExpanded(true);

  if ( ui->projectsTree->topLevelItemCount() )
  {
    ProjectEnableActions = false;

    // project actions
    ui->actionProjectSaveAs->setEnabled(true);
    ui->actionProjectSave->setEnabled(true);
    ui->actionProjectSaveAll->setEnabled(true);
    ui->actionProjectClose->setEnabled(true);
    ui->actionProjectCloseAll->setEnabled(true);

    // space actions
    ui->actionSpaceNew->setEnabled(true);
    ui->actionSpaceDelete->setEnabled(true);
    ui->actionSpaceSimulate->setEnabled(true);

    // curve actions
    ui->actionCurveNew->setEnabled(true);
    ui->actionCurveDelete->setEnabled(true);
  }

  ui->projectsTree->setCurrentItem(projectItem);
}

void MainWindow::on_actionProjectClose_triggered()
{
  QTreeWidgetItem * item = currentProjectItem();
  if ( !item ) return;

  QTreeWidgetItem * citem = currentProjectCurveItem();
  for( int c = 0; c < citem->childCount(); ++c )
  {
    delete p->curveHash.take(citem->child(c));
    QWidget * w = p->curveWidgetHash.take(citem->child(c));
    p->widgetCurveHash.take(w);
    delete w;
  }

  QTreeWidgetItem * sitem = currentProjectSpaceItem();
  for( int s = 0; s < sitem->childCount(); ++s )
  {
    delete p->spaceHash.take(sitem->child(s));
    QWidget * w = p->spaceWidgetHash.take(sitem->child(s));
    p->widgetSpaceHash.take(w);
    delete w;
  }

  delete item;

  static int ProjectEnableActions = true;

  if ( !ui->projectsTree->topLevelItemCount() )
  {
    ProjectEnableActions = true;

    // project actions
    ui->actionProjectSaveAs->setEnabled(false);
    ui->actionProjectSave->setEnabled(false);
    ui->actionProjectSaveAll->setEnabled(false);
    ui->actionProjectClose->setEnabled(false);
    ui->actionProjectCloseAll->setEnabled(false);

    // space actions
    ui->actionSpaceNew->setEnabled(false);
    ui->actionSpaceDelete->setEnabled(false);
    ui->actionSpaceSimulate->setEnabled(false);

    // curve actions
    ui->actionCurveNew->setEnabled(false);
    ui->actionCurveDelete->setEnabled(false);
  }
}

void MainWindow::on_actionProjectOpen_triggered()
{
  QStringList filepaths = QFileDialog::getOpenFileNames(this,
                                                 trUtf8("Open"),
                                                 QLibraryInfo::location(QLibraryInfo::DataPath),
                                                 trUtf8("SRPS Files (*.srps)"));

  if ( filepaths.isEmpty() ) return;

  p->File_RW->setSpaceKey(p->SRPS_Factory->spaceFactoryDefault());
  p->File_RW->setCurveKey(p->SRPS_Factory->curveFactoryDefault());

  foreach( QString path, filepaths )
    p->File_RW->readFile(path);
}

void MainWindow::on_actionProjectSave_triggered()
{
  QTreeWidgetItem * item = currentProjectItem();
  if (!item) return;

  QString filepath = item->toolTip(0);
  if ( filepath.isEmpty() )
    on_actionProjectSaveAs_triggered();
  else
  {
    p->File_RW->setCurveRWKey(p->SRPS_Factory->curveRWFactoryDefault());
    p->File_RW->setSpaceRWKey(p->SRPS_Factory->spaceRWFactoryDefault());
    p->File_RW->saveFile(filepath,currentProjectSpaces(),currentProjectCurves());
  }
}

void MainWindow::on_actionProjectSaveAs_triggered()
{
  QTreeWidgetItem * item = currentProjectItem();
  if (!item) return;

  QString filepath = QFileDialog::getSaveFileName(this,
                                                  trUtf8("Guardar como"),
                                                  QLibraryInfo::location(QLibraryInfo::DataPath),
                                                  trUtf8("Proyecto SRPS (*.srps)"));

  if (filepath.isEmpty()) return;

  item->setToolTip(0,filepath);
  on_actionProjectSave_triggered();
}

}}
