/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_FILERW_H
#define GUI_FILERW_H

#include <SRPS/Space>
#include <SRPS/SpaceFactory>
#include <SRPS/SpaceRW>
#include <SRPS/SpaceRWFactory>

#include <SRPS/Curve>
#include <SRPS/CurveFactory>
#include <SRPS/CurveRW>
#include <SRPS/CurveRWFactory>

#include <QtCore/QObject>
#include <QtCore/QFile>

namespace SRPS {
namespace Implementation {

class FileRW : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(FileRW)

  SpaceFactory * space_factory;
  CurveFactory * curve_factory;

  SpaceRWFactory * space_rw_factory;
  CurveRWFactory * curve_rw_factory;

  QVariantHash space_rw_conf;
  QVariantHash curve_rw_conf;

  QString space_rw_key;
  QString curve_rw_key;
  QString space_key;
  QString curve_key;

public:

    explicit FileRW(QObject *parent = 0);
    virtual ~FileRW();

    void setSpaceRWKey( const QString & k, const QVariantHash & conf = QVariantHash() );
    void setCurveRWKey( const QString & k, const QVariantHash & conf = QVariantHash() );
    void setSpaceKey( const QString & k );
    void setCurveKey( const QString & k );

    void setFactory( SRPS::SpaceRWFactory * rw_factory );
    void setFactory( SRPS::CurveRWFactory * rw_factory );
    void setFactory( SRPS::SpaceFactory * space_factory );
    void setFactory( SRPS::CurveFactory * curve_factory );

    int readFile( const QString & filepath );

    int saveFile( const QString & filepath,
                  const QList<SRPS::Space*> & spaces,
                  const QList<SRPS::Curve*> & curves );

    int exportToFile( const QString & filepath, const SRPS::Curve * curve );
    int exportToFile( const QString & filepath, const SRPS::Space * space );

private:

    bool generalCheck();
    void readOne( QFile * file );

    QFile * openFile( const QString & filepath, QFile::OpenMode mode );

signals:

    void projectFound( const QString & filepath );
    void curveReaded( const QString & filepath, SRPS::Curve * curve );
    void spaceReaded( const QString & filepath, SRPS::Space * space );

    void fileSaved( const QString & filepath );

    void exportedToFile( const QString & filepath, const SRPS::Curve * curve );
    void exportedToFile( const QString & filepath, const SRPS::Space * space );

    void error( const QString & message );
};

} // namespace Implemenation
} // namespace SRPS

Q_DECLARE_METATYPE(SRPS::Curve*)
Q_DECLARE_METATYPE(SRPS::Space*)

#endif // GUI_FILERW_H
