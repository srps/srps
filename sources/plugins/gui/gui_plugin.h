/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_PLUGIN_H
#define GUI_PLUGIN_H

#include <QtGui/QMainWindow>

class QTreeWidgetItem;

namespace SRPS {

class Space;
class SpaceEditor;
class SpaceGenerator;
class Curve;
class CurveEditor;
class CurveVisualizer;
class Space;

namespace Implementation {

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  Q_DISABLE_COPY(MainWindow)

  class Private;
  Private * p;
  Ui::MainWindow *ui;

public:

  explicit MainWindow(QWidget *parent = 0);
  virtual ~MainWindow();

  Q_INVOKABLE void setResourceLoader( QObject * pl );
  Q_INVOKABLE void setPluginLoader( QObject * pl );

protected:
  void changeEvent(QEvent *e);

  // project helpers
  QTreeWidgetItem * currentProjectItem() const;
  QTreeWidgetItem * currentProjectCurveItem() const;
  QTreeWidgetItem * currentProjectSpaceItem() const;
  QList<SRPS::Curve*> currentProjectCurves() const;
  QList<SRPS::Space*> currentProjectSpaces() const;

  // curve helpers
  SRPS::Curve * createCurve() const;
  SRPS::CurveEditor * createCurveEditor() const;
  SRPS::CurveVisualizer * createCurveVisualizer() const;
  void curveitem_doubleclicked( QTreeWidgetItem * item );

  // space helpers
  SRPS::Space * createSpace() const;
  SRPS::SpaceEditor * createSpaceEditor() const;
  SRPS::SpaceGenerator * createSpaceGenerator() const;
  void spaceitem_doubleclicked( QTreeWidgetItem * item );

private slots:

  void deletedPluginLoader();
  void deletedResourceLoader();

  void on_actionAboutQt_triggered();
  void on_actionAboutSRPS_triggered();
  void on_actionQuit_triggered();
  void on_actionSettings_triggered();

  void on_projectsTree_itemDoubleClicked(QTreeWidgetItem* item, int column);

  // test action
  void on_actionTestPRNG_triggered();

  // project actions
  void on_actionProjectNew_triggered();
  void on_actionProjectClose_triggered();
  void on_actionProjectOpen_triggered(); // importing files too
  void on_actionProjectSave_triggered();
  void on_actionProjectSaveAs_triggered();

  // space actions
  void on_actionSpaceNew_triggered( SRPS::Space * space = 0 );
  void on_actionSpaceDelete_triggered();
  void on_actionSpaceSimulate_triggered();

  void spacePropertyUpdated( const QString & name, const QVariant & value );

  // curve actions
  void on_actionCurveNew_triggered( SRPS::Curve * curve = 0 );
  void on_actionCurveDelete_triggered();

  void curvePropertyUpdated( const QString & name, const QVariant & value );

  // File Manager
  void on_FileRW_projectFound( const QString & f );
  void on_FileRW_fileSaved( const QString & f );
  void on_FileRW_error( const QString & e );
  void on_FileRW_exportedToFile( const QString & filepath, const SRPS::Curve * curve );
  void on_FileRW_exportedToFile( const QString & filepath, const SRPS::Space * space );
  void on_FileRW_curveReaded( const QString & filepath, SRPS::Curve * curve );
  void on_FileRW_spaceReaded( const QString & filepath, SRPS::Space * space );

  void on_actionExportToFile_triggered();

};

} // namespace Implementation
} // namespace SRPS
#endif // GUI_PLUGIN_H
