/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_simulatordialog.h"
#include "ui_gui_simulatordialog.h"

#include <SRPS/Simulator>
#include <SRPS/ComponentStatusWidget>

#include <QtCore/QTimerEvent>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>

namespace SRPS {
namespace Implementation {

SimulatorDialog::SimulatorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SimulatorDialog)
{
    ui->setupUi(this);
    tid = 0;
    simulator = 0;

    setWindowTitle(trUtf8("Simulación"));
}

SimulatorDialog::~SimulatorDialog()
{
    delete ui;
}

int SimulatorDialog::startTracking( SRPS::Simulator * _s )
{
  QWidget * w = 0;
  simulator = _s;
  if (simulator )
  {
    w = simulator->componentStatusWidget();
  }

  if (!w)
    w = new QLabel(tr("Espere mientras termina el simulador"));
  ui->verticalLayout->insertWidget(0,w);

  tid = startTimer(100);

  connect(ui->buttonBox->button(QDialogButtonBox::Cancel),SIGNAL(clicked()),
          this,SLOT(cancelSimulator()));

  int r = exec();

  killTimer(tid);

  return r;
}

void SimulatorDialog::cancelSimulator()
{
  if ( simulator )
    simulator->cancel();

//  ui->buttonBox->clear();
//  QPushButton * b = ui->buttonBox->addButton(QDialogButtonBox::Close);
//  connect(b,SIGNAL(clicked()),
//          this,SLOT(reject()));
}

void SimulatorDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SimulatorDialog::timerEvent(QTimerEvent * e)
{
  if ( simulator )
  {
    ComponentStatusWidget * csw = qobject_cast<ComponentStatusWidget*>(ui->verticalLayout->itemAt(0)->widget());

    if ( csw )
      csw->componentStatusWidgetReload();

    if ( simulator->hasFinished() || simulator->isCanceled() )
    {
      disconnect(ui->buttonBox->button(QDialogButtonBox::Cancel),SIGNAL(clicked()),
              this,SLOT(cancelSimulator()));
      ui->buttonBox->clear();
      QPushButton * b = ui->buttonBox->addButton(QDialogButtonBox::Close);
      connect(b,SIGNAL(clicked()),
              this,SLOT(accept()));
      killTimer(tid);
    }
  }
}

} // namespace Implementation
} // namespace SRPS
