/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_plugin.h"
#include "ui_gui_plugin.h"
#include "gui_plugin_private.h"

#include <SRPS/CurveEditor>
#include <SRPS/CurveVisualizer>

#include <QtGui/QMessageBox>
#include <QtGui/QTabWidget>
#include <QtGui/QMdiSubWindow>
#include <QtGui/QLabel>

namespace SRPS {
namespace Implementation {

// curve helpers

SRPS::Curve * MainWindow::createCurve() const
{
  return p->SRPS_Factory->curveFactoryCreate(QString());
}

SRPS::CurveEditor * MainWindow::createCurveEditor() const
{
  SRPS::CurveEditor * editor = p->SRPS_Factory->curveEditorFactoryCreate(QString());
  if ( !editor )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo editor"),
                          trUtf8("Comprobar configuracion."));
    return 0;
  }

  QWidget * widget = qobject_cast<QWidget*>(editor->componentAsQObject());
  if ( !widget )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo editor"),
                          trUtf8("COmprobar configuración, no es gráfico."));
    delete editor;
    return 0;
  }

  return editor;
}

SRPS::CurveVisualizer * MainWindow::createCurveVisualizer() const
{
  SRPS::CurveVisualizer * visualizer = p->SRPS_Factory->curveVisualizerFactoryCreate(QString());
  if ( !visualizer )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo visualizador"),
                          trUtf8("Comprobar configuración."));
    return 0;
  }

  QWidget * widget = qobject_cast<QWidget*>(visualizer->componentAsQObject());
  if ( !widget )
  {
    QMessageBox::critical(const_cast<MainWindow*>(this),
                          trUtf8("Nuevo visualizador"),
                          trUtf8("Comprobar configuración, no es gráfico."));
    delete visualizer;
    return 0;
  }

  return visualizer;
}

void MainWindow::curveitem_doubleclicked( QTreeWidgetItem * item )
{
  if ( !p->curveWidgetHash.contains(item) ) return;

  QWidget * widget = p->curveWidgetHash.value(item);
  QMdiSubWindow * sub = qobject_cast<QMdiSubWindow*>(widget);

  if ( !sub )
  {
    delete widget;

    CurveEditor * editor = createCurveEditor();
    if ( editor )
      editor->curveEdit(p->curveHash.value(item));

    CurveVisualizer * visualizer = createCurveVisualizer();
    if ( visualizer )
      visualizer->curveVisualize(p->curveHash.value(item));

    QTabWidget * tab = new QTabWidget;
    if ( visualizer )
      tab->addTab(qobject_cast<QWidget*>(visualizer->componentAsQObject()),trUtf8("Vista"));
    else
      tab->addTab(new QLabel(trUtf8("No hay vistas disponibles")),trUtf8("Vista"));
    if ( editor )
      tab->addTab(qobject_cast<QWidget*>(editor->componentAsQObject()),trUtf8("Editor"));
    else
      tab->addTab(new QLabel(trUtf8("No hay editor disponibles")),trUtf8("Editor"));

    sub = ui->mdiArea->addSubWindow(tab);
    sub->setAttribute(Qt::WA_DeleteOnClose,true);
    sub->setWindowTitle(item->text(0));

    p->curveWidgetHash.insert(item,sub);
    p->widgetCurveHash.insert(sub,item);
  }

  if ( sub )
    sub->showMaximized();
}

void MainWindow::on_actionCurveNew_triggered( SRPS::Curve * curve )
{
  static int NewCurveCounter = 0;

  if (!curve )
    curve = createCurve();
  if ( !curve ) return;

  QCurve * qcurve = new QCurve(this);
  qcurve->setOwnership(true);
  qcurve->setCurve(curve);

  QTreeWidgetItem * projectCurveItem = currentProjectCurveItem();
  if ( !projectCurveItem) return;

  QString curveName = curve->componentProperty("srps.name").toString();
  if ( curveName.isEmpty() )
    curveName = trUtf8("Curva sin nombre %1").arg(++NewCurveCounter);

  QTreeWidgetItem * curveItem = new QTreeWidgetItem;
  curveItem->setText(0,curveName);
//  curveItem->setFlags(Qt::ItemIsSelectable);

  projectCurveItem->addChild(curveItem);
  projectCurveItem->setExpanded(true);

  ui->projectsTree->setCurrentItem(curveItem);
  on_projectsTree_itemDoubleClicked(curveItem,0);

  p->curveWidgetHash.insert(curveItem,0);
  p->curveHash.insert(curveItem,qcurve);
  connect(qcurve,SIGNAL(curvePropertyUpdated(QString,QVariant)),
          this,SLOT(curvePropertyUpdated(QString,QVariant)));
}

void MainWindow::on_actionCurveDelete_triggered()
{
  QTreeWidgetItem * item = ui->projectsTree->currentItem();

  if ( p->curveWidgetHash.contains(item) )
  {
    QCurve * curve = p->curveHash.take(item);
    QWidget * w = p->curveWidgetHash.take(item);
    p->widgetCurveHash.remove(w);
    delete w;
    delete item;
    delete curve;
  }
}

void MainWindow::curvePropertyUpdated( const QString & name, const QVariant & value )
{
  QCurve * qcurve = qobject_cast<QCurve*>(sender());
  if ( !qcurve ) return;

  QString text = value.toString();
  if ( name != "srps.name" ) return;
  if ( text.isEmpty() )
    text = trUtf8("Curva sin nombre");

  QList<QTreeWidgetItem*> items = p->curveHash.keys(qcurve);
  foreach( QTreeWidgetItem * item, items )
  {
    item->setText(0,text);
    if ( p->curveWidgetHash.value(item) )
      p->curveWidgetHash.value(item)->setWindowTitle(text);
  }
}

}}
