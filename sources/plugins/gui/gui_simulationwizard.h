/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef GUI_SIMULATIONWIZARD_H
#define GUI_SIMULATIONWIZARD_H

#include <QtCore/QPair>
#include <QtCore/QVariantHash>

#include <QtGui/QWizard>
#include <QtGui/QWizardPage>
#include <QtGui/QSpinBox>
#include <QtGui/QCheckBox>

#include "gui_srpsfactory.h"
#include "gui_singlenewcomponentwidget.h"
#include "gui_multiplenewcomponentwidget.h"

namespace SRPS {
namespace Implementation {

class SimulationWizardSinglePage : public QWizardPage
{
    Q_OBJECT
    Q_DISABLE_COPY(SimulationWizardSinglePage)

    SingleNewComponentWidget * widget;

    public:

    explicit SimulationWizardSinglePage(QWidget * parent = 0);
    virtual ~SimulationWizardSinglePage();

    virtual bool isComplete();

    void setFactory( SRPSFactory * factory );
    void setComponents( const QStringList & components );
    void setup();

    QString selectedComponent() const;

    QVariantHash selectedComponentConfiguration() const;
};

class SimulationWizardMultiplePage : public QWizardPage
{
    Q_OBJECT
    Q_DISABLE_COPY(SimulationWizardMultiplePage)

    MultipleNewComponetWidget * widget;

    public:

    explicit SimulationWizardMultiplePage(QWidget* parent = 0);
    virtual ~SimulationWizardMultiplePage();

    void setFactory( SRPSFactory * factory );
    void setComponents( const QStringList & components );
    void setup();

    bool validatePage();

    virtual bool isComplete();

    QList<QPair<QString,QVariantHash> > selectedComponentConfigurations() const;

};

class SimulationWizardFinalPage : public QWizardPage
{
    Q_OBJECT
    Q_DISABLE_COPY(SimulationWizardFinalPage)

    QSpinBox * spin;
    QCheckBox * check;

    public:

    explicit SimulationWizardFinalPage(QWidget* parent = 0);
    virtual ~SimulationWizardFinalPage();

    int iterations() const;
    bool accumulate() const;

};

class SimulationWizard : public QWizard
{
    Q_OBJECT
    Q_DISABLE_COPY(SimulationWizard);

    SimulationWizardMultiplePage * modelPage;
    SimulationWizardMultiplePage * accumulatorPage;
    SimulationWizardSinglePage * prngPage;
    SimulationWizardSinglePage * dprngPage;
    SimulationWizardSinglePage * simulatorPage;
    SimulationWizardFinalPage * finalPage;

public:

    explicit SimulationWizard(QWidget * parent = 0);
    virtual ~SimulationWizard();

    QList<QPair<QString,QVariantHash> > modelConfigurations() const;
    QList<QPair<QString,QVariantHash> > accumulatorConfigurations() const;

    QPair<QString,QVariantHash> prngConfiguration() const;
    QPair<QString,QVariantHash> dprngConfiguration() const;

    QPair<QString,QVariantHash> simulatorConfiguration() const;

    int iterations() const;
    bool accumulateBaseSpace() const;

    void setFactory( SRPSFactory * factory );

};

} // namespace Implementation
} // namespace SRPS

#endif // GUI_SIMULATIONWIZARD_H
