/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "gui_settingsdialog.h"
#include "ui_gui_settingsdialog.h"
#include "gui_srpsfactory.h"

#include <SRPS/ComponentFactoryWidget>
#include <SRPS/SettingsManager>

#include <Qt>
#include <QtCore/QHashIterator>
#include <QtCore/QPointer>
#include <QtGui/QTreeWidgetItem>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

namespace SRPS {
namespace Implementation {

class SettingsDialog::Private
{
public:
    QTreeWidgetItem * generalItem;
    QTreeWidgetItem * translationsItem;
    QTreeWidgetItem * resourcesItem;
    QTreeWidgetItem * factoriesItem;
    QTreeWidgetItem * pluginsItem;

    QTreeWidgetItem * curveFactoryItem;
    QTreeWidgetItem * curveEditorFactoryItem;
    QTreeWidgetItem * curveRWFactoryItem;
    QTreeWidgetItem * curveVisualizerFactoryItem;
    QTreeWidgetItem * spaceFactoryItem;
    QTreeWidgetItem * spaceEditorFactoryItem;
    QTreeWidgetItem * spaceRWFactoryItem;
    QTreeWidgetItem * spaceGeneratorFactoryItem;
    QTreeWidgetItem * prngFactoryItem;
    QTreeWidgetItem * dprngFactoryItem;
    QTreeWidgetItem * modelFactoryItem;
    QTreeWidgetItem * accumulatorFactoryItem;
    QTreeWidgetItem * simulatorFactoryItem;

    QHash<QTreeWidgetItem*,QWidget*> table;

    QPointer<SRPSFactory> factory;

    QHash<QString, QVariantHash> defaultComponentSettings;
};

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    p ( new Private ),
    ui(new Ui::SettingsDialog)
{
  ui->setupUi(this);

  // toplevel items creation
  p->generalItem = new QTreeWidgetItem(QStringList(trUtf8("General")));
  ui->settingsTree->addTopLevelItem(p->generalItem);

  p->factoriesItem = new QTreeWidgetItem(QStringList(trUtf8("Fábricas")));
  ui->settingsTree->addTopLevelItem(p->factoriesItem);

  p->curveFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Curvas")));
  p->factoriesItem->addChild(p->curveFactoryItem);

  p->curveEditorFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Editores de curvas")));
  p->factoriesItem->addChild(p->curveEditorFactoryItem);

  p->curveRWFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Serializadores de curvas")));
  p->factoriesItem->addChild(p->curveRWFactoryItem);

  p->curveVisualizerFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Visualizadores de curvas")));
  p->factoriesItem->addChild(p->curveVisualizerFactoryItem);

  p->spaceFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Espacios")));
  p->factoriesItem->addChild(p->spaceFactoryItem);

  p->spaceEditorFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Editor de espacios")));
  p->factoriesItem->addChild(p->spaceEditorFactoryItem);

  p->spaceGeneratorFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Generador de espacios")));
  p->factoriesItem->addChild(p->spaceGeneratorFactoryItem);

  p->spaceRWFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Serialziador espacios")));
  p->factoriesItem->addChild(p->spaceRWFactoryItem);

  p->prngFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("PRNG")));
  p->factoriesItem->addChild(p->prngFactoryItem);

  p->dprngFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("DPRNG")));
  p->factoriesItem->addChild(p->dprngFactoryItem);

  p->modelFactoryItem = new QTreeWidgetItem(QStringList(trUtf8("Modelos")));
  p->factoriesItem->addChild(p->modelFactoryItem);

  p->accumulatorFactoryItem= new QTreeWidgetItem(QStringList(trUtf8("Acumuladores")));
  p->factoriesItem->addChild(p->accumulatorFactoryItem);

  p->simulatorFactoryItem= new QTreeWidgetItem(QStringList(trUtf8("Simuladores")));
  p->factoriesItem->addChild(p->simulatorFactoryItem);

  // item -> widget switch configuration
  p->table.insert( p->generalItem, ui->generalPage );

  // hide header
  ui->settingsTree->header()->hide();

  // display first.
  ui->settingsTree->setCurrentItem( p->generalItem );
  ui->settingsView->setCurrentWidget(p->table.value(p->generalItem));

  // connections

  connect( ui->settingsTree, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
           this,SLOT(changeSettingsPage(QTreeWidgetItem*)));

  connect( ui->defaultCurveConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultCurveEditorConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultCurveRWConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultCurveVisualizerConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultSpaceConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultSpaceEditorConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultSpaceRWConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));
  connect( ui->defaultPRNGConfigurationButton, SIGNAL(clicked()),
           this,SLOT(configureComponentSwitcher()));

  ui->defaultPRNGLabel->hide();
  ui->defaultPRNGConfigurationButton->hide();
  ui->defaultPRNGSelect->hide();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
    delete p;
}

static void renderComponentWidgets( QTreeWidgetItem * parent,
                                    SRPS::ComponentFactory * factory,
                                    const QStringList & keys,
                                    QHash<QTreeWidgetItem*,QWidget*> * table,
                                    QStackedWidget * stacked )
{
  if ( !parent || !factory || !table || !stacked)
    return;

  foreach( QString key, keys )
  {
    QTreeWidgetItem * child = new QTreeWidgetItem(QStringList(key));
    parent->addChild(child);

    QWidget * widget = factory->componentFactoryWidgetFor(key);
    if( !widget )
      widget = new QLabel(SettingsDialog::trUtf8("Non-GUI Configurable component."));

    stacked->addWidget(widget);
    table->insert(child,widget);
  }
}

void SettingsDialog::setSRPSFactory(QObject *object)
{
  p->factory = qobject_cast<SRPSFactory*>(object);

  if ( !p->factory ) return;

  // Curves
  {
    QStringList keys = p->factory->curveFactoryKeys();

    ui->defaultCurveSelect->addItems(keys);

    renderComponentWidgets(p->curveFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // CurveEditors
  {
    QStringList keys = p->factory->curveEditorFactoryKeys();

    ui->defaultCurveEditorSelect->addItems(keys);

    renderComponentWidgets(p->curveEditorFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // Curve RWs
  {
    QStringList keys = p->factory->curveRWFactoryKeys();

    ui->defaultCurveRWSelect->addItems(keys);

    renderComponentWidgets(p->curveRWFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // Curve Visualizers
  {
    QStringList keys = p->factory->curveVisualizerFactoryKeys();

    ui->defaultCurveVisualizerSelect->addItems(keys);

    renderComponentWidgets(p->curveVisualizerFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // Spaces
  {
    QStringList keys = p->factory->spaceFactoryKeys();

    ui->defaultSpaceSelect->addItems(keys);

    renderComponentWidgets(p->spaceFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // SpaceEditors
  {
    QStringList keys = p->factory->spaceEditorFactoryKeys();

    ui->defaultSpaceEditorSelect->addItems(keys);

    renderComponentWidgets(p->spaceEditorFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // SpaceRWs
  {
    QStringList keys = p->factory->spaceRWFactoryKeys();

    ui->defaultSpaceRWSelect->addItems(keys);

    renderComponentWidgets(p->spaceRWFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // SpaceGenerator
  {
    QStringList keys = p->factory->spaceGeneratorFactoryKeys();

//    ui->defaultSpaceRWSelect->addItems(keys);

    renderComponentWidgets(p->spaceGeneratorFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  // PRNG
  {
    QStringList keys = p->factory->prngFactoryKeys();

    ui->defaultPRNGSelect->addItems(keys);

    renderComponentWidgets(p->prngFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

//  // PRNG
  {
    QStringList keys = p->factory->dprngFactoryKeys();

//    ui->defaultDPRNGSelect->addItems(keys);

    renderComponentWidgets(p->dprngFactoryItem,p->factory,keys,&p->table,ui->settingsView);
  }

  //  // Model
    {
      QStringList keys = p->factory->modelFactoryKeys();

  //    ui->defaultDPRNGSelect->addItems(keys);

      renderComponentWidgets(p->modelFactoryItem,p->factory,keys,&p->table,ui->settingsView);
    }

  //  // Accumulator
    {
      QStringList keys = p->factory->accumulatorFactoryKeys();

  //    ui->defaultDPRNGSelect->addItems(keys);

      renderComponentWidgets(p->accumulatorFactoryItem,p->factory,keys,&p->table,ui->settingsView);
    }

  // Simulator
    {
      QStringList keys = p->factory->simulatorFactoryKeys();

      renderComponentWidgets(p->simulatorFactoryItem,p->factory,keys,&p->table,ui->settingsView);
    }

  loadSettings();
}

void SettingsDialog::loadSettings()
{
  if ( !p->factory )
    return;

  // Curve
  {
    QString component = p->factory->curveFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultCurve);

    ui->defaultCurveSelect->setCurrentIndex(
        ui->defaultCurveSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // Curve Editor
  {
    QString component = p->factory->curveEditorFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultCurveEditor);

    ui->defaultCurveEditorSelect->setCurrentIndex(
        ui->defaultCurveEditorSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // Curve RWs
  {
    QString component = p->factory->curveRWFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultCurveRW);

    ui->defaultCurveRWSelect->setCurrentIndex(
        ui->defaultCurveRWSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // Curve Visualizers
  {
    QString component = p->factory->curveVisualizerFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultCurveVisualizer);

    ui->defaultCurveVisualizerSelect->setCurrentIndex(
        ui->defaultCurveVisualizerSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // Space
  {
    QString component = p->factory->spaceFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultSpace);

    ui->defaultSpaceSelect->setCurrentIndex(
        ui->defaultSpaceSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // Space Editor
  {
    QString component = p->factory->spaceEditorFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultSpaceEditor);

    ui->defaultSpaceEditorSelect->setCurrentIndex(
        ui->defaultSpaceEditorSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // Space RW
  {
    QString component = p->factory->spaceRWFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultSpaceRW);

    ui->defaultSpaceRWSelect->setCurrentIndex(
        ui->defaultSpaceRWSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // PRNG
  {
    QString component = p->factory->prngFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultPRNG);

    ui->defaultPRNGSelect->setCurrentIndex(
        ui->defaultPRNGSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  // PRNG
  {
    QString component = p->factory->dprngFactoryDefault();
    QVariantHash ps = p->factory->componentFactoryDefaultParametersFor(
                          p->factory->DefaultDPRNG);

//    ui->defaultDPRNGSelect->setCurrentIndex(
//        ui->defaultDPRNGSelect->findText(component));

    p->defaultComponentSettings.insert(component,ps);
  }

  p->factoriesItem->setExpanded(true);
  for( int classIndex = 0;
       classIndex < p->factoriesItem->childCount();
       ++classIndex )
  {
    QTreeWidgetItem * classFactory = p->factoriesItem->child(classIndex);
    classFactory->setExpanded(true);
    for( int componentIndex = 0;
         componentIndex < classFactory->childCount();
         ++componentIndex )
    {
      QTreeWidgetItem * item = classFactory->child(componentIndex);
      QString key = item->text(0);

      QWidget * widget = p->table.value(item);
      SRPS::ComponentFactoryWidget * cfw = qobject_cast<SRPS::ComponentFactoryWidget*>(widget);

      if( cfw )
        cfw->componentFactoryWidgetSetParameters(
            p->factory->componentFactoryDefaultParametersFor(key));
    }
  }
}

void SettingsDialog::saveSettings()
{
  if ( !p->factory )
    return;

  // Curve
  {
    QString component = ui->defaultCurveSelect->currentText();
    p->factory->setCurveFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultCurve,
        p->defaultComponentSettings.value(component));
  }

  // Curve Editors
  {
    QString component = ui->defaultCurveEditorSelect->currentText();
    p->factory->setCurveEditorFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultCurveEditor,
        p->defaultComponentSettings.value(component));
  }

  // Curve RWs
  {
    QString component = ui->defaultCurveRWSelect->currentText();
    p->factory->setCurveRWFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultCurveRW,
        p->defaultComponentSettings.value(component));
  }

  // Curve Visualizers
  {
    QString component = ui->defaultCurveVisualizerSelect->currentText();
    p->factory->setCurveVisualizerFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultCurveVisualizer,
        p->defaultComponentSettings.value(component));
  }

  // Space
  {
    QString component = ui->defaultSpaceSelect->currentText();
    p->factory->setSpaceFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultSpace,
        p->defaultComponentSettings.value(component));
  }

  // SpaceRW
  {
    QString component = ui->defaultSpaceRWSelect->currentText();
    p->factory->setSpaceRWFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultSpaceRW,
        p->defaultComponentSettings.value(component));
  }

  // PRNG
  {
    QString component = ui->defaultPRNGSelect->currentText();
    p->factory->setPRNGFactoryDefault(component);
    p->factory->componentFactorySetDefaultParametersFor(
        p->factory->DefaultPRNG,
        p->defaultComponentSettings.value(component));
  }

  // DPRNG
  {
//    QString component = ui->defaultDPRNGSelect->currentText();
//    p->factory->setDPRNGFactoryDefault(component);
//    p->factory->componentFactorySetDefaultParametersFor(
//        p->factory->DefaultDPRNG,
//        p->defaultComponentSettings.value(component));
  }

  for( int classIndex = 0;
       classIndex < p->factoriesItem->childCount();
       ++classIndex )
  {
    QTreeWidgetItem * classFactory = p->factoriesItem->child(classIndex);
    for( int componentIndex = 0;
         componentIndex < classFactory->childCount();
         ++componentIndex )
    {
      QTreeWidgetItem * item = classFactory->child(componentIndex);
      QString key = item->text(0);

      QWidget * widget = p->table.value(item);
      SRPS::ComponentFactoryWidget * cfw = qobject_cast<SRPS::ComponentFactoryWidget*>(widget);

      if( cfw )
        p->factory->componentFactorySetDefaultParametersFor(key,
                                                            cfw->componentFactoryWidgetParameters());
    }
  }
}

void SettingsDialog::reject()
{
  loadSettings();
  QDialog::reject();
}

void SettingsDialog::accept()
{
  saveSettings();
  QDialog::accept();
}

void SettingsDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SettingsDialog::changeSettingsPage( QTreeWidgetItem * item )
{
    ui->settingsView->setCurrentWidget( p->table.value(item) );
}

void SettingsDialog::configureDefaultComponent( const QString & key )
{
  if ( !p->factory )
  {
    QMessageBox::information(this,
                             trUtf8("Component Configuration"),
                             trUtf8("Can't found factory for component: %1")
                              .arg(key));
    return;
  }

  SRPS::ComponentFactoryWidget * cfw = 0;
  QWidget * w = 0;

  if ( key.isEmpty() )
    return;

  w = p->factory->componentFactoryWidgetFor(key);
  if ( w )
    cfw = qobject_cast<SRPS::ComponentFactoryWidget*>(w);

  if ( !w )
  {
    QMessageBox::information(this,
                             trUtf8("Component Configuration"),
                             trUtf8("%1 component doesn't need to be configured.")
                              .arg(key));
    return;
  }

//  if ( !cfw )
//    QMessageBox::information(this,
//                             trUtf8("Component Configuration"),
//                             trUtf8("Widget for '%1' doesn't implements SRPS::ComponentFactoryWidget interface. Settings won't be saved.")
//                              .arg(key));

  QVariantHash parameters = p->defaultComponentSettings.value(key);
  if ( parameters.isEmpty() )
    parameters = p->factory->componentFactoryDefaultParametersFor(key);

  if ( cfw ) cfw->componentFactoryWidgetSetParameters(parameters);

  QDialog dlg;
  QDialogButtonBox * buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok|
                                                      QDialogButtonBox::Cancel);
  QVBoxLayout * layout = new QVBoxLayout;
  layout->addWidget(w);
  layout->addWidget(buttonBox);
  dlg.setLayout(layout);

  connect(buttonBox,SIGNAL(accepted()),
          &dlg,SLOT(accept()));
  connect(buttonBox,SIGNAL(rejected()),
          &dlg,SLOT(reject()));

  if ( dlg.exec() == QDialog::Accepted && cfw )
    p->defaultComponentSettings.insert(key,cfw->componentFactoryWidgetParameters());
}

void SettingsDialog::configureComponentSwitcher()
{
  QToolButton * button = qobject_cast<QToolButton*>(sender());

  if ( button == ui->defaultCurveConfigurationButton )
    configureDefaultComponent(ui->defaultCurveSelect->currentText());

  if ( button == ui->defaultCurveEditorConfigurationButton )
    configureDefaultComponent(ui->defaultCurveEditorSelect->currentText());

  if ( button == ui->defaultCurveRWConfigurationButton )
    configureDefaultComponent(ui->defaultCurveRWSelect->currentText());

  if ( button == ui->defaultCurveVisualizerConfigurationButton )
    configureDefaultComponent(ui->defaultCurveVisualizerSelect->currentText());

  if ( button == ui->defaultSpaceConfigurationButton )
    configureDefaultComponent(ui->defaultSpaceSelect->currentText());

  if ( button == ui->defaultSpaceEditorConfigurationButton )
    configureDefaultComponent(ui->defaultSpaceEditorSelect->currentText());

  if ( button == ui->defaultSpaceRWConfigurationButton )
    configureDefaultComponent(ui->defaultSpaceRWSelect->currentText());

  if ( button == ui->defaultPRNGConfigurationButton )
    configureDefaultComponent(ui->defaultPRNGSelect->currentText());

//  if ( button == ui->defaultDPRNGConfigurationButton )
//    configureDefaultComponent(ui->defaultDPRNGSelect->currentText());
}

} // namespace Implementation
} // namespace SRPS
