/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curveeditors_model.h"

namespace SRPS {
namespace Implementation {

class CurveTableModel::Private
{
  public:
  Private():curve(0){}
  QCurve * curve;
  Curve::MIDList variables;
  Curve::MIDList tuples;
};

CurveTableModel::CurveTableModel( QObject * parent )
  : QAbstractTableModel(parent),
    p( new Private )
{
}

CurveTableModel::~CurveTableModel()
{
  delete p;
}

QCurve * CurveTableModel::curve() const
{
  return p->curve;
}

void CurveTableModel::setCurve( QCurve * s)
{
  if ( p->curve == s )
    return;
  if ( p->curve )
    disconnect( p->curve->componentAsQObject(),SIGNAL(destroyed()),this,SLOT(curveDestroyed()));
  p->curve = s;
  p->variables.clear();
  p->tuples.clear();
  if ( p->curve )
  {
    p->variables = p->curve->variableMIDs(Curve::CreationOrder );
    p->tuples = p->curve->tupleMIDs(Curve::CreationOrder);
    connect(p->curve->componentAsQObject(),SIGNAL(destroyed()),this,SLOT(curveDestroyed()));
  }
  reset();
}

Curve::VariableList CurveTableModel::variables() const
{
  return p->curve->variables();
}

Curve::TupleList CurveTableModel::tuples() const
{
  return p->curve->tuples();
}

int CurveTableModel::rowCount(const QModelIndex &/*parent*/) const
{
  if ( !p->curve )
    return 0;
  return p->tuples.count();
}

int CurveTableModel::columnCount(const QModelIndex &/*parent*/) const
{
  if ( !p->curve )
    return 0;
  return p->variables.count();
}

QVariant CurveTableModel::data(const QModelIndex &index, int role) const
{
  if( role != Qt::DisplayRole )
    return QVariant();

  if( index.row() >= rowCount() || index.column() >= columnCount() )
    return QVariant();

  return p->curve->tupleValue(p->tuples.at( index.row()),
                              p->variables.at(index.column()));
}

QVariant CurveTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if( !p->curve )
    return QVariant();
  if ( role != Qt::DisplayRole )
    return QVariant();

  static const QString HeaderTemplate = QString("%1: %2");
  QString name = HeaderTemplate.arg(section);

  if ( orientation == Qt::Horizontal )
  {
    if ( section >= columnCount() )
      return QVariant();

    if ( role == Qt::ToolTipRole )
    {
      QString d = p->curve->variableProperty(p->variables.at(section),"srps.description").toString();
      if ( !d.isEmpty() )
        return d;
    }

    return name.arg(p->curve->variableProperty(p->variables.at(section),"srps.name").toString());
  }
  else
  {
    if ( section >= rowCount() )
      return QVariant();

    if ( role == Qt::ToolTipRole )
    {
      QString d = p->curve->tupleProperty(p->tuples.at(section),"srps.description").toString();
      if ( !d.isEmpty() )
        return d;
    }

    return name.arg(p->curve->tupleProperty(p->tuples.at(section),"srps.name").toString());
  }
}

Qt::ItemFlags CurveTableModel::flags( const QModelIndex & index ) const
{
  return QAbstractTableModel::flags(index)|Qt::ItemIsEditable;
}

bool CurveTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!p->curve)
    return false;
  if ( !index.isValid())
    return false;
  if ( role != Qt::EditRole )
    return false;
  if ( index.row() >= rowCount() || index.column() >= columnCount())
    return false;
  if ( !p->curve->tupleSetValue(p->tuples.at(index.row()),
                                p->variables.at(index.column()),
                                value) )
    return false;
  emit dataChanged( index,index );
  return true;
}

bool CurveTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
  if( !p->curve)
    return false;
  int final = row+count;
  if( row < 0 || row >= rowCount() || final > rowCount() )
    return false;
  beginRemoveRows(parent,row,final-1);
  while( count-- )
  {
    p->curve->deleteTuple( p->tuples.takeAt(row));
  }
  endRemoveRows();
  return true;
}

bool CurveTableModel::removeColumns(int column, int count, const QModelIndex &parent)
{
  if( !p->curve)
    return false;
  int final = column+count;
  if( column < 0 || column>= columnCount() || final > columnCount() )
    return false;
  beginRemoveColumns(parent,column,final-1);
  while( count-- )
  {
    p->curve->deleteVariable( p->variables.takeAt( column ) );
  }
  endRemoveColumns();
  return true;
}

SRPS::Curve::Variable CurveTableModel::indexToVariable( const QModelIndex & index ) const
{
  if ( !p->curve ) return Curve::Variable();

  return p->curve->toVariable( p->variables.at( index.column() ));
}

SRPS::Curve::Tuple CurveTableModel::indexToTuple( const QModelIndex & index ) const
{
  if ( !p->curve ) return Curve::Tuple();

  return p->curve->toTuple( p->tuples.at( index.row() ));
}

void CurveTableModel::updateVariableHeader( const SRPS::Curve::Variable & variable )
{
  if ( !variable.isValid() ) return;

  int index = p->variables.indexOf(variable.mid());
  emit headerDataChanged(Qt::Horizontal,index,index);
}

void CurveTableModel::updateTupleHeader( const SRPS::Curve::Tuple & tuple )
{
  if ( !tuple.isValid() ) return;

  int index = p->tuples.indexOf(tuple.mid());
  emit headerDataChanged(Qt::Vertical,index,index);
}

bool CurveTableModel::addTuple()
{
  if ( !p->curve )
    return false;
  Curve::MemoryIdentifier t = p->curve->createTupleMID();
  int r = p->tuples.count();
  beginInsertRows(QModelIndex(),r,r);
  p->tuples.append(t);
  endInsertRows();
  return true;
}

bool CurveTableModel::appendVariable()
{
  if ( !p->curve )
    return false;
  Curve::MemoryIdentifier v = p->curve->createVariableMID();
  int c = p->variables.count();
  beginInsertColumns(QModelIndex(),c,c);
  p->variables.append(v);
  endInsertColumns();
  return true;
}

void CurveTableModel::curveDestroyed()
{
  //disconnect( p->curve->componentAsQObject(), SIGNAL(destroyed()),this,SLOT(curveDestroyed()));
  p->curve = 0;
  p->variables.clear();
  p->tuples.clear();
  reset();
}

} // namespace Plugin
} // namespace SRPS
