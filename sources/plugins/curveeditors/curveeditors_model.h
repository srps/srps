/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_CURVETABLEMODEL_H
#define SRPS_PLUGIN_CURVETABLEMODEL_H

#include <SRPS/Curve>
#include <SRPS/QCurve>
#include <QtCore/QAbstractTableModel>

namespace SRPS {
namespace Implementation {

class CurveTableModel : public QAbstractTableModel
{
  Q_OBJECT
  Q_DISABLE_COPY(CurveTableModel)

  class Private;
  Private * p;

  public:

    explicit CurveTableModel( QObject * parent );
    virtual ~CurveTableModel();

    QCurve * curve() const;
    void setCurve( QCurve * );

    Curve::VariableList variables() const;
    Curve::TupleList tuples() const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    Qt::ItemFlags flags( const QModelIndex & index ) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex());

    SRPS::Curve::Variable indexToVariable( const QModelIndex & index ) const;
    SRPS::Curve::Tuple indexToTuple( const QModelIndex & index ) const;

    void updateVariableHeader( const SRPS::Curve::Variable & variable );
    void updateTupleHeader( const SRPS::Curve::Tuple & tuple );

  public slots:

    bool addTuple();
    bool appendVariable();

  private slots:

    void curveDestroyed();
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_CURVETABLEMODEL_H
