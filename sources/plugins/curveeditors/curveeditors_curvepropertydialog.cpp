/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curveeditors_curvepropertydialog.h"
#include "ui_curveeditors_curvepropertydialog.h"

#include <QtGui/QPushButton>

namespace SRPS {
namespace Implementation {

CurvePropertyDialog::CurvePropertyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CurvePropertyDialog)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

CurvePropertyDialog::~CurvePropertyDialog()
{
    delete ui;
}

QString CurvePropertyDialog::name() const
{
  return ui->nameEdit->text();
}

void CurvePropertyDialog::setName( const QString & name )
{
  ui->nameEdit->setText(name);
}

QString CurvePropertyDialog::value() const
{
  return ui->valueText->toPlainText();
}

void CurvePropertyDialog::setValue( const QString & value )
{
  ui->valueText->setPlainText(value);
}

void CurvePropertyDialog::setNewMode()
{
  ui->nameEdit->setReadOnly(false);
  ui->valueText->setReadOnly(false);
}

void CurvePropertyDialog::setEditMode()
{
  ui->nameEdit->setReadOnly(true);
  ui->valueText->setReadOnly(false);
}

void CurvePropertyDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace Implementation
} // namespace SRPS

void SRPS::Implementation::CurvePropertyDialog::on_nameEdit_textChanged(QString t)
{
  ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!t.isEmpty());
}
