/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_CURVEEDITOR_H
#define SRPS_PLUGIN_CURVEEDITOR_H

#include <SRPS/Curve>
#include <SRPS/CurveEditor>
#include <QtGui/QMainWindow>

#include <QtCore/QModelIndex>

class QTableWidgetItem;

namespace SRPS {
namespace Implementation {

namespace Ui {
  class CurveEditor;
}

class CurveEditor : public QMainWindow, public SRPS::CurveEditor
{
  Q_OBJECT
  Q_DISABLE_COPY(CurveEditor)
  Q_INTERFACES(SRPS::CurveEditor SRPS::Component)
  class Private;
  Private * p;
  Ui::CurveEditor * ui;

public:

  explicit CurveEditor( QWidget * parent = 0 );
  virtual ~CurveEditor();

  static const QString StaticClassName;


  QObject * componentAsQObject();
  const QObject * componentAsConstQObject() const;
  QString componentClassName() const;


  SRPS::QCurve * curveEdited() const;
  bool curveEdit( SRPS::QCurve * curve );

protected:

  void showEvent(QShowEvent *);

private slots:

  void on_curvePropertyTable_itemDoubleClicked(QTableWidgetItem* item);
  void on_curvePropertyTable_doubleClicked(QModelIndex index);
  void on_actionCurveRemoveProperty_triggered();
  void on_actionCurveRemoveProperties_triggered();
  void on_actionCurveAddProperty_triggered();
  void on_curveDescriptionEdit_textChanged(QString );
  void on_curveNameEdit_textChanged(QString );

  void on_actionTupleRemoveProperty_triggered();
  void on_actionTupleRemoveProperties_triggered();
  void on_actionTupleAddProperty_triggered();
  void on_tupleDescriptionEdit_textChanged(QString );
  void on_tupleNameEdit_textChanged(QString );

  void on_actionVariableRemoveProperty_triggered();
  void on_actionVariableRemoveProperties_triggered();
  void on_actionVariableAddProperty_triggered();
  void on_variableDescriptionEdit_textChanged(QString );
  void on_variableNameEdit_textChanged(QString );
  void on_tableEditor_clicked(QModelIndex index);
  void on_actionDeleteVariables_triggered();
  void on_actionNewVariable_triggered();
  void on_actionDeleteTuples_triggered();
  void on_actionNewTuple_triggered();

  void updateCurveDock();

  void setCurrentTuple( const SRPS::Curve::Tuple & );
  void setCurrentVariable( const SRPS::Curve::Variable & );

  void variableDoubleClicked( int );
  void tupleDoubleClicked( int );

  void headerDataChanged( Qt::Orientation orientation, int first );

  void curveChanged();
};

} // namespace Plugin
} // namespace SRPS

#endif // SRPS_PLUGIN_PLUGIN_CURVEEDITOR_H
