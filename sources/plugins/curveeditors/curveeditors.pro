include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )
TARGET = $${SRPS_NAME}CurveEditors
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$SRPS_PLUGINS_PATH
SOURCES += curveeditors_plugin.cpp \
    curveeditors_model.cpp \
    curveeditors_editor.cpp \
    curveeditors_curvepropertydialog.cpp
HEADERS += curveeditors_plugin.h \
    curveeditors_model.h \
    curveeditors_editor.h \
    curveeditors_curvepropertydialog.h
FORMS += curveeditors_editor.ui \
    curveeditors_curvepropertydialog.ui

RESOURCES += \
    resources.qrc
