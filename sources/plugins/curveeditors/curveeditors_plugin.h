/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

  #ifndef SRPS_CURVEEDITORS_PLUGIN_H
#define SRPS_CURVEEDITORS_PLUGIN_H

#include <QtCore/QObject>
#include <SRPS/CurveEditorFactory>

namespace SRPS {
namespace Implementation {

class CurveEditorFactory : public QObject, public SRPS::CurveEditorFactory
{
    Q_OBJECT
    Q_INTERFACES(SRPS::CurveEditorFactory)

public:
    explicit CurveEditorFactory(QObject *parent = 0);
    virtual ~CurveEditorFactory();

    // CurveEditorFactory
    QStringList curveEditorFactoryKeys() const;
    SRPS::CurveEditor * curveEditorFactoryCreate (const QString &curveEditor,
                                                  const QVariantHash &parameters=
                                                    QVariantHash());
};

} // namespace Implementation
} // namespace SRPS

#endif // SRPS_CURVEEDITORS_PLUGIN_H
