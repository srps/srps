/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curveeditors_editor.h"
#include "ui_curveeditors_editor.h"
#include "curveeditors_model.h"
#include "curveeditors_curvepropertydialog.h"

#include <SRPS/Curve>

#include <QtCore/QVector>
#include <QtGui/QShowEvent>
#include <QtGui/QInputDialog>

namespace SRPS {
namespace Implementation {

class CurveEditor::Private
{
  public:
  Private():curve(0),model(0){}
  SRPS::QCurve * curve;
  CurveTableModel * model;

  SRPS::Curve::Variable currentVariable;
  SRPS::Curve::Tuple currentTuple;
  QPointer<QLabel> sl;
};

CurveEditor::CurveEditor( QWidget * parent )
  : QMainWindow(parent),
    SRPS::CurveEditor(),
    p( new Private ),
    ui( new Ui::CurveEditor )
{
  ui->setupUi(this);
  p->sl = new QLabel;

  p->model = new CurveTableModel(this);
  ui->tableEditor->setModel( p->model );

  connect(p->model,SIGNAL(dataChanged(QModelIndex,QModelIndex)),this,SLOT(curveChanged()));
  connect(p->model,SIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(curveChanged()));
  connect(p->model,SIGNAL(columnsInserted(QModelIndex,int,int)),this,SLOT(curveChanged()));
  connect(p->model,SIGNAL(rowsRemoved(QModelIndex,int,int)),this,SLOT(curveChanged()));
  connect(p->model,SIGNAL(columnsRemoved(QModelIndex,int,int)),this,SLOT(curveChanged()));

  connect(p->model,SIGNAL(headerDataChanged(Qt::Orientation,int,int)),
          this,SLOT(headerDataChanged(Qt::Orientation,int)));

  setCurrentTuple(SRPS::Curve::Tuple());
  setCurrentVariable(SRPS::Curve::Variable());

  ui->statusbar->addPermanentWidget(p->sl);
}

CurveEditor::~CurveEditor()
{
  delete ui;
  delete p;
}

const QString CurveEditor::StaticClassName = QLatin1String("srps.curve.editor");

QObject * CurveEditor::componentAsQObject()
{
  return this;
}

const QObject * CurveEditor::componentAsConstQObject() const
{
  return this;
}

QString CurveEditor::componentClassName() const
{
  return metaObject()->className();
}

SRPS::QCurve * CurveEditor::curveEdited() const
{
  return p->curve;
}

bool CurveEditor::curveEdit(SRPS::QCurve * s )
{
  if ( p->curve == s )
    return true;
  p->curve = s;
  p->model->setCurve( s );
  curveChanged();
  updateCurveDock();
  return true;
}

void CurveEditor::showEvent(QShowEvent * r)
{
  ui->curveDock->show();
  ui->variableDock->show();
  ui->tupleDock->show();
  QMainWindow::showEvent(r);
}

void CurveEditor::on_actionDeleteVariables_triggered()
{
  QSet<int> colums;
  int min = p->model->columnCount() + 1;
  QItemSelectionModel * selection = ui->tableEditor->selectionModel();
  foreach( QModelIndex index, selection->selectedIndexes() )
  {
    colums.insert( index.column() );
    if ( index.column() < min )
      min = index.column();
  }
  p->model->removeColumns(min,colums.count());
}

void CurveEditor::on_actionNewVariable_triggered()
{
  p->model->appendVariable();
}

void CurveEditor::on_actionDeleteTuples_triggered()
{
  QSet<int> rows;
  int min = p->model->rowCount()+1;
  QItemSelectionModel * selection = ui->tableEditor->selectionModel();
  foreach( QModelIndex index, selection->selectedIndexes() )
  {
    rows.insert( index.row() );
    if ( index.row() < min )
      min = index.row();
  }
  p->model->removeRows(min,rows.count());
}

void CurveEditor::on_actionNewTuple_triggered()
{
  p->model->addTuple();
}

void CurveEditor::updateCurveDock()
{
  while( ui->curvePropertyTable->rowCount() )
    ui->curvePropertyTable->removeRow(0);

  if( p->curve )
  {
    ui->curveNameEdit->setEnabled(true);
    ui->curveNameEdit->setText(p->curve->componentProperty("srps.name").toString());

    ui->curveDescriptionEdit->setEnabled(true);
    ui->curveDescriptionEdit->setText(p->curve->componentProperty("srps.description").toString());

    ui->curvePropertyTable->setEnabled(true);
    foreach( QString pname, p->curve->componentPropertyNames() )
    {
      if ( pname == QLatin1String("srps.name") ||
           pname == QLatin1String("srps.description") )
        continue;

      QTableWidgetItem * pitem = new QTableWidgetItem;
      pitem->setText(pname);
      pitem->setFlags( Qt::ItemFlags(pitem->flags() & ~Qt::ItemIsEditable) );

      QTableWidgetItem * vitem = new QTableWidgetItem;
      vitem->setText(p->curve->componentProperty(pname).toString());
      vitem->setFlags( Qt::ItemFlags(vitem->flags() & ~Qt::ItemIsEditable) );

      ui->curvePropertyTable->insertRow(0);
      ui->curvePropertyTable->setItem(0,0,pitem);
      ui->curvePropertyTable->setItem(0,1,vitem);
    }
  }
  else
  {
    ui->curveNameEdit->setEnabled(false);
    ui->curveNameEdit->setText(tr("No name."));

    ui->curveDescriptionEdit->setEnabled(false);
    ui->curveDescriptionEdit->setText(tr("No description."));

    ui->curvePropertyTable->setEnabled(false);
  }
}

void CurveEditor::setCurrentTuple( const SRPS::Curve::Tuple & t )
{
  p->currentTuple = t;

  while( ui->tuplePropertyTable->rowCount() )
    ui->tuplePropertyTable->removeRow(0);

  if ( p->currentTuple.isValid() )
  {
    ui->tupleNameEdit->setEnabled(true);
    ui->tupleNameEdit->setText(p->currentTuple.property("srps.name").toString());

    ui->tupleDescriptionEdit->setEnabled(true);
    ui->tupleDescriptionEdit->setText(p->currentTuple.property("srps.description").toString());

    ui->tuplePropertyTable->setEnabled(true);
    foreach( QString pname, p->currentTuple.propertyNames() )
    {
      if ( pname == QLatin1String("srps.name") ||
           pname == QLatin1String("srps.description") )
        continue;

      QTableWidgetItem * pitem = new QTableWidgetItem;
      pitem->setText(pname);
      pitem->setFlags( Qt::ItemFlags(pitem->flags() & ~Qt::ItemIsEditable) );

      QTableWidgetItem * vitem = new QTableWidgetItem;
      vitem->setText(p->currentTuple.property(pname).toString());
      vitem->setFlags( Qt::ItemFlags(vitem->flags() & ~Qt::ItemIsEditable) );

      ui->tuplePropertyTable->insertRow(0);
      ui->tuplePropertyTable->setItem(0,0,pitem);
      ui->tuplePropertyTable->setItem(0,1,vitem);
    }
  }
  else
  {
    ui->tupleNameEdit->setEnabled(false);
    ui->tupleNameEdit->setText(tr("No name."));

    ui->tupleDescriptionEdit->setEnabled(false);
    ui->tupleDescriptionEdit->setText(tr("No description."));

    ui->tuplePropertyTable->setEnabled(false);
  }
}

void CurveEditor::setCurrentVariable( const SRPS::Curve::Variable & v )
{
  p->currentVariable = v;

  while( ui->variablePropertyTable->rowCount() )
    ui->variablePropertyTable->removeRow(0);

  if ( p->currentVariable.isValid() )
  {
    ui->variableNameEdit->setEnabled(true);
    ui->variableNameEdit->setText(p->currentVariable.property("srps.name").toString());

    ui->variableDescriptionEdit->setEnabled(true);
    ui->variableDescriptionEdit->setText(p->currentVariable.property("srps.description").toString());

    ui->variablePropertyTable->setEnabled(true);
    foreach( QString pname, p->currentVariable.propertyNames() )
    {
      if ( pname == QLatin1String("srps.name") ||
           pname == QLatin1String("srps.description") )
        continue;

      QTableWidgetItem * pitem = new QTableWidgetItem;
      pitem->setText(pname);
      pitem->setFlags( Qt::ItemFlags(pitem->flags() & ~Qt::ItemIsEditable) );

      QTableWidgetItem * vitem = new QTableWidgetItem;
      vitem->setText(p->currentVariable.property(pname).toString());
      vitem->setFlags( Qt::ItemFlags(vitem->flags() & ~Qt::ItemIsEditable) );

      ui->variablePropertyTable->insertRow(0);
      ui->variablePropertyTable->setItem(0,0,pitem);
      ui->variablePropertyTable->setItem(0,1,vitem);
    }
  }
  else
  {
    ui->variableNameEdit->setEnabled(false);
    ui->variableNameEdit->setText(tr("No name."));

    ui->variableDescriptionEdit->setEnabled(false);
    ui->variableDescriptionEdit->setText(tr("No description."));

    ui->variablePropertyTable->setEnabled(false);
  }
}

void CurveEditor::variableDoubleClicked( int li )
{
  QString old = p->model->headerData(li,Qt::Horizontal,Qt::DisplayRole).toString();
  QString name = QInputDialog::getText(this,tr("Variable name"),tr("Change variable name:"),QLineEdit::Normal,old);
  p->model->setHeaderData(li,Qt::Horizontal,name,Qt::EditRole);
}

void CurveEditor::tupleDoubleClicked( int li )
{
  QString old = p->model->headerData(li,Qt::Vertical,Qt::DisplayRole).toString();
  QString name = QInputDialog::getText(this,tr("Variable name"),tr("Change variable name:"),QLineEdit::Normal,old);
  p->model->setHeaderData(li,Qt::Vertical,name,Qt::EditRole);
}

void CurveEditor::headerDataChanged( Qt::Orientation orientation, int first )
{
  if ( orientation == Qt::Horizontal )
    ui->tableEditor->resizeColumnToContents(first);
  else
    ui->tableEditor->resizeRowToContents(first);
}

void CurveEditor::curveChanged()
{
  if (!p->curve)
  {
    statusBar()->showMessage(
        tr("%n Variable(s). ","",0)+
        tr("%n Tuple(s). ","",0));

    setCurrentTuple(SRPS::Curve::Tuple());
    setCurrentVariable(SRPS::Curve::Variable());
    return;
  }
  int vc = p->curve->variableCount();
  int tc = p->curve->tupleCount();

  if ( !tc ) setCurrentTuple(SRPS::Curve::Tuple());
  if ( !vc ) setCurrentVariable(SRPS::Curve::Variable());

  statusBar()->showMessage(
      tr("%n Variable(s). ","",vc)+
      tr("%n Tuple(s). ","",tc));
  ui->tableEditor->resizeColumnsToContents();
  ui->tableEditor->resizeRowsToContents();
}

void SRPS::Implementation::CurveEditor::on_curvePropertyTable_itemDoubleClicked(QTableWidgetItem* item)
{
  if (!item ) return;

  int r = item->row();

  CurvePropertyDialog dlg;
  dlg.setEditMode();

  dlg.setName(ui->curvePropertyTable->item(r,0)->text());
  dlg.setValue(ui->curvePropertyTable->item(r,1)->text());

  if ( dlg.exec() )
  {
    p->curve->componentSetProperty(dlg.name(),dlg.value());
    updateCurveDock();
  }
}

} // namespace Plugin
} // namespace SRPS

void SRPS::Implementation::CurveEditor::on_tableEditor_clicked(QModelIndex index)
{
  setCurrentTuple( p->model->indexToTuple(index) );
  setCurrentVariable( p->model->indexToVariable(index) );
}

//

void SRPS::Implementation::CurveEditor::on_curveNameEdit_textChanged(QString t )
{
  if ( !p->curve ) return;

    p->curve->componentSetProperty("srps.name", t);
}

void SRPS::Implementation::CurveEditor::on_curveDescriptionEdit_textChanged(QString t)
{
  if ( !p->curve ) return;

  p->curve->componentSetProperty("srps.description", t);
}

void SRPS::Implementation::CurveEditor::on_actionCurveAddProperty_triggered()
{
  if ( !p->curve ) return;

  SRPS::Implementation::CurvePropertyDialog dlg;
  dlg.setNewMode();
  if ( dlg.exec() == dlg.Accepted )
  {
    QString name = dlg.name();
    QString value = dlg.value();

    if ( name == "srps.name" )
    {
      ui->curveNameEdit->setText(value);
    }
    else if ( name == "srps.description" )
    {
      ui->curveDescriptionEdit->setText(value);
    }
    else
    {
      p->curve->componentSetProperty( name, value );
      updateCurveDock();
    }
  }
}

void SRPS::Implementation::CurveEditor::on_actionCurveRemoveProperties_triggered()
{
  if ( !p->curve ) return;

  foreach( QString name, p->curve->componentPropertyNames() )
  {
    if ( name == "srps.name" || name == "srps.description" ) continue;

    p->curve->componentSetProperty(name, QVariant());
  }
  updateCurveDock();
}

void SRPS::Implementation::CurveEditor::on_actionCurveRemoveProperty_triggered()
{
  if ( !p->curve ) return;

  int cr = ui->curvePropertyTable->currentRow();
  QTableWidgetItem * item = ui->curvePropertyTable->item(cr,0);
  if ( !item ) return;

  if ( !item->isSelected() ) return;

  QString name = item->text();
  p->curve->componentSetProperty(name,QVariant());
  updateCurveDock();
}

//

void SRPS::Implementation::CurveEditor::on_tupleNameEdit_textChanged(QString t )
{
  if ( !p->currentTuple.isValid() ) return;

    p->currentTuple.setProperty("srps.name", t);
    p->model->updateTupleHeader(p->currentTuple);
}

void SRPS::Implementation::CurveEditor::on_tupleDescriptionEdit_textChanged(QString t)
{
  if ( !p->currentTuple.isValid() ) return;

  p->currentTuple.setProperty("srps.description", t);
  p->model->updateTupleHeader(p->currentTuple);
}

void SRPS::Implementation::CurveEditor::on_actionTupleAddProperty_triggered()
{
  if ( !p->currentTuple.isValid() ) return;

  SRPS::Implementation::CurvePropertyDialog dlg;
  dlg.setNewMode();
  if ( dlg.exec() == dlg.Accepted )
  {
    QString name = dlg.name();
    QString value = dlg.value();

    if ( name == "srps.name" )
    {
      ui->tupleNameEdit->setText(value);
    }
    else if ( name == "srps.description" )
    {
      ui->tupleDescriptionEdit->setText(value);
    }
    else
    {
      p->currentTuple.setProperty( name, value );
      setCurrentTuple(p->currentTuple);
    }
  }
}

void SRPS::Implementation::CurveEditor::on_actionTupleRemoveProperties_triggered()
{
  foreach( QString name, p->currentTuple.propertyNames() )
  {
    if ( name == "srps.name" || name == "srps.description" ) continue;

    p->currentTuple.setProperty(name, QVariant());
  }
  setCurrentTuple(p->currentTuple);
}

void SRPS::Implementation::CurveEditor::on_actionTupleRemoveProperty_triggered()
{
  int cr = ui->tuplePropertyTable->currentRow();
  QTableWidgetItem * item = ui->tuplePropertyTable->item(cr,0);
  if ( !item ) return;

  if ( !item->isSelected() ) return;

  QString name = item->text();
  p->currentTuple.setProperty(name,QVariant());
  setCurrentTuple(p->currentTuple);
}

//

void SRPS::Implementation::CurveEditor::on_variableNameEdit_textChanged(QString t )
{
  if ( !p->currentVariable.isValid() ) return;

    p->currentVariable.setProperty("srps.name", t);
    p->model->updateVariableHeader(p->currentVariable);
}

void SRPS::Implementation::CurveEditor::on_variableDescriptionEdit_textChanged(QString t)
{
  if ( !p->currentVariable.isValid() ) return;

  p->currentVariable.setProperty("srps.description", t);
  p->model->updateVariableHeader(p->currentVariable);
}

void SRPS::Implementation::CurveEditor::on_actionVariableAddProperty_triggered()
{
  if ( !p->currentVariable.isValid() ) return;

  SRPS::Implementation::CurvePropertyDialog dlg;
  dlg.setNewMode();
  if ( dlg.exec() == dlg.Accepted )
  {
    QString name = dlg.name();
    QString value = dlg.value();

    if ( name == "srps.name" )
    {
      ui->variableNameEdit->setText(value);
    }
    else if ( name == "srps.description" )
    {
      ui->variableDescriptionEdit->setText(value);
    }
    else
    {
      p->currentVariable.setProperty( name, value );
      setCurrentVariable(p->currentVariable);
    }
  }
}

void SRPS::Implementation::CurveEditor::on_actionVariableRemoveProperties_triggered()
{
  foreach( QString name, p->currentVariable.propertyNames() )
  {
    if ( name == "srps.name" || name == "srps.description" ) continue;

    p->currentVariable.setProperty(name, QVariant());
  }
  setCurrentVariable(p->currentVariable);
}

void SRPS::Implementation::CurveEditor::on_actionVariableRemoveProperty_triggered()
{
  int cr = ui->variablePropertyTable->currentRow();
  QTableWidgetItem * item = ui->variablePropertyTable->item(cr,0);
  if ( !item ) return;

  if ( !item->isSelected() ) return;

  QString name = item->text();
  p->currentVariable.setProperty(name,QVariant());
  setCurrentVariable(p->currentVariable);
}

void SRPS::Implementation::CurveEditor::on_curvePropertyTable_doubleClicked(QModelIndex index)
{

}


