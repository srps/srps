/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include <SRPS/Distribution>
#include <SRPS/DistributionFactory>
#include <SRPS/PRNG>
#include <SRPS/PRNGFactory>

class Test_Distribution : public QObject
{
    Q_OBJECT

public:
    Test_Distribution();

    static const int SCOUNT = 100000000;

private Q_SLOTS:

    void initTestCase();
    void cleanupTestCase();

    void speedDistributions();

private:

    QList<QPluginLoader*> loaders;
    QList<SRPS::DistributionFactory*> factories;
    QList<SRPS::PRNGFactory*> prngFactories;
};

Test_Distribution::Test_Distribution()
{

}

void Test_Distribution::initTestCase()
{
    qDebug()<< Q_FUNC_INFO;
    QDir pluginDir( QLibraryInfo::location(QLibraryInfo::PluginsPath)+"/srps");
    foreach( QString filepath, pluginDir.entryList(QStringList("*"),QDir::Files|QDir::Readable|QDir::NoDotAndDotDot))
    {
        filepath = pluginDir.absoluteFilePath(filepath);
        QPluginLoader * pl = new QPluginLoader(filepath);
        if ( !pl->load() )
        {
            qDebug() << pl->errorString();
            delete pl;
            continue;
        }
        QObject * obj = pl->instance();
        if ( !obj )
        {
            qDebug() << filepath << "no root";
            pl->unload();
            delete pl;
            continue;
        }

        SRPS::DistributionFactory * factory = qobject_cast<SRPS::DistributionFactory*>(obj);
        if(factory)
        {
            // has factory
            qDebug() << filepath << "SRPS::Distributions" << factory->distributionClasses();
            loaders << pl;
            factories << factory;
        }
        else if( SRPS::PRNGFactory * prngf = qobject_cast<SRPS::PRNGFactory*>(obj) )
        {
            // has factory
            qDebug() << filepath << "SRPS::PRNGs" << prngf->prngClasses();
            if ( !loaders.contains(pl) )
                loaders << pl;
            prngFactories << prngf;
        }
        else
        {
            qDebug() << filepath << "no SRPS::DistributionFactory or SRPS::PRNGFactories";
            pl->unload();
            delete pl;
        }
    }

    qDebug() << "factory count" << factories.count();
    qDebug() << "prng factory count" << prngFactories.count();
}

void Test_Distribution::cleanupTestCase()
{
    qDebug() << Q_FUNC_INFO;
    foreach( QPluginLoader * pl, loaders)
    {
        pl->unload();
        delete pl;
    }
    factories.clear();
    loaders.clear();
    prngFactories.clear();
}

void Test_Distribution::speedDistributions()
{
    QVERIFY(factories.count());
    foreach( SRPS::DistributionFactory * factory, factories )
    {
        foreach( QString key, factory->distributionClasses() )
        {
            qDebug() << "testing:" << key;
            SRPS::Distribution * distribution = factory->createDistribution(key);
            QVERIFY(distribution);
            QVERIFY(key == distribution->thisClassName());

            foreach( SRPS::PRNGFactory * prngFactory, prngFactories )
            {
                foreach( QString pkey, prngFactory->prngClasses() )
                {
                    SRPS::PRNG * prng = prngFactory->createPRNG(pkey);
                    QVERIFY(prng);
                    QVERIFY(pkey == prng->thisClassName());
                    qDebug() << "with:" << pkey;
//                    qDebug() << "src seed:" << prng->seed().toBase64();
                    prng->setStream(0);
//                    qDebug() << "stream 0 seed:" << prng->seed().toBase64();

                    qDebug() << "speed for: " << SCOUNT*4 << "samples";
                    distribution->setPRNG(prng);
                    QBENCHMARK {

                        double r;
                        for( int i = SCOUNT; i > 0; --i )
                        {
                            r = prng->nextCC();
                            r = prng->nextOC();
                            r = prng->nextCO();
                            r = prng->nextOO();
                        }
                    }
                    distribution->setPRNG(0);
                    delete prng;
                }
            }
            delete distribution;
        }
    }
}

QTEST_MAIN(Test_Distribution);

#include "tst_distribution.moc"
