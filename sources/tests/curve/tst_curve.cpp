/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include <SRPS/Curve>
#include <SRPS/CurveFactory>

class Test_Curve : public QObject
{
    Q_OBJECT

public:
    Test_Curve();

    static const int PCOUNT = 1024;
    static const int VCOUNT = 1024;
    static const int TCOUNT = 1024;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void readCurveDefaultReferences();
    void readCurveDefaultMIDs();
    void writeCurveReferences();
    void writeCurveMIDs();

private:

    QList<QPluginLoader*> loaders;
    QList<SRPS::CurveFactory*> factories;
};

Test_Curve::Test_Curve()
{

}

void Test_Curve::initTestCase()
{
    qDebug()<< Q_FUNC_INFO;
    QDir pluginDir( QLibraryInfo::location(QLibraryInfo::PluginsPath)+"/srps");
    foreach( QString filepath, pluginDir.entryList(QStringList("*"),QDir::Files|QDir::Readable|QDir::NoDotAndDotDot))
    {
        filepath = pluginDir.absoluteFilePath(filepath);
        QPluginLoader * pl = new QPluginLoader(filepath);
        if ( !pl->load() )
        {
            qDebug() << pl->errorString();
            delete pl;
            continue;
        }
        QObject * obj = pl->instance();
        if ( !obj )
        {
            qDebug() << filepath << "no root";
            pl->unload();
            delete pl;
            continue;
        }
        SRPS::CurveFactory * factory = qobject_cast<SRPS::CurveFactory*>(obj);
        if(!factory)
        {
            qDebug() << filepath << "no SRPS::CurveFactory";
            pl->unload();
            delete pl;
            continue;
        }

        // has factory
        qDebug() << filepath << "SRPS::Curves" << factory->curveFactoryKeys();
        loaders << pl;
        factories << factory;
    }

    qDebug() << "factory count" << factories.count();
}

void Test_Curve::cleanupTestCase()
{
    qDebug() << Q_FUNC_INFO;
    foreach( QPluginLoader * pl, loaders)
    {
        pl->unload();
        delete pl;
    }
    factories.clear();
    loaders.clear();
}

void Test_Curve::readCurveDefaultReferences()
{
    QVERIFY(factories.count());
    foreach( SRPS::CurveFactory * factory, factories )
    {
        foreach( QString key, factory->curveFactoryKeys() )
        {
            qDebug() << "testing:" << key;
            SRPS::Curve * curve = factory->curveFactoryCreate(key);
            QVERIFY(curve);
            QVERIFY(key == curve->componentClassName());

            QBENCHMARK {
                // properties
                QVERIFY(curve->componentPropertyCount() == curve->componentPropertyNames().count());
                foreach( QString pname, curve->componentPropertyNames() )
                    qDebug() << "has property:" << pname << "=>" << curve->componentProperty(pname);

                // variables
                QVERIFY(curve->variableCount() == curve->variables().count());
                foreach( SRPS::Curve::Variable var, curve->variables() )
                {
                    QVERIFY(var.isValid());
                    qDebug() << "found variable" << var.mid();
                    QVERIFY(var.propertyCount() == var.propertyNames().count());
                    foreach( QString pname, var.propertyNames() )
                        qDebug() << "has property:" << pname << "=>" << var.property(pname);
                }

                // tuples
                QVERIFY(curve->tupleCount() == curve->tuples().count());
                foreach( SRPS::Curve::Tuple tuple, curve->tuples() )
                {
                    QVERIFY(tuple.isValid());
                    qDebug() << "found tuple"<< tuple.mid();
                    QVERIFY(tuple.propertyCount() == tuple.propertyNames().count());
                    foreach( QString pname, tuple.propertyNames() )
                        qDebug() << "has property:" << pname << "=>" << tuple.property(pname);
                    foreach( SRPS::Curve::Variable var, curve->variables())
                        qDebug() << "value of:" << var.mid() << "=>" << tuple.value(var);
                }
            }

            delete curve;
        }
    }
}

void Test_Curve::readCurveDefaultMIDs()
{
    QVERIFY(factories.count());
    foreach( SRPS::CurveFactory * factory, factories )
    {
        foreach( QString key, factory->curveFactoryKeys() )
        {
            qDebug() << "testing:" << key;
            SRPS::Curve * curve = factory->curveFactoryCreate(key);
            QVERIFY(curve);
            QVERIFY(key == curve->componentClassName());

            QBENCHMARK {
                // properties
                QVERIFY(curve->componentPropertyCount() == curve->componentPropertyNames().count());
                foreach( QString pname, curve->componentPropertyNames() )
                    qDebug() << "has property:" << pname << "=>" << curve->componentProperty(pname);

                // variables
                QVERIFY(curve->variableCount() == curve->variableMIDs().count());
                foreach( SRPS::Curve::MemoryIdentifier var, curve->variableMIDs() )
                {
                    qDebug() << "found variable" << var;
                    QVERIFY(curve->variablePropertyCount(var) == curve->variablePropertyNames(var).count());
                    foreach( QString pname, curve->variablePropertyNames(var) )
                        qDebug() << "has property:" << pname << "=>" << curve->variableProperty(var,pname);
                }

                // tuples
                QVERIFY(curve->tupleCount() == curve->tupleMIDs().count());
                foreach( SRPS::Curve::MemoryIdentifier tuple, curve->tupleMIDs() )
                {
                    qDebug() << "found tuple"<< tuple;
                    QVERIFY(curve->tuplePropertyCount(tuple) == curve->tuplePropertyNames(tuple).count());
                    foreach( QString pname, curve->tuplePropertyNames(tuple) )
                        qDebug() << "has property:" << pname << "=>" << curve->tupleProperty(tuple,pname);
                    foreach( SRPS::Curve::MemoryIdentifier var, curve->variableMIDs())
                        qDebug() << "value of:" << var << "=>" << curve->tupleValue(tuple,var);
                }
            }

            delete curve;
        }
    }
}

void Test_Curve::writeCurveReferences()
{
    QVERIFY(factories.count());
    foreach( SRPS::CurveFactory * factory, factories )
    {
        foreach( QString key, factory->curveFactoryKeys() )
        {
            qDebug() << "testing:" << key;
            QBENCHMARK{

                SRPS::Curve * curve = factory->curveFactoryCreate(key);
                QVERIFY(curve);
                QVERIFY(key == curve->componentClassName());

                // write some properties
                for( int i = 0; i < PCOUNT; i++ )
                    QVERIFY(curve->componentSetProperty(QString("__test_p_%1").arg(i+1), i+1 ) );

                // check some properties
                for( int i = 0; i < PCOUNT; i++ )
                {
                    QVERIFY(curve->componentProperty(QString("__test_p_%1").arg(i+1)).toInt() == (i+1));
                    curve->componentSetProperty(QString("__test_p_%1").arg(i+1),QVariant());
                }

                // write some vars
                SRPS::Curve::VariableList myvars;
                for( int i = 0; i < VCOUNT; ++i )
                {
                    SRPS::Curve::Variable var = curve->createVariable();
                    QVERIFY(var.isValid());
                    myvars << var;
                    QVERIFY(var.setProperty(QString("__test_var_id"),var.mid()));
                }

                // test some vars
                foreach( SRPS::Curve::Variable var, myvars )
                    QVERIFY(var.property(QString("__test_var_id")).toUInt() == var.mid());

                // write some tuples
                SRPS::Curve::TupleList mytuples;
                for( int i = 0; i < TCOUNT; ++i )
                {
                    SRPS::Curve::Tuple tuple = curve->createTuple();
                    QVERIFY(tuple.isValid());
                    mytuples << tuple;
                    tuple.setProperty(QString("__test_tuple_id"),tuple.mid());
                    foreach( SRPS::Curve::Variable var, myvars)
                        tuple.setValue(var,QString("%1_%2").arg(tuple.mid()).arg(var.mid()));
                }

                // test some tuples
                foreach(SRPS::Curve::Tuple tuple, mytuples)
                {
                    QVERIFY(tuple.property(QString("__test_tuple_id")).toUInt()==tuple.mid());
                    foreach( SRPS::Curve::Variable var, myvars)
                        QVERIFY(tuple.value(var).toString()==QString("%1_%2").arg(tuple.mid()).arg(var.mid()));
                }

                foreach( SRPS::Curve::Tuple t, mytuples)
                    curve->deleteTuple(t);

                foreach( SRPS::Curve::Variable v, myvars)
                    curve->deleteVariable(v);

                delete curve;
            }
        }
    }
}

void Test_Curve::writeCurveMIDs()
{
    QVERIFY(factories.count());
    foreach( SRPS::CurveFactory * factory, factories )
    {
        foreach( QString key, factory->curveFactoryKeys() )
        {
            qDebug() << "testing:" << key;

            QBENCHMARK{

                SRPS::Curve * curve = factory->curveFactoryCreate(key);
                QVERIFY(curve);
                QVERIFY(key == curve->componentClassName());

                // write some properties
                for( int i = 0; i < PCOUNT; i++ )
                    QVERIFY(curve->componentSetProperty(QString("__test_p_%1").arg(i+1), i+1 ) );

                // check some properties
                for( int i = 0; i < PCOUNT; i++ )
                {
                    QVERIFY(curve->componentProperty(QString("__test_p_%1").arg(i+1)).toInt() == (i+1));
                    curve->componentSetProperty(QString("__test_p_%1").arg(i+1),QVariant());
                }

                // write some vars
                SRPS::Curve::MIDList myvars;
                for( int i = 0; i < VCOUNT; ++i )
                {
                    SRPS::Curve::MemoryIdentifier var = curve->createVariableMID();
                    myvars << var;
                    curve->variableSetProperty(var,QString("__test_var_id"),var);
                }

                // test some vars
                foreach( SRPS::Curve::MemoryIdentifier var, myvars )
                    QVERIFY(curve->variableProperty(var,QString("__test_var_id")).toUInt() == var);

                // write some tuples
                SRPS::Curve::MIDList mytuples;
                for( int i = 0; i < TCOUNT; ++i )
                {
                    SRPS::Curve::MemoryIdentifier tuple = curve->createTupleMID();
                    mytuples << tuple;
                    curve->tupleSetProperty(tuple,QString("__test_tuple_id"),tuple);
                    foreach( SRPS::Curve::MemoryIdentifier var, myvars)
                        curve->tupleSetValue(tuple,var,QString("%1_%2").arg(tuple).arg(var));
                }

                // test some tuples
                foreach(SRPS::Curve::MemoryIdentifier tuple, mytuples)
                {
                    QVERIFY(curve->tupleProperty(tuple,QString("__test_tuple_id")).toUInt()==tuple);
                    foreach( SRPS::Curve::MemoryIdentifier var, myvars)
                        QVERIFY(curve->tupleValue(tuple,var).toString()==QString("%1_%2").arg(tuple).arg(var));
                }

                foreach( SRPS::Curve::MemoryIdentifier t, mytuples)
                    curve->deleteTuple(t);

                foreach( SRPS::Curve::MemoryIdentifier v, myvars)
                    curve->deleteVariable(v);
                delete curve;
            }
        }
    }
}

QTEST_MAIN(Test_Curve);

#include "tst_curve.moc"
