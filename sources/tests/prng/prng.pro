#-------------------------------------------------
#
# Project created by QtCreator 2010-08-12T19:32:09
#
#-------------------------------------------------

include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )

TARGET = Test$${SRPS_NAME}PRNG
DESTDIR = $${SRPS_BINARIES_PATH}

TEMPLATE = app

!exists( dSFMT.cpp ) {
    message( transforming dSFMT.c )
    unix:{
                message( "cp ../../plugins/prngs/dSFMT-src-2.1/dSFMT.c dSFMT.cpp" )
                system( cp ../../plugins/prngs/dSFMT-src-2.1/dSFMT.c dSFMT.cpp )
        }
        win32:{
                message( "copy ..\..\plugins\prngs\dSFMT-src-2.1\dSFMT.c dSFMT.cpp" )
                system( copy ..\..\plugins\prngs\dSFMT-src-2.1\dSFMT.c dSFMT.cpp )
        }
}

INCLUDEPATH +=../../plugins/prngs/dSFMT-src-2.1/

SOURCES += tst_prng.cpp \
    ../../extra/timer/src/timer.cpp\
    dSFMT.cpp

QMAKE_CXXFLAGS += -DDSFMT_MEXP=19937

contains(CONFIG,SIMD){

  unix {
    message("Configuring to use sse2, if test not pass disable it.")
    QMAKE_CXXFLAGS += -msse2 \
        -fno-strict-aliasing \
        -DHAVE_SSE2=1 -O2
  }
  win32 {
    message("Win32 sse2 no enabled")
    QMAKE_CXXFLAGS += -DHAVE_SSE2=0
  }
}else {
    message("For sse2 use: qmake CONFIG+=SIMD")
}
