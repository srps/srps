/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <QtCore>
#include <QtGui/QApplication>

#include <SRPS/PRNG>
#include <SRPS/PRNGFactory>

#include "../../extra/timer/src/Timer.h"
#include "../../plugins/prngs/dSFMT-src-2.1/dSFMT.h"

#include <ctime>

int main(int argc, char **argv)
{
  QApplication qapp(argc,argv);

  // in million.
  int BASE = 1000000;
  int FACTOR = 10;
  int ITERATIONS = 10;

  QString param(argv[1]);
  bool ok;
  FACTOR = param.toInt(&ok);
  if (!ok ) FACTOR = 10;
  param = QString(argv[2]);
  ITERATIONS = param.toInt(&ok);
  if (!ok ) ITERATIONS = 10;

  qsrand(time(0));

  {
    qDebug()<< "TEST:" << "testing qrand()";
    double val = 0;

    Timer timer;
    double timed = 0;

    int iter = ITERATIONS;
    while(iter--)
    {
      timer.start();
      int tmp = FACTOR*BASE;
      while(tmp--)
        val = double(qrand());
      timer.stop();
      timed += timer.getElapsedTimeInMilliSec();
    }

    qDebug()<< "TEST:" << timed/ITERATIONS << "ms mean for:" << ITERATIONS <<"|" << FACTOR << "million samples";
  }

  {
    qDebug()<< "TEST:" << "testing static dSFMT";
    double val = 0;

    Timer timer;
    double timed = 0;

    dsfmt_t prng;
    dsfmt_init_gen_rand(&prng,qrand());

    int iter = ITERATIONS;
    while(iter--)
    {
      timer.start();
      int tmp = FACTOR*BASE;
      while(tmp--)
        val = dsfmt_genrand_open_open(&prng);
      timer.stop();
      timed += timer.getElapsedTimeInMilliSec();
    }

    qDebug()<< "TEST:" << timed/ITERATIONS << "ms mean for:" << ITERATIONS <<"|" << FACTOR << "million samples";
  }



  QDir pluginDir( QLibraryInfo::location(QLibraryInfo::PluginsPath)+"/srps");
  foreach( QString filepath, pluginDir.entryList(QStringList("*"),QDir::Files|QDir::Readable|QDir::NoDotAndDotDot))
  {
      filepath = pluginDir.absoluteFilePath(filepath);
      QPluginLoader * pl = new QPluginLoader(filepath);
      if ( !pl->load() )
      {
          qDebug() << pl->errorString();
          delete pl;
          continue;
      }
      QObject * obj = pl->instance();
      if ( !obj )
      {
          qDebug() << filepath << "no root";
          pl->unload();
          delete pl;
          continue;
      }
      SRPS::PRNGFactory * factory = qobject_cast<SRPS::PRNGFactory*>(obj);
      if(!factory)
      {
//          qDebug() << filepath << "no SRPS::PRNGFactory";
          pl->unload();
          delete pl;
          continue;
      }

      // has factory
      qDebug() << "TEST:" << filepath << "SRPS::PRNGs" << factory->prngFactoryKeys();
      foreach( QString k, factory->prngFactoryKeys() )
      {
        SRPS::PRNG * prng = factory->prngFactoryCreate(k);
        if( !prng )
        {
          qDebug() << "TEST:" << "cant create" << k;
          continue;
        }
        else
        {
          qDebug() << "TEST:" << "testing" << k;
          double val = 0;

          Timer timer;
          double timed = 0;

          int iter = ITERATIONS;
          while(iter--)
          {
            timer.start();
            int tmp = FACTOR*BASE;
            while(tmp--)
              val = prng->nextOO();
            timer.stop();
            timed += timer.getElapsedTimeInMilliSec();
          }

          qDebug() << "TEST:" << timed/ITERATIONS << "ms mean for:" << ITERATIONS <<"|" << FACTOR << "million samples";
          delete prng;
        }
      }

      pl->unload();
      delete pl;
  }

  return 0;
}
