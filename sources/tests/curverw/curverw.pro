#-------------------------------------------------
#
# Project created by QtCreator 2010-08-12T19:32:09
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

include ( ../../../configuration/srps.conf )
include ( ../../../configuration/srps.pri )

TARGET = Test$${SRPS_NAME}CurveRW
DESTDIR = $${SRPS_BINARIES_PATH}
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += tst_curverw.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
