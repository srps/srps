/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include <SRPS/Curve>
#include <SRPS/CurveFactory>
#include <SRPS/CurveRW>
#include <SRPS/CurveRWFactory>

class Test_CurveRW : public QObject
{
    Q_OBJECT

public:
    Test_CurveRW();

    static const int PCOUNT = 5;
    static const int VCOUNT = 5;
    static const int TCOUNT = 5 ;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void writeCurve();
    void readCurve();

private:

    QList<QByteArray> datas;

    QList<QPluginLoader*> loaders;
    QList<SRPS::CurveRWFactory*> factories;
    QList<SRPS::CurveFactory*> curveFactories;
};

Test_CurveRW::Test_CurveRW()
{

}

void Test_CurveRW::initTestCase()
{
    qDebug()<< Q_FUNC_INFO;
    QDir pluginDir( QLibraryInfo::location(QLibraryInfo::PluginsPath)+"/srps");
    foreach( QString filepath, pluginDir.entryList(QStringList("*"),QDir::Files|QDir::Readable|QDir::NoDotAndDotDot))
    {
        filepath = pluginDir.absoluteFilePath(filepath);
        QPluginLoader * pl = new QPluginLoader(filepath);
        if ( !pl->load() )
        {
            qDebug() << pl->errorString();
            delete pl;
            continue;
        }
        QObject * obj = pl->instance();
        if ( !obj )
        {
            qDebug() << filepath << "no root";
            pl->unload();
            delete pl;
            continue;
        }

        SRPS::CurveRWFactory * factory = qobject_cast<SRPS::CurveRWFactory*>(obj);
        if(factory)
        {
            // has factory
            qDebug() << filepath << "SRPS::CurveRWs" << factory->curveRWClasses();
            loaders << pl;
            factories << factory;
        }
        else if( SRPS::CurveFactory * curvef = qobject_cast<SRPS::CurveFactory*>(obj) )
        {
            // has factory
            qDebug() << filepath << "SRPS::Curves" << curvef->curveClasses();
            if ( !loaders.contains(pl) )
                loaders << pl;
            curveFactories << curvef;
        }
        else
        {
            qDebug() << filepath << "no SRPS::CurveRWFactories or SRPS::CurveFactories";
            pl->unload();
            delete pl;
        }
    }

    qDebug() << "factory count" << factories.count() << curveFactories.count();
}

void Test_CurveRW::cleanupTestCase()
{
    qDebug() << Q_FUNC_INFO;
    foreach( QPluginLoader * pl, loaders)
    {
        pl->unload();
        delete pl;
    }
    factories.clear();
    loaders.clear();
}

void Test_CurveRW::writeCurve()
{
    QVERIFY(factories.count() && curveFactories.count());
    foreach( SRPS::CurveRWFactory * factory, factories )
    {
        foreach( QString key, factory->curveRWClasses() )
        {
            qDebug() << "testing:" << key;
            SRPS::CurveRW * curverw = factory->createCurveRW(key);
            QVERIFY(curverw);
            QVERIFY(key == curverw->thisClassName());

            foreach( SRPS::CurveFactory * cf, curveFactories )
            {
                foreach( QString ckey , cf->curveClasses() )
                {
                    qDebug() << " with testing:" << ckey;
                    SRPS::Curve * curve = cf->createCurve(ckey);
                    QVERIFY(curve);
                    QVERIFY(ckey == curve->thisClassName());

                    for( int p = 1; p <= PCOUNT; ++p )
                        curve->setProperty(QString("p_%1").arg(p),p);

                    for( int v = 1; v <= VCOUNT; ++v )
                    {
                        SRPS::Curve::Variable var = curve->createVariable();
                        var.setProperty("name",v);
                        for( int p = 0; p < PCOUNT; ++p )
                            var.setProperty(QString("p_%1").arg(p),p);
                    }

                    for( int t = 1; t <= TCOUNT; ++t )
                    {
                        SRPS::Curve::Tuple tuple = curve->createTuple();
                        tuple.setProperty("count",t);
                        for( int p = 1; p <= PCOUNT; ++p )
                            tuple.setProperty(QString("p_%1").arg(p),p);

                        foreach( SRPS::Curve::ConstVariable var, tuple.variables( SRPS::Curve::CreationOrder ) )
                            tuple.setValue( var, QString("%1 %2").arg(t).arg(var.property("name").toString()));
                    }

                    QByteArray data;
                    QBuffer device(&data);
                    device.open(QBuffer::WriteOnly);

                    int result = curverw->write(curve,&device);
                    qDebug() << result << curverw->resultString(result);

                    datas.append(data);

                    qDebug() << data;

                    delete curve;
                }
            }

            delete curverw;
        }
    }
}

void Test_CurveRW::readCurve()
{
    QVERIFY(factories.count() && curveFactories.count() && datas.count());
    foreach( SRPS::CurveRWFactory * factory, factories )
    {
        foreach( QString key, factory->curveRWClasses() )
        {
            qDebug() << "testing:" << key;
            SRPS::CurveRW * curverw = factory->createCurveRW(key);
            QVERIFY(curverw);
            QVERIFY(key == curverw->thisClassName());

            foreach( SRPS::CurveFactory * cf, curveFactories )
            {
                foreach( QString ckey , cf->curveClasses() )
                {
                    qDebug() << "testing:" << ckey;
                    SRPS::Curve * curve = cf->createCurve(ckey);
                    QVERIFY(curve);
                    QVERIFY(ckey == curve->thisClassName());

                    QByteArray data = datas.takeFirst();
                    QBuffer device(&data);
                    device.open(QBuffer::ReadOnly);

                    int result = curverw->read(&device,curve);
                    qDebug() << result << curverw->resultString(result);

                    for( int p = 1; p <= PCOUNT; ++p )
                        QVERIFY(curve->getProperty(QString("p_%1").arg(p)).toInt() == p);

                    SRPS::Curve::VariableList vars = curve->variables(curve->CreationOrder);
                    for( int v = 1; v <= VCOUNT; ++v )
                    {
                        SRPS::Curve::Variable var = vars.at(v-1);
                        QVERIFY(var.property("name").toInt() == v );
                        for( int p = 0; p < PCOUNT; ++p )
                            QVERIFY(var.property(QString("p_%1").arg(p)).toInt() == p);
                    }

                    SRPS::Curve::TupleList tuples = curve->tuples(curve->CreationOrder);
                    for( int t = 1; t <= TCOUNT; ++t )
                    {
                        SRPS::Curve::Tuple tuple = tuples.at(t-1);
                        QVERIFY(tuple.property("count").toInt() == t);
                        for( int p = 1; p <= PCOUNT; ++p )
                            QVERIFY(tuple.property(QString("p_%1").arg(p)).toInt() == p);

                        foreach( SRPS::Curve::ConstVariable var, tuple.variables( SRPS::Curve::CreationOrder ) )
                            QVERIFY(tuple.value( var ).toString() == QString("%1 %2").arg(t).arg(var.property("name").toString()));
                    }

                    delete curve;
                }
            }

            delete curverw;
        }
    }
}

QTEST_MAIN(Test_CurveRW);

#include "tst_curverw.moc"
