/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include <SRPS/Space>
#include <SRPS/SpaceFactory>

class Test_Space : public QObject
{
    Q_OBJECT

public:
    Test_Space();

    static const int PCOUNT = 256;
    static const int CCOUNT = 256;
    static const int SCOUNT = 256;
    static const int ICOUNT = 256;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void readSpaceDefaultReferences();
    void readSpaceDefaultMIDs();
    void writeSpaceReferences();
    void writeSpaceMIDs();

private:

    QList<QPluginLoader*> loaders;
    QList<SRPS::SpaceFactory*> factories;
};

Test_Space::Test_Space()
{

}

void Test_Space::initTestCase()
{
    qDebug()<< Q_FUNC_INFO;
    QDir pluginDir( QLibraryInfo::location(QLibraryInfo::PluginsPath)+"/srps");
    foreach( QString filepath, pluginDir.entryList(QStringList("*"),QDir::Files|QDir::Readable|QDir::NoDotAndDotDot))
    {
        filepath = pluginDir.absoluteFilePath(filepath);
        QPluginLoader * pl = new QPluginLoader(filepath);
        if ( !pl->load() )
        {
            qDebug() << pl->errorString();
            delete pl;
            continue;
        }
        QObject * obj = pl->instance();
        if ( !obj )
        {
            qDebug() << filepath << "no root";
            pl->unload();
            delete pl;
            continue;
        }
        SRPS::SpaceFactory * factory = qobject_cast<SRPS::SpaceFactory*>(obj);
        if(!factory)
        {
            qDebug() << filepath << "no SRPS::SpaceFactory";
            pl->unload();
            delete pl;
            continue;
        }

        // has factory
        qDebug() << filepath << "SRPS::Spaces" << factory->spaceFactoryKeys();
        loaders << pl;
        factories << factory;
    }

    qDebug() << "factory count" << factories.count();
}

void Test_Space::cleanupTestCase()
{
    qDebug() << Q_FUNC_INFO;
    foreach( QPluginLoader * pl, loaders)
    {
        pl->unload();
        delete pl;
    }
    factories.clear();
    loaders.clear();
}

void Test_Space::readSpaceDefaultReferences()
{
    QVERIFY(factories.count());
    foreach( SRPS::SpaceFactory * factory, factories )
    {
        foreach( QString key, factory->spaceFactoryKeys() )
        {
            qDebug() << "testing:" << key;
            SRPS::Space * space = factory->spaceFactoryCreate(key);
            QVERIFY(space);
            QVERIFY(key == space->componentClassName());

            QBENCHMARK {
                // properties
                QVERIFY(space->componentPropertyCount() == space->componentPropertyNames().count());
                foreach( QString pname, space->componentPropertyNames() )
                    qDebug() << "has property:" << pname << "=>" << space->componentProperty(pname);
            }

            delete space;
        }
    }
}

void Test_Space::readSpaceDefaultMIDs()
{
    QVERIFY(factories.count());
    foreach( SRPS::SpaceFactory * factory, factories )
    {
        foreach( QString key, factory->spaceFactoryKeys() )
        {
            qDebug() << "testing:" << key;
            SRPS::Space * space = factory->spaceFactoryCreate(key);
            QVERIFY(space);
            QVERIFY(key == space->componentClassName());

            QBENCHMARK {
                // properties
                QVERIFY(space->componentPropertyCount() == space->componentPropertyNames().count());
                foreach( QString pname, space->componentPropertyNames() )
                    qDebug() << "has property:" << pname << "=>" << space->componentProperty(pname);
            }

            delete space;
        }
    }
}

void Test_Space::writeSpaceReferences()
{
    QVERIFY(factories.count());
    foreach( SRPS::SpaceFactory * factory, factories )
    {
        foreach( QString key, factory->spaceFactoryKeys() )
        {
            qDebug() << "testing:" << key;
            QBENCHMARK{

                SRPS::Space * space = factory->spaceFactoryCreate(key);
                QVERIFY(space);
                QVERIFY(key == space->componentClassName());

                // write some properties
                for( int i = 0; i < PCOUNT; i++ )
                    QVERIFY(!space->componentSetProperty(QString("__test_p_%1").arg(i+1), i+1 ) );

                // check some properties
                for( int i = 0; i < PCOUNT; i++ )
                {
                    QVERIFY(space->componentProperty(QString("__test_p_%1").arg(i+1)).toInt() == (i+1));
                    space->componentSetProperty(QString("__test_p_%1").arg(i+1),QVariant());
                }

                SRPS::Space::CellList cells;
                for( int c= 0; c < CCOUNT; ++c )
                {
                    SRPS::Space::Cell cell =space->createCell();
                    cells << cell;
                    cell.setProperty("_id_",c);
                }

                SRPS::Space::IndividualList individuals;

                SRPS::Space::SpeciesList species;
                for( int c= 0; c < SCOUNT; ++c )
                {
                    SRPS::Space::Species specie = space->createSpecies();
                    species << specie;
                    specie.setProperty("_id_",c);

                    SRPS::Space::IndividualList is;
                    for( int i = 0; i < ICOUNT; ++i )
                    {
                        SRPS::Space::Individual individual = specie.createIndividual();
                        QVERIFY(individual.setProperty("_id_",QString("%1 %2").arg(c).arg(individual.mid())));
                        is << individual;
                        individuals << individual;
                    }
                    specie.setProperty(QString("_is_"),QVariant::fromValue(is));
                }

                SRPS::Space::CellList cellCopy = cells;
                while( !individuals.isEmpty() )
                {
                    if ( cellCopy.isEmpty() )
                        cellCopy = cells;
                    SRPS::Space::Cell mycell = cellCopy.takeFirst();
                    QVERIFY(mycell.insertIndividual(individuals.takeFirst()));
                }

                cells = space->cells(space->CreationOrder);
                for( int c= 0; c < CCOUNT; ++c )
                {
                    QVERIFY(cells.at(c).property("_id_").toInt() == c);
                    QVERIFY(cells.at(c).individualCount() == ICOUNT) ;
                    QVERIFY(cells.at(c).speciesCount() == SCOUNT) ;
                    foreach(SRPS::Space::Individual i, cells.at(c).individuals())
                        QVERIFY(i.constCell() == cells.at(c));
                }

                species = space->species(space->CreationOrder);
                for( int c= 0; c < SCOUNT; ++c )
                {
                    QVERIFY(species.at(c).property("_id_").toInt() == c);
                    QVERIFY(species.at(c).individualCount() == ICOUNT) ;
                    SRPS::Space::IndividualList is =species.at(c).individuals(space->CreationOrder);
                    QVERIFY(species.at(c).property(QString("_is_")).value<SRPS::Space::IndividualList>() == is );
                    foreach(SRPS::Space::Individual i, is)
                    {
                        QVERIFY(i.property("_id_").toString() == QString("%1 %2").arg(c).arg(i.mid()));
                        QVERIFY(i.constSpecies() == species.at(c));
                    }
                }
            }
        }
    }
}

void Test_Space::writeSpaceMIDs()
{
    QVERIFY(factories.count());
    foreach( SRPS::SpaceFactory * factory, factories )
    {
        foreach( QString key, factory->spaceFactoryKeys() )
        {
            qDebug() << "testing:" << key;

            QBENCHMARK{

                SRPS::Space * space = factory->spaceFactoryCreate(key);
                QVERIFY(space);
                QVERIFY(key == space->componentClassName());

                // write some properties
                for( int i = 0; i < PCOUNT; i++ )
                    QVERIFY(!space->componentSetProperty(QString("__test_p_%1").arg(i+1), i+1 ) );

                // check some properties
                for( int i = 0; i < PCOUNT; i++ )
                {
                    QVERIFY(space->componentProperty(QString("__test_p_%1").arg(i+1)).toInt() == (i+1));
                    space->componentSetProperty(QString("__test_p_%1").arg(i+1),QVariant());
                }

                SRPS::Space::MIDList cells;
                for( int c= 0; c < CCOUNT; ++c )
                {
                    SRPS::Space::MemoryIdentifier cell =space->createCellMID();
                    cells << cell;
                    space->cellSetProperty(cell,"_id_",c);
                }

                QList< QPair<SRPS::Space::MemoryIdentifier,SRPS::Space::MemoryIdentifier> > individuals;

                SRPS::Space::MIDList species;
                for( int c= 0; c < SCOUNT; ++c )
                {
                    SRPS::Space::MemoryIdentifier specie = space->createSpeciesMID();
                    species << specie;
                    space->speciesSetProperty(specie,QString("_id_"),c);

                    SRPS::Space::MIDList is;
                    for( int i = 0; i < ICOUNT; ++i )
                    {
                        SRPS::Space::MemoryIdentifier individual = space->speciesCreateIndividualMID(specie);
                        QVERIFY(space->individualSetProperty(individual,"_id_",QString("%1 %2").arg(c).arg(individual)));
                        is << individual;
                        individuals << QPair<SRPS::Space::MemoryIdentifier,SRPS::Space::MemoryIdentifier>(specie,individual);
                    }
                    space->speciesSetProperty(specie,QString("_is_"),QVariant::fromValue(is));
                }


                SRPS::Space::MIDList cellsCopy = cells;
                while( !individuals.isEmpty() )
                {
                    if (cellsCopy.isEmpty())
                        cellsCopy = cells;
                    SRPS::Space::MemoryIdentifier mycell = cellsCopy.takeFirst();
                    QPair<SRPS::Space::MemoryIdentifier,SRPS::Space::MemoryIdentifier> data = individuals.takeFirst();
                    space->cellInsertIndividual(mycell,data.first, data.second);
                }

                cells = space->cellMIDs(space->CreationOrder);
                for( int c= 0; c < CCOUNT; ++c )
                {
                    QVERIFY(space->cellProperty(cells.at(c),QString("_id_")).toInt() == c);
                    QVERIFY(space->cellIndividualCount(cells.at(c)) == ICOUNT);
                    QVERIFY(space->cellSpeciesCount(cells.at(c)) == SCOUNT);
                    foreach(SRPS::Space::MemoryIdentifier i, space->cellIndividualMIDs(cells.at(c)))
                        QVERIFY(space->individualCellMID(i) == cells.at(c));
                }

                species = space->speciesMIDs(space->CreationOrder);
                for( int c= 0; c < SCOUNT; ++c )
                {
                    QVERIFY(space->speciesProperty(species.at(c),QString("_id_")).toInt() == c);
                    QVERIFY(space->speciesIndividualCount(species.at(c)) == ICOUNT);
                    SRPS::Space::MIDList is = space->speciesProperty(species.at(c),QString("_is_")).value<SRPS::Space::MIDList>();
                    QVERIFY(space->speciesIndividualMIDs(species.at(c),space->CreationOrder) == is );
                    foreach(SRPS::Space::MemoryIdentifier i, is)
                    {
                        QVERIFY(space->individualSpeciesMID(i) == species.at(c));
                        QVERIFY(space->individualProperty(i,"_id_").toString() == QString("%1 %2").arg(c).arg(i));
                    }
                }

                delete space;
            }
        }
    }
}

QTEST_MAIN(Test_Space);

#include "tst_space.moc"
