/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef PROGRAM_PLUGINLOADER_H
#define PROGRAM_PLUGINLOADER_H

#include <SRPS/PluginLoader>

namespace SRPS {
namespace Implementation {

class PluginLoader : public SRPS::PluginLoader
{
  Q_OBJECT
  Q_DISABLE_COPY(PluginLoader)
  class Private;
  Private *p;

public:
  explicit PluginLoader(QObject *parent = 0);
  virtual ~PluginLoader();

  QObjectList plugins() const;

  SRPS::SettingsManager * settingsManagerFor( const QObject * obj,
                                              bool withName = false ) const;

  bool loadPlugin( const QString & filepath );

//  bool unloadPlugin( const QString & filepath );

  int loadDir( const QString & dir );

  bool registerPlugin( QObject * object );

  bool unregisterPlugin( QObject * object );

private slots:

  void deleteRegisteredPlugin( QObject * );

};

} // namespace Implementation
} // namespace SRPS

#endif // PROGRAM_PLUGINLOADER_H
