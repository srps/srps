/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "program_settingsmanager.h"
#include "program_pluginloader.h"

#include <SRPS/Plugin>
#include <QtCore/QPluginLoader>
#include <QtCore/QDir>

namespace SRPS {
namespace Implementation {

class PluginLoader::Private
{
public:
    QList<QPluginLoader*> loaders;
    QObjectList registeredQObjects;
};

PluginLoader::PluginLoader(QObject *parent) :
    SRPS::PluginLoader(parent),
    p( new Private )
{
}

PluginLoader::~PluginLoader()
{
  foreach( QPluginLoader * pl, p->loaders )
  {
    if ( !pl )
      continue;

    SRPS::Plugin * asPlugin = qobject_cast<SRPS::Plugin*>(pl->instance());
    if (!asPlugin)
      continue;

    qDebug("%s: found plugin interface.",Q_FUNC_INFO);

    bool result = false;
    SRPS::SettingsManager * settings = settingsManagerFor( pl->instance() );

    result = asPlugin->pluginWriteSettings(settings);

    qDebug("%s: writed settings: %s",
           Q_FUNC_INFO,
           result?"true":"false");

    result = asPlugin->pluginFinalize();

    qDebug("%s: finalized: %s",
           Q_FUNC_INFO,
           result?"true":"false");

    delete settings;
  }

  delete p;
}

QObjectList PluginLoader::plugins() const
{
  QObjectList result;
  foreach( QPluginLoader * pl, p->loaders )
    result << pl->instance();
  return result << p->registeredQObjects;
}

// caller of this method MUST delete returned object
SRPS::SettingsManager * PluginLoader::settingsManagerFor( const QObject * obj,
                                                  bool /*withName*/ ) const
{
  if ( !obj )
    return new SettingsManager;

  QString className = obj->metaObject()->className();
  className = className.toLower().replace("::","/");
//  QString objectName = obj->objectName();

  return new SettingsManager(className);
}

bool PluginLoader::loadPlugin( const QString & filepath )
{
  QPluginLoader * ploader = new QPluginLoader(filepath);

  if ( ploader->isLoaded() )
  {
    qWarning("%s: %s, already loaded.",
             Q_FUNC_INFO,
             filepath.toStdString().c_str());

    delete ploader;
    return false;
  }

  if ( !ploader->load() )
  {
    qWarning("%s: %s, cant be loaded: ",
             Q_FUNC_INFO,
             ploader->errorString().toStdString().c_str());

    delete ploader;
    return false;
  }

  QObject * plugin_object = ploader->instance();
  if ( !plugin_object )
  {
    qWarning("%s: %s, has no root.",
             Q_FUNC_INFO,
             ploader->errorString().toStdString().c_str());

    delete ploader;
    return false;
  }

  SRPS::Plugin * asPlugin = qobject_cast<SRPS::Plugin*>(plugin_object);
  if ( asPlugin )
  {
    qDebug("%s: found plugin interface.",Q_FUNC_INFO);

    bool result = false;
    result = asPlugin->pluginInitialize();
    qDebug("%s: initialized: %s",
           Q_FUNC_INFO,
           result?"true":"false");

    SRPS::SettingsManager * settings = settingsManagerFor( plugin_object );
    result = asPlugin->pluginReadSettings(settings);
    qDebug("%s: readed settings: %s",
           Q_FUNC_INFO,
           result?"true":"false");

    delete settings;
  }

  p->loaders << ploader;

  plugin_object->setProperty("__srps__plugin_loader__filepath",filepath);

  qDebug("%s: loaded: %s[%s]",
         Q_FUNC_INFO,
         plugin_object->metaObject()->className(),
         plugin_object->objectName().toStdString().c_str());

  emit pluginLoaded(plugin_object,filepath);

  return true;
}

//bool PluginLoader::unloadPlugin( const QString & filepath )
//{

//}

int PluginLoader::loadDir( const QString & _dir )
{
  QDir dir(_dir);
  dir.makeAbsolute();
  QStringList files = dir.entryList(QStringList("*"),
                                    QDir::Files|QDir::Readable);
  int c = 0;
  foreach( QString file, files )
    if (loadPlugin( dir.absoluteFilePath(file) ))
      ++c;
  return c;
}

bool PluginLoader::registerPlugin( QObject * object )
{
  if ( p->registeredQObjects.contains(object) )
    return false;
  p->registeredQObjects << object;
  emit pluginLoaded(object,"memory");
  return true;
}


bool PluginLoader::unregisterPlugin( QObject * object )
{
  if ( !p->registeredQObjects.contains(object) )
    return false;
  deleteRegisteredPlugin(object);
  return true;
}

void PluginLoader::deleteRegisteredPlugin( QObject * obj )
{
  if ( p->registeredQObjects.removeAll(obj) )
    emit pluginUnloaded(obj,"memory");
}

} // namespace Implementation
} // namespace SRPS
