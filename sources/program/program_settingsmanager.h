/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef PROGRAM_SETTINGSMANAGER_H
#define PROGRAM_SETTINGSMANAGER_H

#include <SRPS/SettingsManager>

namespace SRPS {
namespace Implementation {

/*!
  \brief This class uses Qt QSettings for settings manager.

  The settings are stored in INI Format in application's dir path with name:
  [ApplicationName].settings
 */
class SettingsManager : public SRPS::SettingsManager
{
  Q_DISABLE_COPY(SettingsManager)
  class Private;
  Private * p;

public:
  explicit SettingsManager( const QString & prefix = QString() );
  virtual ~SettingsManager();

  bool isValid() const;

  QStringList keys() const;

  void setValue ( const QString & key,
                  const QVariant & value );

  QVariant value ( const QString & key,
                   const QVariant & defaultValue = QVariant() ) const;
};

} // namespace Implementation
} // namespace SRPS

#endif // PROGRAM_SETTINGSMANAGER_H
