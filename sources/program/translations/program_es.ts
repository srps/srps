<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>QtWindowListMenu</name>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="124"/>
        <source>&amp;Windows</source>
        <translation>&amp;Ventanas</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="144"/>
        <source>&amp;Tile</source>
        <translation>&amp;Titulo</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="145"/>
        <source>Tile the windows</source>
        <translation>Reorganizar</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="147"/>
        <source>&amp;Cascade</source>
        <translation>Cascada</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="148"/>
        <source>Cascade the windows</source>
        <translation>Ventanas en cascada</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="153"/>
        <source>Ne&amp;xt</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="154"/>
        <source>Move the focus to the next window</source>
        <translation>CAmbia el foco a la siguiente ventana</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="156"/>
        <source>Pre&amp;vious</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="157"/>
        <source>Move the focus to the previous window</source>
        <translation>Cmabia el foco a la ventana anterior</translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="272"/>
        <source>&amp;%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qtwindowlistmenu-2.2_1-opensource/src/qtwindowlistmenu.cpp" line="274"/>
        <source>%1 %2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="17"/>
        <source>About SRPS</source>
        <translation>Acerca de SRPS</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="46"/>
        <source>icon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="65"/>
        <source>SRPS</source>
        <translation>SRPS</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="75"/>
        <source>Species Richness Pattern Simulator</source>
        <translation>Simulador de Patrones de Riqueza de Especies</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="93"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="110"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
</context>
<context>
    <name>SRPS::Program::AccumulatorDialog</name>
    <message>
        <location filename="accumulatordialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogo</translation>
    </message>
    <message>
        <location filename="accumulatordialog.ui" line="20"/>
        <source>Select accumulators</source>
        <translation>Seleccione los acumuladores</translation>
    </message>
</context>
<context>
    <name>SRPS::Program::CurveSetupDialog</name>
    <message>
        <location filename="curvesetupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogo</translation>
    </message>
    <message>
        <location filename="curvesetupdialog.ui" line="22"/>
        <source>Variable count:</source>
        <translation>Cuantas variables:</translation>
    </message>
    <message>
        <location filename="curvesetupdialog.ui" line="39"/>
        <source>Tuple count:</source>
        <translation>Cuantas tuplas:</translation>
    </message>
</context>
<context>
    <name>SRPS::Program::Initializer</name>
    <message>
        <location filename="initializer.cpp" line="87"/>
        <source>SRPS::PluginLoader created.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="initializer.cpp" line="94"/>
        <source>Main window created.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>SRPS</source>
        <translation>SRPS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>&amp;SRPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="38"/>
        <source>New</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="55"/>
        <source>&amp;Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <source>&amp;Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <source>About&amp;SRPS</source>
        <translation>Acerca de SRPS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="101"/>
        <source>About&amp;Qt</source>
        <translation>Acerca de Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>&amp;Settings</source>
        <translation>Configuraciones</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="111"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="119"/>
        <source>&amp;Messages</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="124"/>
        <location filename="mainwindow.cpp" line="199"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="127"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="135"/>
        <location filename="mainwindow.cpp" line="225"/>
        <location filename="mainwindow.cpp" line="263"/>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="146"/>
        <location filename="mainwindow.cpp" line="245"/>
        <location filename="mainwindow.cpp" line="254"/>
        <location filename="mainwindow.cpp" line="304"/>
        <source>SaveAs</source>
        <translation>Guardar como</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <location filename="mainwindow.cpp" line="299"/>
        <location filename="mainwindow.cpp" line="314"/>
        <location filename="mainwindow.cpp" line="321"/>
        <location filename="mainwindow.cpp" line="328"/>
        <location filename="mainwindow.cpp" line="331"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="mainwindow.cpp" line="341"/>
        <location filename="mainwindow.cpp" line="348"/>
        <location filename="mainwindow.cpp" line="354"/>
        <location filename="mainwindow.cpp" line="367"/>
        <location filename="mainwindow.cpp" line="375"/>
        <location filename="mainwindow.cpp" line="382"/>
        <location filename="mainwindow.cpp" line="392"/>
        <location filename="mainwindow.cpp" line="398"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="168"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="176"/>
        <source>SaveAll</source>
        <translation>Guardar Todo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <source>CloseAll</source>
        <translation>Cerrar Todo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <location filename="mainwindow.ui" line="192"/>
        <source>NewSpace</source>
        <translation>Nuevo Espacio</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="197"/>
        <source>NewCurve</source>
        <translation>Nueva Curva</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="205"/>
        <source>Visualize</source>
        <translation>Visualizar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>Accumulator</source>
        <translation>Acumular</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="221"/>
        <source>Simulator</source>
        <translation>Simular</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="114"/>
        <source>Save?</source>
        <translation>Guardar?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <source>Do you want to save this curve?</source>
        <translation>¿Desea guardar la curva?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Space %1[*]</source>
        <translation>Espacio %1[*]</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="192"/>
        <source>Curve %1[*]</source>
        <translation>Curva %1[*]</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="225"/>
        <source>Saving error: %</source>
        <translation>Error al guardar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="227"/>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Curve saved to: %1</source>
        <translation>Curva guardada en: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="254"/>
        <source>Cant not create Curve Writer, why?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>Saving error: %1</source>
        <translation>Error al guardar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="300"/>
        <source>There is no Curve Writers, load a plugin.</source>
        <translation>No hay escritores de Curvas, cargue un plugin.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="315"/>
        <source>Can&apos;t open file: %1</source>
        <translation>No se puede abrir el archivo: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Select Curve writer.</source>
        <translation>Seleccione un escritor de Curva</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="329"/>
        <source>Cant create component, why?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="332"/>
        <source>Curve exported to: %1</source>
        <translation>Curva exportada a %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="349"/>
        <source>Cant file for importing: %1</source>
        <translation>No se puede abrir el archivo para importar.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>File not recognized</source>
        <translation>Archivo no reconocido</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="367"/>
        <source>bad file header</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="376"/>
        <source>Unknow file object</source>
        <translation>Objeto desconocido</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="383"/>
        <source>No factory for component: %1. Load a plugin.</source>
        <translation>No hay fabrica para el componente: %1, cargue un plugin.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Cant import curve</source>
        <translation>No se puede importar la Curva</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="398"/>
        <source>Can&apos;t import curve: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Curve Imported %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="442"/>
        <source>Visualizer %1</source>
        <translation>Visualizador %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="477"/>
        <source>Accumulator %1</source>
        <translation>Acumulador %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="584"/>
        <location filename="mainwindow.cpp" line="591"/>
        <location filename="mainwindow.cpp" line="636"/>
        <source>Write</source>
        <translation>Escribir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="585"/>
        <source>Can&apos;t write to device</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="592"/>
        <source>Write to device incomplete: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>Error at Curve writing: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="790"/>
        <source>Invalid parameters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>Empty parameters</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::MessageLogger</name>
    <message>
        <location filename="messagelogger.cpp" line="40"/>
        <source>SRPS::MessagerLogger created</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::MessageLoggerTableModel</name>
    <message>
        <location filename="messageloggertablemodel.cpp" line="110"/>
        <source>Information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="112"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="114"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="116"/>
        <source>Other: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="136"/>
        <source>Datetime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="138"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="140"/>
        <source>Sender</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messageloggertablemodel.cpp" line="142"/>
        <source>Message</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::MessageViewer</name>
    <message>
        <location filename="messageviewer.ui" line="14"/>
        <source>Messages.</source>
        <translation>Mensajes</translation>
    </message>
</context>
<context>
    <name>SRPS::Program::PluginLoader</name>
    <message>
        <location filename="pluginloader.cpp" line="68"/>
        <source>Plugin already loaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pluginloader.cpp" line="87"/>
        <source>No root instance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pluginloader.cpp" line="111"/>
        <source>Loaded %1:
%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pluginloader.cpp" line="119"/>
        <source>Can&apos;t manage %1 is not a SRPS::QObjectFactory</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::Resources</name>
    <message>
        <location filename="resources.cpp" line="53"/>
        <source>Resource already loaded: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="resources.cpp" line="62"/>
        <source>Resource loaded: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="resources.cpp" line="69"/>
        <source>Can&apos;t load resource: %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::SettingsDialog</name>
    <message>
        <location filename="settingsdialog.ui" line="17"/>
        <source>Settings</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="53"/>
        <source>Default Components</source>
        <translation>Componentes por defecto</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="59"/>
        <source>Curve:</source>
        <translation>Curva:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="69"/>
        <source>Space:</source>
        <translation>Espacio:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="79"/>
        <source>PRNG::</source>
        <translation>Generador:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="89"/>
        <source>CurveView</source>
        <translation>Visualizador
Curve:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="119"/>
        <source>load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="158"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="183"/>
        <source>Component</source>
        <translation>Componente</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="188"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="72"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="76"/>
        <source>Resources</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="80"/>
        <source>Plugins</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="197"/>
        <location filename="settingsdialog.cpp" line="243"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="197"/>
        <source>Can&apos;t find plugin loader</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="201"/>
        <location filename="settingsdialog.cpp" line="206"/>
        <location filename="settingsdialog.cpp" line="208"/>
        <location filename="settingsdialog.cpp" line="252"/>
        <location filename="settingsdialog.cpp" line="254"/>
        <source>Load plugin</source>
        <translation>Cargar plugin</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="243"/>
        <source>Can&apos;t find resource loader</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="247"/>
        <source>Load resource</source>
        <translation>Cagar recurso</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="252"/>
        <source>Resource loaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="254"/>
        <source>Resource not loaded</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::SimulationModelWizardPage</name>
    <message>
        <location filename="simulationmodelwizardpage.ui" line="14"/>
        <source>WizardPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="simulationmodelwizardpage.ui" line="17"/>
        <source>Model configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="simulationmodelwizardpage.ui" line="20"/>
        <source>Select and configurate models</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SRPS::Program::SimulationWizard</name>
    <message>
        <location filename="simulationwizard.cpp" line="131"/>
        <location filename="simulationwizard.cpp" line="145"/>
        <source>Simulator</source>
        <translation>Simulador</translation>
    </message>
    <message>
        <location filename="simulationwizard.cpp" line="132"/>
        <source>No Space supplied</source>
        <translation></translation>
    </message>
    <message>
        <location filename="simulationwizard.cpp" line="146"/>
        <source>Parameters errors</source>
        <translation></translation>
    </message>
    <message>
        <location filename="simulationwizard.cpp" line="214"/>
        <source>Simulation %1</source>
        <translation>Simulación %1</translation>
    </message>
</context>
<context>
    <name>SRPS::Program::SpaceSetupDialog</name>
    <message>
        <location filename="spacesetupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="spacesetupdialog.ui" line="32"/>
        <source>Generador interno</source>
        <translation></translation>
    </message>
    <message>
        <location filename="spacesetupdialog.ui" line="40"/>
        <source>Cell count</source>
        <translation>Cuantas celdas</translation>
    </message>
    <message>
        <location filename="spacesetupdialog.ui" line="57"/>
        <source>Species count</source>
        <translation>Cuantas especies</translation>
    </message>
    <message>
        <location filename="spacesetupdialog.ui" line="74"/>
        <source>Individual count:</source>
        <translation>Cuantos individuos</translation>
    </message>
    <message>
        <location filename="spacesetupdialog.ui" line="105"/>
        <source>Otro generador</source>
        <translation></translation>
    </message>
    <message>
        <location filename="spacesetupdialog.cpp" line="81"/>
        <source>Cell %1</source>
        <translation>Celda</translation>
    </message>
    <message>
        <location filename="spacesetupdialog.cpp" line="87"/>
        <source>Species %1</source>
        <translation>Especie</translation>
    </message>
</context>
<context>
    <name>SimulationAccumulatorWizardPage</name>
    <message>
        <location filename="simulationaccumulatorwizardpage.ui" line="14"/>
        <source>WizardPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="simulationaccumulatorwizardpage.ui" line="17"/>
        <source>Accumulator configuration</source>
        <translation>Acumuladores</translation>
    </message>
    <message>
        <location filename="simulationaccumulatorwizardpage.ui" line="20"/>
        <source>Select and configure accumulators</source>
        <translation>Sleccione los acumuladores a utilizar</translation>
    </message>
</context>
<context>
    <name>SimulationConfigurationWizardPage</name>
    <message>
        <location filename="simulationconfigurationwizardpage.ui" line="14"/>
        <source>WizardPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="simulationconfigurationwizardpage.ui" line="17"/>
        <source>Simulator configuration</source>
        <translation>Configuración del simulador</translation>
    </message>
    <message>
        <location filename="simulationconfigurationwizardpage.ui" line="20"/>
        <source>Configure general settigns</source>
        <translation>Opciones generales</translation>
    </message>
    <message>
        <location filename="simulationconfigurationwizardpage.ui" line="26"/>
        <source>Total iterations:</source>
        <translation>Numero simulaciones</translation>
    </message>
    <message>
        <location filename="simulationconfigurationwizardpage.ui" line="46"/>
        <source>PRNG:</source>
        <translation>Generador</translation>
    </message>
    <message>
        <location filename="simulationconfigurationwizardpage.ui" line="56"/>
        <source>Distribution:</source>
        <translation>Distribución</translation>
    </message>
</context>
<context>
    <name>SimulationDialog</name>
    <message>
        <location filename="simulationdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simulationdialog.ui" line="20"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simulationdialog.ui" line="26"/>
        <source>Iterations:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simulationdialog.ui" line="53"/>
        <source>Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simulationdialog.ui" line="63"/>
        <source>Accumulators</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
