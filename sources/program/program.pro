include ( ../../configuration/srps.conf )
include ( ../../configuration/srps.pri )
TEMPLATE = app
VERSION = $$SRPS_VERSION
TARGET = $$SRPS_NAME
DESTDIR = $$SRPS_BINARIES_PATH
HEADERS += \
    program_pluginloader.h \
    program_settingsmanager.h \
    program_resourceloader.h
FORMS +=
QT += script

# RESOURCES += resources.qrc

SOURCES += \
    main.cpp \
    program_pluginloader.cpp \
    program_settingsmanager.cpp \
    program_resourceloader.cpp
#TRANSLATIONS += translations/program_es.ts

RESOURCES += \
    resource.qrc

OTHER_FILES += \
    boot.qs
