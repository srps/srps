/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "program_resourceloader.h"

#include <QtCore/QHash>
#include <QtCore/QDir>
#include <QtCore/QResource>

namespace SRPS {
namespace Implementation {

class ResourceLoader::Private
{
public:
  QHash<QString,QString> loadedResources;
};

ResourceLoader::ResourceLoader(QObject *parent) :
    SRPS::ResourceLoader(parent),
    p( new Private )
{
}

ResourceLoader::~ResourceLoader()
{
  delete p;
}

QStringList ResourceLoader::loadedResources() const
{
  return p->loadedResources.keys();
}

QString ResourceLoader::mapRootFor( const QString & path ) const
{
  return p->loadedResources.value(path);
}

int ResourceLoader::loadDir( const QString & path )
{
  QDir dir(path);
  dir.makeAbsolute();

  QStringList files = dir.entryList(QStringList("*.rcc"),
                                    QDir::Files|QDir::Readable);
  int c = 0;
  foreach( QString file, files )
    if (loadResource(dir.absoluteFilePath(file) ))
      ++c;
  return c;
}

bool ResourceLoader::loadResource( const QString & path,
                                   const QString & mapRoot )
{
  if ( path.isEmpty() )
  {
      return false;
  }

  QFileInfo info(path);
  QString npath = info.absoluteFilePath();
  if ( p->loadedResources.contains(npath) )
      return false;

  if ( QResource::registerResource(npath,mapRoot) )
  {
      qDebug("%s: loaded %s with root '%s'",
             Q_FUNC_INFO,
             npath.toAscii().data(),
             mapRoot.toAscii().data());

      p->loadedResources[npath] = mapRoot;
      emit loadedResource(npath,mapRoot);
      return  true;
  }
  return false;
}

bool ResourceLoader::unloadResource( const QString & path,
                                     const QString & mapRoot )
{
  if ( path.isEmpty() )
  {
      return false;
  }

  QFileInfo info(path);
  QString npath = info.absoluteFilePath();
  if ( !p->loadedResources.contains(npath) )
      return false;

  if ( QResource::unregisterResource(npath,mapRoot) )
  {
      qDebug("%s: unloaded %s with root '%s'",
             Q_FUNC_INFO,
             npath.toAscii().data(),
             mapRoot.toAscii().data());

      p->loadedResources.remove(npath);
      emit unloadedResource(npath,mapRoot);
      return  true;
  }
  return false;
}

} // namespace Implementation
} // namespace SRPS
