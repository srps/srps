/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef PROGRAM_RESOURCELOADER_H
#define PROGRAM_RESOURCELOADER_H

#include <SRPS/ResourceLoader>

namespace SRPS {
namespace Implementation {

/*!
  \brief This class Manages SRPS Resources.
 */
class ResourceLoader : public SRPS::ResourceLoader
{
  Q_OBJECT
  SRPS_DISABLE_COPY(ResourceLoader)
  class Private;
  Private * p;

public:

    explicit ResourceLoader(QObject *parent = 0);
    virtual ~ResourceLoader();

    QStringList loadedResources() const;

    QString mapRootFor( const QString & path ) const;

    int loadDir( const QString & path );

    bool loadResource( const QString & path,
                       const QString & mapRoot = QString() );

    bool unloadResource( const QString & path,
                         const QString & mapRoot = QString() );

};

} // namespace Implementation
} // namespace SRPS

#endif // PROGRAM_RESOURCELOADER_H
