/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "program_settingsmanager.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>
#include <QtCore/QDir>

namespace SRPS {
namespace Implementation {

class SettingsManager::Private
{
public:
  QSettings  * settings;
};

SettingsManager::SettingsManager(const QString & prefix) :
    p ( new Private )
{
  p->settings = new QSettings( QCoreApplication::applicationDirPath()+
                               QDir::separator()+
                               QCoreApplication::applicationName()+".settings",
                               QSettings::IniFormat);

  if ( prefix.isEmpty() )
    p->settings->beginGroup(QCoreApplication::applicationName());
  else
    p->settings->beginGroup(prefix);
}

SettingsManager::~SettingsManager()
{
  delete p->settings;
  delete p;
}

bool SettingsManager::isValid() const
{
  return p->settings->status() == QSettings::NoError;
}

QStringList SettingsManager::keys() const
{
  return p->settings->childKeys();
}

void SettingsManager::setValue ( const QString & key,
                                 const QVariant & value )
{
  p->settings->setValue(key,value);
}

QVariant SettingsManager::value ( const QString & key,
                                  const QVariant & defaultValue ) const
{
  return p->settings->value(key,defaultValue);
}

} // namespace Implementation
} // namespace SRPS
