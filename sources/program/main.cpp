/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "program_pluginloader.h"
#include "program_settingsmanager.h"
#include "program_resourceloader.h"

#include <QtCore/QByteArray>
#include <QtCore/QLibraryInfo>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QMutex>
#include <QtCore/QDateTime>
#include <QtCore/QMutexLocker>
#include <QtGui/QApplication>
#include <QtScript/QScriptEngine>

#include <ctime>

static QFile * static_debug_file = 0;
static QMutex static_debug_file_mutex;

static QString readFile( const QString & path )
{
    QFile file(path);
    if ( !file.exists(path) )
    {
        qDebug("%s: file '%s' doesn't exists.",Q_FUNC_INFO,path.toAscii().data());
        return QString();
    }
    if( !file.open(QFile::ReadOnly) )
    {
        qDebug("%s: can't open file '%s' for reading: %s",Q_FUNC_INFO,path.toAscii().data(),file.errorString().toAscii().data());
        return QString();
    }
    return file.readAll();
}

static void cleanUp()
{
    if(static_debug_file)
    {
        qDebug("Application closed.");
        static_debug_file->close();
        delete static_debug_file;
        static_debug_file = 0;
    }
}

static void myMsgHandler(QtMsgType type, const char * msg)
{
    if ( !static_debug_file )
    {
        static_debug_file = new QFile(QLibraryInfo::location(QLibraryInfo::BinariesPath)+QDir::separator()+QCoreApplication::applicationName()+".log");
        static_debug_file->open(QIODevice::WriteOnly|QIODevice::Append);
        qDebug("Starting Application ...");
        qAddPostRoutine(cleanUp);
    }
    if (static_debug_file->isOpen())
    {
        QMutexLocker locker(&static_debug_file_mutex);
        static_debug_file->write(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz").toAscii());
        switch(type)
        {
        case QtWarningMsg:
            static_debug_file->write(" [Warning] ");
            break;
        case QtCriticalMsg:
            static_debug_file->write(" [Critical]: ");
            break;
        case QtFatalMsg:
            static_debug_file->write(" [Fatal] ");
            break;
        default:
            static_debug_file->write(" [Info] ");
            break;
        }
        static_debug_file->write(msg);
        static_debug_file->write("\n");
        static_debug_file->flush();
    }
}

int main(int argc, char *argv[])
{
  qsrand(time(0));
  qInstallMsgHandler(myMsgHandler);
  QCoreApplication::setApplicationName("SRPS");
  QCoreApplication::setApplicationVersion("1.0");
  qsrand( QTime::currentTime().msec() );
  QApplication a(argc, argv);

  //load resources
  SRPS::Implementation::ResourceLoader resourceLoader;
  resourceLoader.loadDir(QLibraryInfo::location(QLibraryInfo::PrefixPath)+
                         QDir::separator()+
                         ".."+
                         QDir::separator()+
                         "resources");

  SRPS::Implementation::PluginLoader pluginLoader;

  pluginLoader.registerPlugin(&resourceLoader);

  pluginLoader.loadDir(
      QLibraryInfo::location(
          QLibraryInfo::PluginsPath)+
          QDir::separator()+
          QCoreApplication::applicationName().toLower());

  pluginLoader.loadDir(
      QLibraryInfo::location(
          QLibraryInfo::PluginsPath)+
          QDir::separator()+
          QCoreApplication::applicationName().toUpper());

  SRPS::SettingsManager * PLSM = pluginLoader.settingsManagerFor(&pluginLoader);

  PLSM->setValue("last_loaded_plugins_count",pluginLoader.plugins().count());
  delete PLSM;

  QScriptEngine engine;

  QScriptValue scriptPluginLoader =
      engine.newQObject( &pluginLoader,
                         engine.QtOwnership,
                         QScriptEngine::ExcludeChildObjects|
                         QScriptEngine::ExcludeSuperClassMethods|
//                         QScriptEngine::ExcludeSuperClassProperties|
                         /*QScriptEngine::ExcludeSuperClassContent|*/
                         QScriptEngine::ExcludeDeleteLater|
                         QScriptEngine::AutoCreateDynamicProperties|
                         QScriptEngine::PreferExistingWrapperObject|
                         QScriptEngine::SkipMethodsInEnumeration);

  engine.globalObject().setProperty("PluginLoader",
                                    scriptPluginLoader,
                                    QScriptValue::Undeletable|
                                    QScriptValue::ReadOnly);

  QScriptValue scriptResourceLoader =
      engine.newQObject( &resourceLoader,
                         engine.QtOwnership,
                         QScriptEngine::ExcludeChildObjects|
//                         QScriptEngine::ExcludeSuperClassMethods|
                         QScriptEngine::ExcludeSuperClassProperties|
                         /*QScriptEngine::ExcludeSuperClassContent|*/
                         QScriptEngine::ExcludeDeleteLater|
                         QScriptEngine::AutoCreateDynamicProperties|
                         QScriptEngine::PreferExistingWrapperObject|
                         QScriptEngine::SkipMethodsInEnumeration);

  engine.globalObject().setProperty("ResourceLoader",
                                    scriptResourceLoader,
                                    QScriptValue::Undeletable|
                                    QScriptValue::ReadOnly);

  QString programstr = readFile(":/srps/boot.qs");
  if(programstr.isEmpty())
      return -1;

  QScriptValue result = engine.evaluate(programstr,":/srps/boot.qs");
  if ( result.isError() )

      qFatal("%s: %s",Q_FUNC_INFO,result.toString().toAscii().data());

  if ( engine.hasUncaughtException() )
  {
      QScriptValue exception = engine.uncaughtException();
      int line = engine.uncaughtExceptionLineNumber();
      QStringList backtrace = engine.uncaughtExceptionBacktrace();

      qFatal("%s: %s at line %i",
             Q_FUNC_INFO,
             exception.toString().toAscii().data(),
             line);

      foreach( QString bt, backtrace )
          qFatal("%s:    %s",Q_FUNC_INFO,bt.toAscii().data());

      return -1;
  }

  SRPS::Implementation::SettingsManager ASM;
  ASM.setValue("started_at",
               QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));

  return a.exec();
}
