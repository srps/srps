/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "prng.h"

namespace SRPS {

  PRNG::PRNG():
      Component(),
      AbstractPRNG()
{
}

PRNG::~PRNG()
{
}

QByteArray PRNG::seed() const
{
  qDebug("%s: NOT IMPLEMENTED",Q_FUNC_INFO);
  return QByteArray();
}

bool PRNG::setSeed( const QByteArray & /*s*/ )
{
  qDebug("%s: NOT IMPLEMENTED",Q_FUNC_INFO);
  return false;
}

bool PRNG::setStream( quint32 /*stream*/ )
{
  qDebug("%s: NOT IMPLEMENTED",Q_FUNC_INFO);
  return false;
}

} // namespace SRPS
