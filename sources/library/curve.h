/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVE_H
#define SRPS_CURVE_H

#include "component.h"

namespace SRPS {

/*!
  \class SRPS::Curve curve.h SRPS/Curve
  \brief Abstract Curve Component.

  A Curve Component is a general curve abstraction. It contains Variables which
  have a name, descripion and properties. It contains Tuples which have a name,
  description, properties and the value of the variables. A Curve keep track of
  the tuple and variable management (creation, deletion and modification). It acts
  as a container, only keeps track on creation order of tuples and variables. The
  Curve allows Tuple and Variable access and modificaction throught References.

  The Curve client create Curve's tuples and variales. Order them on its own
  container.
  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT Curve : public Component
{
  SRPS_DISABLE_COPY(Curve)

public:

  /*!
    Curve's identifier type.
   */
  typedef quint32 MemoryIdentifier;
  typedef QList<MemoryIdentifier> MIDList;

  /*!
    Sort order for retreaving Variables, Tuples and Identifiers
   */
  enum SortOrder {
    Unordered = 0, /**< Unespecified order. */
    CreationOrder = 1, /**< Creation order, first created first. */
    InverseCreationOrder = 2/**< Creation order, first created last. */
  };

private:

  class PrivateReference; // puntero a puntero de curva ( tiene el contador )
  PrivateReference * pr;

  class SRPS_EXPORT AbstractReference
  {
  private:
    friend class SRPS::Curve;

    mutable PrivateReference * _shared_counter;

    MemoryIdentifier _memory_identifier;

  protected:

    AbstractReference();

  public:

    virtual ~AbstractReference();

    /*!
      \brief Check for reference validity.
      If refence is invalid, its detached.
      \return True if the reference is valid, any operation are safe.
      \return False, if the reference is invalid or Curve doesn't exists anymore.\
      any operation can cause application crash.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool isValid() const;

    /*!
      \brief Detach reference from Curve and make it invalid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    void detach() const;

    /*!
      \brief Reference MemoryIdentifier.
      \return Reference MemoryIdentifier.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    MemoryIdentifier mid() const;

  protected:

    AbstractReference( PrivateReference * p, MemoryIdentifier id );
    AbstractReference( const AbstractReference & o );
    AbstractReference & operator=( const AbstractReference & o );

    bool operator==( const AbstractReference & o ) const;
    bool operator!=( const AbstractReference & o ) const;

    const Curve * const_curve() const;
    Curve * curve() const;
  };

public:

  class SRPS_EXPORT ReferencePropertyGetter
  {
  public:
    virtual ~ReferencePropertyGetter();

    virtual bool hasProperty( const QString & name ) const = 0;

    virtual int propertyCount() const = 0;

    virtual QStringList propertyNames() const = 0;

    virtual QVariant property(const QString & name,
                              const QVariant & defaultResult = QVariant()) const = 0;
  };

  class SRPS_EXPORT ReferencePropertySetter
  {
  public:

    virtual ~ReferencePropertySetter();

    virtual int setProperty(const QString & name,
                            const QVariant & value ) const = 0;
  };

  class SRPS_EXPORT ConstVariable : public AbstractReference, public ReferencePropertyGetter
  {
  protected:

    friend class SRPS::Curve;

    ConstVariable( PrivateReference * p, MemoryIdentifier id );

  public:

    ConstVariable();
    ConstVariable( const ConstVariable & o );
    ConstVariable & operator=( const ConstVariable & o );
    virtual ~ConstVariable();

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers the same variable in Curve.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator==( const ConstVariable & o ) const;

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers distinct variables.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator!=( const ConstVariable & o ) const;

    bool isValid() const; // variableIsValid

    /*!
      \brief Check if variable has property.
      \param name, property target name.
      \return True if variable has the property.
      \return False if variable doesn't have property.
      \note this method relies on Curve::variableHasProperty()
      \note this method doesn't check if variable is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool hasProperty(const QString &name) const; // variableHasProperty

    /*!
      \brief Queries variable property count.
      \return Property count.
      \return Cero if variable doesn't have properties or MemoryIdentifiers not valid.
      \note this method relies on Curve::variablePropertyCount()
      \note this method doesn't check if variable is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    int propertyCount() const; // variableropertyCount

    /*!
      \brief Queries variable property name list.
      \return List of property names.
      \return Empty list if variable doesn't have properties or MemoryIdentifiers not valid.
      \note this method relies on Curve::variablePropertyNames()
      \note this method doesn't check if variable is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    QStringList propertyNames() const; // variablePropertyNames

    /*!
      \brief Queries variable property.
      \param name, property name.
      \param defaultValue, return fallback
      \return Valid QVariant if value exists.
      \return defaultValue if property doesn't exists.
      \note this method relies on Curve::variableProperty()
      \note this method doesn't check if variable is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    QVariant property(const QString & name,
                      const QVariant & defaultResult = QVariant()) const; // variableProperty
  };

  class SRPS_EXPORT Variable : public ConstVariable, public ReferencePropertySetter
  {
  private:

    friend class SRPS::Curve;

    Variable( PrivateReference * p, MemoryIdentifier id );

  public:

    Variable();
    Variable( const Variable & o );
    Variable & operator=( const Variable & o );
    virtual ~Variable();

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers the same variable in Curve.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator==( const ConstVariable & o ) const;

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers distinct variables.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator!=( const ConstVariable & o ) const;

    /*!
      \brief Sets variable's property.
      \param name, property name.
      \param value, target value
      \return -1 if property not exist
      \return 0 if property was changed or deleted ( invalid QVariant )
      \note this method relies on Curve::variableSetProperty()
      \note this method doesn't check if variable is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    int setProperty(const QString & name, const QVariant & value ) const; // variableSetProperty
  };

  typedef QList<ConstVariable> ConstVariableList;
  typedef QList<Variable> VariableList;

  class SRPS_EXPORT ConstTuple : public AbstractReference, public ReferencePropertyGetter
  {
  protected:

    friend class SRPS::Curve;

    ConstTuple( PrivateReference * p, MemoryIdentifier id );

  public:

    ConstTuple();
    ConstTuple( const ConstTuple & o );
    ConstTuple & operator=( const ConstTuple & o );
    virtual ~ConstTuple();

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers the same tuple in Curve.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator==( const ConstTuple & o ) const;

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers distinct tuples.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator!=( const ConstTuple & o ) const;

    bool isValid() const; // tupleIsValid

    /*!
      \brief Check if tuple has property.
      \param name, property target name.
      \return True if tuple has the property.
      \return False if tuple doesn't have property.
      \note this method relies on Curve::tupleHasProperty()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool hasProperty(const QString &name) const; // tupleHasProperty

    /*!
      \brief Queries tuple property count.
      \return Property count.
      \return Cero if tuple doesn't have properties or MemoryIdentifiers not valid.
      \note this method relies on Curve::tuplePropertyCount()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    int propertyCount() const; // tuplePropertyCount

    /*!
      \brief Queries tuple property name list.
      \return List of property names.
      \return Empty list if tuple doesn't have properties or MemoryIdentifiers not valid.
      \note this method relies on Curve::tuplePropertyNames()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    QStringList propertyNames() const; // tuplePropertyNames

    /*!
      \brief Queries tuple property.
      \param name, property name.
      \param defaultValue, return fallback
      \return Valid QVariant if value exists.
      \return defaultValue if property doesn't exists.
      \note this method relies on Curve::tupleProperty()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    QVariant property(const QString & name,
                      const QVariant & defaultResult = QVariant()) const; // tupleProperty

    /*!
      \brief Variable count.
      \return Tuples's valid variable count.
      \note this method relies on Curve::variableCount()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    int variableCount() const; // tupleVariableCount

    /*!
      \brief ConstVariable list.
      \param so, desired SortOrder.
      \return Tuple's valid ConstVariableList in \so SortOrder.
      \note this method relies on Curve::constVariables()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    ConstVariableList constVariables( const SortOrder & so = Unordered ) const; // tupleConstVariables

    /*!
      \brief Queries tuple value.
      \param variable, variable Reference.
      \param defaultValue, return fallback
      \return Valid QVariant if value exists.
      \return defaultValue if value doesn't exists or \c variable doesn't belongs \
      to tuple's Curve.
      \note this method relies on Curve::tupleValue()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    QVariant value( const ConstVariable & variable, const QVariant & defaultValue = QVariant() ) const; // tupleValue
  };

  class SRPS_EXPORT Tuple : public ConstTuple, public ReferencePropertySetter
  {
  private:

    friend class SRPS::Curve;

    Tuple( PrivateReference * p, MemoryIdentifier id );

  public:

    Tuple();
    Tuple( const Tuple & o );
    Tuple & operator=( const Tuple & o );
    virtual ~Tuple();

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers the same tuple in Curve.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator==( const ConstTuple & o ) const;

    /*!
      \brief Not Equal operator
      \param o, another reference.
      \return true if both references refers distinct tuples.
      \return false if not.
      \note this method doesn't verify references' validity.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool operator!=( const ConstTuple & o ) const;

    /*!
      \brief Changes to new or deleted property.
      If the value is null QVariant it means delete.
      \param name, property name.
      \param value, Target value.
      \return true if value was changed or deleted.
      \return false if not.
      \note this method relies on Curve::tupleSetProperty()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    int setProperty(const QString & name, const QVariant & value ) const; // tupleSetProperty

    /*!
      \brief Changes to new or deleted value.
      If the value is null QVariant it means delete.
      \param variable, tuple's variable.
      \param value, Target value.
      \return true if value was changed or deleted.
      \return false if not. \c variable is not valid or not belongs to the \
      tuples's curve.
      \note this method relies on Curve::tupleSetValue()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool setValue( const ConstVariable & variable, const QVariant & value ) const; // tupleSetValue

    /*!
      \brief Clear all variable values.
      \note this method relies on Curve::tupleClearValues()
      \note this method doesn't check if tuple is valid. If referece is invalid
      application crash may occur.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool clearValues(); // tupleClearValues
  };

  typedef QList<ConstTuple> ConstTupleList;
  typedef QList<Tuple> TupleList;

protected:

  Curve();

public:

  virtual ~Curve();

  /*!
    \brief Clone Curve.
    \return Curves's clone.
    \return 0 id the Curve component can't be cloned.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual Curve * clone() const = 0;

  virtual void clear() = 0;

  // Variable Curve API

  /*!
    \brief Variable count.
    \return Curves's variable count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int variableCount() const = 0;

  /*!
    \brief Variable MemoryIdentifier list.
    \param so, desired SortOrder.
    \return Variable MemoryIdentifier list in \so SortOrder.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual MIDList variableMIDs( const SortOrder & ord = Unordered ) const = 0;

  /*!
    \brief ConstVariable list.
    \param so, desired SortOrder.
    \return ConstVariable list in \so SortOrder.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual ConstVariableList constVariables( const SortOrder &  so = Unordered ) const = 0;

  /*!
    \brief Variable list.
    \param so, desired SortOrder.
    \return Variable list in \so SortOrder.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual VariableList variables( const SortOrder & so = Unordered ) const = 0;

  /*!
    \brief Create a variable.
    \return Valid MemoryIdentifier is variable was created.
    \return Invalid MemoryIdentifier is variable can't be created.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual MemoryIdentifier createVariableMID() = 0;

  /*!
    \brief Create a new Variable.
    This method relies in createVariableMID()
    \return Variable referencing the new variable.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual Variable createVariable() = 0;

  /*!
    \brief Deletes a variable.
    If deletion was successfull, all variable's properties must be deleted.
    \param mid, variable MemoryIdentifier
    \return true if variable was deleted.
    \return false if not, MemoryIdentifier is invalid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool deleteVariable(const MemoryIdentifier &) = 0;

  /*!
    \brief Delete a ConstVariable/Variable.
    \param variable, target ConstVariable/Variable.
    \return true, if the ConstVariable/Variable was deleted.
    \return false, of not or \c variable doesn't belong to this Curve.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool deleteVariable( const ConstVariable & variable ) = 0;

  /*!
    \brief Deletes a variable.
    If deletion was successfull, all variable's properties must be deleted.
    Base class implementation delete variables one by one using deleteVariable()
    \return Deleted variable count
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int deleteAllVariables() = 0;

  // Variable Reference API

  /*!
    \brief Check if variable is valid
    \param mid, variable MemoryIdentifier
    \return True if variable is valid.
    \return False if not, MemoryIdentifier is not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool variableIsValid( const MemoryIdentifier & mid) const = 0;

  /*!
    \brief Check if variable has property.
    \param mid, variable MemoryIdentifier
    \param name, property target name.
    \return True if variable has the property.
    \return False if variable doesn't have property or MemoryIdentifiers not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool variableHasProperty( const MemoryIdentifier & mid,
                                    const QString & name ) const = 0;

  /*!
    \brief Queries variable property count.
    \param mid, variable MemoryIdentifier
    \return Property count.
    \return Cero if variable doesn't have properties or MemoryIdentifiers not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int variablePropertyCount( const MemoryIdentifier & mid) const = 0;

  /*!
    \brief Queries variable property name list.
    \param mid, variable MemoryIdentifier
    \return List of property names.
    \return Empty list if variable doesn't have properties or MemoryIdentifiers not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QStringList variablePropertyNames( const MemoryIdentifier & mid) const = 0;

  /*!
    \brief Queries variable property.
    \param mid, variable MemoryIdentifier
    \param name, property name.
    \param defaultValue, return fallback
    \return Valid QVariant if value exists.
    \return defaultValue if value doesn't exists or MemoryIdentifiers or name is \
    not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QVariant variableProperty( const MemoryIdentifier & mid,
                                     const QString & name,
                                     const QVariant & defaultValue =
                                      QVariant() ) const = 0;

  /*!
    \brief Queries variable property.
    \param mid, variable MemoryIdentifier
    \param name, property name.
    \param defaultValue, return fallback
    \return Valid QVariant if value exists.
    \return defaultValue if value doesn't exists or MemoryIdentifiers or name is \
    not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int variableSetProperty( const MemoryIdentifier & mid,
                                    const QString & name,
                                    const QVariant & value ) = 0;

  // Tuple Space API

  /*!
    \brief Tuple count.
    \return Curves's tuple count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int tupleCount() const = 0;

  /*!
    \brief Tuple MemoryIdentifier list.
    \param so, desired SortOrder.
    \return Tuple MemoryIdentifier list in \so SortOrder.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual MIDList tupleMIDs( const SortOrder & so = Unordered ) const = 0;

  /*!
    \brief ConstTuple list.
    \param so, desired SortOrder.
    \return ConstTuple list in \so SortOrder.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual ConstTupleList constTuples( const SortOrder & so = Unordered ) const = 0;

  /*!
    \brief Tuple list.
    \param so, desired SortOrder.
    \return Tuple list in \so SortOrder.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual TupleList tuples( const SortOrder & so = Unordered ) const = 0;

  /*!
    \brief Create a tuple.
    \return Valid MemoryIdentifier is tuple was created.
    \return Invalid MemoryIdentifier is tuple can't be created.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual MemoryIdentifier createTupleMID() = 0;

  /*!
    \brief Create a new Tuple.
    This method relies in createTupleMID()
    \return Tuple referencing the new tuple.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual Tuple createTuple() = 0;

  /*!
    \brief Deletes a tuple.
    If deletion was successfull, all tuple's properties and values must be deleted.
    Base class implementations delete tuples one by one using deleteTuple
    \param mid, tuple MemoryIdentifier
    \return true if tuple was deleted.
    \return false if not, MemoryIdentifier is invalid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool deleteTuple(const MemoryIdentifier & mid) = 0;

  /*!
    \brief Delete a ConstTuple/Tuple.
    \param tuple, target ConstTuple/Tuple.
    \return true, if the ConstTuple/Tuple was deleted.
    \return false, of not or \c tuple doesn't belong to this Curve.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool deleteTuple( const ConstTuple & tuple ) = 0;

  /*!
    \brief Deletes all tuples.
    If deletion was successfull, all tuple's properties and values must be deleted.
    \param mid, tuple MemoryIdentifier
    \return Deleted tuple count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int deleteAllTuples() =0;

  // Tuple Reference API

  /*!
    \brief Check if tuple is valid
    \param mid, tuple MemoryIdentifier
    \return True if tuple is valid.
    \return False if not, MemoryIdentifier is not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool tupleIsValid( const MemoryIdentifier &) const = 0;

  /*!
    \brief Check if tuple has property.
    \param mid, tuple MemoryIdentifier
    \param name, property target name.
    \return True if tuple has the property.
    \return False if tuple doesn't have property or MemoryIdentifiers not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool tupleHasProperty( const MemoryIdentifier & mid,
                                 const QString & name ) const = 0;

  /*!
    \brief Queries tuple property count.
    \param mid, tuple MemoryIdentifier
    \return Property count.
    \return Cero if tuple doesn't have properties or MemoryIdentifiers not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int tuplePropertyCount( const MemoryIdentifier & mid) const = 0;

  /*!
    \brief Queries tuple property name list.
    \param mid, tuple MemoryIdentifier
    \return List of property names.
    \return Empty list if tuple doesn't have properties or MemoryIdentifiers not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QStringList tuplePropertyNames( const MemoryIdentifier & mid ) const = 0;

  /*!
    \brief Queries tuple property.
    \param mid, tuple MemoryIdentifier
    \param name, property name.
    \param defaultValue, return fallback
    \return Valid QVariant if value exists.
    \return defaultValue if value doesn't exists or MemoryIdentifiers or name is \
    not valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QVariant tupleProperty( const MemoryIdentifier & mid,
                                  const QString & name,
                                  const QVariant & defaultValue =
                                    QVariant() ) const = 0;

  /*!
    \brief Queries tuple value.
    \param tmid, tuple MemoryIdentifier
    \param vmid, variable MemoryIdentifier
    \param defaultValue, return fallback
    \return Valid QVariant if value exists.
    \return defaultValue if value doesn't exists or MemoryIdentifiers are invalid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QVariant tupleValue( const MemoryIdentifier & tmid,
                               const MemoryIdentifier & vmid,
                               const QVariant & defaultValue =
                                 QVariant()) const = 0;

  /*!
    \brief Tuple's valid Variable count.
    That's it, vaariables with valid values.
    \param tmid, tuple MemoryIdentifier
    \return Tuple's valid Variable count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int tupleVariableCount( const MemoryIdentifier & tmid ) const = 0;

  /*!
    \brief Tuple's valid Variable MIDList
    \param tmid, tuple MemoryIdentifier
    \param so desired SortOrder
    \return Tuple's valid Variable count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual MIDList tupleVariableMIDs( const MemoryIdentifier & tmid,
                                     const SortOrder & so = Unordered ) const = 0;

  /*!
    \brief Tuple's ConstVariableList
    That's it, ConstVariables with valid values int tuple.
    \param tmid, tuple MemoryIdentifier
    \return Tuple's valid Variable count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual ConstVariableList tupleConstVariables( const MemoryIdentifier & tmid,
                                                 const SortOrder & so = Unordered ) const = 0;

  /*!
    \brief Changes to new or deleted property.
    If the value is null QVariant it means delete.
    \param mid, tuple MemoryIdentifier
    \param name, property name.
    \param value, Target value.
    \return true if value was changed or deleted.
    \return false if not. MemoryIdentifiers aren't valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int tupleSetProperty( const MemoryIdentifier & mid,
                                 const QString & name,
                                 const QVariant & value ) = 0;

  /*!
    \brief Changes to new or deleted value.
    If the value is null QVariant it means delete.
    \param tmid, tuple MemoryIdentifier
    \param vmid, variable MemoryIdentifier
    \param value, Target value.
    \return true if value was changed or deleted.
    \return false if not. MemoryIdentifiers aren't valid.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool tupleSetValue( const MemoryIdentifier & tmid,
                              const MemoryIdentifier & vmid,
                              const QVariant & value = QVariant()) = 0;

  /*!
    \brief Clear all tuple variable values.
    \param tmid, tuple MemoryIdentifier
    \return true if tuple's values were cleared
    \return values changed count
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int tupleClearValues( const MemoryIdentifier & tmid ) = 0;

  // Converter API

  /*!
    \brief Converts between Variable and MemoryIdentifier.
    This method doesn't check if MemoryIdentifier is valid.
    \param mid, target MemoryIdentifier.
    \return Converted MemoryIdentifier as Variable.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  Variable toVariable( const MemoryIdentifier & mid ) const;

  /*!
    \brief Converts between Variable and MemoryIdentifier.
    This method doesn't check if MemoryIdentifier is valid.
    \param mid, target MemoryIdentifier.
    \return Converted MemoryIdentifier as ConstVariable.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  ConstVariable toConstVariable( const MemoryIdentifier & mid ) const;

  /*!
    \brief Converts between Variable types.
    This method doesn't check if ConstVariable is valid.
    \param const_variable, target ConstVariable
    \return Converted ConstVariable as Variable.
    \return Invalid Variable if \c tuple doesn't belongs to this Curve.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  Variable toVariable( const ConstVariable & const_variable ) const;

  /*!
    \brief Converts between Tuple and MemoryIdentifier.
    This method doesn't check if MemoryIdentifier is valid.
    \param mid, target MemoryIdentifier.
    \return Converted MemoryIdentifier as Tuple.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  Tuple toTuple( const MemoryIdentifier & mid) const;

  /*!
    \brief Converts between Tuple and MemoryIdentifier.
    This method doesn't check if MemoryIdentifier is valid.
    \param mid, target MemoryIdentifier.
    \return Converted MemoryIdentifier as ConstTuple.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  ConstTuple toConstTuple( const MemoryIdentifier & mid ) const;

  /*!
    \brief Converts between Tuple types.
    This method doesn't check if ConstTuple is valid.
    \param const_tuple, target ConstTuple
    \return Converted ConstTuple as Tuple.
    \return Invalid Tuple if \c tuple doesn't belongs to this Curve.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  Tuple toTuple( const ConstTuple & const_tuple ) const;

  /*!
    \brief Check for Reference ownership.
    \param reference, target reference.
    This method doesn't check if reference is valid.
    \return true, if the Reference belongs to this Curve.
    \return false, if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  bool isReferenceFromThis( const AbstractReference & reference ) const;

};

} // namespace SRPS

Q_DECLARE_METATYPE(SRPS::Curve::Variable)
Q_DECLARE_METATYPE(SRPS::Curve::ConstVariable)
Q_DECLARE_METATYPE(SRPS::Curve::Tuple)
Q_DECLARE_METATYPE(SRPS::Curve::ConstTuple)
Q_DECLARE_METATYPE(SRPS::Curve::VariableList)
Q_DECLARE_METATYPE(SRPS::Curve::ConstVariableList)
Q_DECLARE_METATYPE(SRPS::Curve::TupleList)
Q_DECLARE_METATYPE(SRPS::Curve::ConstTupleList)

Q_DECLARE_INTERFACE(SRPS::Curve,"SRPS::Curve/1.0.0")

#endif // SRPS_CURVE_H
