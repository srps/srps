/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "curvelcrud.h"

namespace SRPS {

CurveLCRUD::CurveLCRUD()
{
}

CurveLCRUD::~CurveLCRUD()
{
}

QStringList CurveLCRUD::curveList() const
{
  return QStringList();
}

QString CurveLCRUD::curveCreate( const SRPS::Curve * /*curve*/ )
{
  return QString();
}

int CurveLCRUD::curveCreate( const QString & /*key*/, SRPS::Curve * /*curve*/ )
{
  return registerLastOperationResult(-1,tr("Creation operation not suported"));
}

int CurveLCRUD::curveUpdate( const QString & /*key*/, const SRPS::Curve * /*curve*/ )
{
  return registerLastOperationResult(-1,tr("Update operation not suported"));
}

int CurveLCRUD::curveDelete( const QString & /*key*/ )
{
  return registerLastOperationResult(-1,tr("Delete operation not suported"));
}

} // namecurve SRPS
