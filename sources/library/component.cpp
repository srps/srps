/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "component.h"

namespace SRPS
{

class Component::SRPS_DECLARE_PRIVATE_CLASS_NAME
{
public:
  SRPS_DECLARE_PRIVATE_CLASS_NAME():
      code(-1),text()
  {}

  mutable int code;
  mutable QString text;
};

Component::Component():
    SRPS_DECLARE_PRIVATE_CLASS_MEMBER(new SRPS_DECLARE_PRIVATE_CLASS_NAME)
{
  registerLastOperationResult(-1,tr("Default implementation"));
}

int Component::registerLastOperationResult( int code, const QString & text ) const
{
  SRPS_DECLARE_PRIVATE_CLASS_MEMBER->code = code;
  SRPS_DECLARE_PRIVATE_CLASS_MEMBER->text = text;
  return code;
}

bool Component::registerLastOperationResult( bool result, const QString & text ) const
{
  SRPS_DECLARE_PRIVATE_CLASS_MEMBER->code = result?0:-1;
  SRPS_DECLARE_PRIVATE_CLASS_MEMBER->text = text;
  return result;
}

Component::~Component()
{
  delete SRPS_DECLARE_PRIVATE_CLASS_MEMBER;
}

QVariantHash Component::componentCreationParameters()
{
  return QVariantHash();
}

QObject * Component::componentAsQObject()
{
    return 0;
}

const QObject * Component::componentAsConstQObject() const
{
    return 0;
}

QWidget * Component::componentStatusWidget() const
{
    return 0;
}

QWidget * Component::componentConfigurationWidget()
{
    return 0;
}

QWidget * Component::componentAboutWidget() const
{
    return 0;
}

bool Component::componentHasProperty( const QString & /*name*/ ) const
{
    return registerLastOperationResult(false,
                                       tr("Default implementation"));
}

bool Component::componentIsStaticProperty( const QString & /*name*/ ) const
{
  return registerLastOperationResult(false,
                                     tr("Default implementation"));
}

int Component::componentPropertyCount() const
{
    return registerLastOperationResult(false,
                                       tr("Default implementation"));
}

QStringList Component::componentPropertyNames() const
{
    return QStringList();
}

QVariant Component::componentProperty( const QString & /*name*/,
                                       const QVariant & defaultValue ) const
{
    return defaultValue;
}

int Component::componentSetProperty( const QString & /*name*/,
                                      const QVariant & /*value*/ )
{
  return registerLastOperationResult(-1,
                                     tr("Default implementation"));
}

int Component::componentLastOperationResultCode() const
{
  return SRPS_DECLARE_PRIVATE_CLASS_MEMBER->code;
}

QString Component::componentLastOperationResultText() const
{
  return SRPS_DECLARE_PRIVATE_CLASS_MEMBER->text;
}

} // namespace SRPS
