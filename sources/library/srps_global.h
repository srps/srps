/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_GLOBAL_H
#define SRPS_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QtCore/QMetaType>
#include <QtPlugin>

#if defined(SRPS_LIBRARY)
#  define SRPS_EXPORT Q_DECL_EXPORT
#else
#  define SRPS_EXPORT Q_DECL_IMPORT
#endif

#ifndef SRPS_DISABLE_COPY
#define SRPS_DISABLE_COPY(Class) \
    private:\
        Class( const Class & );\
        Class & operator=(const Class & );
#endif

#ifndef SRPS_DECLARE_PRIVATE_CLASS_NAME
#define SRPS_DECLARE_PRIVATE_CLASS_NAME _SRPS_Declared_Private_Class_
#endif

#ifndef SRPS_DECLARE_PRIVATE_CLASS_MEMBER
#define SRPS_DECLARE_PRIVATE_CLASS_MEMBER _this_private_class_member_
#endif

#ifndef SRPS_DECLARE_PRIVATE_CLASS
#define SRPS_DECLARE_PRIVATE_CLASS \
    private: \
        class SRPS_DECLARE_PRIVATE_CLASS_NAME; \
        SRPS_DECLARE_PRIVATE_CLASS_NAME * \
            SRPS_DECLARE_PRIVATE_CLASS_MEMBER;
#endif

#ifndef SRPS_USE_PRIVATE
#define SRPS_USE_PRIVATE(p) \
        SRPS_DECLARE_PRIVATE_CLASS_NAME * p = \
          this->SRPS_DECLARE_PRIVATE_CLASS_MEMBER;
#endif

/*!
  \namespace SRPS
  \brief Namespace for SRPS implementation.

  All SRPS code is this namespace, including SRPS implementations. It is
  reserved for SRPS extension. For your own extension, plugins or libraries
  define another namespace.
 */
namespace SRPS
{
};

#endif // SRPS_GLOBAL_H
