/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_DPRNG_H
#define SRPS_DPRNG_H

#include "component.h"
#include "abstractprng.h"

namespace SRPS {

class PRNG;

/*!
  \class SRPS::DPRNG dprng.h SRPS/DPRNG
  \brief Abstract interface for all Pseudo-Random Number Generators with \
  Specific probability distribution.

  It must transform a PRNG output to implemented Distribution.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT DPRNG : public Component, public AbstractPRNG
{
  SRPS_DISABLE_COPY(DPRNG)

protected:
      DPRNG();

public:

    class SRPS_EXPORT Range
    {
      double p_min;
      double p_max;
      public:

        Range(double min = 0.0, double max = 1.0 );

        double min() const;
        double max() const;

        void setMin( double d );
        void setMax( double d );

        double length() const;
    };

    virtual ~DPRNG();

    virtual PRNG * prng() const = 0;
    virtual bool setPRNG( PRNG * ) = 0;

    virtual Range range() const = 0;
    virtual bool setRange( const Range & range ) = 0;
};

} // namespace

Q_DECLARE_METATYPE(SRPS::DPRNG::Range);
Q_DECLARE_INTERFACE(SRPS::DPRNG,"SRPS::DPRNG/1.0.0");

#endif // SRPS_DPRNG_H
