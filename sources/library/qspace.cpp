/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "qspace.h"

namespace SRPS {

class QSpace::Private
{
public:
  Private():
      space(0),
      ownership(false)
  {}
  SRPS::Space * space;
  bool ownership;
};

QSpace::QSpace(QObject *parent) :
    QObject(parent),
    SRPS::Space(),
    p( new Private )
{
}

QSpace::~QSpace()
{
  if ( p->ownership )
    delete p->space;
  delete p;
}

SRPS::Space * QSpace::getSpace() const
{
  return p->space;
}

bool QSpace::setSpace( SRPS::Space * space )
{
  if ( p->space != space )
  {
    p->space = space;
    emit spaceChanged(p->space);
    return true;
  }
  return false;
}

bool QSpace::hasOwnership() const
{
  return p->ownership;
}

void QSpace::setOwnership( bool s ) const
{
  p->ownership = s;
}

// Component
QObject * QSpace::componentAsQObject()
{
  return this;
}

const QObject * QSpace::componentAsConstQObject() const
{
  return this;
}

QString QSpace::componentClassName() const
{
  return metaObject()->className();
}

QWidget * QSpace::componentStatusWidget() const
{
  if (!p->space) return 0;

  return p->space->componentStatusWidget();
}

QWidget * QSpace::componentConfigurationWidget()
{
  if (!p->space) return 0;

  return p->space->componentConfigurationWidget();
}

QWidget * QSpace::componentAboutWidget() const
{
  if (!p->space) return 0;

  return p->space->componentAboutWidget();
}

bool QSpace::componentHasProperty( const QString & name ) const
{
  if (!p->space) return false;

  return p->space->componentHasProperty(name);
}

int QSpace::componentPropertyCount() const
{
  if (!p->space) return 0;

  return p->space->componentPropertyCount();
}

QStringList QSpace::componentPropertyNames() const
{
  if (!p->space) return QStringList();

  return p->space->componentPropertyNames();
}

QVariant QSpace::componentProperty( const QString & name,
                                    const QVariant & defaultValue ) const
{
  if (!p->space) return defaultValue;

  return p->space->componentProperty(name,defaultValue);
}

int QSpace::componentSetProperty( const QString & name,
                                   const QVariant & value )
{
  if (!p->space) return -1;

  int result = p->space->componentSetProperty(name,value);

  if ( !result )
  {
    emit spaceUpdated();
    emit spacePropertyUpdated(name,value);
  }

  return result;
}

// SRPS::Space
SRPS::Space * QSpace::clone() const
{
  if (!p->space) return 0;

  return p->space->clone();
}

SRPS::Space * QSpace::cloneUndistributed() const
{
  if (!p->space) return 0;

  return p->space->cloneUndistributed();
}

void QSpace::clear()
{
  if ( !p->space ) return;

  p->space->clear();

  emit spaceUpdated();
  emit spaceCleared();
}

void QSpace::undistribute()
{
  if ( !p->space ) return;

  p->space->undistribute();

  emit spaceUpdated();
  emit spaceUndistributed();
}

// Individual Space interface

int QSpace::individualCount() const
{
  if (!p->space) return 0;

  return p->space->individualCount();
}

QSpace::MIDList QSpace::individualMIDs( const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->individualMIDs(so);
}

QSpace::ConstIndividualList QSpace::constIndividuals( const QSpace::SortOrder & so ) const
{
  ConstIndividualList result;

  foreach( MemoryIdentifier mid, individualMIDs(so) )
    result << toConstIndividual(mid);

  return result;
}

QSpace::IndividualList QSpace::individuals( const QSpace::SortOrder & so ) const
{
  IndividualList result;

  foreach( MemoryIdentifier mid, individualMIDs(so) )
    result << toIndividual(mid);

  return result;
}

int QSpace::distributedIndividualCount() const
{
  if ( !p->space ) return 0;

  return p->space->distributedIndividualCount();
}

QSpace::MIDList QSpace::distributedIndividualMIDs( const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->distributedIndividualMIDs(so);
}

QSpace::ConstIndividualList QSpace::constDistributedIndividuals( const QSpace::SortOrder & so ) const
{
  ConstIndividualList result;

  foreach( MemoryIdentifier mid, distributedIndividualMIDs(so) )
    result << toConstIndividual(mid);

  return result;
}

QSpace::IndividualList QSpace::distributedIndividuals( const QSpace::SortOrder & so ) const
{
  IndividualList result;

  foreach( MemoryIdentifier mid, distributedIndividualMIDs(so) )
    result << toIndividual(mid);

  return result;
}

int QSpace::undistributedIndividualCount() const
{
  if ( !p->space ) return 0;

  return p->space->undistributedIndividualCount();
}

QSpace::MIDList QSpace::undistributedIndividualMIDs( const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->distributedIndividualMIDs(so);
}

QSpace::ConstIndividualList QSpace::constUndistributedIndividuals( const QSpace::SortOrder & so ) const
{
  ConstIndividualList result;

  foreach( MemoryIdentifier mid, undistributedIndividualMIDs(so) )
    result << toConstIndividual(mid);

  return result;
}

QSpace::IndividualList QSpace::undistributedIndividuals( const QSpace::SortOrder & so ) const
{
  IndividualList result;

  foreach( MemoryIdentifier mid, undistributedIndividualMIDs(so) )
    result << toIndividual(mid);

  return result;
}

// Invidual Reference Interface

bool QSpace::individualIsValid( const QSpace::MemoryIdentifier & imid ) const
{
  if (!p->space) return false;

  return p->space->individualIsValid(imid);
}

bool QSpace::individualHasProperty( const QSpace::MemoryIdentifier & imid,
                                    const QString & name ) const
{
  if (!p->space) return false;

  return p->space->individualHasProperty(imid,name);
}

int QSpace::individualPropertyCount( const QSpace::MemoryIdentifier & imid ) const
{
  if (!p->space) return 0;

  return p->space->individualPropertyCount(imid);
}

QStringList QSpace::individualPropertyNames( const QSpace::MemoryIdentifier & imid) const
{
  if (!p->space) return QStringList();

  return p->space->individualPropertyNames(imid);
}

QVariant QSpace::individualProperty( const QSpace::MemoryIdentifier & imid,
                                     const QString & name,
                                     const QVariant & defaultValue ) const
{
  if (!p->space) return defaultValue;

  return p->space->individualProperty(imid,name,defaultValue);
}

QSpace::MemoryIdentifier
    QSpace::individualSpeciesMID( const QSpace::MemoryIdentifier & imid ) const
{
  if (!p->space) return MemoryIdentifier();

  return p->space->individualSpeciesMID(imid);
}

QSpace::ConstSpecies
    QSpace::individualConstSpecies( const QSpace::MemoryIdentifier & imid ) const
{
  return toConstSpecies(individualSpeciesMID(imid));
}

QSpace::MemoryIdentifier
    QSpace::individualCellMID( const QSpace::MemoryIdentifier & imid ) const
{
  if (!p->space) return MemoryIdentifier();

  return p->space->individualCellMID(imid);
}

QSpace::ConstCell
    QSpace::individualConstCell( const QSpace::MemoryIdentifier & imid ) const
{
  return toConstCell(individualCellMID(imid));
}

bool QSpace::individualIsDistributed(const MemoryIdentifier & mid) const
{
  if( !p->space ) return false;

  return p->space->individualIsDistributed(mid);
}

int QSpace::individualSetProperty( const QSpace::MemoryIdentifier & imid,
                                    const QString & name,
                                    const QVariant &  value)
{
  if (!p->space) return false;

  bool result = p->space->individualSetProperty(imid,name,value);

  if ( result )
  {
    emit spaceUpdated();
    emit individualPropertyUpdated(toConstIndividual(imid),name,value);
  }

  return result;
}

// Species Space Interface

int QSpace::speciesCount() const
{
  if (!p->space) return 0;

  return p->space->speciesCount();
}

QSpace::MIDList QSpace::speciesMIDs(const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->speciesMIDs(so);
}

QSpace::ConstSpeciesList QSpace::constSpecies(const QSpace::SortOrder & so ) const
{
  ConstSpeciesList result;

  foreach( MemoryIdentifier mid, speciesMIDs(so) )
    result << toConstSpecies(mid);

  return result;
}

QSpace::SpeciesList QSpace::species(const QSpace::SortOrder & so ) const
{
  SpeciesList result;

  foreach( MemoryIdentifier mid, speciesMIDs(so) )
    result << toSpecies(mid);

  return result;
}

int QSpace::distributedSpeciesCount() const
{
  if ( !p->space ) return 0;

  return p->space->distributedSpeciesCount();
}

QSpace::MIDList QSpace::distributedSpeciesMIDs( const SortOrder & so ) const
{
  if ( !p->space ) return MIDList();

  return p->space->distributedSpeciesMIDs(so);
}

QSpace::ConstSpeciesList QSpace::constDistributedSpecies( const SortOrder & so ) const
{
  ConstSpeciesList result;

  foreach( MemoryIdentifier mid, distributedSpeciesMIDs(so) )
    result << toConstSpecies(mid);

  return result;
}

QSpace::SpeciesList QSpace::distributedSpecies(const SortOrder & so ) const
{
  SpeciesList result;

  foreach( MemoryIdentifier mid, distributedSpeciesMIDs(so) )
    result << toSpecies(mid);

  return result;
}

int QSpace::undistributedSpeciesCount() const
{
  if ( !p->space ) return  0;

  return p->space->undistributedSpeciesCount();
}

QSpace::MIDList QSpace::undistributedSpeciesMIDs( const SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->undistributedSpeciesMIDs(so);
}

QSpace::ConstSpeciesList QSpace::constUndistributedSpecies( const SortOrder & so ) const
{
  ConstSpeciesList result;

  foreach( MemoryIdentifier mid, undistributedSpeciesMIDs(so) )
    result << toConstSpecies(mid);

  return result;
}

QSpace::SpeciesList QSpace::undistributedSpecies( const SortOrder & so ) const
{
  SpeciesList result;

  foreach( MemoryIdentifier mid, undistributedSpeciesMIDs(so) )
    result << toSpecies(mid);

  return result;
}

int QSpace::emptySpeciesCount() const
{
  if ( !p->space ) return 0;

  return p->space->emptySpeciesCount();
}

QSpace::MIDList QSpace::emptySpeciesMIDs( const SortOrder & so ) const
{
  if ( !p->space ) return MIDList();

  return p->space->emptySpeciesMIDs(so);
}

QSpace::ConstSpeciesList QSpace::constEmptySpecies( const SortOrder & so ) const
{
  ConstSpeciesList result;

  foreach( MemoryIdentifier mid, emptySpeciesMIDs(so) )
    result << toConstSpecies(mid);

  return result;
}

QSpace::SpeciesList QSpace::emptySpecies( const SortOrder & so ) const
{
  SpeciesList result;

  foreach( MemoryIdentifier mid, emptySpeciesMIDs(so) )
    result << toSpecies(mid);

  return result;
}

int QSpace::nonEmptySpeciesCount() const
{
  if ( !p->space ) return  0;

  return p->space->nonEmptySpeciesCount();
}

QSpace::MIDList QSpace::nonEmptySpeciesMIDs( const SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->nonEmptySpeciesMIDs(so);
}

QSpace::ConstSpeciesList QSpace::constNonEmptySpecies( const SortOrder & so ) const
{
  ConstSpeciesList result;

  foreach( MemoryIdentifier mid, nonEmptySpeciesMIDs(so) )
    result << toConstSpecies(mid);

  return result;
}

QSpace::SpeciesList QSpace::nonEmptySpecies( const SortOrder & so ) const
{
  SpeciesList result;

  foreach( MemoryIdentifier mid, nonEmptySpeciesMIDs(so) )
    result << toSpecies(mid);

  return result;
}

QSpace::MemoryIdentifier QSpace::createSpeciesMID()
{
  if (!p->space) return MemoryIdentifier();

  MemoryIdentifier result = p->space->createSpeciesMID();

  if ( result != MemoryIdentifier() )
  {
    emit spaceUpdated();
    emit speciesCreated(toConstSpecies(result));
  }

  return result;
}

QSpace::Species QSpace::createSpecies()
{
  return toSpecies(createSpeciesMID());
}

bool QSpace::deleteSpecies( const QSpace::MemoryIdentifier & smid )
{
  if (!p->space) return 0;

  bool result = p->space->deleteSpecies(smid);

  if ( result )
  {
    emit spaceUpdated();
    emit speciesDeleted(toConstSpecies(smid));
  }

  return result;
}

bool QSpace::deleteSpecies( const QSpace::ConstSpecies & s )
{
  if ( !isReferenceFromThis(s) ) return false;

  if (!p->space) return false;

  return deleteSpecies(s.mid());
}

int QSpace::deleteAllSpecies()
{
  if ( !p->space ) return  0;

  int result = p->space->deleteAllSpecies();

  if ( result )
  {
    emit spaceUpdated();
    emit allSpeciesDeleted();
  }

  return result;
}

int QSpace::deleteDistributedSpecies()
{
  if ( !p->space ) return  0;

  int result = p->space->deleteDistributedSpecies();

  if ( result )
  {
    emit spaceUpdated();
    emit distributedSpeciesDeleted();
  }

  return result;
}

int QSpace::deleteUndistributedSpecies()
{
  if ( !p->space ) return  0;

  int result = p->space->deleteUndistributedSpecies();

  if ( result )
  {
    emit spaceUpdated();
    emit undistributedSpeciesDeleted();
  }

  return result;
}

int QSpace::deleteEmptySpecies()
{
  if ( !p->space ) return  0;

  int result = p->space->deleteEmptySpecies();

  if ( result )
  {
    emit spaceUpdated();
    emit emptySpeciesDeleted();
  }

  return result;
}

int QSpace::deleteNonEmptySpecies()
{
  if ( !p->space ) return  0;

  int result = p->space->deleteNonEmptySpecies();

  if ( result )
  {
    emit spaceUpdated();
    emit nonEmptySpeciesDeleted();
  }

  return result;
}

// Species Reference Interface

bool QSpace::speciesIsValid( const QSpace::MemoryIdentifier & smid ) const
{
  if (!p->space) return false;

  return p->space->speciesIsValid(smid);
}

bool QSpace::speciesHasProperty( const QSpace::MemoryIdentifier & smid,
                                 const QString & name ) const
{
  if (!p->space) return false;

  return p->space->speciesHasProperty(smid,name);
}

int QSpace::speciesPropertyCount( const QSpace::MemoryIdentifier & smid ) const
{
  if (!p->space) return 0;

  return p->space->speciesPropertyCount(smid);
}

QStringList QSpace::speciesPropertyNames( const QSpace::MemoryIdentifier & smid ) const
{
  if (!p->space) return QStringList();

  return p->space->speciesPropertyNames(smid);
}

QVariant QSpace::speciesProperty( const QSpace::MemoryIdentifier & smid,
                                  const QString & name,
                                  const QVariant & defaultValue ) const
{
  if (!p->space) return defaultValue;

  return p->space->speciesProperty(smid,name,defaultValue);
}

int QSpace::speciesIndividualCount( const QSpace::MemoryIdentifier & smid ) const
{
  if (!p->space) return 0;

  return p->space->speciesIndividualCount(smid);
}

QSpace::MIDList
    QSpace::speciesIndividualMIDs( const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->speciesIndividualMIDs(smid,so);
}

QSpace::ConstIndividualList
    QSpace::speciesConstIndividuals( const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  ConstIndividualList result;

  foreach( MemoryIdentifier mid, speciesIndividualMIDs(smid,so) )
    result << toConstIndividual(mid);

  return result;
}

QSpace::IndividualList
    QSpace::speciesIndividuals( const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  IndividualList result;

  foreach( MemoryIdentifier mid, speciesIndividualMIDs(smid,so) )
    result << toIndividual(mid);

  return result;
}

int QSpace::speciesDistributedIndividualCount(
    const QSpace::MemoryIdentifier & mid) const
{
  if ( !p->space ) return  0;

  return p->space->speciesDistributedIndividualCount(mid);
}

QSpace::MIDList QSpace::speciesDistributedIndividualMIDs(
    const QSpace::MemoryIdentifier & mid,
    const QSpace::SortOrder & so ) const
{
  if ( !p->space ) return  MIDList();

  return p->space->speciesDistributedIndividualMIDs(mid,so);
}

QSpace::ConstIndividualList
    QSpace::speciesDistributedConstIndividuals(const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  ConstIndividualList result;

  foreach( MemoryIdentifier mid, speciesDistributedIndividualMIDs(smid,so) )
    result << toConstIndividual(mid);

  return result;
}

int QSpace::speciesUndistributedIndividualCount(
    const QSpace::MemoryIdentifier & mid) const
{
  if ( !p->space ) return  0;

  return p->space->speciesUndistributedIndividualCount(mid);
}

QSpace::MIDList QSpace::speciesUndistributedIndividualMIDs(
    const QSpace::MemoryIdentifier & mid,
    const QSpace::SortOrder & so ) const
{
  if ( !p->space ) return  MIDList();

  return p->space->speciesUndistributedIndividualMIDs(mid,so);
}

QSpace::ConstIndividualList
    QSpace::speciesUndistributedConstIndividuals(const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  ConstIndividualList result;

  foreach( MemoryIdentifier mid, speciesUndistributedIndividualMIDs(smid,so) )
    result << toConstIndividual(mid);

  return result;
}

bool QSpace::speciesIsDistributed( const QSpace::MemoryIdentifier & smid ) const
{
  if ( !p->space ) return false;

  return p->space->speciesIsDistributed(smid);
}

bool QSpace::speciesIsUndistributed( const QSpace::MemoryIdentifier & smid ) const
{
  if ( !p->space ) return false;

  return p->space->speciesIsUndistributed(smid);
}

int QSpace::speciesDistributedCellCount( const QSpace::MemoryIdentifier & mid ) const
{
  if( !p->space ) return 0;

  return p->space->speciesDistributedCellCount(mid);
}

QSpace::MIDList QSpace::speciesDistributedCellMIDs(
    const QSpace::MemoryIdentifier & smid,
    const QSpace::SortOrder & so ) const
{
  if ( !p->space ) return MIDList();

  return p->space->speciesDistributedCellMIDs(smid,so);
}

QSpace::ConstCellList QSpace::speciesDistributedConstCells(
    const QSpace::MemoryIdentifier & smid,
    const QSpace::SortOrder & so ) const
{
  ConstCellList result;

  foreach( MemoryIdentifier mid, speciesDistributedCellMIDs(smid,so) )
    result << toConstCell(mid);

  return result;
}

int QSpace::speciesSetProperty( const QSpace::MemoryIdentifier & smid,
                                 const QString & name,
                                 const QVariant & value )
{
  if (!p->space) return -1;

  int result = p->space->speciesSetProperty(smid,name,value);

  if ( !result )
  {
    emit spaceUpdated();
    emit speciesUpdated(toConstSpecies(smid));
    emit speciesPropertyUpdated(toConstSpecies(smid),name,value);
  }

  return result;
}

QSpace::MemoryIdentifier QSpace::speciesCreateIndividualMID(
    const QSpace::MemoryIdentifier & smid )
{
  if (!p->space) return MemoryIdentifier(0);

  MemoryIdentifier result = p->space->speciesCreateIndividualMID(smid);

  if ( p->space->individualIsValid(result) )
  {
    emit spaceUpdated();
    emit speciesUpdated(toConstSpecies(smid));
    emit individualCreated(toConstIndividual(result));
  }

  return result;
}

QSpace::Individual QSpace::speciesCreateIndividual(
    const QSpace::MemoryIdentifier & smid )
{
  return toIndividual(speciesCreateIndividualMID(smid));
}

bool QSpace::speciesDeleteIndividual(
    const MemoryIdentifier &smid, const MemoryIdentifier & imid )
{
  if ( !p->space ) return false;

  bool result = p->space->speciesDeleteIndividual(smid,imid);

  if ( result )
  {
    emit individualDeleted(toConstIndividual(imid));
  }

  return result;
}

int QSpace::speciesDeleteIndividuals(const MemoryIdentifier & smid)
{
  if ( !p->space ) return 0;

  int result = p->space->speciesDeleteIndividuals(smid);

  if ( result )
  {
    emit spaceUpdated();
    emit speciesIndividualsDeleted(toConstSpecies(smid));
  }

  return result;
}

int QSpace::speciesDeleteDistributedIndividuals(const MemoryIdentifier &smid)
{
  if ( !p->space ) return 0;

  int result = p->space->speciesDeleteDistributedIndividuals(smid);

  if ( result )
  {
    emit spaceUpdated();
    emit speciesDistributedIndividualsDeleted(toConstSpecies(smid));
  }

  return result;
}

int QSpace::speciesDeleteUndistributedIndividuals(const MemoryIdentifier &smid)
{
  if ( !p->space ) return 0;

  int result = p->space->speciesDeleteUndistributedIndividuals(smid);

  if ( result )
  {
    emit spaceUpdated();
    emit speciesUndistributedIndividualsDeleted(toConstSpecies(smid));
  }

  return result;
}

QSpace::IndividualList
    QSpace::speciesDistributedIndividuals(const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  IndividualList result;

  foreach( MemoryIdentifier mid, speciesDistributedIndividualMIDs(smid,so) )
    result << toIndividual(mid);

  return result;
}

QSpace::IndividualList
    QSpace::speciesUndistributedIndividuals(const QSpace::MemoryIdentifier & smid,
                                   const QSpace::SortOrder & so ) const
{
  IndividualList result;

  foreach( MemoryIdentifier mid, speciesUndistributedIndividualMIDs(smid,so) )
    result << toIndividual(mid);

  return result;
}

int QSpace::speciesUndistributeCells( const MemoryIdentifier & smid )
{
  if ( !p->space ) return  0;

  bool result = p->space->speciesUndistributeCells(smid);

  if ( result )
  {
    emit spaceUpdated();
    emit speciesUndistributed(toConstSpecies(smid));
  }

  return result;
}

int QSpace::speciesUndistributeIndividuals( const QSpace::MemoryIdentifier & smid )
{
  if ( !p->space ) return  0;

  bool result = p->space->speciesUndistributeIndividuals(smid);
  if ( result )
  {
    emit spaceUpdated();
    emit speciesUndistributed(toConstSpecies(smid));
  }

  return result;
}

// Cell Sapce Interface

int QSpace::cellCount() const
{
  if (!p->space) return 0;

  return p->space->cellCount();
}

QSpace::MIDList QSpace::cellMIDs( const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->cellMIDs(so);
}

QSpace::ConstCellList QSpace::constCells( const SortOrder & so ) const
{
  ConstCellList result;

  foreach( MemoryIdentifier mid, cellMIDs(so) )
    result << toConstCell(mid);

  return result;
}

QSpace::CellList QSpace::cells( const SortOrder & so ) const
{
  CellList result;

  foreach( MemoryIdentifier mid, cellMIDs(so) )
    result << toCell(mid);

  return result;
}

int QSpace::emptyCellCount() const
{
  if (!p->space) return 0;

  return p->space->emptyCellCount();
}

QSpace::MIDList QSpace::emptyCellMIDs(const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->emptyCellMIDs(so);
}

QSpace::ConstCellList QSpace::emptyConstCells( const SortOrder & so ) const
{
  ConstCellList result;

    foreach( MemoryIdentifier mid, emptyCellMIDs(so) )
      result << toConstCell(mid);

    return result;
}

QSpace::CellList QSpace::emptyCells( const SortOrder & so ) const
{
  CellList result;

    foreach( MemoryIdentifier mid, emptyCellMIDs(so) )
      result << toCell(mid);

    return result;
}

int QSpace::nonEmptyCellCount() const
{
  if (!p->space) return 0;

  return p->space->nonEmptyCellCount();
}

QSpace::MIDList QSpace::nonEmptyCellMIDs(const QSpace::SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->nonEmptyCellMIDs(so);
}

QSpace::ConstCellList QSpace::nonEmptyConstCells( const SortOrder & so ) const
{
  ConstCellList result;

    foreach( MemoryIdentifier mid, nonEmptyCellMIDs(so) )
      result << toConstCell(mid);

    return result;
}

QSpace::CellList QSpace::nonEmptyCells( const SortOrder & so ) const
{
  CellList result;

    foreach( MemoryIdentifier mid, nonEmptyCellMIDs(so) )
      result << toCell(mid);

    return result;
}

QSpace::MemoryIdentifier QSpace::createCellMID()
{
  if (!p->space) return MemoryIdentifier();

  MemoryIdentifier result = p->space->createCellMID();

  if ( result != MemoryIdentifier() )
  {
    emit spaceUpdated();
    emit cellCreated(toConstCell(result));
  }

  return result;
}

QSpace::Cell QSpace::createCell()
{
  return toCell(createCellMID());
}

bool QSpace::deleteCell( const QSpace::MemoryIdentifier & cmid)
{
  if (!p->space) return false;

  bool result = p->space->deleteCell(cmid);

  if ( result )
  {
    emit spaceUpdated();
    emit cellDeleted(toConstCell(cmid));
  }

  return result;
}

bool QSpace::deleteCell( const QSpace::ConstCell & cell)
{
  if ( !isReferenceFromThis(cell) ) return false;

  return p->space->deleteCell(cell.mid());
}

int QSpace::deleteAllCells()
{
  if( !p->space ) return 0;

  int result = p->space->deleteAllCells();

  if ( result )
  {
    emit spaceUpdated();
    emit cellsDeleted();
  }

  return result;
}

int QSpace::deleteEmptyCells()
{
  if( !p->space ) return 0;

  int result = p->space->deleteEmptyCells();

  if ( result )
  {
    emit spaceUpdated();
    emit emptyCellsDeleted();
  }

  return result;
}

int QSpace::deleteNonEmptyCells()
{
  if( !p->space ) return 0;

  int result = p->space->deleteNonEmptyCells();

  if ( result )
  {
    emit spaceUpdated();
    emit nonEmptyCellsDeleted();
  }

  return result;
}

// Cell Reference Interface

bool QSpace::cellIsValid( const QSpace::MemoryIdentifier & cmid ) const
{
  if (!p->space) return false;

  return p->space->cellIsValid(cmid);
}

bool QSpace::cellHasProperty( const QSpace::MemoryIdentifier & cmid,
                              const QString & name ) const
{
  if (!p->space) return false;

  return p->space->cellHasProperty(cmid,name);
}

int QSpace::cellPropertyCount( const QSpace::MemoryIdentifier & cmid ) const
{
  if (!p->space) return 0;

  return p->space->cellPropertyCount(cmid);
}

QStringList
    QSpace::cellPropertyNames( const QSpace::MemoryIdentifier & cmid) const
{
  if (!p->space) return QStringList();

  return p->space->cellPropertyNames(cmid);
}

QVariant QSpace::cellProperty( const QSpace::MemoryIdentifier & cmid,
                               const QString & name,
                               const QVariant & defaultValue ) const
{
  if (!p->space) return defaultValue;

  return p->space->cellProperty(cmid,name,defaultValue);
}

int QSpace::cellIndividualCount( const QSpace::MemoryIdentifier & cmid ) const
{
  if (!p->space) return 0;

  return p->space->cellIndividualCount(cmid);
}

int QSpace::cellSpeciesIndividualCount( const QSpace::MemoryIdentifier & cmid,
                                        const QSpace::MemoryIdentifier & smid ) const
{
  if (!p->space) return 0;

  return p->space->cellSpeciesIndividualCount(cmid,smid);
}

QSpace::MIDList
    QSpace::cellIndividualMIDs( const QSpace::MemoryIdentifier & cmid,
                                const SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->cellIndividualMIDs(cmid,so);
}

QSpace::ConstIndividualList QSpace::cellConstIndividuals( const MemoryIdentifier & cmid,
                                          const SortOrder & so ) const
{
  ConstIndividualList result;

    foreach( MemoryIdentifier mid, cellIndividualMIDs(cmid,so) )
      result << toConstIndividual(mid);

    return result;
}

QSpace::MIDList QSpace::cellSpeciesIndividualMIDs( const MemoryIdentifier & cmid,
                                           const MemoryIdentifier & smid,
                                           const SortOrder & so ) const
{
  if ( !p->space ) return MIDList();

  return p->space->cellSpeciesIndividualMIDs(cmid,smid,so);
}

QSpace::ConstIndividualList QSpace::cellSpeciesConstIndividuals( const MemoryIdentifier & cmid,
                                                         const MemoryIdentifier & smid,
                                          const SortOrder & so ) const
{
  ConstIndividualList result;

    foreach( MemoryIdentifier mid, cellSpeciesIndividualMIDs(cmid,smid,so) )
      result << toConstIndividual(mid);

    return result;
}

int QSpace::cellSpeciesCount( const QSpace::MemoryIdentifier & cmid ) const
{
  if (!p->space) return 0;

  return p->space->cellSpeciesCount(cmid);
}

bool QSpace::cellHasSpecies( const QSpace::MemoryIdentifier & cmid,
                             const QSpace::MemoryIdentifier & smid ) const
{
  if (!p->space) return false;

  return p->space->cellHasSpecies(cmid,smid);
}

QSpace::MIDList QSpace::cellSpeciesMIDs( const MemoryIdentifier & cmid,
                                         const SortOrder & so ) const
{
  if (!p->space) return MIDList();

  return p->space->cellSpeciesMIDs(cmid,so);
}

QSpace::ConstSpeciesList QSpace::cellConstSpecies( const MemoryIdentifier & cmid,
                                         const SortOrder & so ) const
{
  ConstSpeciesList result;

    foreach( MemoryIdentifier mid, cellSpeciesMIDs(cmid,so) )
      result << toConstSpecies(mid);

    return result;
}

int QSpace::cellSetProperty( const QSpace::MemoryIdentifier & cmid,
                              const QString & name,
                              const QVariant & value )
{
  if (!p->space) return false;

  bool result = p->space->cellSetProperty(cmid,name,value);

  if ( !result )
  {
    emit spaceUpdated();
    emit cellPropertyUpdated(toConstCell(cmid),name,value);
  }

  return result;
}

QSpace::IndividualList QSpace::cellIndividuals(const MemoryIdentifier &cmid, const SortOrder &so) const
{
  IndividualList result;

    foreach( MemoryIdentifier mid, cellIndividualMIDs(cmid,so) )
      result << toIndividual(mid);

    return result;
}

QSpace::IndividualList QSpace::cellSpeciesIndividuals(const MemoryIdentifier &cmid,
                                               const MemoryIdentifier &smid,
                                               const SortOrder &so) const
{
  IndividualList result;

    foreach( MemoryIdentifier mid, cellSpeciesIndividualMIDs(cmid,smid,so) )
      result << toIndividual(mid);

    return result;
}

bool QSpace::cellInsertIndividual( const QSpace::MemoryIdentifier & cmid,
                                   const QSpace::MemoryIdentifier & smid,
                                   const QSpace::MemoryIdentifier & imid )
{
  if (!p->space) return false;

  bool result = p->space->cellInsertIndividual(cmid,smid,imid);

  if ( result )
  {
    emit spaceUpdated();
    emit cellUpdated(toConstCell(cmid));
    emit cellIndividualInserted(toConstCell(cmid),toConstIndividual(imid));
  }

  return result;
}

bool QSpace::cellRemoveIndividual( const QSpace::MemoryIdentifier & cmid,
                                   const QSpace::MemoryIdentifier & smid,
                                   const QSpace::MemoryIdentifier & imid )
{
  if (!p->space) return false;

  bool result = p->space->cellRemoveIndividual(cmid,smid,imid);

  if ( result )
  {
    emit spaceUpdated();
    emit cellUpdated(toConstCell(cmid));
    emit cellIndividualRemoved(toConstCell(cmid),toConstIndividual(imid));
  }

  return result;
}

int QSpace::cellRemoveIndividuals(const MemoryIdentifier & cmid)
{
  if( !p->space ) return 0;

  int result = p->space->cellRemoveIndividuals(cmid);

  if( result )
  {
    emit spaceUpdated();
    emit cellCleared(toConstCell(cmid));
  }

  return result;
}

int QSpace::cellRemoveSpecies(const QSpace::MemoryIdentifier & cmid,
                              const QSpace::MemoryIdentifier & smid )
{
  if ( !p->space ) return 0;

  int result = p->space->cellRemoveSpecies(cmid,smid);

  if ( result )
  {
    emit spaceUpdated();
    emit cellCleared(toConstCell(cmid));
  }

  return result;
}

int QSpace::cellRemoveAllSpecies(const QSpace::MemoryIdentifier & cmid )
{
  if ( !p->space ) return 0;

  int result = p->space->cellRemoveAllSpecies(cmid);

  if ( result )
  {
    emit spaceUpdated();
    emit cellCleared(toConstCell(cmid));
  }

  return result;
}

} // namespace SRPS
