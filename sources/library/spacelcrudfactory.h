/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACELCRUDFACTORY_H
#define SRPS_SPACELCRUDFACTORY_H

#include "srps_global.h"
#include <QtCore/QStringList>
#include <QtCore/QVariantHash>

namespace SRPS
{

class SpaceLCRUD;

/*!
  \class SRPS::SpaceLCRUDFactory spacefactory.h SRPS/SpaceLCRUDFactory
  \brief Abstract interface SRPS::SpaceLCRUD Factories

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SpaceLCRUDFactory
{
  SRPS_DISABLE_COPY(SpaceLCRUDFactory)

protected:

  SpaceLCRUDFactory();

public:

  virtual ~SpaceLCRUDFactory();

  /*!
    \brief Returns the available SpaceLCRUD class names for creation.
    \return An empty list, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList spaceLCRUDFactoryKeys() const;

  /*!
    \brief Creates the specified SpaceLCRUD component.
    \param component SpaceLCRUD's class name.
    \param parameters SpaceLCRUD's creation paremeters.
    \return 0, default implementation.
    \note If the provied parameters aren't valid, the method must use defaults \
    parameters.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual SRPS::SpaceLCRUD *
      spaceLCRUDFactoryCreate( const QString & spaceLCRUD,
                            const QVariantHash & parameters
                                                  = QVariantHash() );
};

}

Q_DECLARE_INTERFACE(SRPS::SpaceLCRUDFactory,"SRPS::SpaceLCRUDFactory/1.0.0")

#endif // SPACEFACTORY_H
