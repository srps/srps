/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_RESOURCELOADER_H
#define SRPS_RESOURCELOADER_H

#include "srps_global.h"
#include <QtCore/QObject>
#include <QtCore/QStringList>

namespace SRPS {

/*!
  \class SRPS::ResourceLoader resourceloader.h SRPS/ResourceLoader
  \brief Abstract interface for Qt resource loading, .rcc files.

  Default implementations returns default values and does nothing.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT ResourceLoader : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(ResourceLoader)

  Q_PROPERTY( QStringList loadedResources READ loadedResources )

protected:

  explicit ResourceLoader(QObject *parent = 0);

public:

  virtual ~ResourceLoader();

  /*!
    \brief List loaded resources.
    \return List loaded resources.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QStringList loadedResources() const;

  /*!
    \brief Map root of resource.
    \param path, loaded resource.
    \return Map root of resource.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QString mapRootFor( const QString & path ) const;

signals:

  /*!
    \brief Emited when a new resource file was loaded.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  void loadedResource( const QString & path,
                       const QString & mapRoot = QString() );

  /*!
    \brief Emited when a loaded resource file was unloaded.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  void unloadedResource( const QString & path,
                         const QString & mapRoot = QString() );

public slots:

  /*!
    \brief Load a dir
    \param path, dir path.
    \return loaded resource count.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int loadDir( const QString & path );

  /*!
    \brief Load a resource file.
    \param path, resource file path.
    \param mapRoot, prefix of resource.
    \return true if resource was loaded. loadedResource() signal must be emited.
    \return false, if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool loadResource( const QString & path,
                             const QString & mapRoot = QString() );

  /*!
    \brief Unload a previous loaded resourve file.
    \param path, loaded resource file path.
    \param mapRoot, prefix of resource.
    \return true if resource was unloaded. unloadedResource() signal must be emited.
    \return false, if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool unloadResource( const QString & path,
                               const QString & mapRoot = QString() );

};

} // namespace SRPS

#endif // SRPS_RESOURCELOADER_H
