/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_COMPONENTSTATUSWIDGET_H
#define SRPS_COMPONENTSTATUSWIDGET_H

#include "srps_global.h"

namespace SRPS {

/*!
  \class SRPS::ComponentStatusWidget \
         componentfactorywidget.h \
         SRPS/ComponentStatusWidget
  \brief Abstract interface SRPS::Component Status Widget

  A SRPS::ComponentStatusWidget shows to user a Component's state

  All funcitons are implemented to do nothing or return default values.
  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT ComponentStatusWidget
{
protected:

  ComponentStatusWidget();

public:

  virtual ~ComponentStatusWidget();

  /*!
    \brief Reload component status

    Default implementation does nothing.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual void componentStatusWidgetReload();

};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::ComponentStatusWidget,
                    "SRPS::ComponentStatusWidget/1.0.0");

#endif // SRPS_COMPONENTSTATUSWIDGET_H
