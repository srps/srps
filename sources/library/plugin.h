/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGIN_H
#define SRPS_PLUGIN_H

#include "srps_global.h"

namespace SRPS
{

class SettingsManager;

/*!
  \class SRPS::Plugin plugin.h SRPS/Plugin
  \brief Abstract interface for SRPS Plugins.

  Default implementations returns default values and does nothing.

  PluginLoader calls pluginInitialize() and then pluginReadSettings() when a
  plugin is loaded. And pluginFinalize() and then pluginWriteSettings() when
  the plugin is unloaded.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT Plugin
{
  SRPS_DISABLE_COPY(Plugin)

protected:

  Plugin();

public:
  virtual ~Plugin();

  /*!
    \brief Return last error string.
    \return Return last error string.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QString pluginErrorString() const;

  /*!
    \brief Check if plugin was initialized.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool pluginIsInitialized() const;

  /*!
    \brief Check if plugin was finalized.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool pluginIsFinalized() const;

  /*!
    \brief Read settings
    \param sm, SettingsManager
    \return true if settigns was readed.
    \return false if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool pluginReadSettings( const SettingsManager * sm );

  /*!
    \brief Tries to initialize plugin.
    \return true if plugin was initialized.
    \return false if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool pluginInitialize();

  /*!
    \brief Tries to finalize plugin.
    \return true if plugin was finalized.
    \return false if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool pluginFinalize();

  /*!
    \brief Write settings
    \param sm, SettingsManager
    \return true if settigns was writed.
    \return false if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool pluginWriteSettings( SettingsManager * sm );
};

} // namespace SRPS

#include <QtPlugin>
Q_DECLARE_INTERFACE(SRPS::Plugin,"SRPS::Plugin/1.0.0")

#endif // SRPS_PLUGIN_H
