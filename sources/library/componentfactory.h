/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_COMPONENTFACTORY_H
#define SRPS_COMPONENTFACTORY_H

#include "srps_global.h"
#include <QtCore/QVariantHash>

namespace SRPS {

/*!
  \class SRPS::ComponentFactory componentfactory.h SRPS/ComponentFactory
  \brief Abstract interface SRPS::Component Factories

  A SRPS::ComponentFactory can create SRPS::Components, it must provide a
  SRPS::Component class name list that can create.

  The ComponentFactory could provide a graphical user interface for configuring
  and specified component construction and manage default parameters for
  component construction.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT ComponentFactory
{
  SRPS_DISABLE_COPY(ComponentFactory)

protected:

      ComponentFactory();

public:

  virtual ~ComponentFactory();

  /*!
    \brief Create a GUI for configuring the specified component's creation \
    parameters.
    \return 0, default implementation.
    \note The returned QWidget MUST implements and declare use of \
    SRPS::ComponentFactoryWidget
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QWidget * componentFactoryWidgetFor(const QString & key) const;

  /*!
    \brief Returns the current default creation parameters for the speficied \
    component.
    \return An empty QVariantHash, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QVariantHash componentFactoryDefaultParametersFor(const QString & key) const;

  /*!
    \brief Change the current default creation parameters for the speficied
    component.
    \return false, default implementation.
    \return True if and only if the all parameters are valid and doesn't contains
    unnecessary parameters.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool componentFactorySetDefaultParametersFor(const QString & key,
                                              const QVariantHash & parameters);
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::ComponentFactory,"SRPS::ComponentFactory/1.0.0");

#endif // SRPS_COMPONENTFACTORY_H
