/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACERW_H
#define SRPS_SPACERW_H

#include "component.h"

class QIODevice;
class QString;

namespace SRPS {

class Space;

/*!
  \class SRPS::SpaceRW spacerw.h SRPS/SpaceRW
  \brief Abstract Space Read/Writer Component.

  Component for Space reading and writing to streams.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SpaceRW : public Component
{
  SRPS_DISABLE_COPY(SpaceRW)

protected:

  SpaceRW();

public:

  virtual ~SpaceRW();

  /*!
    \brief Prepare reading from stream.

    \param device Stream device
    \param space Target Space.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareSpaceRead( QIODevice * device, SRPS::Space * space ) = 0;

  /*!
    \brief Read a space from prepared stream into prepared space

    \return Result code.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int readSpace() = 0;

  /*!
    \brief Prepare writing from stream.

    \param device Stream device
    \param space Target Space.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareSpaceWrite( const SRPS::Space * space, QIODevice * device ) = 0;

  /*!
    \brief Write prepared space into prepared device

    \return Result code.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int writeSpace() = 0;

};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::SpaceRW,"SRPS::SpaceRW/1.0.0");

#endif // SPACERW_H
