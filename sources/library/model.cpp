/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "model.h"

namespace SRPS {

Model::Model()
  : Component()
{
}

Model::~Model()
{
}

bool Model::needSourceSpace() const
{
  return false;
}

void Model::setSourceSpace( const SRPS::Space * /*space*/ )
{

}

bool Model::needSourceCells() const
{
  return false;
}

void Model::setSourceCells( const SRPS::Space::ConstCellList & /*cells*/ )
{

}

bool Model::needSourceCellMIDs() const
{
  return false;
}

void Model::setSourceCellMIDs( const SRPS::Space::MIDList & /*cells*/ )
{

}

bool Model::needSourceSpecies() const
{
  return false;
}

void Model::setSourceSpeciess( const SRPS::Space::ConstSpeciesList & /*species*/ )
{

}

bool Model::needSourceSpeciesMIDs() const
{
  return false;
}

void Model::setSourceSpeciesMIDs( const SRPS::Space::MIDList & /*species*/ )
{

}

bool Model::needSourceIndividuals() const
{
  return false;
}

void Model::setSourceIndividuals( const SRPS::Space::ConstIndividualList & /*individuals*/ )
{

}

bool Model::needSourceIndividualMIDs() const
{
  return false;
}

void Model::setSourceIndividualsMIDs( const SRPS::Space::MIDList & /*species*/ )
{

}

//

bool Model::needTargetSpace() const
{
  return false;
}

void Model::setTargetSpace( SRPS::Space * /*space*/ )
{

}

bool Model::needTargetCells() const
{
  return false;
}

void Model::setTargetCells( const SRPS::Space::CellList & /*cells*/ )
{

}

bool Model::needTargetCellMIDs() const
{
  return false;
}

void Model::setTargetCellMIDs( const SRPS::Space::MIDList & /*cells*/ )
{

}

bool Model::needTargetSpecies() const
{
  return false;
}

void Model::setTargetSpeciess( const SRPS::Space::SpeciesList & /*species*/ )
{

}

bool Model::needTargetSpeciesMIDs() const
{
  return false;
}

void Model::setTargetSpeciesMIDs( const SRPS::Space::MIDList & /*species*/ )
{

}

bool Model::needTargetIndividuals() const
{
  return false;
}

void Model::setTargetIndividuals( const SRPS::Space::IndividualList & /*individuals*/ )
{

}

bool Model::needTargetIndividualMIDs() const
{
  return false;
}

void Model::setTargetIndividualsMIDs( const SRPS::Space::MIDList & /*species*/ )
{

}

bool Model::needPRNG() const
{
  return false;
}

void Model::setPRNG( SRPS::PRNG * /*prng*/ )
{

}

bool Model::needDPRNG() const
{
  return false;
}

void Model::setDPRNG( SRPS::DPRNG * /*dprng*/ )
{

}

} // namespace SRPS
