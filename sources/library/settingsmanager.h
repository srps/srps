/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SETTINGSMANAGER_H
#define SRPS_SETTINGSMANAGER_H

#include "srps_global.h"

#include <QtCore/QStringList>
#include <QtCore/QVariant>

namespace SRPS {

/*!
  \class SRPS::SettingsManager settingsmanager.h SRPS/SettingsManager
  \brief Abstract interface for settings manager

  Default implementations returns default values and does nothing.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SettingsManager
{
  SRPS_DISABLE_COPY(SettingsManager)

protected:

  SettingsManager();

public:

  virtual ~SettingsManager();

  /*!
    \brief Check if the object is valid.
    \return True is values can be saved or retrived.
    \return False if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool isValid() const;

  /*!
    \brief List of key values.
    \return List of key values.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QStringList keys()const;

  /*!
    \brief Update or delete a key.
    If \c value is a invalid QVariant it means delete.
    \param key, target key.
    \param value, target value
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual void setValue ( const QString & key,
                          const QVariant & value );

  /*!
    \brief Query a key value.
    \param key, target key.
    \param defaultValue, return fallback value.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QVariant value ( const QString & key,
                           const QVariant & defaultValue = QVariant() ) const;

};

} // namespace SRPS

#endif // SRPS_SETTINGSMANAGER_H
