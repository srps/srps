/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVELCRUDFACTORY_H
#define SRPS_CURVELCRUDFACTORY_H

#include "srps_global.h"
#include <QtCore/QStringList>
#include <QtCore/QVariantHash>

namespace SRPS
{

class CurveLCRUD;

/*!
  \class SRPS::CurveLCRUDFactory curvefactory.h SRPS/CurveLCRUDFactory
  \brief Abstract interface SRPS::CurveLCRUD Factories

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT CurveLCRUDFactory
{
  SRPS_DISABLE_COPY(CurveLCRUDFactory)

protected:

  CurveLCRUDFactory();

public:

  virtual ~CurveLCRUDFactory();

  /*!
    \brief Returns the available CurveLCRUD class names for creation.
    \return An empty list, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList curveLCRUDFactoryKeys() const;

  /*!
    \brief Creates the specified CurveLCRUD component.
    \param component CurveLCRUD's class name.
    \param parameters CurveLCRUD's creation paremeters.
    \return 0, default implementation.
    \note If the provied parameters aren't valid, the method must use defaults \
    parameters.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual SRPS::CurveLCRUD *
      curveLCRUDFactoryCreate( const QString & curveLCRUD,
                            const QVariantHash & parameters
                                                  = QVariantHash() );
};

}

Q_DECLARE_INTERFACE(SRPS::CurveLCRUDFactory,"SRPS::CurveLCRUDFactory/1.0.0")

#endif // CURVEFACTORY_H
