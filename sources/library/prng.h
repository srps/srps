/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PRNG_H
#define SRPS_PRNG_H

#include "component.h"
#include "abstractprng.h"

namespace SRPS {

/*!
  \class SRPS::PRNG prng.h SRPS/PRNG
  \brief Abstract interface for all Pseudo-Random Number Generators.

  In [0,1] Interval Only.

  PRNG is stream-oriented generator.

  Default implementation does nothing and returns invalid results.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT PRNG : public Component, public AbstractPRNG
{
    SRPS_DISABLE_COPY(PRNG)

protected:

        PRNG();

public:

    virtual ~PRNG();

    /*!
      \brief Current seed of generator.
      \return Binaty seed.
      \note It can be portable but subclass specific.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual QByteArray seed() const;

    /*!
      \brief Set binary seed of generator.
      \return True if the generator's seed was changed.
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual bool setSeed( const QByteArray & s );

    /*!
      \brief Change the generator strem.
      \return true if generator stream was changed.
      \return false if not.
      \note Changing stream change seed And the seed must be the initial of the
      stream. Useful for reproducing sequences.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual bool setStream( quint32 stream );
};

} // namespace

#endif // PRNG_H
