/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACEEDITOR_H
#define SRPS_SPACEEDITOR_H

#include "component.h"

namespace SRPS {

class Space;

/*!
  \class SRPS::SpaceEditor spaceeditor.h SRPS/SpaceEditor
  \brief Abstract SpaceEditor Component.

  Component for Space editing, intended to be subclassed for a QWidget

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SpaceEditor : public Component
{
  SRPS_DISABLE_COPY(SpaceEditor)

protected:

  SpaceEditor();

public:

  virtual ~SpaceEditor();

  /*!
    \brief Current Edited Space.
    \return The current edited Space.
    \return null if component is not editing a space.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual SRPS::Space * spaceEdited() const;

  /*!
    \brief Change the Current Edited Space.
    \param space Target SRPS::Space for editing.
    \return true if \c space is been edited.
    \return false is not.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool spaceEdit( SRPS::Space * space );
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::SpaceEditor,"SRPS::SpaceEditor/1.0.0")

#endif // SPACEVIEW_H
