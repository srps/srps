/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVEVISUALIZER_H
#define SRPS_CURVEVISUALIZER_H

#include "component.h"

#include <QtGui/QImage>

namespace SRPS {

class QCurve;

/*!
  \class SRPS::CurveVisualizer curveeditor.h SRPS/CurveVisualizer
  \brief Abstract CurveVisualizer Component.

  Component for Curve visualizing, intended to be subclassed for a QWidget

  A CurveVisualizer can visualized many curves at time.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT CurveVisualizer : public Component
{
  SRPS_DISABLE_COPY(CurveVisualizer)

protected:

  CurveVisualizer();

public:

  virtual ~CurveVisualizer();

  /*!
    \brief Current Visualized Curves.
    \return Current Visualized Curve List.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual const SRPS::QCurve* visualizedCurve() const;

  /*!
    \brief Add Curve to Visualization
    \param curve Target SRPS::Curve for editing.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual void curveVisualize( const SRPS::QCurve * curve );

  /*!
    \brief Save visualization to Image
    \param width Image width size.
    \param height Image height size.
    \return QIMage with result.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QImage saveVisualizationToImage( int width = -1,
                                           int heigth = -1 ) const;
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::CurveVisualizer,"SRPS::CurveVisualizer/1.0.0")

#endif // CURVEVIEW_H
