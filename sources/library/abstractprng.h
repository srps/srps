/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_ABSTRACTPRNG_H
#define SRPS_ABSTRACTPRNG_H

#include "srps_global.h"

namespace SRPS {

/*!
  \class SRPS::AbstractPRNG abstractprng.h SRPS/AbstractPRNG
  \brief Abstract interface for all Pseudo-Random Number Generators.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT AbstractPRNG
{
  SRPS_DISABLE_COPY(AbstractPRNG)

protected:

    AbstractPRNG();

public:

    virtual ~AbstractPRNG();

    /*!
      \brief Next Real in Open-Open Interval.
      \return Next Real in Open-Open Interval.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual double nextOO() = 0;

    /*!
      \brief Next Real in Open-CloseOpen Interval.
      \return Next Real in Open-Close Interval.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual double nextOC() = 0;

    /*!
      \brief Next Real in Close-Open Interval.
      \return Next Real in Close-Open Interval.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual double nextCO() = 0;

    /*!
      \brief Next Real in Close-Close Interval.
      \return Next Real in Close-Close Interval.
      \since 1.0
      \author Cesar Augusto Arana Collazos
     */
    virtual double nextCC() = 0;
};

} // namespace

#endif // SRPS_ABSTRACTPRNG_H
