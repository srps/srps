/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_MODEL_H
#define SRPS_MODEL_H

#include "space.h"
#include "prng.h"
#include "dprng.h"

namespace SRPS {

/*!
  \class SRPS::Model model.h SRPS/Model
  \brief SRPS Model

  A SRPS Model distributes individuals on a Space, a Model can use source Space
  and a PRNG and a DPRNG

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT Model : public Component
{
  SRPS_DISABLE_COPY(Model)

  protected:

    Model();

public:

    virtual ~Model();

    // Source Space Needs

    virtual bool needSourceSpace() const;
    virtual void setSourceSpace( const SRPS::Space * space );

    virtual bool needSourceCells() const;
    virtual void setSourceCells( const SRPS::Space::ConstCellList & cells );

    virtual bool needSourceCellMIDs() const;
    virtual void setSourceCellMIDs( const SRPS::Space::MIDList & cells );

    virtual bool needSourceSpecies() const;
    virtual void setSourceSpeciess( const SRPS::Space::ConstSpeciesList & species );

    virtual bool needSourceSpeciesMIDs() const;
    virtual void setSourceSpeciesMIDs( const SRPS::Space::MIDList & species );

    virtual bool needSourceIndividuals() const;
    virtual void setSourceIndividuals( const SRPS::Space::ConstIndividualList & individuals );

    virtual bool needSourceIndividualMIDs() const;
    virtual void setSourceIndividualsMIDs( const SRPS::Space::MIDList & species );

    // Target Space Need

    virtual bool needTargetSpace() const;
    virtual void setTargetSpace( SRPS::Space * space );

    virtual bool needTargetCells() const;
    virtual void setTargetCells( const SRPS::Space::CellList & cells );

    virtual bool needTargetCellMIDs() const;
    virtual void setTargetCellMIDs( const SRPS::Space::MIDList & cells );

    virtual bool needTargetSpecies() const;
    virtual void setTargetSpeciess( const SRPS::Space::SpeciesList & species );

    virtual bool needTargetSpeciesMIDs() const;
    virtual void setTargetSpeciesMIDs( const SRPS::Space::MIDList & species );

    virtual bool needTargetIndividuals() const;
    virtual void setTargetIndividuals( const SRPS::Space::IndividualList & individuals );

    virtual bool needTargetIndividualMIDs() const;
    virtual void setTargetIndividualsMIDs( const SRPS::Space::MIDList & species );

    // PRNG Needs

    virtual bool needPRNG() const;
    virtual void setPRNG( SRPS::PRNG * prng );

    // DPRNG Needs

    virtual bool needDPRNG() const;
    virtual void setDPRNG( SRPS::DPRNG * dprng );

    // Model

    virtual bool setup() = 0;
    virtual bool finished() const = 0;
    virtual void model() = 0;

};

} // namespace SRPS

#endif // SRPS_MODEL_H
