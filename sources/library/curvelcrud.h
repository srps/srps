/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVELCRUD_H
#define SRPS_CURVELCRUD_H

#include "component.h"

class QIODevice;
class QString;
class QSqlDatabase;

namespace SRPS {

class Curve;

/*!
  \class SRPS::CurveLCRUD curvelcrud.h SRPS/CurveLCRUD
  \brief Abstract Curve Read/Writer Component.

  Component for Curve reading and writing to streams.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT CurveLCRUD : public Component
{
  SRPS_DISABLE_COPY(CurveLCRUD)

protected:

  CurveLCRUD();

public:

  virtual ~CurveLCRUD();

  /*!
    \brief Prepare component with database

    \param database target database

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareDatabase( QSqlDatabase & database ) = 0;

  /*!
    \brief Prepare reading from stream.

    \param device Stream device
    \param curve Target Curve.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList curveList() const;

  /*!
    \brief Create the specified Curve in database.
    Use componentLastOperationResultCode() and componentLastOperationResultText()
    to know the method result and human readable text. If the curve can't be created
    in database and empty strinf is returned.
    \param curve Target Curve
    \return Unique text identifier
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QString curveCreate( const SRPS::Curve * curve );

  /*!
    \brief Read the specified curve from database
    \param curve's unique text identifier
    \param curve Target Curve
    \return Result code
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int curveCreate( const QString & key, SRPS::Curve * curve );

  /*!
    \brief Updated a specified curve
    \param curve's unique text identifier
    \param curve Target Curve
    \return Result code
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int curveUpdate( const QString & key, const SRPS::Curve * curve );

  /*!
    \brief Deleted the specified curve from database
    \param curve's unique text identifier
    \return Result code
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int curveDelete( const QString & key );
};

} // namecurve SRPS

Q_DECLARE_INTERFACE(SRPS::CurveLCRUD,"SRPS::CurveLCRUD/1.0.0");

#endif // CURVELCRUD_H
