/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "qcurve.h"

namespace SRPS {

class QCurve::Private
{
public:
  Private():
      curve(0),
      ownership(false)
  {}

  SRPS::Curve * curve;
  bool ownership;
};

QCurve::QCurve(QObject *parent) :
    QObject(parent),
    SRPS::Curve(),
    p( new Private )
{
}

QCurve::~QCurve()
{
  if ( p->ownership )
    delete p->curve;
  delete p;
}

bool QCurve::setCurve( SRPS::Curve * curve )
{
  if ( p->curve != curve )
  {
    p->curve = curve;
    emit curveChanged(curve);
    return true;
  }
  return false;
}

SRPS::Curve * QCurve::getCurve() const
{
  return p->curve;
}

bool QCurve::hasOwnership() const
{
  return p->ownership;
}

void QCurve::setOwnership( bool o )
{
  p->ownership = o;
}

// Component

QObject * QCurve::componentAsQObject()
{
  return this;
}

const QObject * QCurve::componentAsConstQObject() const
{
  return this;
}

QString QCurve::componentClassName() const
{
  return metaObject()->className();
}

QWidget * QCurve::componentStatusWidget() const
{
  if ( !p->curve ) return 0;

  return p->curve->componentStatusWidget();
}

QWidget * QCurve::componentConfigurationWidget()
{
  if ( !p->curve ) return 0;

  return p->curve->componentConfigurationWidget();
}

QWidget * QCurve::componentAboutWidget() const
{
  if ( !p->curve ) return 0;

  return p->curve->componentAboutWidget();
}

bool QCurve::componentHasProperty( const QString & name ) const
{
  if ( !p->curve ) return false;

  return p->curve->componentHasProperty(name);
}

int QCurve::componentPropertyCount() const
{
  if ( !p->curve ) return 0;

  return p->curve->componentPropertyCount();
}
QStringList QCurve::componentPropertyNames() const
{
  if ( !p->curve ) return QStringList();

  return p->curve->componentPropertyNames();
}

QVariant QCurve::componentProperty( const QString & name,
                            const QVariant & defaultValue ) const
{
  if ( !p->curve ) return defaultValue;

  return p->curve->componentProperty(name,defaultValue);
}

int QCurve::componentSetProperty( const QString & name,
                                   const QVariant & value )
{
  if ( !p->curve ) return -1;

  int result = p->curve->componentSetProperty(name,value);

  if ( !result )
  {
    emit curveUpdated();
    emit curvePropertyUpdated(name,value);
  }

  return result;
}

// SRPS::Curve
SRPS::Curve * QCurve::clone() const
{
  if ( !p->curve ) return 0;

  return p->curve->clone();
}

void QCurve::clear()
{
  if ( !p->curve ) return;

  p->curve->clear();
  emit curveUpdated();
  emit curveCleared();
}

int QCurve::variableCount() const
{
  if ( !p->curve ) return 0;

  return p->curve->variableCount();
}

QCurve::MIDList QCurve::variableMIDs( const SortOrder & ord ) const
{
  if ( !p->curve ) return MIDList();

  return p->curve->variableMIDs(ord);
}

QCurve::ConstVariableList
    QCurve::constVariables( const QCurve::SortOrder &  so ) const
{
  ConstVariableList result;

  foreach( const MemoryIdentifier & vmid, variableMIDs(so) )
    result << toConstVariable(vmid);

  return result;
}

QCurve::VariableList
    QCurve::variables( const QCurve::SortOrder & so ) const
{
  VariableList result;

  foreach( const MemoryIdentifier & vmid, variableMIDs(so) )
    result << toVariable(vmid);

  return result;
}

Curve::MemoryIdentifier QCurve::createVariableMID()
{
  if ( !p->curve ) return MemoryIdentifier();

  MemoryIdentifier vmi = p->curve->createVariableMID();

  if ( p->curve->variableIsValid(vmi) )
  {
    emit curveUpdated();
    emit variableCreated( toConstVariable(vmi) );
  }

  return vmi;
}

Curve::Variable QCurve::createVariable()
{
  return toVariable(createVariableMID());
}

bool QCurve::deleteVariable( const QCurve::MemoryIdentifier & vmi)
{
  if ( !p->curve ) return false;

  bool result = p->curve->deleteVariable(vmi);
  if ( result )
  {
    emit curveUpdated();
    emit variableDeleted(toConstVariable(vmi));
  }
  return result;
}

bool QCurve::deleteVariable( const ConstVariable & variable )
{
  if ( !isReferenceFromThis(variable) ) return false;

  return deleteVariable(variable.mid());
}

int QCurve::deleteAllVariables()
{
  if ( !p->curve ) return 0;

  int result = p->curve->deleteAllVariables();

  if ( result )
  {
    emit curveUpdated();
    emit variablesDeleted();
  }

  return result;
}

bool QCurve::variableIsValid( const MemoryIdentifier & vmi ) const
{
  if ( !p->curve ) return false;

  return p->curve->variableIsValid(vmi);
}

bool QCurve::variableHasProperty( const MemoryIdentifier & vmi,
                                  const QString & name ) const
{
  if ( !p->curve ) return false;

  return p->curve->variableHasProperty(vmi,name);
}

int QCurve::variablePropertyCount( const MemoryIdentifier & vmi ) const
{
  if ( !p->curve ) return 0;

  return p->curve->variablePropertyCount(vmi);
}

QStringList QCurve::variablePropertyNames( const MemoryIdentifier & vmi) const
{
  if ( !p->curve ) return QStringList();

  return p->curve->variablePropertyNames(vmi);
}

QVariant QCurve::variableProperty( const MemoryIdentifier & vmi,
                                   const QString & name,
                                   const QVariant & defaultValue ) const
{
  if ( !p->curve ) return defaultValue;

  return p->curve->variableProperty(vmi,name,defaultValue);
}

int QCurve::variableSetProperty( const MemoryIdentifier & vmi,
                                  const QString & name,
                                  const QVariant & value )
{
  if ( !p->curve ) return -1;

  int result = p->curve->variableSetProperty(vmi,name,value);

  if ( result )
  {
    emit curveUpdated();
    emit variableUpdated(toConstVariable(vmi));
    emit variablePropertyUpdated(toConstVariable(vmi),name,value);
  }

  return result;
}

//

int QCurve::tupleCount() const
{
  if ( !p->curve ) return 0;

  return p->curve->tupleCount();
}

QCurve::MIDList QCurve::tupleMIDs( const SortOrder & ord ) const
{
  if ( !p->curve ) return MIDList();

  return p->curve->tupleMIDs(ord);
}

QCurve::ConstTupleList
    QCurve::constTuples( const QCurve::SortOrder &  so ) const
{
  ConstTupleList result;

  foreach( const MemoryIdentifier & vmid, tupleMIDs(so) )
    result << toConstTuple(vmid);

  return result;
}

QCurve::TupleList
    QCurve::tuples( const QCurve::SortOrder & so ) const
{
  TupleList result;

  foreach( const MemoryIdentifier & vmid, tupleMIDs(so) )
    result << toTuple(vmid);

  return result;
}

Curve::MemoryIdentifier QCurve::createTupleMID()
{
  if ( !p->curve ) return MemoryIdentifier();

  MemoryIdentifier tmi = p->curve->createTupleMID();

  if ( p->curve->tupleIsValid(tmi) )
  {
    emit curveUpdated();
    emit tupleCreated(toConstTuple(tmi) );
  }

  return tmi;
}

Curve::Tuple QCurve::createTuple()
{
  return toTuple(createTupleMID());
}

bool QCurve::deleteTuple(const MemoryIdentifier & tmi )
{
  if ( !p->curve ) return false;

  bool result = p->curve->deleteTuple(tmi);

  if ( result )
  {
    emit curveUpdated();
    emit tupleDeleted(toConstTuple(tmi));
  }

  return result;
}

bool QCurve::deleteTuple(const ConstTuple & tuple )
{
  if ( !isReferenceFromThis(tuple) ) return false;

  return deleteTuple(tuple.mid());
}

int QCurve::deleteAllTuples()
{
  if ( !p->curve ) return 0;

  int result = p->curve->deleteAllTuples();

  if ( result )
  {
    emit curveUpdated();
    emit tuplesDeleted();
  }

  return result;
}

bool QCurve::tupleIsValid( const MemoryIdentifier & tmi ) const
{
  if ( !p->curve ) return false;

  return p->curve->tupleIsValid(tmi);
}

bool QCurve::tupleHasProperty( const MemoryIdentifier & tmi,
                               const QString & name ) const
{
  if ( !p->curve ) return false;

  return p->curve->tupleHasProperty(tmi,name);
}

int QCurve::tuplePropertyCount( const MemoryIdentifier & tmi ) const
{
  if ( !p->curve ) return 0;

  return p->curve->tuplePropertyCount(tmi);
}

QStringList QCurve::tuplePropertyNames( const MemoryIdentifier & tmi ) const
{
  if ( !p->curve ) return QStringList();

  return p->curve->tuplePropertyNames(tmi);
}

QVariant QCurve::tupleProperty( const MemoryIdentifier & tmi,
                                const QString & name,
                                const QVariant & defaultValue ) const
{
  if ( !p->curve ) return defaultValue;

  return p->curve->tupleProperty(tmi,name,defaultValue);
}

QVariant QCurve::tupleValue( const MemoryIdentifier & tmi,
                             const MemoryIdentifier & vmi,
                             const QVariant & defaultValue ) const
{
  if ( !p->curve ) return false;

  return p->curve->tupleValue(tmi,vmi,defaultValue);
}

int QCurve::tupleVariableCount( const MemoryIdentifier & tmid ) const
{
  if ( !p->curve ) return 0;

  return p->curve->tupleVariableCount(tmid);
}

QCurve::MIDList QCurve::tupleVariableMIDs( const MemoryIdentifier & tmid,
                                           const SortOrder & so ) const
{
  if ( !p->curve ) return MIDList();

  return p->curve->tupleVariableMIDs(tmid,so);
}

QCurve::ConstVariableList QCurve::tupleConstVariables( const MemoryIdentifier & tmid,
                                                       const SortOrder & so ) const
{
  ConstVariableList result;

  foreach( MemoryIdentifier vmid, tupleVariableMIDs(tmid,so) )
    result << toConstVariable(vmid);

  return result;
}

int QCurve::tupleClearValues( const QCurve::MemoryIdentifier & tmid )
{
  if ( !p->curve ) return false;

  int result = p->curve->tupleClearValues(tmid);
  if ( result )
  {
    emit curveUpdated();
    emit tupleUpdated(toConstTuple(tmid));
  }

  return result;
}

int QCurve::tupleSetProperty( const MemoryIdentifier & tmi,
                               const QString & name,
                               const QVariant & value )
{
  if ( !p->curve ) return -1;

  bool result = p->curve->tupleSetProperty(tmi,name,value);

  if ( result )
  {
    emit curveUpdated();
    emit tupleUpdated(toConstTuple(tmi));
    emit tuplePropertyUpdated(toConstTuple(tmi),
                              name,value);
  }

  return result;
}

bool QCurve::tupleSetValue( const MemoryIdentifier & tmi,
                            const MemoryIdentifier & vmi,
                            const QVariant & value )
{
  if ( !p->curve ) return false;

  bool result = p->curve->tupleSetValue(tmi,vmi,value);

  if ( result )
  {
    emit curveUpdated();
    emit tupleUpdated(toConstTuple(tmi));
    emit tupleValueUpdated(toConstTuple(tmi),
                           toConstVariable(vmi),
                           value);
  }

  return result;
}

} // namespace SRPS
