/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACEEDITORFACTORY_H
#define SRPS_SPACEEDITORFACTORY_H

#include "srps_global.h"
#include <QtCore/QStringList>
#include <QtCore/QVariantHash>

namespace SRPS
{

class SpaceEditor;

/*!
  \class SRPS::SpaceEditorFactory spacefactory.h SRPS/SpaceEditorFactory
  \brief Abstract interface SRPS::SpaceEditor Factories

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SpaceEditorFactory
{
  SRPS_DISABLE_COPY(SpaceEditorFactory)

protected:

  SpaceEditorFactory();

public:

  virtual ~SpaceEditorFactory();

  /*!
    \brief Returns the available SpaceEditor class names for creation.
    \return An empty list, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList spaceEditorFactoryKeys() const;

  /*!
    \brief Creates the specified SpaceEditor component.
    \param component SpaceEditor's class name.
    \param parameters SpaceEditor's creation paremeters.
    \return 0, default implementation.
    \note If the provied parameters aren't valid, the method must use defaults \
    parameters.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual SRPS::SpaceEditor *
      spaceEditorFactoryCreate( const QString & spaceEditor,
                                const QVariantHash & parameters
                                                  = QVariantHash() );
};

}

Q_DECLARE_INTERFACE(SRPS::SpaceEditorFactory,"SRPS::SpaceEditorFactory/1.0.0")

#endif // SpaceFACTORY_H
