/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "plugin.h"

using SRPS::Plugin;

Plugin::Plugin()
{
}

Plugin::~Plugin()
{

}

QString Plugin::pluginErrorString() const
{
  return QString();
}

bool Plugin::pluginIsInitialized() const
{
  return false;
}

bool Plugin::pluginIsFinalized() const
{
  return false;
}

bool Plugin::pluginReadSettings( const SettingsManager * /*sm*/ )
{
  return false;
}

bool Plugin::pluginInitialize()
{
  return false;
}

bool Plugin::pluginFinalize()
{
  return false;
}

bool Plugin::pluginWriteSettings( SettingsManager * /*sm*/ )
{
  return false;
}
