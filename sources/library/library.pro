include( ../../configuration/srps.defs )
include( ../../configuration/srps.conf )
TARGET = library
TEMPLATE = lib
VERSION = $$SRPS_VERSION
TARGET = $$qtLibraryTarget($$SRPS_NAME)
DESTDIR = $$SRPS_LIBRARIES_PATH
DEFINES += SRPS_LIBRARY
SOURCES += component.cpp \
    componentfactory.cpp \
    componentfactorywidget.cpp \
    curve.cpp \
    qcurve.cpp \
    curvefactory.cpp \
    settingsmanager.cpp \
    plugin.cpp \
    pluginloader.cpp \
    resourceloader.cpp \
    curveeditor.cpp \
    curveeditorfactory.cpp \
    curverw.cpp \
    curverwfactory.cpp \
    space.cpp \
    spacefactory.cpp \
    spaceeditor.cpp \
    spaceeditorfactory.cpp \
    curvevisualizer.cpp \
    curvevisualizerfactory.cpp \
    qspace.cpp \
    spacerwfactory.cpp \
    spacerw.cpp \
    abstractprng.cpp \
    prng.cpp \
    prngfactory.cpp \
    dprngfactory.cpp \
    dprng.cpp \
    model.cpp \
    accumulator.cpp \
    modelfactory.cpp \
    accumulatorfactory.cpp \
    spacegenerator.cpp \
    spacegeneratorfactory.cpp \
    componentconfigurationwidget.cpp \
    spacelcrudfactory.cpp \
    spacelcrud.cpp \
    curvelcrudfactory.cpp \
    curvelcrud.cpp \
    componentstatuswidget.cpp \
    simulator.cpp \
    simulatorfactory.cpp

HEADERS += srps_global.h \
    component.h \
    componentfactory.h \
    componentfactorywidget.h \
    curve.h \
    qcurve.h \
    curvefactory.h \
    settingsmanager.h \
    plugin.h \
    pluginloader.h \
    resourceloader.h \
    curveeditor.h \
    curveeditorfactory.h \
    curverw.h \
    curverwfactory.h \
    space.h \
    spacefactory.h \
    spaceeditor.h \
    spaceeditorfactory.h \
    curvevisualizer.h \
    curvevisualizerfactory.h \
    qspace.h \
    spacerwfactory.h \
    spacerw.h \
    abstractprng.h \
    prng.h \
    prngfactory.h \
    dprngfactory.h \
    dprng.h \
    model.h \
    accumulator.h \
    modelfactory.h \
    accumulatorfactory.h \
    spacegenerator.h \
    spacegeneratorfactory.h \
    componentconfigurationwidget.h \
    spacelcrudfactory.h \
    spacelcrud.h \
    curvelcrudfactory.h \
    curvelcrud.h \
    componentstatuswidget.h \
    simulator.h \
    simulatorfactory.h
