/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACELCRUD_H
#define SRPS_SPACELCRUD_H

#include "component.h"

class QIODevice;
class QString;
class QSqlDatabase;

namespace SRPS {

class Space;

/*!
  \class SRPS::SpaceLCRUD spacelcrud.h SRPS/SpaceLCRUD
  \brief Abstract Space Read/Writer Component.

  Component for Space reading and writing to streams.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SpaceLCRUD : public Component
{
  SRPS_DISABLE_COPY(SpaceLCRUD)

protected:

  SpaceLCRUD();

public:

  virtual ~SpaceLCRUD();

  /*!
    \brief Prepare component with database

    \param database target database

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareDatabase( QSqlDatabase & database ) = 0;

  /*!
    \brief Prepare reading from stream.

    \param device Stream device
    \param curve Target Space.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList spaceList() const;

  /*!
    \brief Create the specified Space in database.
    Use componentLastOperationResultCode() and componentLastOperationResultText()
    to know the method result and human readable text. If the space can't be created
    in database and empty strinf is returned.
    \param space Target Space
    \return Unique text identifier
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QString spaceCreate( const SRPS::Space * space );

  /*!
    \brief Read the specified space from database
    \param space's unique text identifier
    \param space Target Space
    \return Result code
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int spaceCreate( const QString & key, SRPS::Space * space );

  /*!
    \brief Updated a specified space
    \param space's unique text identifier
    \param space Target Space
    \return Result code
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int spaceUpdate( const QString & key, const SRPS::Space * space );

  /*!
    \brief Deleted the specified space from database
    \param space's unique text identifier
    \return Result code
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int spaceDelete( const QString & key );
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::SpaceLCRUD,"SRPS::SpaceLCRUD/1.0.0");

#endif // SPACELCRUD_H
