/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVEEDITOR_H
#define SRPS_CURVEEDITOR_H

#include "component.h"

namespace SRPS {

class QCurve;

/*!
  \class SRPS::CurveEditor curveeditor.h SRPS/CurveEditor
  \brief Abstract CurveEditor Component.

  Component for Curve editing, intended to be subclassed for a QWidget

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT CurveEditor : public Component
{
  SRPS_DISABLE_COPY(CurveEditor)

protected:

  CurveEditor();

public:

  virtual ~CurveEditor();

  /*!
    \brief Current Edited Curve.
    \return The current edited Curve.
    \return null if component is not editing a curve.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual SRPS::QCurve * curveEdited() const;

  /*!
    \brief Change the Current Edited Curve.
    \param curve Target SRPS::Curve for editing.
    \return true if \c curve is been edited.
    \return false is not.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool curveEdit( SRPS::QCurve * curve );
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::CurveEditor,"SRPS::CurveEditor/1.0.0")

#endif // CURVEVIEW_H
