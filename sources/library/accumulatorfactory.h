/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_ACCUMULATORFACTORY_H
#define SRPS_ACCUMULATORFACTORY_H

#include "srps_global.h"
#include <QtCore/QStringList>
#include <QtCore/QVariantHash>

namespace SRPS
{

class Accumulator;

/*!
  \class SRPS::AccumulatorFactory accumulatorfactory.h SRPS/AccumulatorFactory
  \brief Abstract interface SRPS::Accumulator Factories

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT AccumulatorFactory
{
  SRPS_DISABLE_COPY(AccumulatorFactory)

protected:

  AccumulatorFactory();

public:

  virtual ~AccumulatorFactory();

  /*!
    \brief Returns the available Accumulator class names for creation.
    \return An empty list, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList accumulatorFactoryKeys() const;

  /*!
    \brief Creates the specified Accumulator component.
    \param component Accumulator's class name.
    \param parameters Accumulator's creation paremeters.
    \return 0, default implementation.
    \note If the provied parameters aren't valid, the method must use defaults \
    parameters.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual SRPS::Accumulator * accumulatorFactoryCreate( const QString & accumulator,
                                                const QVariantHash & parameters
                                                  = QVariantHash() );
};

}

Q_DECLARE_INTERFACE(SRPS::AccumulatorFactory,"SRPS::AccumulatorFactory/1.0.0")

#endif // ACCUMULATORFACTORY_H
