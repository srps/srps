/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_CURVERW_H
#define SRPS_CURVERW_H

#include "component.h"

class QIODevice;
class QString;

namespace SRPS {

class Curve;

/*!
  \class SRPS::CurveRW curverw.h SRPS/CurveRW
  \brief Abstract Curve Read/Writer Component.

  Component for Curve reading and writing to streams.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT CurveRW : public Component
{
  SRPS_DISABLE_COPY(CurveRW)

protected:

  CurveRW();

public:

  virtual ~CurveRW();

  /*!
    \brief Prepare reading from stream.

    \param device Stream device
    \param curve Target Curve.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareCurveRead( QIODevice * device, SRPS::Curve * curve ) = 0;

  /*!
    \brief Read a curve from prepared stream into prepared curve

    \return Result code.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int readCurve() = 0;

  /*!
    \brief Prepare writing from stream.

    \param device Stream device
    \param curve Target Curve.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareCurveWrite( const SRPS::Curve * curve, QIODevice * device ) = 0;

  /*!
    \brief Write prepared curve into prepared device

    \return Result code.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int writeCurve() = 0;
};

} // namecurve SRPS

Q_DECLARE_INTERFACE(SRPS::CurveRW,"SRPS::CurveRW/1.0.0");

#endif // CURVERW_H
