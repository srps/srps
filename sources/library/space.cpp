/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "space.h"

namespace SRPS
{

//
// BEGIN PrivateReference
//

class Space::PrivateReference
{
  Q_DISABLE_COPY(PrivateReference)

public:

    PrivateReference( SRPS::Space * c )
      : counter(),space(c)
    {
    }

    QAtomicInt counter;
    SRPS::Space * space; // no es dueño
};

//
// END PrivateReference
//

//
// BEGIN Space::AbstractReference
//

Space::AbstractReference::AbstractReference( Space::PrivateReference * pr, Space::MemoryIdentifier mid )
  : _shared_counter(pr),_memory_identifier(mid)
{
  if ( _shared_counter )
    _shared_counter->counter.ref();
}

Space::AbstractReference::AbstractReference( const Space::AbstractReference & o )
  : _shared_counter(o._shared_counter),_memory_identifier(o._memory_identifier)
{
  if ( _shared_counter )
    _shared_counter->counter.ref();
}

Space::AbstractReference & Space::AbstractReference::operator=( const Space::AbstractReference & o )
{
  if ( this != & o )
  {
    if ( _shared_counter )
    {
      if ( !_shared_counter->counter.deref() )
        delete _shared_counter;
    }
    _shared_counter = o._shared_counter;
    _memory_identifier = o._memory_identifier;
    if ( _shared_counter )
      _shared_counter->counter.ref();
  }
  return *this;
}

bool Space::AbstractReference::operator==( const AbstractReference & o ) const
{
  if ( this != & o )
    return _shared_counter == o._shared_counter && _memory_identifier == o._memory_identifier;
  return true;
}
bool Space::AbstractReference::operator!=( const AbstractReference & o ) const
{
  if ( this != & o )
    return _shared_counter != o._shared_counter || _memory_identifier != o._memory_identifier;
  return false;
}

void Space::AbstractReference::detach() const
{
  if ( _shared_counter )
  {
    if ( !_shared_counter->counter.deref() )
      delete _shared_counter;
    _shared_counter = 0;
  }
}

const Space * Space::AbstractReference::const_space() const
{
  return _shared_counter->space;
}

Space * Space::AbstractReference::space() const
{
  return _shared_counter->space;
}

Space::AbstractReference::AbstractReference()
  : _shared_counter(0),_memory_identifier(0)
{
}

Space::AbstractReference::~AbstractReference()
{
  if ( _shared_counter )
  {
    if ( !_shared_counter->counter.deref() )
      delete _shared_counter;
  }
}

bool Space::AbstractReference::isValid() const
{
  if ( !_shared_counter )
    return false;
  if ( !_shared_counter->space )
  {
    detach();
    return false;
  }
  return true;
}

Space::MemoryIdentifier Space::AbstractReference::mid() const
{
  return _memory_identifier;
}

//
// END Space::AbstractReference
//

//
// BEGIN Space::ReferencePropertyGetter
//

Space::ReferencePropertyGetter::~ReferencePropertyGetter()
{
}

//
// END Space::ReferencePropertyGetter
//

//
// BEGIN Space::ReferencePropertySetter
//

Space::ReferencePropertySetter::~ReferencePropertySetter()
{
}

//
// END Space::ReferencePropertySetter
//

//
// BEGIN Space::ConstIndividual
//

Space::ConstIndividual::ConstIndividual( Space::PrivateReference * pr, Space::MemoryIdentifier mid )
    : AbstractReference(pr,mid)
{
}

Space::ConstIndividual::ConstIndividual( const Space::ConstIndividual & o )
  : AbstractReference(o._shared_counter,o._memory_identifier)
{
}

Space::ConstIndividual & Space::ConstIndividual::operator=( const Space::ConstIndividual & o )
{
  if ( this != & o )
      AbstractReference::operator=(o);
  return *this;
}

bool Space::ConstIndividual::operator==( const ConstIndividual & o ) const
{
  if ( this != & o )
    return AbstractReference::operator==(o);
  return true;
}

bool Space::ConstIndividual::operator!=( const ConstIndividual & o ) const
{
  if ( this != & o )
    return AbstractReference::operator!=(o);
  return false;
}

Space::ConstIndividual::ConstIndividual()
  : AbstractReference()
{
}

Space::ConstIndividual::~ConstIndividual()
{
}

bool Space::ConstIndividual::isValid() const
{
    if ( !Space::AbstractReference::isValid() )
      return false;
    return const_space()->individualIsValid( mid() );
}

bool Space::ConstIndividual::hasProperty( const QString & name ) const
{
  return const_space()->individualHasProperty( mid(), name );
}

int Space::ConstIndividual::propertyCount() const
{
  return const_space()->individualPropertyCount( mid() );
}

QStringList Space::ConstIndividual::propertyNames() const
{
  return const_space()->individualPropertyNames( mid() );
}

QVariant Space::ConstIndividual::property( const QString & name, const QVariant & defaultValue ) const
{
  return const_space()->individualProperty( mid(), name, defaultValue );
}

Space::ConstSpecies Space::ConstIndividual::constSpecies() const
{
    return const_space()->individualConstSpecies(mid());
}

Space::ConstCell Space::ConstIndividual::constCell() const
{
    return const_space()->individualConstCell(mid());
}

bool Space::ConstIndividual::isDistributed() const
{
  return const_space()->individualIsDistributed(mid());
}

//
// END Space::ConstIndividual
//

//
// BEGIN Space::Individual
//

Space::Individual::Individual( Space::PrivateReference * pr, Space::MemoryIdentifier mid )
    : ConstIndividual(pr,mid)
{
}

Space::Individual::Individual( const Space::Individual & o )
  : ConstIndividual(o._shared_counter,o._memory_identifier)
{
}

Space::Individual & Space::Individual::operator=( const Space::Individual & o )
{
  if ( this != & o )
      ConstIndividual::operator=(o);
  return *this;
}

bool Space::Individual::operator==( const ConstIndividual & o ) const
{
  if ( this != & o )
    return ConstIndividual::operator==(o);
  return true;
}

bool Space::Individual::operator!=( const ConstIndividual & o ) const
{
  if ( this != & o )
    return ConstIndividual::operator!=(o);
  return false;
}

Space::Individual::Individual()
   : Space::ConstIndividual()
{
}

Space::Individual::~Individual()
{
}

int Space::Individual::setProperty(const QString & name, const QVariant & value ) const
{
    return space()->individualSetProperty(mid(),name,value);
}

//
// END Space::Individual
//

//
// BEGIN Space::ConstSpecies
//

Space::ConstSpecies::ConstSpecies( Space::PrivateReference * p, Space::MemoryIdentifier id )
  : Space::AbstractReference(p,id)
{
}

Space::ConstSpecies::ConstSpecies()
  : Space::AbstractReference()
{
}

Space::ConstSpecies::ConstSpecies( const Space::ConstSpecies & o )
  : Space::AbstractReference(o)
{
}

Space::ConstSpecies & Space::ConstSpecies::operator=( const Space::ConstSpecies & o )
{
  if ( this != & o )
    Space::AbstractReference::operator =(o);
  return *this;
}

Space::ConstSpecies::~ConstSpecies()
{
}

bool Space::ConstSpecies::operator==( const Space::ConstSpecies & o ) const
{
  if ( this != & o )
    return Space::AbstractReference::operator ==(o);
  return true;
}

bool Space::ConstSpecies::operator!=( const Space::ConstSpecies & o ) const
{
  if ( this != & o )
    return Space::AbstractReference::operator !=(o);
  return false;
}

bool Space::ConstSpecies::isValid() const
{
  if ( !Space::AbstractReference::isValid() )
    return false;
  return const_space()->speciesIsValid( mid() );
}

bool Space::ConstSpecies::hasProperty( const QString & name ) const
{
  return const_space()->speciesHasProperty( mid(), name );
}

int Space::ConstSpecies::propertyCount() const
{
  return const_space()->speciesPropertyCount( mid() );
}

QStringList Space::ConstSpecies::propertyNames() const
{
  return const_space()->speciesPropertyNames( mid() );
}

QVariant Space::ConstSpecies::property( const QString & name, const QVariant & defaultValue ) const
{
  return const_space()->speciesProperty( mid(), name, defaultValue );
}

int Space::ConstSpecies::individualCount() const
{
    return const_space()->speciesIndividualCount( mid() );
}

Space::ConstIndividualList Space::ConstSpecies::constIndividuals( const Space::SortOrder & ord ) const
{
    return const_space()->speciesConstIndividuals(mid(),ord);
}

int Space::ConstSpecies::distributedIndividualCount() const
{
  return const_space()->speciesDistributedIndividualCount(mid());
}

Space::ConstIndividualList Space::ConstSpecies::constDistributedIndividuals( const SortOrder & ord) const
{
  return const_space()->speciesDistributedConstIndividuals(mid(),ord);
}

int Space::ConstSpecies::undistributedIndividualCount() const
{
  return const_space()->speciesUndistributedIndividualCount(mid());
}

Space::ConstIndividualList Space::ConstSpecies::constUndistributedIndividuals( const SortOrder & ord ) const
{
  return const_space()->speciesUndistributedConstIndividuals(mid(),ord);
}

bool Space::ConstSpecies::isDistributed() const
{
  return const_space()->speciesIsDistributed(mid());
}

bool Space::ConstSpecies::isUndistributed() const
{
  return const_space()->speciesIsUndistributed(mid());
}

int Space::ConstSpecies::distributedCellCount() const
{
  return const_space()->speciesDistributedCellCount(mid());
}

Space::ConstCellList Space::ConstSpecies::constDistributedCells(const SortOrder & so) const
{
  return const_space()->speciesDistributedConstCells(mid(),so);
}

//
// END Space::ConstSpecies
//

//
// BEGIN Space::Species
//

Space::Species::Species( Space::PrivateReference * p, Space::MemoryIdentifier id )
  : ConstSpecies(p,id)
{
}

Space::Species::Species()
  : ConstSpecies()
{
}

Space::Species::Species( const Space::Species & o )
  : ConstSpecies(o)
{
}

Space::Species & Space::Species::operator=( const Space::Species & o )
{
  if ( this != & o )
    Space::ConstSpecies::operator =(o);
  return *this;
}

Space::Species::~Species()
{
}

bool Space::Species::operator==( const Space::ConstSpecies & o ) const
{
  if ( this != & o )
    return Space::ConstSpecies::operator ==(o);
  return true;
}

bool Space::Species::operator!=( const Space::ConstSpecies & o ) const
{
  if ( this != & o )
    return Space::ConstSpecies::operator !=(o);
  return false;
}

int Space::Species::setProperty(const QString & name, const QVariant & value ) const
{
    return space()->speciesSetProperty(mid(),name,value);
}

Space::IndividualList Space::Species::individuals( const Space::SortOrder & ord ) const
{
    return const_space()->speciesIndividuals(mid(),ord);
}

Space::Individual Space::Species::createIndividual() const
{
    return space()->speciesCreateIndividual(mid());
}

bool Space::Species::deleteIndividual( const Space::ConstIndividual & i ) const
{
    if (!const_space()->isReferenceFromThis(i))
        return false;
    return space()->speciesDeleteIndividual(mid(),i.mid());
}

int Space::Species::deleteIndividuals() const
{
  return space()->speciesDeleteIndividuals(mid());
}

int Space::Species::deleteDistributedIndividuals() const
{
  return space()->speciesDeleteDistributedIndividuals(mid());
}

int Space::Species::deleteUndistributedIndividuals() const
{
  return space()->speciesDeleteUndistributedIndividuals(mid());
}

Space::IndividualList Space::Species::distributedIndividuals( const SortOrder & ord) const
{
  return space()->speciesDistributedIndividuals(mid(),ord);
}

Space::IndividualList Space::Species::undistributedIndividuals( const SortOrder & ord) const
{
  return space()->speciesUndistributedIndividuals(mid(),ord);
}

int Space::Species::undistributeCells()
{
  return space()->speciesUndistributeCells(mid());
}

int Space::Species::undistributeIndividuals()
{
  return space()->speciesUndistributeIndividuals(mid());
}

//
// END Space::Species
//

//
// BEGIN Space::ConstCell
//

Space::ConstCell::ConstCell( Space::PrivateReference * p, Space::MemoryIdentifier id )
  : Space::AbstractReference(p,id)
{
}

Space::ConstCell::ConstCell()
  : Space::AbstractReference()
{
}

Space::ConstCell::ConstCell( const Space::ConstCell & o )
  : Space::AbstractReference(o)
{
}

Space::ConstCell & Space::ConstCell::operator=( const Space::ConstCell & o )
{
  if ( this != & o )
    Space::AbstractReference::operator =(o);
  return *this;
}

Space::ConstCell::~ConstCell()
{
}

bool Space::ConstCell::operator==( const Space::ConstCell & o ) const
{
  if ( this != & o )
    return Space::AbstractReference::operator ==(o);
  return true;
}

bool Space::ConstCell::operator!=( const Space::ConstCell & o ) const
{
  if ( this != & o )
    return Space::AbstractReference::operator !=(o);
  return false;
}

bool Space::ConstCell::isValid() const
{
  if ( !Space::AbstractReference::isValid() )
    return false;
  return const_space()->cellIsValid( mid() );
}

bool Space::ConstCell::hasProperty( const QString & name ) const
{
  return const_space()->cellHasProperty( mid(), name );
}

int Space::ConstCell::propertyCount() const
{
  return const_space()->cellPropertyCount( mid() );
}

QStringList Space::ConstCell::propertyNames() const
{
  return const_space()->cellPropertyNames( mid() );
}

QVariant Space::ConstCell::property( const QString & name, const QVariant & defaultValue ) const
{
  return const_space()->cellProperty( mid(), name, defaultValue );
}

int Space::ConstCell::individualCount() const
{
    return const_space()->cellIndividualCount( mid() );
}

int Space::ConstCell::individualCount( const Space::ConstSpecies & s ) const
{
    if ( !const_space()->isReferenceFromThis(s) )
        return false;
    return const_space()->cellSpeciesIndividualCount( mid(), s.mid() );
}

Space::ConstIndividualList
    Space::ConstCell::constIndividuals(const SortOrder & ord) const
{
  return const_space()->cellConstIndividuals(ord);
}

Space::ConstIndividualList
    Space::ConstCell::constIndividuals(const ConstSpecies & smid,
                                       const SortOrder & so ) const
{
  if ( !const_space()->isReferenceFromThis(smid) )
      return Space::ConstIndividualList();
  return const_space()->cellSpeciesConstIndividuals(mid(),smid.mid(),so);
}

int Space::ConstCell::speciesCount() const
{
    return const_space()->cellSpeciesCount( mid() );
}

bool Space::ConstCell::contains( const ConstSpecies & s ) const
{
    if ( !const_space()->isReferenceFromThis(s) )
        return false;
    return const_space()->cellHasSpecies(mid(),s.mid());
}

Space::ConstSpeciesList Space::ConstCell::constSpecies(const SortOrder & ord ) const
{
    return const_space()->cellConstSpecies(mid(),ord);
}

//
// END Space::ConstCell
//

//
// BEGIN Space::Cell
//

Space::Cell::Cell( Space::PrivateReference * p, Space::MemoryIdentifier id )
  : Space::ConstCell(p,id)
{
}

Space::Cell::Cell()
  : Space::ConstCell()
{
}

Space::Cell::Cell( const Space::Cell & o )
  : Space::ConstCell(o)
{
}

Space::Cell & Space::Cell::operator=( const Space::Cell & o )
{
  if ( this != & o )
    Space::ConstCell::operator =(o);
  return *this;
}

Space::Cell::~Cell()
{
}

bool Space::Cell::operator==( const Space::ConstCell & o ) const
{
  if ( this != & o )
    return Space::ConstCell::operator ==(o);
  return true;
}

bool Space::Cell::operator!=( const Space::ConstCell & o ) const
{
  if ( this != & o )
    return Space::ConstCell::operator !=(o);
  return false;
}

int Space::Cell::setProperty(const QString & name, const QVariant & value ) const
{
    return space()->cellSetProperty(mid(),name,value);
}

Space::IndividualList Space::Cell::individuals( const Space::SortOrder & ord ) const
{
    return space()->cellIndividuals(mid(),ord);
}

Space::IndividualList Space::Cell::individuals(const ConstSpecies & s, const SortOrder & ord) const
{
    return space()->cellSpeciesIndividuals(mid(),s.mid(),ord);
}

bool Space::Cell::insertIndividual( const Space::ConstIndividual & i) const
{
    if(!const_space()->isReferenceFromThis(i))
        return false;
    return space()->cellInsertIndividual(mid(),const_space()->individualSpeciesMID(i.mid()),i.mid());
}

bool Space::Cell::removeIndividual( const Space::ConstIndividual & i) const
{
    if(!const_space()->isReferenceFromThis(i))
        return false;
    return space()->cellRemoveIndividual(mid(),const_space()->individualSpeciesMID(i.mid()),i.mid());
}

int Space::Cell::removeIndividuals()
{
  return space()->cellRemoveIndividuals(mid());
}

int Space::Cell::removeSpecies( const Space::ConstSpecies & s)
{
  if(!const_space()->isReferenceFromThis(s))
      return 0;
  return space()->cellRemoveSpecies(mid(),s.mid());
}

int Space::Cell::removeAllSpecies()
{
  return space()->cellRemoveAllSpecies(mid());
}

//
// END Space::Cell
//

Space::Space() :
    Component(),
    pr( new PrivateReference(this) )
{
  pr->counter.ref();
}

Space::~Space()
{
  if ( pr )
  {
    pr->space = 0;
    if ( !pr->counter.deref() )
      delete pr;
  }
}

Space::Individual Space::toIndividual( const MemoryIdentifier & i) const
{
    return Individual(pr,i);
}

Space::ConstIndividual Space::toConstIndividual( const MemoryIdentifier & i) const
{
    return ConstIndividual(pr,i);
}

Space::Individual Space::toIndividual( const Space::ConstIndividual & i ) const
{
    if(!isReferenceFromThis(i))
        return Individual();
    return Individual(pr,i.mid());
}

Space::Species Space::toSpecies( const MemoryIdentifier & i) const
{
    return Species(pr,i);
}

Space::ConstSpecies Space::toConstSpecies( const MemoryIdentifier & i) const
{
    return ConstSpecies(pr,i);
}

Space::Species Space::toSpecies( const Space::ConstSpecies & i ) const
{
    if(!isReferenceFromThis(i))
        return Species();
    return Species(pr,i.mid());
}

Space::Cell Space::toCell( const MemoryIdentifier & i) const
{
    return Cell(pr,i);
}

Space::ConstCell Space::toConstCell( const MemoryIdentifier & i) const
{
    return ConstCell(pr,i);
}

Space::Cell Space::toCell( const Space::ConstCell & i ) const
{
    if(!isReferenceFromThis(i))
        return Cell();
    return Cell(pr,i.mid());
}

bool Space::isReferenceFromThis(const Space::AbstractReference & i) const
{
    return (this == i.const_space());
}

} // namespace SRPS
