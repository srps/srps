/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "pluginloader.h"
#include "plugin.h"

using SRPS::PluginLoader;

PluginLoader::PluginLoader(QObject *parent)
  : QObject(parent)
{
}

PluginLoader::~PluginLoader()
{
}

QObjectList PluginLoader::plugins() const
{
  return QObjectList();
}

SRPS::SettingsManager * PluginLoader::settingsManagerFor( const QObject * /*obj*/,
                                                  bool /*withName*/ ) const
{
  return 0;
}

bool PluginLoader::loadPlugin( const QString & /*filepath*/ )
{
  return false;
}

bool PluginLoader::unloadPlugin( const QString & /*filepath*/ )
{
  return false;
}

int PluginLoader::loadDir( const QString & /*dir*/ )
{
  return 0;
}

bool PluginLoader::registerPlugin( QObject * /*object*/ )
{
  return false;
}

bool PluginLoader::unregisterPlugin( QObject * /*object*/ )
{
  return false;
}
