/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "componentfactory.h"
#include "componentfactorywidget.h"

#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtGui/QTextEdit>

static QString readFile( const QString & f )
{
  QFile file(f);

  if ( !file.open( QFile::ReadOnly) )
    return QObject::tr("cant open %1").arg(f);

  QTextStream stream ( &file );
  return stream.readAll();
}

using SRPS::ComponentFactory;

ComponentFactory::ComponentFactory()
{
}

ComponentFactory::~ComponentFactory()
{
}

QWidget * ComponentFactory::componentFactoryWidgetFor(const QString & k) const
{
  QTextEdit * text = new QTextEdit;
  text->setReadOnly(true);
  text->setText(readFile(QString(":/%1/configuration.html").arg(QString(k).replace(".","/").replace("::","/").toLower())));
  return text;
}

QVariantHash ComponentFactory::componentFactoryDefaultParametersFor(const QString &) const
{
  return QVariantHash();
}

bool ComponentFactory::componentFactorySetDefaultParametersFor(
                          const QString & /*key*/,
                          const QVariantHash & /*parameters*/)
{
  return false;
}
