/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SIMULATOR_H
#define SRPS_SIMULATOR_H

#include "component.h"

namespace SRPS {

class Space;
class SpaceFactory;

class Curve;
class CurveFactory;

class Accumulator;
class AccumulatorFactory;

class PRNG;
class PRNGFactory;

class DPRNG;
class DPRNGFactory;

class Model;
class ModelFactory;


/*!
  \class SRPS::Simulator prngfactory.h SRPS/Simulator
  \brief Abstract interface SRPS::Simulator Factories

  A Simulator components perform simulation especified by SimulationJob object.
  It must be Asynchronous, that is, method MUST Not block. The only blocking function
  is waitSimulator.

  It must provide a StatusWidget for tracking simulator state. Useful for users.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT Simulator : public Component
{
  SRPS_DISABLE_COPY(Simulator)

protected:

      Simulator();

public:

  struct ComponentConfiguration
  {
    QString component;
    QVariantHash configuration;
  };

  typedef QList<ComponentConfiguration> ComponentConfigurationList;

  class SRPS_EXPORT Job
  {

    class Private;
    Private * p;

  public:

    Job();
    ~Job();

    Job( const Job & o );

    Job & operator=( const Job & o );

    bool accumulateOnly() const;
    void setAccumulateOnly( bool );

    ComponentConfiguration modelConfiguration() const;
    void setModelConfiguration( const ComponentConfiguration & model_key );

    ComponentConfiguration prngConfiguration() const;
    void setPRNGConfiguration( const ComponentConfiguration & prng_key );

    ComponentConfiguration dprngConfiguration() const;
    void setDPRNGConfiguration( const ComponentConfiguration & dprng_key );

    ComponentConfigurationList accumulatorConfigurations() const;
    void setAccumulatorConfigurations( const ComponentConfigurationList & accumulators_key );

    int iterations() const;
    void setIterations( int model_iterations );

  };

  typedef QList<SRPS::Simulator::Job> JobList;

  virtual ~Simulator();


  /*!
    \brief Wait for Simulator component finish.
    \note Must block until simulator finish or cancel jobs.
   */
  virtual void waitSimulator() = 0;

  /*!
    \brief Says to Simualtor to cancel operations.
    \note Must NOT block.
   */
  virtual void cancel() = 0;

  virtual bool isCanceled() const = 0;

  virtual bool hasFinished() const = 0;

  virtual void setFactory( SRPS::CurveFactory * ) = 0;
  virtual void setFactory( SRPS::AccumulatorFactory * ) = 0;
  virtual void setFactory( SRPS::ModelFactory * ) = 0;
  virtual void setFactory( SRPS::PRNGFactory * ) = 0;
  virtual void setFactory( SRPS::DPRNGFactory * ) = 0;

  /*!
    \brief Set Source Space.
    \note Must NOT block.
   */
  virtual void setSourceSpace( const SRPS::Space * ) = 0;

  /*!
    \brief Return all result curves.
    \note Must NOT block.
   */
  virtual QList<SRPS::Curve*> getResults() const = 0;

  /*!
    \brief Says to Simulator to start Jobs
    \note Must NOT block.
   */
  virtual void startJobs( const QList<Simulator::Job> & jobs ) = 0;

};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::Simulator,"SRPS::Simulator/1.0.0");

#endif // SRPS_SIMULATOR_H
