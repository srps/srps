/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_COMPONENTCONFIGURATIONWIDGET_H
#define SRPS_COMPONENTCONFIGURATIONWIDGET_H

#include "srps_global.h"
#include <QtCore/QVariantHash>

namespace SRPS {

/*!
  \class SRPS::ComponentConfigurationWidget \
         componentfactorywidget.h \
         SRPS/ComponentConfigurationWidget
  \brief Abstract interface SRPS::Component Configuration Widget

  A SRPS::ComponentConfigurationWidget can get or sets Component's parameters.

  All funcitons are implemented to do nothing or return default values.
  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT ComponentConfigurationWidget
{
protected:

  ComponentConfigurationWidget();

public:

  virtual ~ComponentConfigurationWidget();

  /*!
    \brief Return the configuration component parameters.

    \return An empty QVariantHash, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QVariantHash componentConfigurationWidgetParameters() const;

  /*!
    \brief Set the configuration widget component parameters.

    Default implementation does nothing.
    \return false, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool componentConfigurationWidgetSetParameters(const QVariantHash & parameters);

  /*!
    \brief Load parameters from component to GUI

    Default implementation does nothing.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual void componentConfigurationWidgetGetComponentParameters();

  /*!
    \brief Save parameters from GUI to component

    Default implementation does nothing.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual void componentConfigurationWidgetSetComponentParameters();

};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::ComponentConfigurationWidget,
                    "SRPS::ComponentConfigurationWidget/1.0.0");

#endif // SRPS_COMPONENTCONFIGURATIONWIDGET_H
