/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_COMPONENTFACTORYWIDGET_H
#define SRPS_COMPONENTFACTORYWIDGET_H

#include "srps_global.h"
#include <QtCore/QVariantHash>

namespace SRPS {

/*!
  \class SRPS::ComponentFactoryWidget \
         componentfactorywidget.h \
         SRPS/ComponentFactoryWidget
  \brief Abstract interface SRPS::Component Factory Widget

  A SRPS::ComponentFactoryWidget can get or sets Component's creation factory
  parameters.

  All funcitons are implemented to do nothing or return default values.
  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT ComponentFactoryWidget
{
protected:

  ComponentFactoryWidget();

public:

  virtual ~ComponentFactoryWidget();

  /*!
    \brief Return the factory's creation parameters.

    \return An empty QVariantHash, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QVariantHash componentFactoryWidgetParameters() const;

  /*!
    \brief Set the factory's creation parameters.

    Default implementation does nothing.
    \return false, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool componentFactoryWidgetSetParameters(const QVariantHash & parameters);
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::ComponentFactoryWidget,
                    "SRPS::ComponentFactoryWidget/1.0.0");

#endif // SRPS_COMPONENTFACTORYWIDGET_H
