/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include <QtCore/QStringList>
#include <QtCore/QVariant>

#include "curve.h"

namespace SRPS
{

//
// BEGIN PrivateReference
//

class Curve::PrivateReference
{
  SRPS_DISABLE_COPY(PrivateReference)

public:

    PrivateReference( SRPS::Curve * c )
      : counter(),curve(c)
    {
    }

    QAtomicInt counter;
    SRPS::Curve * curve; // no es dueño
};

//
// END PrivateReference
//

//
// BEGIN Curve::AbstractReference
//

Curve::AbstractReference::AbstractReference( Curve::PrivateReference * pr, Curve::MemoryIdentifier mid )
  : _shared_counter(pr),_memory_identifier(mid)
{
  if ( _shared_counter )
    _shared_counter->counter.ref();
}

Curve::AbstractReference::AbstractReference( const Curve::AbstractReference & o )
  : _shared_counter(o._shared_counter),_memory_identifier(o._memory_identifier)
{
  if ( _shared_counter )
    _shared_counter->counter.ref();
}

Curve::AbstractReference & Curve::AbstractReference::operator=( const Curve::AbstractReference & o )
{
  if ( this != & o )
  {
    if ( _shared_counter )
    {
      if ( !_shared_counter->counter.deref() )
        delete _shared_counter;
    }
    _shared_counter = o._shared_counter;
    _memory_identifier = o._memory_identifier;
    if ( _shared_counter )
      _shared_counter->counter.ref();
  }
  return *this;
}

bool Curve::AbstractReference::operator==( const AbstractReference & o ) const
{
  if ( this != & o )
    return _shared_counter == o._shared_counter && _memory_identifier == o._memory_identifier;
  return true;
}
bool Curve::AbstractReference::operator!=( const AbstractReference & o ) const
{
  if ( this != & o )
    return _shared_counter != o._shared_counter || _memory_identifier != o._memory_identifier;
  return false;
}

void Curve::AbstractReference::detach() const
{
  if ( _shared_counter )
  {
    if ( !_shared_counter->counter.deref() )
      delete _shared_counter;
    _shared_counter = 0;
  }
}

const Curve * Curve::AbstractReference::const_curve() const
{
  return _shared_counter->curve;
}

Curve * Curve::AbstractReference::curve() const
{
  return _shared_counter->curve;
}

Curve::AbstractReference::AbstractReference()
  : _shared_counter(0),_memory_identifier(0)
{
}

Curve::AbstractReference::~AbstractReference()
{
  if ( _shared_counter )
  {
    if ( !_shared_counter->counter.deref() )
      delete _shared_counter;
  }
}

bool Curve::AbstractReference::isValid() const
{
  if ( !_shared_counter )
    return false;
  if ( !_shared_counter->curve )
  {
    detach();
    return false;
  }
  return true;
}

Curve::MemoryIdentifier Curve::AbstractReference::mid() const
{
  return _memory_identifier;
}

//
// END Curve::AbstractReference
//

//
// BEGIN Curve::ReferencePropertyGetter
//

Curve::ReferencePropertyGetter::~ReferencePropertyGetter()
{
}

//
// END Curve::ReferencePropertyGetter
//

//
// BEGIN Curve::ReferencePropertySetter
//

Curve::ReferencePropertySetter::~ReferencePropertySetter()
{
}

//
// END Curve::ReferencePropertySetter
//

//
// BEGIN Curve::ConstVariable
//

Curve::ConstVariable::ConstVariable( Curve::PrivateReference * p, Curve::MemoryIdentifier id )
  : Curve::AbstractReference(p,id)
{
}

Curve::ConstVariable::ConstVariable()
  : Curve::AbstractReference()
{
}

Curve::ConstVariable::ConstVariable( const Curve::ConstVariable & o )
  : Curve::AbstractReference(o)
{
}

Curve::ConstVariable & Curve::ConstVariable::operator=( const Curve::ConstVariable & o )
{
  if ( this != & o )
    Curve::AbstractReference::operator =(o);
  return *this;
}

Curve::ConstVariable::~ConstVariable()
{
}

bool Curve::ConstVariable::operator==( const Curve::ConstVariable & o ) const
{
  if ( this != & o )
    return Curve::AbstractReference::operator ==(o);
  return true;
}

bool Curve::ConstVariable::operator!=( const Curve::ConstVariable & o ) const
{
  if ( this != & o )
    return Curve::AbstractReference::operator !=(o);
  return false;
}

bool Curve::ConstVariable::isValid() const
{
  if ( !Curve::AbstractReference::isValid() )
    return false;
  return const_curve()->variableIsValid( mid() );
}

bool Curve::ConstVariable::hasProperty( const QString & name ) const
{
  return const_curve()->variableHasProperty( mid(), name );
}

int Curve::ConstVariable::propertyCount() const
{
  return const_curve()->variablePropertyCount( mid() );
}

QStringList Curve::ConstVariable::propertyNames() const
{
  return const_curve()->variablePropertyNames( mid() );
}

QVariant Curve::ConstVariable::property( const QString & name, const QVariant & defaultValue ) const
{
  return const_curve()->variableProperty( mid(), name, defaultValue );
}

//
// END Curve::ConstVariable
//

//
// BEGIN Curve::Variable
//

Curve::Variable::Variable( Curve::PrivateReference * p, Curve::MemoryIdentifier id )
  : Curve::ConstVariable(p,id)
{
}

Curve::Variable::Variable()
  : Curve::ConstVariable()
{
}

Curve::Variable::Variable( const Curve::Variable & o )
  : Curve::ConstVariable(o)
{
}

Curve::Variable & Curve::Variable::operator=( const Curve::Variable & o )
{
  if ( this != &o )
    Curve::ConstVariable::operator =(o);
  return *this;
}

Curve::Variable::~Variable()
{
}

bool Curve::Variable::operator==( const ConstVariable & o ) const
{
  if ( this != &o )
    return Curve::AbstractReference::operator ==(o);
  return true;
}

bool Curve::Variable::operator!=( const ConstVariable & o ) const
{
  if ( this != &  o )
    return Curve::AbstractReference::operator !=(o);
  return false;
}

int Curve::Variable::setProperty( const QString & name, const QVariant & value ) const
{
  return curve()->variableSetProperty( mid(), name, value );
}

//
// END Curve::Variable
//

//
// BEGIN Curve::ConstTuple
//

Curve::ConstTuple::ConstTuple( Curve::PrivateReference * p, Curve::MemoryIdentifier id )
  : Curve::AbstractReference(p,id)
{
}

Curve::ConstTuple::ConstTuple()
  : Curve::AbstractReference()
{
}

Curve::ConstTuple::ConstTuple( const Curve::ConstTuple & o )
  : Curve::AbstractReference(o)
{
}

Curve::ConstTuple & Curve::ConstTuple::operator=( const Curve::ConstTuple & o )
{
  if ( this != & o )
    Curve::AbstractReference::operator =(o);
  return *this;
}

Curve::ConstTuple::~ConstTuple()
{
}

bool Curve::ConstTuple::operator==( const Curve::ConstTuple & o ) const
{
  if ( this != & o )
    return Curve::AbstractReference::operator ==(o);
  return true;
}

bool Curve::ConstTuple::operator!=( const Curve::ConstTuple & o ) const
{
  if ( this != & o )
    return Curve::AbstractReference::operator !=(o);
  return false;
}

bool Curve::ConstTuple::isValid() const
{
  if ( !Curve::AbstractReference::isValid() )
    return false;
  return const_curve()->tupleIsValid( mid() );
}

bool Curve::ConstTuple::hasProperty( const QString & name ) const
{
  return const_curve()->tupleHasProperty( mid(), name );
}

int Curve::ConstTuple::propertyCount() const
{
  return const_curve()->tuplePropertyCount( mid() );
}

QStringList Curve::ConstTuple::propertyNames() const
{
  return const_curve()->tuplePropertyNames( mid() );
}

QVariant Curve::ConstTuple::property( const QString & name, const QVariant & defaultValue ) const
{
  return const_curve()->tupleProperty( mid(), name, defaultValue );
}

QVariant Curve::ConstTuple::value( const ConstVariable & variable, const QVariant & defaultValue ) const
{
  if ( static_cast<Curve::AbstractReference>(variable).const_curve() != const_curve() )
    return QVariant();
  return const_curve()->tupleValue( mid(), variable.mid(), defaultValue );
}

int Curve::ConstTuple::variableCount() const
{
  return const_curve()->tupleVariableCount(mid());
}

Curve::ConstVariableList Curve::ConstTuple::constVariables(const Curve::SortOrder & ord) const
{
  return const_curve()->tupleConstVariables( mid(),ord );
}

//
// END Curve::ConstTuple
//

//
// BEGIN Curve::Tuple
//

Curve::Tuple::Tuple( Curve::PrivateReference * p, Curve::MemoryIdentifier id )
  : Curve::ConstTuple(p,id)
{
}

Curve::Tuple::Tuple()
  : Curve::ConstTuple()
{
}

Curve::Tuple::Tuple( const Curve::Tuple & o )
  : Curve::ConstTuple(o)
{
}

Curve::Tuple & Curve::Tuple::operator=( const Curve::Tuple & o )
{
  if ( this != &o )
    Curve::ConstTuple::operator =(o);
  return *this;
}

Curve::Tuple::~Tuple()
{
}

bool Curve::Tuple::operator==( const ConstTuple & o ) const
{
  if ( this != &o )
    return Curve::AbstractReference::operator ==(o);
  return true;
}

bool Curve::Tuple::operator!=( const ConstTuple & o ) const
{
  if ( this != &o )
    return Curve::AbstractReference::operator !=(o);
  return false;
}

int Curve::Tuple::setProperty( const QString & name, const QVariant & value ) const
{
  return curve()->tupleSetProperty( mid(), name, value );
}

bool Curve::Tuple::setValue( const ConstVariable & variable, const QVariant & value ) const
{
  if ( !const_curve()->isReferenceFromThis(variable) )
    return false;
  return curve()->tupleSetValue( mid(), variable.mid(), value );
}

bool Curve::Tuple::clearValues()
{
  return curve()->tupleClearValues( mid() );
}

//
// END Curve::Tuple
//

//
// BEGIN Curve
//

Curve::Curve()
    : SRPS::Component(),
    pr( new PrivateReference(this) )
{
  pr->counter.ref();
}

Curve::~Curve()
{
  if ( pr )
  {
    pr->curve = 0;
    if ( !pr->counter.deref() )
      delete pr;
  }
}

Curve::Variable Curve::createVariable()
{
    return toVariable(createVariableMID());
}

bool Curve::deleteVariable( const Curve::ConstVariable & variable )
{
    if ( !isReferenceFromThis(variable) )
        return false;
    return deleteVariable(variable.mid());
}

Curve::ConstVariableList Curve::constVariables( const Curve::SortOrder & ord ) const
{
    Curve::ConstVariableList result;
    Curve::MIDList pre = variableMIDs(ord);

    Curve::MIDList::const_iterator iterator = pre.constBegin();
    Curve::MIDList::const_iterator end      = pre.constEnd();
    for( ; iterator != end; ++iterator )
        result << toConstVariable(*iterator);

    return result;
}

Curve::VariableList Curve::variables( const Curve::SortOrder & ord ) const
{
    Curve::VariableList result;
    Curve::MIDList pre = variableMIDs(ord);

    Curve::MIDList::const_iterator iterator = pre.constBegin();
    Curve::MIDList::const_iterator end      = pre.constEnd();
    for( ; iterator != end; ++iterator )
        result << toVariable(*iterator);

    return result;
}

Curve::Tuple Curve::createTuple()
{
    return toTuple(createTupleMID());
}

bool Curve::deleteTuple( const Curve::ConstTuple & variable )
{
    if ( !isReferenceFromThis(variable) )
        return false;
    return deleteTuple(variable.mid());
}

Curve::ConstTupleList Curve::constTuples( const Curve::SortOrder & ord ) const
{
    Curve::ConstTupleList result;
    Curve::MIDList pre = variableMIDs(ord);

    Curve::MIDList::const_iterator iterator = pre.constBegin();
    Curve::MIDList::const_iterator end      = pre.constEnd();
    for( ; iterator != end; ++iterator )
        result << toConstTuple(*iterator);

    return result;
}

Curve::TupleList Curve::tuples( const Curve::SortOrder & ord ) const
{
    Curve::TupleList result;
    Curve::MIDList pre = variableMIDs(ord);

    Curve::MIDList::const_iterator iterator = pre.constBegin();
    Curve::MIDList::const_iterator end      = pre.constEnd();
    for( ; iterator != end; ++iterator )
        result << toTuple(*iterator);

    return result;
}

Curve::Variable Curve::toVariable( const Curve::MemoryIdentifier & i) const
{
    return Curve::Variable(pr,i);
}

Curve::ConstVariable Curve::toConstVariable( const Curve::MemoryIdentifier & i ) const
{
    return Curve::ConstVariable(pr,i);
}

Curve::Variable Curve::toVariable( const Curve::ConstVariable & v) const
{
    return Curve::Variable(pr,v.mid());
}

Curve::Tuple Curve::toTuple( const Curve::MemoryIdentifier & i) const
{
    return Curve::Tuple(pr,i);
}

Curve::ConstTuple Curve::toConstTuple( const Curve::MemoryIdentifier & i) const
{
    return Curve::ConstTuple(pr,i);
}

Curve::Tuple Curve::toTuple( const Curve::ConstTuple & v) const
{
    return Curve::Tuple(pr,v.mid());
}

bool Curve::isReferenceFromThis( const AbstractReference & r ) const
{
    return (this == r.const_curve());
}

//
// END Curve
//

} // namespace SRPS
