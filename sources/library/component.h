/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_COMPONENT_H
#define SRPS_COMPONENT_H

#include "srps_global.h"
#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QCoreApplication>

namespace SRPS {

/*!
  \class SRPS::Component component.h SRPS/Component
  \brief Abstract interface for all SRPS Components.

  A SRPS::Component can be casted to QObject if the implementation allow that.
  It must provide a class name that can be the real C++ class name or another
  unique string with NO spaces.

  A SRPS::Component could provide graphical user interface for see the
  component's status, component's configuration and/or component's about
  information.

  A SRPS::Component could handle properties: QString -> QVariant pairs.

  Only one function, componentClassName() must be override the other has default
  implementations. returns default values and does nothing.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT Component
{
  SRPS_DISABLE_COPY(Component)
  Q_DECLARE_TR_FUNCTIONS(SRPS::Component)

  SRPS_DECLARE_PRIVATE_CLASS

protected:

  Component();

  /*!
    \brief Register last operation

    \param code operation result code
    \param text human readable text
   */
  int registerLastOperationResult( int code, const QString & text ) const;

  /*!
    \brief Register last operation
    Use this for opeartions that returns true or false.

    \param text human readable text
   */
  bool registerLastOperationResult( bool result, const QString & text ) const;

public:

  virtual ~Component();

  /*!
    \brief Component's creation paremeters
    \return empty, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QVariantHash componentCreationParameters();

  /*!
    \brief Cast component to QObject
    \return 0, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QObject * componentAsQObject();

  /*!
    \brief Cast component to const QObject
    \return 0, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual const QObject * componentAsConstQObject() const;

  /*!
    \brief Component's class name.
    \return The real C++ class name or an unique identifier.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QString componentClassName() const = 0;

  /*!
    \brief Component status widget.
    \return 0, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QWidget * componentStatusWidget() const;

  /*!
    \brief Component configuration widget.
    \return 0, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QWidget * componentConfigurationWidget();

  /*!
    \brief Component configuration widget.
    \return 0, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QWidget * componentAboutWidget() const;


  /*!
    \brief Test for component's property.
    \param name, property name.
    \return false, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool componentHasProperty( const QString & name ) const;

  /*!
    \brief Test if component's property is static, it can't be removed.
    \param name, property name.
    \return false, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual bool componentIsStaticProperty( const QString & name ) const;

  /*!
    \brief Component's property count.
    \return 0, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int componentPropertyCount() const;

  /*!
    \brief Component's property names.
    \return Empty list, default implementation.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QStringList componentPropertyNames() const;

  /*!
    \brief Request Component's property.
    \param name, property name.
    \param defaultValue, fallback return.
    \return defaultValue
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual QVariant componentProperty( const QString & name,
                                      const QVariant & defaultValue =
                                        QVariant() ) const;

  /*!
    \brief Set Component's property.
    It must return true if and only if the property has been changed, or removed
    or changed to default ( when value is a null QVariant )
    \param name, property name.
    \param value, new property value.
    \return zero if property was changed or deleted ( invalid QVariant )
    \return 1 if property was reset to default ( invalid QVariant )
    \return -1 if the property does't exist.
    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int componentSetProperty( const QString & name,
                                     const QVariant & value );

  /*!
    \brief Last Operation Code
    By conviention, a last operation code zero means successfull operation
    (no errors), a positive integer means successfull depending of last.
    operation. A negative integers always means an error.
    \return Last operation code. -1 is default implementation.
   */
  virtual int componentLastOperationResultCode() const;

  /*!
    \brief Operation's result Human readable text
    \return Human readable text
   */
  virtual QString componentLastOperationResultText() const;
};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::Component,"SRPS::Component/1.0.0");

#endif // COMPONENT_H
