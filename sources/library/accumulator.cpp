/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "accumulator.h"

namespace SRPS {

Accumulator::Accumulator()
  : Component()
{
}

Accumulator::~Accumulator()
{
}

bool Accumulator::needSpace() const
{
  return false;
}

void Accumulator::setSpace( const SRPS::Space * /*space*/ )
{

}

bool Accumulator::needCells() const
{
  return false;
}

void Accumulator::setCells( const SRPS::Space::ConstCellList & /*cells*/ )
{

}

bool Accumulator::needCellMIDs() const
{
  return false;
}

void Accumulator::setCellMIDs( const SRPS::Space::MIDList & /*cells*/ )
{

}

bool Accumulator::needSpecies() const
{
  return false;
}

void Accumulator::setSpeciess( const SRPS::Space::ConstSpeciesList & /*species*/ )
{

}

bool Accumulator::needSpeciesMIDs() const
{
  return false;
}

void Accumulator::setSpeciesMIDs( const SRPS::Space::MIDList & /*species*/ )
{

}

bool Accumulator::needIndividuals() const
{
  return false;
}

void Accumulator::setIndividuals( const SRPS::Space::ConstIndividualList & /*individuals*/ )
{

}

bool Accumulator::needIndividualMIDs() const
{
  return false;
}

void Accumulator::setIndividualsMIDs( const SRPS::Space::MIDList & /*species*/ )
{

}

} // namespace SRPS
