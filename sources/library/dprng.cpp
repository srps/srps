/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "dprng.h"

namespace SRPS {

DPRNG::Range::Range(double min, double max )
  : p_min(min),p_max(max)
{
  if ( min > max )
    qSwap(min,max);
}

double DPRNG::Range::min() const
{
  return p_min;
}

double DPRNG::Range::max() const
{
  return p_max;
}

void DPRNG::Range::setMin( double d )
{
  if ( d > p_max )
    p_max = d;
  p_min = d;
}

void DPRNG::Range::setMax( double d )
{
  if ( d < p_min )
    p_min = d;
  p_max = d;
}

double DPRNG::Range::length() const
{
  return p_max-p_min;
}

DPRNG::DPRNG():
    Component(),
    AbstractPRNG()
{
}

DPRNG::~DPRNG()
{
}

//PRNG * DPRNG::prng() const
//{
//  return 0;
//}

//bool DPRNG::setPRNG( PRNG * )
//{
//  return false;
//}

//DPRNG::Range DPRNG::range() const
//{
//  return Range();
//}

//bool DPRNG::setRange( const Range & /*range*/ )
//{
//  return false;
//}

} // namespace SRPS
