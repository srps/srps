/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_PLUGINLOADER_H
#define SRPS_PLUGINLOADER_H

#include "srps_global.h"
#include <QtCore/QObject>

namespace SRPS {

class SettingsManager;

/*!
  \class SRPS::PluginLoader pluginloader.h SRPS/PluginLoader
  \brief Abstract interface for Qt plugin loading.

  Default implementations returns default values and does nothing.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT PluginLoader : public QObject
{
  Q_OBJECT
  Q_PROPERTY( QObjectList plugins READ plugins )

protected:

  explicit PluginLoader(QObject *parent = 0);

public:

  virtual ~PluginLoader();

  /*!
    \brief Loaded plugin list.
    \return Loaded plugin list. QPluginLoader root components.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual QObjectList plugins() const;

  /*!
    \brief SettingsManager for plugin or QObject.

    The provided QObject can be any QObject. If null is provided, Application
    SettingsManager must be returned.
    \return SettingsManager for plugin or QObject.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual SRPS::SettingsManager * settingsManagerFor( const QObject * obj,
                                              bool withName = false ) const;

signals:

  /*!
    \brief Emited when a plugin was loaded.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  void pluginLoaded( QObject *, const QString & filepath );

  /*!
    \brief Emited when a plugin was unloaded.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  void pluginUnloaded( QObject *, const QString & filepath );

public slots:

  /*!
    \brief Load a plugin.
    \param filepath, plugin filepath
    \return true if plugin was loaded, pluginLoaded() signal must be emited.
    \return false if not.
    \note \c object should implement SRPS::Plugin interface
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool loadPlugin( const QString & filepath );

  /*!
    \brief Unload a plugin.
    \param filepath, plugin filepath
    \return true if plugin was unloaded, pluginUnloaded() signal must be emited.
    \return false if not.
    \warning Unloading a plugin can cause application crash.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool unloadPlugin( const QString & filepath );

  /*!
    \brief Load plugins in directory.
    \param dir, directory path.
    \return Loaded plugin count from directory.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual int loadDir( const QString & dir );

  /*!
    \brief Register a plugin.
    \param object, plugin filepath
    \return true if plugin was registered, pluginLoaded() signal must be emited.
    \return false if not.
    \note \c object should implement SRPS::Plugin interface
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool registerPlugin( QObject * object );

  /*!
    \brief Unregister a plugin.
    \param object, plugin filepath
    \return true if plugin was unregistered, pluginUnloaded() signal must be emited.
    \return false if not.
    \since 1.0
    \author Cesar Augusto Arana Collazos.
   */
  virtual bool unregisterPlugin( QObject * object );

};

} // namespace SRPS

#endif // SRPS_PLUGINLOADER_H
