/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_QSPACE_H
#define SRPS_QSPACE_H

#include "space.h"
#include <QtCore/QObject>

namespace SRPS {

class SRPS_EXPORT QSpace : public QObject, public Space
{
  Q_OBJECT
  Q_DISABLE_COPY(QSpace)
  Q_INTERFACES( SRPS::Space SRPS::Component )
  class Private;
  Private *p;

public:

  explicit QSpace(QObject *parent = 0);
  virtual ~QSpace();

  SRPS::Space * getSpace() const;
  bool setSpace( SRPS::Space * space );

  bool hasOwnership() const;
  void setOwnership( bool ) const;

signals:

  void spaceChanged( SRPS::Space * );

  void spaceUpdated();
  void spaceCleared();
  void spaceUndistributed();
  void spacePropertyUpdated( const QString & name, const QVariant & value );

  void individualCreated(const SRPS::Space::ConstIndividual & individual );
  void individualDeleted(const SRPS::Space::ConstIndividual & individual );
  void individualUpdated(const SRPS::Space::ConstIndividual & individual );
  void individualPropertyUpdated( const SRPS::Space::ConstIndividual & individual,
                                const QString & name,
                                const QVariant & value );
  void individualCellChanged(const SRPS::Space::ConstIndividual & individual );
  void individualsDeleted();
  void individualsUndistibuted();

  void speciesCreated(const SRPS::Space::ConstSpecies & species );
  void speciesDeleted(const SRPS::Space::ConstSpecies & species );
  void speciesUpdated(const SRPS::Space::ConstSpecies & species );
  void speciesPropertyUpdated( const SRPS::Space::ConstSpecies & species,
                                const QString & name,
                                const QVariant & value );
  void speciesIndividualsDeleted(const SRPS::Space::ConstSpecies & species );
  void speciesDistributedIndividualsDeleted(const SRPS::Space::ConstSpecies & species );
  void speciesUndistributedIndividualsDeleted(const SRPS::Space::ConstSpecies & species );
  void speciesUndistributed(const SRPS::Space::ConstSpecies & species);
  void distributedSpeciesDeleted();
  void undistributedSpeciesDeleted();
  void emptySpeciesDeleted();
  void nonEmptySpeciesDeleted();
  void allSpeciesDeleted();
  void allSpeciesUndistributed();

  void cellCreated(const SRPS::Space::ConstCell & cell );
  void cellDeleted(const SRPS::Space::ConstCell & cell );
  void cellUpdated(const SRPS::Space::ConstCell & cell );
  void cellPropertyUpdated( const SRPS::Space::ConstCell & cell,
                            const QString & name,
                            const QVariant & value );

  void cellIndividualInserted( const SRPS::Space::ConstCell & cell,
                               const SRPS::Space::ConstIndividual & individual );
  void cellIndividualRemoved( const SRPS::Space::ConstCell & cell,
                              const SRPS::Space::ConstIndividual & individual );

  void cellSpeciesRemoved( const SRPS::Space::ConstCell & cell,
                          const SRPS::Space::ConstSpecies & species );

  void cellCleared( const SRPS::Space::ConstCell & cell );
  void cellsDeleted();
  void emptyCellsDeleted();
  void nonEmptyCellsDeleted();

public:

  // Component

  /*!
    \return this, not internal space componentAsQObject()
   */
  QObject * componentAsQObject();

  /*!
    \return this, not internal space componentAsConstQObject()
   */
  const QObject * componentAsConstQObject() const;

  /*!
    \return this component class name not internal space class name.
   */
  QString componentClassName() const;

  QWidget * componentStatusWidget() const;
  QWidget * componentConfigurationWidget();
  QWidget * componentAboutWidget() const;

  bool componentHasProperty( const QString & name ) const;
  int componentPropertyCount() const;
  QStringList componentPropertyNames() const;
  QVariant componentProperty( const QString & name,
                              const QVariant & defaultValue = QVariant() ) const;
  int componentSetProperty( const QString & name,
                             const QVariant & value );


  // SRPS::Space
  /*!
    \return internal clone, not this clone
   */
  SRPS::Space * clone() const;

  /*!
    \return internal clone undistributed, not this clone unditribited
   */
  SRPS::Space * cloneUndistributed() const;

  void undistribute();

  void clear();


  // Individual Space interface

  int individualCount() const;

  MIDList individualMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList constIndividuals( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList individuals( const SortOrder & so = Unordered ) const;

  int distributedIndividualCount() const;

  MIDList distributedIndividualMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList constDistributedIndividuals( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList distributedIndividuals( const SortOrder & so = Unordered ) const;

  int undistributedIndividualCount() const;

  MIDList undistributedIndividualMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList constUndistributedIndividuals( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList undistributedIndividuals( const SortOrder & so = Unordered ) const;

  // Invidual Reference Interface

  bool individualIsValid( const MemoryIdentifier &) const;

  bool individualHasProperty( const MemoryIdentifier & mid,
                                 const QString & name ) const;

  int individualPropertyCount( const MemoryIdentifier & mid) const;

  QStringList individualPropertyNames( const MemoryIdentifier & mid ) const;

  QVariant individualProperty( const MemoryIdentifier & mid,
                                       const QString & name,
                                       const QVariant & defaultValue =
                                          QVariant() ) const;

  MemoryIdentifier individualSpeciesMID( const MemoryIdentifier & mid ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpecies individualConstSpecies( const MemoryIdentifier & mid ) const;

  MemoryIdentifier individualCellMID( const MemoryIdentifier & mid ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstCell individualConstCell( const MemoryIdentifier & mid ) const;

  bool individualIsDistributed(const MemoryIdentifier & mid) const;

  int individualSetProperty( const MemoryIdentifier & mid,
                                      const QString & name,
                                      const QVariant & value );

  // Species Space Interface

  int speciesCount() const;

  MIDList speciesMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpeciesList constSpecies( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  SpeciesList species( const SortOrder & so = Unordered ) const;

  int distributedSpeciesCount() const;

  MIDList distributedSpeciesMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpeciesList constDistributedSpecies( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  SpeciesList distributedSpecies( const SortOrder & so = Unordered ) const;

  int undistributedSpeciesCount() const;

  MIDList undistributedSpeciesMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpeciesList constUndistributedSpecies( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  SpeciesList undistributedSpecies( const SortOrder & so = Unordered ) const;

  int emptySpeciesCount() const;

  MIDList emptySpeciesMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpeciesList constEmptySpecies( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  SpeciesList emptySpecies( const SortOrder & so = Unordered ) const;

  int nonEmptySpeciesCount() const;

  MIDList nonEmptySpeciesMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpeciesList constNonEmptySpecies( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  SpeciesList nonEmptySpecies( const SortOrder & so = Unordered ) const;

  MemoryIdentifier createSpeciesMID();

  Species createSpecies();

  bool deleteSpecies( const MemoryIdentifier & imid );

  bool deleteSpecies( const ConstSpecies & species );

  int deleteAllSpecies();

  int deleteDistributedSpecies();

  int deleteUndistributedSpecies();

  int deleteEmptySpecies();

  int deleteNonEmptySpecies();

  // Species Reference Interface

  bool speciesIsValid( const MemoryIdentifier &) const;

  bool speciesHasProperty( const MemoryIdentifier & mid,
                                 const QString & name ) const;

  int speciesPropertyCount( const MemoryIdentifier & mid) const;

  QStringList speciesPropertyNames( const MemoryIdentifier & mid ) const;

  QVariant speciesProperty( const MemoryIdentifier & mid,
                                       const QString & name,
                                       const QVariant & defaultValue =
                                          QVariant() ) const;

  int speciesIndividualCount(const MemoryIdentifier & mid) const;

  MIDList speciesIndividualMIDs( const MemoryIdentifier & mid,
                                         const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList
      speciesConstIndividuals( const MemoryIdentifier & mid,
                               const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList
      speciesIndividuals( const MemoryIdentifier & mid,
                               const SortOrder & so = Unordered ) const;

  int speciesDistributedIndividualCount( const MemoryIdentifier & mid ) const;

  MIDList speciesDistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                    const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList
      speciesDistributedConstIndividuals( const MemoryIdentifier & mid,
                                          const SortOrder & so = Unordered ) const;

  int speciesUndistributedIndividualCount( const MemoryIdentifier & mid ) const;

  MIDList speciesUndistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                      const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList
      speciesUndistributedConstIndividuals( const MemoryIdentifier & mid,
                                            const SortOrder & so = Unordered ) const;

  bool speciesIsDistributed(const MemoryIdentifier & mid) const;

  bool speciesIsUndistributed(const MemoryIdentifier & mid) const;

  int speciesDistributedCellCount( const MemoryIdentifier & mid ) const;

  MIDList speciesDistributedCellMIDs( const MemoryIdentifier & mid,
                                              const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstCellList
      speciesDistributedConstCells( const MemoryIdentifier & mid,
                                    const SortOrder & so = Unordered ) const;

  int speciesSetProperty( const MemoryIdentifier & mid,
                                      const QString & name,
                                      const QVariant & value );

  MemoryIdentifier speciesCreateIndividualMID( const MemoryIdentifier & smid );

  Individual speciesCreateIndividual( const MemoryIdentifier & smid );

  bool speciesDeleteIndividual( const MemoryIdentifier & smid,
                                        const MemoryIdentifier & imid );

  int speciesDeleteIndividuals( const MemoryIdentifier & smid );

  int speciesDeleteDistributedIndividuals( const MemoryIdentifier & smid );

  int speciesDeleteUndistributedIndividuals( const MemoryIdentifier & smid );

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList
      speciesDistributedIndividuals( const MemoryIdentifier & mid,
                                     const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList
      speciesUndistributedIndividuals( const MemoryIdentifier & mid,
                                       const SortOrder & so = Unordered ) const;

  int speciesUndistributeCells( const MemoryIdentifier & smid );

  int speciesUndistributeIndividuals( const MemoryIdentifier & smid );

  // Cell Sapce Interface

  int cellCount() const;

  MIDList cellMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstCellList constCells( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  CellList cells( const SortOrder & so = Unordered ) const;

  int emptyCellCount() const;

  MIDList emptyCellMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstCellList emptyConstCells( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  CellList emptyCells( const SortOrder & so = Unordered ) const;

  int nonEmptyCellCount() const;

  MIDList nonEmptyCellMIDs( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstCellList nonEmptyConstCells( const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  CellList nonEmptyCells( const SortOrder & so = Unordered ) const;

  MemoryIdentifier createCellMID();

  Cell createCell();

  bool deleteCell(const MemoryIdentifier & mid);

  bool deleteCell( const ConstCell & cell );

  int deleteAllCells();

  int deleteEmptyCells();

  int deleteNonEmptyCells();

  // Cell Reference Interface

  bool cellIsValid( const MemoryIdentifier &) const;

  bool cellHasProperty( const MemoryIdentifier & mid,
                                 const QString & name ) const;

  int cellPropertyCount( const MemoryIdentifier & mid) const;

  QStringList cellPropertyNames( const MemoryIdentifier & mid ) const;

  QVariant cellProperty( const MemoryIdentifier & mid,
                                  const QString & name,
                                  const QVariant & defaultValue =
                                    QVariant() ) const;

  int cellIndividualCount( const MemoryIdentifier & mid ) const;

  int cellSpeciesIndividualCount( const MemoryIdentifier & cmid,
                                          const MemoryIdentifier & smid ) const;

  MIDList cellIndividualMIDs( const MemoryIdentifier & mid,
                                      const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList cellConstIndividuals( const MemoryIdentifier & mid,
                                                    const SortOrder & so = Unordered ) const;

  MIDList cellSpeciesIndividualMIDs( const MemoryIdentifier & cmid,
                                             const MemoryIdentifier & smid,
                                             const SortOrder & so = Unordered) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstIndividualList cellSpeciesConstIndividuals( const MemoryIdentifier & cmid,
                                             const MemoryIdentifier & smid,
                                             const SortOrder & so = Unordered) const;

  int cellSpeciesCount( const MemoryIdentifier & cmid ) const;

  bool cellHasSpecies( const MemoryIdentifier & cmid,
                               const MemoryIdentifier & smid ) const;

  MIDList cellSpeciesMIDs( const MemoryIdentifier & cmid,
                                   const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  ConstSpeciesList cellConstSpecies( const MemoryIdentifier & cmid,
                                   const SortOrder & so = Unordered ) const;

  int cellSetProperty( const MemoryIdentifier & mid,
                                 const QString & name,
                                 const QVariant & value );

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList cellIndividuals( const MemoryIdentifier & mid,
                                          const SortOrder & so = Unordered ) const;

  /*!
    Transform each MID to Reference from this.
   */
  IndividualList cellSpeciesIndividuals( const MemoryIdentifier & cmid,
                                                 const MemoryIdentifier & smid,
                                                 const SortOrder & so = Unordered ) const;

  bool cellInsertIndividual( const MemoryIdentifier & cmid,
                                     const MemoryIdentifier & smid,
                                     const MemoryIdentifier & imid );

  bool cellRemoveIndividual( const MemoryIdentifier & cmid,
                                     const MemoryIdentifier & smid,
                                     const MemoryIdentifier & imid );

  int cellRemoveIndividuals(const MemoryIdentifier & cmid);

  int cellRemoveSpecies(const MemoryIdentifier & cmid,
                                      const MemoryIdentifier & smid );

  int cellRemoveAllSpecies(const MemoryIdentifier & cmid );

};

} // namespace SRPS

#endif // QSPACE_H
