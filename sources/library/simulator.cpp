/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "simulator.h"

namespace SRPS {

class Simulator::Job::Private
{
public:

  Simulator::ComponentConfiguration prng;
  Simulator::ComponentConfiguration dprng;
  Simulator::ComponentConfiguration model;
  Simulator::ComponentConfigurationList accumulators;
  int iterations;
  bool accumulateOnly;

  Private()
  {
    iterations = 1;
    accumulateOnly = false;
  }
};

Simulator::Job::Job() :
    p( new Job::Private )
{

}

Simulator::Job::~Job()
{
  delete p;
}

Simulator::Job::Job( const Simulator::Job & o ) :
    p(new Job::Private )
{
  p->accumulateOnly = o.p->accumulateOnly;
  p->accumulators = o.p->accumulators;
  p->dprng = o.p->dprng;
  p->iterations = o.p->iterations;
  p->model = o.p->model;
  p->prng = o.p->prng;
}

Simulator::Job & Simulator::Job::operator=( const Simulator::Job & o )
{
  if ( this != & o )
  {
    p->accumulateOnly = o.p->accumulateOnly;
    p->accumulators = o.p->accumulators;
    p->dprng = o.p->dprng;
    p->iterations = o.p->iterations;
    p->model = o.p->model;
    p->prng = o.p->prng;
  }
  return *this;
}

bool Simulator::Job::accumulateOnly() const
{
  return p->accumulateOnly;
}

void Simulator::Job::setAccumulateOnly( bool b )
{
  p->accumulateOnly = b;
}

Simulator::ComponentConfiguration Simulator::Job::modelConfiguration() const
{
  return p->model;
}

void Simulator::Job::setModelConfiguration( const Simulator::ComponentConfiguration & model_key )
{
  p->model = model_key;
}

Simulator::ComponentConfiguration Simulator::Job::prngConfiguration() const
{
  return p->prng;
}

void Simulator::Job::setPRNGConfiguration( const Simulator::ComponentConfiguration & prng_key )
{
  p->prng = prng_key;
}

Simulator::ComponentConfiguration Simulator::Job::dprngConfiguration() const
{
  return p->dprng;
}

void Simulator::Job::setDPRNGConfiguration( const Simulator::ComponentConfiguration & dprng_key )
{
  p->dprng = dprng_key;
}

Simulator::ComponentConfigurationList Simulator::Job::accumulatorConfigurations() const
{
  return p->accumulators;
}

void Simulator::Job::setAccumulatorConfigurations( const Simulator::ComponentConfigurationList & accumulators_key )
{
  p->accumulators = accumulators_key;
}

int Simulator::Job::iterations() const
{
  return p->iterations;
}

void Simulator::Job::setIterations( int model_iterations )
{
  p->iterations = model_iterations;
}

Simulator::Simulator()
  : Component()
{
}

Simulator::~Simulator()
{
}

} // namespace SRPS
