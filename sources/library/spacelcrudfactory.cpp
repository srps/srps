/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#include "spacelcrudfactory.h"

using SRPS::SpaceLCRUDFactory;

SpaceLCRUDFactory::SpaceLCRUDFactory()
{
}

SpaceLCRUDFactory::~SpaceLCRUDFactory()
{
}

QStringList SpaceLCRUDFactory::spaceLCRUDFactoryKeys() const
{
  return QStringList();
}

SRPS::SpaceLCRUD *
    SpaceLCRUDFactory::spaceLCRUDFactoryCreate( const QString & ,
                                                    const QVariantHash & )
{
  return 0;
}

