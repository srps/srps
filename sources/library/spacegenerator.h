/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACEGENERATOR_H
#define SRPS_SPACEGENERATOR_H

#include "component.h"

namespace SRPS {

class Space;

/*!
  \class SRPS::SpaceGenerator spacerw.h SRPS/SpaceGenerator
  \brief Abstract Space Generator

  Component for Space Generation.

  All funcitons are implemented to do nothing or return default values.

  \since 1.0
  \author Cesar Augusto Arana Collazos
 */
class SRPS_EXPORT SpaceGenerator : public Component
{
  SRPS_DISABLE_COPY(SpaceGenerator)

protected:

  SpaceGenerator();

public:

  virtual ~SpaceGenerator();


  /*!
    \brief Prepare transformation.

    \param space Target Space.
    \return prepare result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int prepareGenerate( SRPS::Space * space ) = 0;

  /*!
    \brief Transform an empty space to generated space.

    \return Result code.

    \since 1.0
    \author Cesar Augusto Arana Collazos
   */
  virtual int generate() = 0;

};

} // namespace SRPS

Q_DECLARE_INTERFACE(SRPS::SpaceGenerator,"SRPS::SpaceGenerator/1.0.0");

#endif // SPACEGenerator_H
