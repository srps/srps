/*
    This file part of SRPS project.
    Component based Species Richness Pattern Simulator.
    Copyright (C) 2010 Cesar Augusto Arana Collazos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Cesar Augusto Arana Collazos
    ksrarc@gmail.com
*/

#ifndef SRPS_SPACE_H
#define SRPS_SPACE_H

#include "component.h"

namespace SRPS {

class SRPS_EXPORT Space : public Component
{
  SRPS_DISABLE_COPY(Space)

public:

  /*!
    Space's identifier type.
   */
  typedef quint32 MemoryIdentifier;
  typedef QList<MemoryIdentifier> MIDList;

  /*!
    Sort order for retreaving Species, Individuals and Cells.
   */
  enum SortOrder {
    Unordered = 0, /**< Unespecified order. */
    CreationOrder = 1, /**< Creation order, first created first. */
    InverseCreationOrder = 2, /**< Creation order, first created last. */
    InsertionOrder = 1,
    InverseInsertionOrder = 2
  };

  class Individual;
  class ConstIndividual;

  typedef QList<ConstIndividual> ConstIndividualList;
  typedef QList<Individual> IndividualList;

  class Species;
  class ConstSpecies;

  typedef QList<ConstSpecies> ConstSpeciesList;
  typedef QList<Species> SpeciesList;

  class Cell;
  class ConstCell;

  typedef QList<ConstCell> ConstCellList;
  typedef QList<Cell> CellList;

  class SRPS_EXPORT ReferencePropertyGetter
  {
  public:

    virtual ~ReferencePropertyGetter();

    virtual bool hasProperty( const QString & name ) const = 0;

    virtual int propertyCount() const = 0;

    virtual QStringList propertyNames() const = 0;

    virtual QVariant property(const QString & name,
                              const QVariant & defaultResult = QVariant()) const = 0;
  };

  class SRPS_EXPORT ReferencePropertySetter
  {
  public:

    virtual ~ReferencePropertySetter();

    virtual int setProperty(const QString & name,
                             const QVariant & value ) const = 0;
  };

private:

  class PrivateReference; // puntero a puntero de curva ( tiene el contador )
  PrivateReference * pr;

  class SRPS_EXPORT AbstractReference
  {
  private:
    friend class SRPS::Space;

    mutable PrivateReference * _shared_counter;

    MemoryIdentifier _memory_identifier;

  protected:

    AbstractReference();

  public:

    virtual ~AbstractReference();

    /*!
      \brief Check for reference validity.
      If refence is invalid, its detached.
      \return True if the reference is valid, any operation are safe.
      \return False, if the reference is invalid or Space doesn't exists anymore.\
      any operation can cause application crash.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool isValid() const;

    /*!
      \brief Detach reference from Space and make it invalid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    void detach() const;

    /*!
      \brief Reference MemoryIdentifier.
      \return Reference MemoryIdentifier.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    MemoryIdentifier mid() const;

  protected:

    AbstractReference( PrivateReference * p, MemoryIdentifier id );
    AbstractReference( const AbstractReference & o );
    AbstractReference & operator=( const AbstractReference & o );

    bool operator==( const AbstractReference & o ) const;
    bool operator!=( const AbstractReference & o ) const;

    const Space * const_space() const;
    Space * space() const;
  };

public:

  class SRPS_EXPORT ConstIndividual : public AbstractReference, public ReferencePropertyGetter
  {
  protected:

    friend class SRPS::Space;

    ConstIndividual( PrivateReference * p, MemoryIdentifier id );

  public:

    ConstIndividual();
    ConstIndividual( const ConstIndividual & o );
    ConstIndividual & operator=( const ConstIndividual & o );
    virtual ~ConstIndividual();

    bool operator==( const ConstIndividual & o ) const;
    bool operator!=( const ConstIndividual & o ) const;

    bool isValid() const; // individualIsValid

    bool hasProperty(const QString &name) const; // individualHasProperty
    int propertyCount() const; // individualPropertyCount
    QStringList propertyNames() const; // individualPropertyNames
    QVariant property(const QString & name, const QVariant & defaultResult = QVariant()) const; // individualProperty

    ConstSpecies constSpecies() const; // individualConstSpecies
    ConstCell constCell() const; // individualConstCell
    bool isDistributed() const; // individualIsDistributed
  };

  class SRPS_EXPORT Individual : public ConstIndividual, public ReferencePropertySetter
  {
  private:

    friend class Space;

    Individual( PrivateReference * p, MemoryIdentifier id );

  public:

    Individual();
    Individual( const Individual & o );
    Individual & operator=( const Individual & o );
    virtual ~Individual();

    bool operator==( const ConstIndividual & o ) const;
    bool operator!=( const ConstIndividual & o ) const;

    int setProperty(const QString & name, const QVariant & value ) const; // individualSetProperty
  };

  class SRPS_EXPORT ConstSpecies : public AbstractReference, public ReferencePropertyGetter
  {
  protected:

    friend class SRPS::Space;

    ConstSpecies( PrivateReference * p, MemoryIdentifier id );

  public:

    ConstSpecies();
    ConstSpecies( const ConstSpecies & o );
    ConstSpecies & operator=( const ConstSpecies & o );

    bool operator==( const ConstSpecies & o ) const;
    bool operator!=( const ConstSpecies & o ) const;

    virtual ~ConstSpecies();

    bool isValid() const; // speciesIsValid

    bool hasProperty(const QString &name) const; // speciesHasProperty
    int propertyCount() const; // speciesPropertyCount
    QStringList propertyNames() const; // speciesPropertyNames
    QVariant property(const QString & name, const QVariant & defaultResult = QVariant()) const; // speciesProperty

    int individualCount() const; // speciesIndividualCount
    ConstIndividualList constIndividuals( const SortOrder & = Unordered ) const; // speciesConstIndividuals

    int distributedIndividualCount() const; // speciesDistributedIndividualCount
    ConstIndividualList constDistributedIndividuals( const SortOrder & = Unordered ) const; // speciesDistributedConstIndividuals

    int undistributedIndividualCount() const; // speciesUndistributedIndividualCount
    ConstIndividualList constUndistributedIndividuals( const SortOrder & = Unordered ) const; // speciesUndistributedConstIndividuals

    bool isDistributed() const; // speciesIsDistributed
    bool isUndistributed() const; // speciesIsUndistributed
    int distributedCellCount() const; // speciesUndistributedCellCount
    ConstCellList constDistributedCells(const SortOrder &) const; // speciesUndistributedConstCells
  };

  class SRPS_EXPORT Species : public ConstSpecies, public ReferencePropertySetter
  {
  protected:

    friend class Space;

    Species( PrivateReference * p, MemoryIdentifier id );

  public:

    Species();
    Species( const Species & o );
    Species & operator=( const Species & o );
    virtual ~Species();

    bool operator==( const ConstSpecies & o ) const;
    bool operator!=( const ConstSpecies & o ) const;

    int setProperty(const QString & name, const QVariant & value ) const; // speciesSetProperty

    IndividualList individuals( const SortOrder & = Unordered ) const; // speciesIndividuals

    Individual createIndividual() const; // speciesCreateIndividual
    bool deleteIndividual( const ConstIndividual & ) const; // speciesDeleteIndividual
    int deleteIndividuals() const; // speciesDeleteIndividuals
    int deleteDistributedIndividuals() const; // speciesDeleteDistributedIndividuals
    int deleteUndistributedIndividuals() const; // speciesDeleteUndistributedIndividuals

    IndividualList distributedIndividuals( const SortOrder & = Unordered ) const; // speciesDistributedIndividuals
    IndividualList undistributedIndividuals( const SortOrder & = Unordered ) const; // speciesUndistributedIndividuals

    int undistributeCells(); // speciesUndistributeCells
    int undistributeIndividuals(); // speciesUndistributeIndividuals
  };

  class SRPS_EXPORT ConstCell : public AbstractReference, public ReferencePropertyGetter
  {
  protected:

    friend class Space;

    ConstCell( PrivateReference * p, MemoryIdentifier id );

  public:

    ConstCell();
    ConstCell( const ConstCell & o );
    ConstCell & operator=( const ConstCell & o );
    virtual ~ConstCell();

    bool operator==( const ConstCell & o ) const;
    bool operator!=( const ConstCell & o ) const;

    bool isValid() const; // cellIsValid

    bool hasProperty(const QString &name) const; // cellHasProperty
    int propertyCount() const; // cellPropertyCount
    QStringList propertyNames() const; // cellPropertyNames
    QVariant property(const QString & name, const QVariant & defaultResult = QVariant()) const; // cellProperty

    int individualCount() const; // cellIndividualCount
    int individualCount( const ConstSpecies & ) const; // cellSpeciesIndividualCount

    ConstIndividualList constIndividuals(const SortOrder & = Unordered) const; // cellConstIndividuals
    ConstIndividualList constIndividuals(const ConstSpecies &, const SortOrder & = Unordered) const; // cellSpeciesConstIndividuals

    int speciesCount() const; // cellSpeciesCount
    bool contains( const ConstSpecies & )  const; // cellHasSpecies

    ConstSpeciesList constSpecies(const SortOrder & = Unordered) const; // cellConstSpecies
  };

  class SRPS_EXPORT Cell : public ConstCell, public ReferencePropertySetter
  {
  protected:

    friend class Space;

    Cell( PrivateReference * p, MemoryIdentifier id );

  public:

    Cell();
    Cell( const Cell & o );
    Cell & operator=( const Cell & o );
    virtual ~Cell();

    bool operator==( const ConstCell & o ) const;
    bool operator!=( const ConstCell & o ) const;

    int setProperty(const QString & name, const QVariant & value ) const; // cellSetProperty

    IndividualList individuals(const SortOrder & = Unordered) const; // cellIndividuals
    IndividualList individuals(const ConstSpecies &, const SortOrder & = Unordered) const; // cellSpeciesIndividuals

    bool insertIndividual( const ConstIndividual & ) const; // cellInsertIndividual
    bool removeIndividual( const ConstIndividual & ) const; // cellRemoveIndividual

    int removeIndividuals(); // cellRemoveIndividuals
    int removeSpecies( const ConstSpecies & ); // cellRemoveSpecies
    int removeAllSpecies(); // cellRemoveAllSpecies
  };

protected:

    Space();

public:

    virtual ~Space();

    virtual SRPS::Space * clone() const = 0;
    virtual SRPS::Space * cloneUndistributed() const = 0;

    /*!
      \brief Undistribute all Individuals, but not delete anything.
     */
    virtual void undistribute() =0;

    /*!
      \brief Delete All, Individuals, Species and Cells
      \note Implementations
     */
    virtual void clear() = 0;

    // Individual Space interface

    /*!
      \brief Individual count.
      \return Spaces's individual count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int individualCount() const = 0;

    /*!
      \brief Individual MemoryIdentifier list.
      \param so, desired SortOrder.
      \return Individual MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList individualMIDs( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief ConstIndividual list.
      \param so, desired SortOrder.
      \return ConstIndividual list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList constIndividuals( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Individual list.
      \param so, desired SortOrder.
      \return Individual list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList individuals( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Distributed Individual count.
      \return Spaces's distributed individual count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int distributedIndividualCount() const = 0;

    /*!
      \brief Distributed Individual MemoryIdentifier list.
      That's it, all Individuals are in cells.
      This method filters individualMIDs()
      \param so, desired SortOrder.
      \return Individual MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList distributedIndividualMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Distributed ConstIndividual list.
      \param so, desired SortOrder.
      \return ConstIndividual list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList constDistributedIndividuals( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Distributed Individual list.
      \param so, desired SortOrder.
      \return Individual list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList distributedIndividuals( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Undistributed Individual count.
      \return Spaces's undistributed individual count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int undistributedIndividualCount() const = 0;

    /*!
      \brief Undistributed Individual MemoryIdentifier list.
      That's it, all Individuals are not in cells.
      This method filters individualMIDs()
      \param so, desired SortOrder.
      \return Individual MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList undistributedIndividualMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Undstributed ConstIndividual list.
      \param so, desired SortOrder.
      \return ConstIndividual list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList constUndistributedIndividuals( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Undistributed Individual list.
      \param so, desired SortOrder.
      \return Individual list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList undistributedIndividuals( const SortOrder & so = Unordered ) const = 0;

    // Invidual Reference Interface

    /*!
      \brief Check if individual is valid
      \param mid, individual MemoryIdentifier
      \return True if individual is valid.
      \return False if not, MemoryIdentifier is not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool individualIsValid( const MemoryIdentifier &) const = 0;

    /*!
      \brief Check if individual has property.
      \param mid, individual MemoryIdentifier
      \param name, property target name.
      \return True if individual has the property.
      \return False if individual doesn't have property or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool individualHasProperty( const MemoryIdentifier & mid,
                                   const QString & name ) const = 0;

    /*!
      \brief Queries individual property count.
      \param mid, individual MemoryIdentifier
      \return Property count.
      \return Cero if individual doesn't have properties or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int individualPropertyCount( const MemoryIdentifier & mid) const = 0;

    /*!
      \brief Queries individual property name list.
      \param mid, individual MemoryIdentifier
      \return List of property names.
      \return Empty list if individual doesn't have properties or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual QStringList individualPropertyNames( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Queries individual property.
      \param mid, individual MemoryIdentifier
      \param name, property name.
      \param defaultValue, return fallback
      \return Valid QVariant if value exists.
      \return defaultValue if value doesn't exists or MemoryIdentifiers or name is \
      not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual QVariant individualProperty( const MemoryIdentifier & mid,
                                         const QString & name,
                                         const QVariant & defaultValue =
                                            QVariant() ) const = 0;

    /*!
      \brief Individual's species MemoryIdentifier
      \return Individual's species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MemoryIdentifier individualSpeciesMID( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Individual's species MemoryIdentifier
      \return Individual's species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpecies individualConstSpecies( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Individual's cell MemoryIdentifier
      \return Individual's cell MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MemoryIdentifier individualCellMID( const MemoryIdentifier & mid ) const = 0;
    /*!
      \brief Individual's cell
      \return Individual's cell
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstCell individualConstCell( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Test if individual is in any cell.
      Base class implementation test id individual's cell returned is valid.
      \return True if individuals is in any cell.
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool individualIsDistributed(const MemoryIdentifier & mid) const =0;

    /*!
      \brief Changes to new or deleted property.
      If the value is null QVariant it means delete.
      \param mid, individual MemoryIdentifier
      \param name, property name.
      \param value, Target value.
      \return true if value was changed or deleted.
      \return false if not. MemoryIdentifiers aren't valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int individualSetProperty( const MemoryIdentifier & mid,
                                        const QString & name,
                                        const QVariant & value ) = 0;

    // Species Space Interface

    /*!
      \brief Species count.
      \return Spaces's species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesCount() const = 0;

    /*!
      \brief Species MemoryIdentifier list.
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList speciesMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief ConstSpecies list.
      \param so, desired SortOrder.
      \return ConstSpecies list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpeciesList constSpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Species list.
      \param so, desired SortOrder.
      \return Species list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual SpeciesList species( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Distributed Source count.
      \return Spaces's distributed species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int distributedSpeciesCount() const = 0;

    /*!
      \brief Distributed Species MemoryIdentifier list.
      That's it, all Speciess are in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList distributedSpeciesMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Distributed ConstSpecies list.
      \param so, desired SortOrder.
      \return ConstSpecies list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpeciesList constDistributedSpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Distributed Species list.
      \param so, desired SortOrder.
      \return Species list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual SpeciesList distributedSpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Undistributed Species count.
      \return Spaces's undistributed species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int undistributedSpeciesCount() const = 0;

    /*!
      \brief Undistributed Species MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList undistributedSpeciesMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Undistributed ConstSpecies list.
      \param so, desired SortOrder.
      \return ConstSpecies list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpeciesList constUndistributedSpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Undistributed Species list.
      \param so, desired SortOrder.
      \return Species list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual SpeciesList undistributedSpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Empty Species count.
      \return Spaces's distributed species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int emptySpeciesCount() const = 0;

    /*!
      \brief Empty Species MemoryIdentifier list.
      That's it, all Speciess are in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList emptySpeciesMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Empty ConstSpecies list.
      \param so, desired SortOrder.
      \return ConstSpecies list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpeciesList constEmptySpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Empty Species list.
      \param so, desired SortOrder.
      \return Species list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual SpeciesList emptySpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Non Empty Species count.
      \return Spaces's undistributed species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int nonEmptySpeciesCount() const = 0;

    /*!
      \brief Non Empty Species MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList nonEmptySpeciesMIDs( const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Non-Empty ConstSpecies list.
      \param so, desired SortOrder.
      \return ConstSpecies list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpeciesList constNonEmptySpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Non-Empty Species list.
      \param so, desired SortOrder.
      \return Species list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual SpeciesList nonEmptySpecies( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Creates an Undistributed Species
      \param smid, Species's Species MemoryIdentifier
      \return Species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MemoryIdentifier createSpeciesMID() = 0;

    /*!
      \brief Create a new Species.
      This method relies in createSpeciesMID()
      \return Species referencing the new variable.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual Species createSpecies() = 0;

    /*!
      \brief Undistribute and Deletes an Species
      \param imid, Species's MemoryIdentifier
      \return true if Species was deleted
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool deleteSpecies( const MemoryIdentifier & imid ) = 0;

    /*!
      \brief Delete a ConstSpecies/Species.
      \param species, target ConstSpecies/Species.
      \return true, if the ConstSpecies/Species was deleted.
      \return false, if not or \c species doesn't belong to this Space.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool deleteSpecies( const ConstSpecies & species ) = 0;

    /*!
      \brief Undistribute and Deletes all Species
      \return deleted species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteAllSpecies() = 0;

    /*!
      \brief Deletes distributed all Species
      \return deleted species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteDistributedSpecies() = 0;

    /*!
      \brief Deletes undistributed all Species
      \return deleted species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteUndistributedSpecies() = 0;

    /*!
      \brief Deletes empty all Species
      \return deleted species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteEmptySpecies() = 0;

    /*!
      \brief Deletes non-empty all Species
      \return deleted species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteNonEmptySpecies() = 0;

    // Species Reference Interface

    /*!
      \brief Check if species is valid
      \param mid, species MemoryIdentifier
      \return True if species is valid.
      \return False if not, MemoryIdentifier is not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool speciesIsValid( const MemoryIdentifier &) const = 0;

    /*!
      \brief Check if species has property.
      \param mid, species MemoryIdentifier
      \param name, property target name.
      \return True if species has the property.
      \return False if species doesn't have property or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool speciesHasProperty( const MemoryIdentifier & mid,
                                   const QString & name ) const = 0;

    /*!
      \brief Queries species property count.
      \param mid, species MemoryIdentifier
      \return Property count.
      \return Cero if species doesn't have properties or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesPropertyCount( const MemoryIdentifier & mid) const = 0;

    /*!
      \brief Queries species property name list.
      \param mid, species MemoryIdentifier
      \return List of property names.
      \return Empty list if species doesn't have properties or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual QStringList speciesPropertyNames( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Queries species property.
      \param mid, species MemoryIdentifier
      \param name, property name.
      \param defaultValue, return fallback
      \return Valid QVariant if value exists.
      \return defaultValue if value doesn't exists or MemoryIdentifiers or name is \
      not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual QVariant speciesProperty( const MemoryIdentifier & mid,
                                         const QString & name,
                                         const QVariant & defaultValue =
                                            QVariant() ) const = 0;

    /*!
      \brief Species individual count.
      \return Spaces's species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesIndividualCount(const MemoryIdentifier & mid) const = 0;

    /*!
      \brief Species individual MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList speciesIndividualMIDs( const MemoryIdentifier & mid,
                                           const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species individual MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList
        speciesConstIndividuals( const MemoryIdentifier & mid,
                                 const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species individual MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList
        speciesIndividuals( const MemoryIdentifier & mid,
                                 const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species distributed individual count.
      \return Spaces's species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesDistributedIndividualCount( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Species distributed individual MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList speciesDistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                      const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species distributed ConstIndividualList.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList
        speciesDistributedConstIndividuals( const MemoryIdentifier & mid,
                                            const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species undistributed individual count.
      \return Spaces's species count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesUndistributedIndividualCount( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Species unditributed individual MemoryIdentifier list.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList speciesUndistributedIndividualMIDs( const MemoryIdentifier & mid,
                                                        const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species undistributed ConstIndividualList.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList
        speciesUndistributedConstIndividuals( const MemoryIdentifier & mid,
                                              const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Test if species is in any cell.
      \return True if speciess is in any cell.
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool speciesIsDistributed(const MemoryIdentifier & mid) const =0;

    /*!
      \brief Test if species is not in any cell.
      \return True if speciess is in not any cell.
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool speciesIsUndistributed(const MemoryIdentifier & mid) const =0;

    /*!
      \brief Species's distributed cell count
      \return Species's distributed cell count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesDistributedCellCount( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Species's distributed cell MemoryIdentifier List
      \return Species's distributed cell MemoryIdentifier List
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList speciesDistributedCellMIDs( const MemoryIdentifier & mid,
                                                const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Species distributed ConstCellList.
      That's it, all Cells were the species is distributed.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstCellList
        speciesDistributedConstCells( const MemoryIdentifier & mid,
                                      const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Changes to new or deleted property.
      If the value is null QVariant it means delete.
      \param mid, species MemoryIdentifier
      \param name, property name.
      \param value, Target value.
      \return true if value was changed or deleted.
      \return false if not. MemoryIdentifiers aren't valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesSetProperty( const MemoryIdentifier & mid,
                                        const QString & name,
                                        const QVariant & value ) = 0;

    /*!
      \brief Create an Individual
      \param smid, species MemoryIdentifier
      \return Individual MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MemoryIdentifier speciesCreateIndividualMID( const MemoryIdentifier & smid ) = 0;

    /*!
      \brief Create an Individual
      \param smid, species MemoryIdentifier
      \return Individual MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual Individual speciesCreateIndividual( const MemoryIdentifier & smid ) = 0;

    /*!
      \brief Deletes an Individual
      \param smid, species MemoryIdentifier
      \para imid, individual MemoryIdentifier
      \return Individual MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool speciesDeleteIndividual( const MemoryIdentifier & smid,
                                          const MemoryIdentifier & imid ) = 0;

    /*!
      \brief Deletes all Individuals
      \param smid, species MemoryIdentifier
      \return Individual MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesDeleteIndividuals( const MemoryIdentifier & smid ) = 0;

    /*!
      \brief Deletes Distributed Individuals
      \param smid, species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesDeleteDistributedIndividuals( const MemoryIdentifier & smid ) = 0;

    /*!
      \brief Deletes Undistributed Individuals
      \param smid, species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesDeleteUndistributedIndividuals( const MemoryIdentifier & smid ) = 0;

    /*!
      \brief Species distributed IndividualList.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList
        speciesDistributedIndividuals( const MemoryIdentifier & mid,
                                       const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Species undistributed IndividualList.
      That's it, all Speciess are not in cells.
      This method filters speciesMIDs()
      \param so, desired SortOrder.
      \return Species MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList
        speciesUndistributedIndividuals( const MemoryIdentifier & mid,
                                         const SortOrder & so = Unordered ) const =0;

    /*!
      \brief Undistributed Species form Cells
      \param smid, species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesUndistributeCells( const MemoryIdentifier & smid ) = 0;

    /*!
      \brief Undistributed Species Individuals form Cells
      \param smid, species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int speciesUndistributeIndividuals( const MemoryIdentifier & smid ) = 0;

    // Cell Sapce Interface

    /*!
      \brief Cell count.
      \return Spaces's cell count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellCount() const = 0;

    /*!
      \brief Cell MemoryIdentifier list.
      \param so, desired SortOrder.
      \return Cell MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList cellMIDs( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief ConstCell list.
      \param so, desired SortOrder.
      \return ConstCell list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstCellList constCells( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Cell list.
      \param so, desired SortOrder.
      \return Cell list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual CellList cells( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Empty Cell count.
      \return Spaces's cell count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int emptyCellCount() const = 0;

    /*!
      \brief Empty Cell MemoryIdentifier list.
      \param so, desired SortOrder.
      \return Cell MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList emptyCellMIDs( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Empty ConstCell list.
      \param so, desired SortOrder.
      \return ConstCell list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstCellList emptyConstCells( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Empty Cell list.
      \param so, desired SortOrder.
      \return Cell list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual CellList emptyCells( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Non Empty Cell count.
      \return Spaces's cell count.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int nonEmptyCellCount() const = 0;

    /*!
      \brief Non Empty Cell MemoryIdentifier list.
      \param so, desired SortOrder.
      \return Cell MemoryIdentifier list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList nonEmptyCellMIDs( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Non Empty ConstCell list.
      \param so, desired SortOrder.
      \return ConstCell list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstCellList nonEmptyConstCells( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Non Empty Cell list.
      \param so, desired SortOrder.
      \return Cell list in \so SortOrder.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual CellList nonEmptyCells( const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Create a cell.
      \return Valid MemoryIdentifier is cell was created.
      \return Invalid MemoryIdentifier is cell can't be created.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MemoryIdentifier createCellMID() = 0;

    /*!
      \brief Create a new Cell.
      This method relies in createCellMID()
      \return Cell referencing the new variable.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual Cell createCell() = 0;

    /*!
      \brief Deletes a cell.
      If deletion was successfull, all cell's properties and values must be deleted.
      But individuals not
      \param mid, cell MemoryIdentifier
      \return true if cell was deleted.
      \return false if not, MemoryIdentifier is invalid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool deleteCell(const MemoryIdentifier & mid) = 0;

    /*!
      \brief Delete a ConstCell/Cell.
      \param cell, target ConstCell/Cell.
      \return true, if the ConstCell/Cell was deleted.
      \return false, of not or \c cell doesn't belong to this Space.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool deleteCell( const ConstCell & cell ) = 0;

    /*!
      \brief Deletes all cells.
      \param mid, cell MemoryIdentifier
      \return deleted cell count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteAllCells() = 0;

    /*!
      \brief Deletes all empty cells.
      \param mid, cell MemoryIdentifier
      \return deleted cell count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteEmptyCells() = 0;

    /*!
      \brief Deletes all non-empty cells.
      \param mid, cell MemoryIdentifier
      \return deleted cell count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int deleteNonEmptyCells() = 0;

    // Cell Reference Interface

    /*!
      \brief Check if cell is valid
      \param mid, cell MemoryIdentifier
      \return True if cell is valid.
      \return False if not, MemoryIdentifier is not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool cellIsValid( const MemoryIdentifier &) const = 0;

    /*!
      \brief Check if cell has property.
      \param mid, cell MemoryIdentifier
      \param name, property target name.
      \return True if cell has the property.
      \return False if cell doesn't have property or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool cellHasProperty( const MemoryIdentifier & mid,
                                   const QString & name ) const = 0;

    /*!
      \brief Queries cell property count.
      \param mid, cell MemoryIdentifier
      \return Property count.
      \return Cero if cell doesn't have properties or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellPropertyCount( const MemoryIdentifier & mid) const = 0;

    /*!
      \brief Queries cell property name list.
      \param mid, cell MemoryIdentifier
      \return List of property names.
      \return Empty list if cell doesn't have properties or MemoryIdentifiers not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual QStringList cellPropertyNames( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Queries cell property.
      \param mid, cell MemoryIdentifier
      \param name, property name.
      \param defaultValue, return fallback
      \return Valid QVariant if value exists.
      \return defaultValue if value doesn't exists or MemoryIdentifiers or name is \
      not valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual QVariant cellProperty( const MemoryIdentifier & mid,
                                    const QString & name,
                                    const QVariant & defaultValue =
                                      QVariant() ) const = 0;

    /*!
      \brief Cells individual count.
      \param mid, cell MemoryIdentifier
      \return Cell individual count ( all species )
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellIndividualCount( const MemoryIdentifier & mid ) const = 0;

    /*!
      \brief Cells species individual count.
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \return Cell species individual count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellSpeciesIndividualCount( const MemoryIdentifier & cmid,
                                            const MemoryIdentifier & smid ) const = 0;

    /*!
      \brief Cells individual MemoryIdentifier List.
      \param mid, cell MemoryIdentifier
      \param so, SortOrder
      \return Individual MemoryIdentifier list ( all species )
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList cellIndividualMIDs( const MemoryIdentifier & mid,
                                        const SortOrder & so = Unordered ) const= 0;
    /*!
      \brief Cells individual MemoryIdentifier List.
      \param mid, cell MemoryIdentifier
      \param so, SortOrder
      \return Individual MemoryIdentifier list ( all species )
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList cellConstIndividuals( const MemoryIdentifier & mid,
                                                      const SortOrder & so = Unordered ) const= 0;

    /*!
      \brief Cells species individual MemoryIdentifier List.
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \param so, SortOrder
      \return Species Individual MemoryIdentifier list
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList cellSpeciesIndividualMIDs( const MemoryIdentifier & cmid,
                                               const MemoryIdentifier & smid,
                                               const SortOrder & so = Unordered) const = 0;

    /*!
      \brief Cells species individual MemoryIdentifier List.
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \param so, SortOrder
      \return Species Individual MemoryIdentifier list
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstIndividualList cellSpeciesConstIndividuals( const MemoryIdentifier & cmid,
                                               const MemoryIdentifier & smid,
                                               const SortOrder & so = Unordered) const = 0;

    /*!
      \brief Cells species count.
      \param cmid, cell MemoryIdentifier
      \return Cell species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellSpeciesCount( const MemoryIdentifier & cmid ) const = 0;

    /*!
      \brief Check if species is in cell.
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \return true if at least 1 individual of species MID \c species is in cell.
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool cellHasSpecies( const MemoryIdentifier & cmid,
                                 const MemoryIdentifier & smid ) const = 0;

    /*!
      \brief Cells species MemoryIdentifier List.
      \param mid, cell MemoryIdentifier
      \param so, SortOrder
      \return Species MemoryIdentifier list
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual MIDList cellSpeciesMIDs( const MemoryIdentifier & cmid,
                                     const SortOrder & so = Unordered ) const = 0;
    /*!
      \brief Cells const species List.
      \param mid, cell MemoryIdentifier
      \param so, SortOrder
      \return Species MemoryIdentifier list
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual ConstSpeciesList cellConstSpecies( const MemoryIdentifier & cmid,
                                     const SortOrder & so = Unordered ) const = 0;

    /*!
      \brief Changes to new or deleted property.
      If the value is null QVariant it means delete.
      \param mid, cell MemoryIdentifier
      \param name, property name.
      \param value, Target value.
      \return true if value was changed or deleted.
      \return false if not. MemoryIdentifiers aren't valid.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellSetProperty( const MemoryIdentifier & mid,
                                   const QString & name,
                                   const QVariant & value ) = 0;

    /*!
      \brief Cells individual List.
      \param mid, cell MemoryIdentifier
      \param so, SortOrder
      \return Individual MemoryIdentifier list ( all species )
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList cellIndividuals( const MemoryIdentifier & mid,
                                            const SortOrder & so = Unordered ) const= 0;

    /*!
      \brief Cells individual List.
      \param mid, cell MemoryIdentifier
      \param so, SortOrder
      \return Individual MemoryIdentifier list ( all species )
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual IndividualList cellSpeciesIndividuals( const MemoryIdentifier & cmid,
                                                   const MemoryIdentifier & smid,
                                                   const SortOrder & so = Unordered ) const= 0;

    /*!
      \brief Insert individual in Cell.
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \param imid, individual MemoryIdentifier
      \return true if individual was inserted.
      \return false if not.
      \return false if individual is inserted in other cell, including this cell.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool cellInsertIndividual( const MemoryIdentifier & cmid,
                                       const MemoryIdentifier & smid,
                                       const MemoryIdentifier & imid ) = 0;

    /*!
      \brief Remove individual from Cell.
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \param imid, individual MemoryIdentifier
      \return true if individual was removed.
      \return false if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual bool cellRemoveIndividual( const MemoryIdentifier & cmid,
                                       const MemoryIdentifier & smid,
                                       const MemoryIdentifier & imid ) = 0;

    /*!
      \brief Remove all individuals
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \return removed individual count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellRemoveIndividuals(const MemoryIdentifier & cmid)=0;

    /*!
      \brief Remove all individuals of species
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellRemoveSpecies(const MemoryIdentifier & cmid,
                                        const MemoryIdentifier & smid )=0;

    /*!
      \brief Remove all individuals
      \param cmid, cell MemoryIdentifier
      \param smid, species MemoryIdentifier
      \return removed species count
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    virtual int cellRemoveAllSpecies(const MemoryIdentifier & cmid )=0;

    // Converter API

    /*!
      \brief Converts between Individual and MemoryIdentifier.
      This method doesn't check if MemoryIdentifier is valid.
      \param mid, target MemoryIdentifier.
      \return Converted MemoryIdentifier as Individual.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    Individual toIndividual( const MemoryIdentifier & mid ) const;

    /*!
      \brief Converts between Individual and MemoryIdentifier.
      This method doesn't check if MemoryIdentifier is valid.
      \param mid, target MemoryIdentifier.
      \return Converted MemoryIdentifier as ConstIndividual.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    ConstIndividual toConstIndividual( const MemoryIdentifier & mid ) const;

    /*!
      \brief Converts between Individual types.
      This method doesn't check if Individual is valid.
      \param const_individual, target ConstIndividual
      \return Converted ConstIndividual as Individual.
      \return Invalid Individual if \c const_species doesn't belongs to this Space.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    Individual toIndividual( const ConstIndividual & ) const;

    /*!
      \brief Converts between Species and MemoryIdentifier.
      This method doesn't check if MemoryIdentifier is valid.
      \param mid, target MemoryIdentifier.
      \return Converted MemoryIdentifier as Species.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    Species toSpecies( const MemoryIdentifier & mid ) const;

    /*!
      \brief Converts between Species and MemoryIdentifier.
      This method doesn't check if MemoryIdentifier is valid.
      \param mid, target MemoryIdentifier.
      \return Converted MemoryIdentifier as ConstSpecies.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    ConstSpecies toConstSpecies( const MemoryIdentifier & mid ) const;

    /*!
      \brief Converts between Species types.
      This method doesn't check if Species is valid.
      \param const_species, target ConstSpecies
      \return Converted ConstSpecies as Species.
      \return Invalid Species if \c const_species doesn't belongs to this Space.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    Species toSpecies( const ConstSpecies & const_species ) const;

    /*!
      \brief Converts between Cell types.
      This method doesn't check if Cell is valid.
      \param const_cell, target ConstCell
      \return Converted ConstCell as Cell.
      \return Invalid Cell if \c const_cell doesn't belongs to this Space.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    Cell toCell( const ConstCell & const_cell ) const;

    /*!
      \brief Converts between Cell and MemoryIdentifier.
      This method doesn't check if MemoryIdentifier is valid.
      \param mid, target MemoryIdentifier.
      \return Converted MemoryIdentifier as Cell.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    Cell toCell( const MemoryIdentifier & mid ) const;

    /*!
      \brief Converts between Cell and MemoryIdentifier.
      This method doesn't check if MemoryIdentifier is valid.
      \param mid, target MemoryIdentifier.
      \return Converted MemoryIdentifier as ConstCell.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    ConstCell toConstCell( const MemoryIdentifier & mid ) const;

    /*!
      \brief Check for Reference ownership.
      \param reference, target reference.
      This method doesn't check if reference is valid.
      \return true, if the Reference belongs to this Space.
      \return false, if not.
      \since 1.0
      \author Cesar Augusto Arana Collazos.
     */
    bool isReferenceFromThis(const AbstractReference & reference ) const;
};

} // namespace SRPS

Q_DECLARE_METATYPE(SRPS::Space::MemoryIdentifier);
Q_DECLARE_METATYPE(SRPS::Space::MIDList);

Q_DECLARE_METATYPE(SRPS::Space::ConstIndividual);
Q_DECLARE_METATYPE(SRPS::Space::Individual);
Q_DECLARE_METATYPE(SRPS::Space::ConstSpecies);
Q_DECLARE_METATYPE(SRPS::Space::Species);
Q_DECLARE_METATYPE(SRPS::Space::ConstCell);
Q_DECLARE_METATYPE(SRPS::Space::Cell);

Q_DECLARE_METATYPE(SRPS::Space::ConstIndividualList);
Q_DECLARE_METATYPE(SRPS::Space::IndividualList);
Q_DECLARE_METATYPE(SRPS::Space::ConstSpeciesList);
Q_DECLARE_METATYPE(SRPS::Space::SpeciesList);
Q_DECLARE_METATYPE(SRPS::Space::ConstCellList);
Q_DECLARE_METATYPE(SRPS::Space::CellList);

Q_DECLARE_INTERFACE(SRPS::Space,"SRPS::Space/1.0.0");

#endif // SRPS_SPACE_H
