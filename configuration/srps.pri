include( srps.defs )

INCLUDEPATH *= $$SRPS_INCLUDES_PATH
LIBS        *= -L$$SRPS_LIBRARIES_PATH
unix:LIBS   *= -l$$SRPS_NAME
win32 {
  CONFIG(debug,debug|release){
    LIBS    *= -l$${SRPS_NAME}d$${SRPS_VERSION_MAJOR}
  }else {
    LIBS    *= -l$${SRPS_NAME}$${SRPS_VERSION_MAJOR}
  }
}

