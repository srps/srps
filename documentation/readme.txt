Component based Species Richness Pattern Simulator | SRPS
=========================================================

Copyright (C) 2010 Cesar Augusto Arana Collazos
All rights reserved.

This is free software licensed under GPLv3, see license.txt
Hosted in http://gitorious.org/srps

For building see 'compiling.txt'

This project uses:
Qt: http://qt.nokia.com/
Qwt: http://qwt.sourceforge.net/
dSFMT: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/
High Resolution Timer: song.ahn@gmail.com
