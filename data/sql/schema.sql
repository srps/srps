-- for checks

CREATE TABLE srps
(
  key TEXT PRIMARY KEY NOT NULL,
  value TEXT NOT NULL
);

-- in general all tables can be in lowercase

-- For Curves

CREATE TABLE srps_curve
(
  id INTEGER PRIMARY KEY NOT NULL,
  key TEXT NOT NULL UNIQUE
);

CREATE TABLE srps_curve_property
(
  curve_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

CREATE TABLE srps_curve_variable
(
  curve_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY NOT NULL
);

CREATE TABLE srps_curve_variable_property
(
  curve_id INTEGER NOT NULL,
  variable_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

CREATE TABLE srps_curve_tuple
(
  curve_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY NOT NULL
);

CREATE TABLE srps_curve_tuple_property
(
  curve_id INTEGER NOT NULL,
  tuple_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

-- For spaces

CREATE TABLE srps_space
(
  id INTEGER PRIMARY KEY NOT NULL,
  key TEXT NOT NULL
);

CREATE TABLE srps_space_property
(
  space_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

CREATE TABLE srps_space_cell
(
  space_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY NOT NULL
);

CREATE TABLE srps_space_cell_property
(
  space_id INTEGER NOT NULL,
  cell_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

CREATE TABLE srps_space_species
(
  space_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY NOT NULL
);

CREATE TABLE srps_space_species_property
(
  space_id INTEGER NOT NULL,
  species_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

CREATE TABLE srps_space_invidual
(
  space_id INTEGER NOT NULL,
  species_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY NOT NULL
);

CREATE TABLE srps_space_individual_property
(
  space_id INTEGER NOT NULL,
  species_id INTEGER NOT NULL,
  individual_id INTEGER NOT NULL,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);
